# package.json information

##dependencies
add `react-google-tag-manager` for google tag manager for analyze data

## dev-dependencies
to fix third-party libraries with flow, install flow-typed

to support test in flow, add `"@babel/preset-flow"` to package.json and then add to .babelrc presets

### optionalChaining
to add optionalChaining and because of error:
  experimental syntax 'optionalChaining' isn't currently enabled
add `@babel/plugin-proposal-optional-chaining`
read: https://babeljs.io/docs/en/babel-plugin-proposal-optional-chaining

###     "react-advance-jalaali-datepicker": "^1.2.2",
this package module work with moment.js. this version work with moment  version "2.24.0" corectly and when moment update get error. for resolve that i change yarn.lock to 

moment@^2.22.1:
  version "2.24.0"
  resolved "https://registry.yarnpkg.com/moment/-/moment-2.24.0.tgz#0d055d53f5052aa653c9f6eb68bb5d12bf5c2b5b"
  integrity sha512-bV7f+6l2QigeBBZSM/6yTNq4P2fNpSWj/0e7jQcy87A8e7o2nAfP/34/2ky5Vw4B9S446EtIhodAzkFCcR4dQg==


 and remove node-modules folder and then run yarn in terminal for install moment in that version i need. because moment.js is package that use inside react-advance-jalaali-datepicker, you cant roleback momentjs with another way.