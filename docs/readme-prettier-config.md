# Prettier configuration

for more information:
https://prettier.io/docs/en/options.html

### trailing commas options:
"none" - No trailing commas.
"es5" - Trailing commas where valid in ES5 (objects, arrays, etc.)
"all" - Trailing commas wherever possible (including function arguments). This requires node 8 or a transform.

### print width
Specify the line length that the printer will wrap on.
For readability we recommend against using more than 80 characters:
maximum line length rules are often set to 100 or 120

