/* eslint-disable no-console */
const express = require("express");
const next = require("next");
const cookiesMiddleware = require("universal-cookie-express");
const routes = require("../routes/routes");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
// Const handle = app.getRequestHandler()
const port = parseInt(process.env.PORT, 10) || 3773;

const handler = routes.getRequestHandler(app);

app.prepare().then(() => {
  express()
    .use(cookiesMiddleware())
    .use(handler)
    .listen(port, err => {
      if (err) {
        throw err;
      }

      if (dev) {
        console.info(`> Ready on http://localhost:${port}`);
      }
    });
});
