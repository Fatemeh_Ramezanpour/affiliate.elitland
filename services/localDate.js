const defaultOptions = {
  year: "numeric",
  month: "long",
  day: "numeric",
  hour: "numeric",
  minute: "numeric",
  second: "numeric"
};

/**
 * Change standard time format to Persian local date
 * @param date standard time like "2019-07-16T12:20:44.000+03:30"
 * @param options format date to display like { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }
 * @return {string} persian local date
 */
const toPersianDate = (date, options = defaultOptions) => {
  try {
    return new Date(date).toLocaleDateString("fa-IR", options);
  } catch {
    return date;
  }
};

export default toPersianDate;
