// const prod = process.env.NODE_ENV === "production";

/* eslint-disable no-unused-vars */
export function apiDomain() {
  return process.env.NEXT_PUBLIC_API_HOST;
}

export function mainDomain() {
  return process.env.NEXT_PUBLIC_DOMAIN;
}

export function socialDomain() {
  return process.env.NEXT_PUBLIC_DOMAIN;
}

export function seoFreindlyUrl(url) {
  return url
    ? url
        .toLowerCase()
        .replace(/[^\p\u0600-\u06FF\da-z-]/gi, `-`)
        .replace(/-{2,}/g, `-`)
    : url;
}

export function profileUrl(fullname, uid) {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/profile/${uid}/${seoFreindlyUrl(fullname)}`;
}

export function moviePageUrl(url) {
  return url;
}

export function posterUrl(src) {
  return process.env.NEXT_PUBLIC_API_HOST + src;
}

export function userDownloadListUrl() {
  return `/download-buy-info`;
}

export function downloadIosApp() {
  return `https://new.sibapp.com/applications/%D8%B3%D9%84%D8%A7%D9%85-%D8%B3%DB%8C%D9%86%D9%85%D8%A7`;
}

export function downloadAndroidApp() {
  return `http://yon.ir/slm20182`;
}

export function homeUrl() {
  return `/`;
}
export function elitlandUrl() {
  return `https://elitland.com/`;
}
// export function elitlandUrl() {
//   if (prod) {
//     return `https://elitland.com/`;
//   } else {
//     return `http://elitland.local`;
//   }
// }

export function downloadUrl(page) {
  const url = `${socialDomain()}/download`;
  if (page && page > 1) {
    return `${url}?page=${page}`;
  }
  return url;
}

export function downloadColleagueUrl(movie_uid, colleague_uid, quality) {
  if (quality) {
    quality = quality.replace(`_`, ``);
    return `${process.env.NEXT_PUBLIC_DOMAIN}/downloadf/${movie_uid}/${quality}/${colleague_uid}`;
  } else {
    return `${process.env.NEXT_PUBLIC_DOMAIN}/download/${movie_uid}/${colleague_uid}`;
  }
}

export function downloadMainUrl(item) {
  if (item.download_type === "series") {
    return `/series/${item.series_uid}`;
  } else {
    return `/download/${item.uid}`;
  }
}

export function canonicalHamgonahLandingUrl() {
  return `${mainDomain()}/hamgonah-campaign`;
}
export function canonicalHamgonahBuyLandingUrl() {
  return `${mainDomain()}/hamgonah-buy`;
}
export function canonicalHamgonahSelectLandingUrl() {
  return `${mainDomain()}/hamgonah-select-quality`;
}

export function wallUrl() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/wall`;
}

export function movieSuggestionUrl() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/taste`;
}

export function videoUrl() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/videos`;
}

export function inplayUrl() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/inplay`;
}

export function adminUrl() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/admin/dashboard`;
}

export function notifUrl() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/notifications`;
}

export function dashboardUrl() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/public/user/dashboard.php`;
}

export function searchPageUrl() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/search/`;
}

export function newsUrl(uid, title) {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/news/${uid}/${seoFreindlyUrl(title)}`;
}

export function persianNewsArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/persian-news`;
  }

  return `${socialDomain()}/persian-news?page=${page}`;
}

export function persianNewsArchiveUrl() {
  return `/persian-news`;
}

export function foreignNewsArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/foreign-news`;
  }

  return `${socialDomain()}/foreign-news?page=${page}`;
}

export function foreignNewsArchiveUrl() {
  return `/foreign-news`;
}

export function castUrl() {
  return `/movie-casts`;
}

export function newsArchiveUrlProd() {
  return `${socialDomain()}/news-archive`;
}

export function newsArchiveUrl() {
  return `/news-archive`;
}

export function watchlistUrl() {
  return `/watchlist`;
}

export function watchlistUrlProd() {
  return `${socialDomain()}/watchlist`;
}

export function movieArchiveUrlProd() {
  return `${socialDomain()}/movie-archive`;
}

export function movieArchiveUrl() {
  return `/movie-archive`;
}

export function movieUrl(uid, title) {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/movie/${uid}/${seoFreindlyUrl(title)}`;
}

export function foreignMovieArchiveUrl() {
  return `/foreign-movie`;
}

export function foreignMovieArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/foreign-movie`;
  }

  return `${socialDomain()}/foreign-movie?page=${page}`;
}

export function persianMovieArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/persian-movie`;
  }

  return `${socialDomain()}/persian-movie?page=${page}`;
}

export function persianMovieArchiveUrl() {
  return `/persian-movie`;
}

export function foreignMoviePaginationUrl() {
  return `/foreign-movie`;
}

export function persianMoviePaginationUrl() {
  return `/persian-movie`;
}

export function reviewsArchiveUrlProd() {
  return `${socialDomain()}/naghd`;
}

export function reviewsArchiveUrl() {
  return `/naghd`;
}

export function reviewsUrl(uid, title) {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/post/${uid}/${seoFreindlyUrl(title)}`;
}

export function persianReviewArchiveUrl() {
  return `/persian-review`;
}

export function foreignReviewArchiveUrl() {
  return `/foreign-review`;
}

export function peopleReviewArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/people-review`;
  }

  return `${socialDomain()}/people-review?page=${page}`;
}

export function peopleReviewArchiveUrl() {
  return `/people-review`;
}

export function articleReviewArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/articles`;
  }

  return `${socialDomain()}/articles?page=${page}`;
}

export function articleReviewArchiveUrl() {
  return `/articles`;
}

export function profileArchiveUrl() {
  return `/profile-archive`;
}

// export function profileUrl(uid, firstname, lastname) {
//   const fullname = lastname !== null ? `${firstname} ${lastname}` : firstname;

//   return `${DOMAIN}/profile/${uid}/${seoFreindlyUrl(fullname)}`;
// }

export function persianProfileArchiveUrl() {
  return `/persian-profile`;
}

export function foreignProfileArchiveUrl() {
  return `/foreign-profile`;
}

export function persianProfileArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/persian-profile`;
  }

  return `${socialDomain()}/persian-profile?page=${page}`;
}

export function foreignProfileArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/foreign-profile`;
  }

  return `${socialDomain()}/foreign-profile?page=${page}`;
}

export function persianReviewArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/persian-review`;
  }

  return `${socialDomain()}/persian-review?page=${page}`;
}

export function foreignReviewArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/foreign-review`;
  }

  return `${socialDomain()}/foreign-review?page=${page}`;
}

export function getMovieDetailUrl(uid) {
  return `/movie-detail/${uid}`;
}

export function downloadUrlMovie(uid, title) {
  if (title) {
    return `/download/${uid}/${seoFreindlyUrl(title)}`;
  }

  return `/download/${uid}`;
}

export function movieVideoUrl(uid, title) {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/video/${uid}/${seoFreindlyUrl(title)}`;
}

export function imageGalleryUrl(fullname, uid, objectTypeUrl) {
  var preUrl = objectTypeUrl === `photo` ? `user-all-image` : `movie-gallery`;
  return `/${preUrl}/${uid}/${seoFreindlyUrl(fullname)}`;
}

export function postUrl(uid, title) {
  if (title) {
    return `/post/${uid}/${seoFreindlyUrl(title)}`;
  }
  return `/post/${uid}`;
}
export function notfound() {
  return `${process.env.NEXT_PUBLIC_DOMAIN}/not-found`;
}

export function videosArchiveUrlProd() {
  return `${socialDomain()}/videos`;
}

export function videosArchiveUrl() {
  return `/videos-archive`;
}
export function videosUrl() {
  return `/videos`;
}
export function videoArchiveUrl() {
  return `/video-archive`;
}
export function anoonsVideoArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/anoons-videos`;
  }

  return `${socialDomain()}/anoons-videos?page=${page}`;
}

export function anoonsVideoArchiveUrl() {
  return `/anoons-videos`;
}
export function musicVideoArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/music-videos`;
  }

  return `${socialDomain()}/music-videos?page=${page}`;
}

export function musicVideoArchiveUrl() {
  return `/music-videos`;
}
export function backStageVideoArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/backstage-videos`;
  }

  return `${socialDomain()}/backstage-videos?page=${page}`;
}

export function backStageVideoArchiveUrl() {
  return `/backstage-videos`;
}
export function interviewVideoArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/interview-videos`;
  }

  return `${socialDomain()}/interview-videos?page=${page}`;
}

export function interviewVideoArchiveUrl() {
  return `/interview-videos`;
}
export function internationalFestivalVideoArchiveUrlProd(page) {
  if (page === undefined) {
    return `${socialDomain()}/international-festival-videos`;
  }

  return `${socialDomain()}/international-festival-videos?page=${page}`;
}

export function engToPerNumber(engNumber) {
  let prsNumber = engNumber;
  if (engNumber !== undefined) {
    const prs = "۰۱۲۳۴۵۶۷۸۹";
    prsNumber = prsNumber.toString().replace(/0|1|2|3|4|5|6|7|8|9/g, function myFunction(x) {
      return prs[x];
    });
  }
  return prsNumber;
}

export function internationalFestivalVideoArchiveUrl() {
  return `/international-festival-videos`;
}

export function hamgonahAward() {
  const url = `/hamgonah-awards`;
  return url;
}

// export function downloadCompanyMomentRepoUrl(uid, page) {
//   if (uid && page) {
//     // return `/download-company-moment-report?page=${page}&&uid=${uid}`;
//     return `/download-company-moment-report?uid=${uid}`;
//   } else if (uid) {
//     return `/download-company-moment-report?uid=${uid}`;
//   }
// }

export function downloadColleagueMomentRepoUrl(uid, page) {
  if (uid && page) {
    // return `/download-company-moment-report?page=${page}&&uid=${uid}`;
    return `/download-colleague-moment-report?uid=${uid}`;
  } else if (uid) {
    return `/download-colleague-moment-report?uid=${uid}`;
  }
}

export function downloadCompanyMomentCSVRepoUrl(uid) {
  return `${apiDomain()}/share/company/finance/details_online_csv.csv?uid=${uid}`;
}
// export function downloadDetailsUnUrl(uid) {
//   return `https://www.salamcinama.ir/download/${uid}`;
// }
export function panelLink(panel) {
  return `/${panel}`;
}

export function backToPageFromLogin(url) {
  return `/${url}`;
}

export function companyDashboardUrl() {
  return `/download-company-dashboard`;
}

export function companyPanelUrl() {
  return `/download-company-panel`;
}

export function companyGeneralMomentUrl(start, end) {
  if (start && end) {
    return `/company-general-moment?start=${start}&end=${end}`;
  }
  return `/company-general-moment`;
}

export function companyGeneralMomentUrlCSV(start, end) {
  if (start && end) {
    return `${apiDomain()}/share/company/finance/details_online_general_csv.xlsx?start_date=${start}&end_date=${end}`;
  }
  return `${apiDomain()}/share/company/finance/details_online_general_csv.xlsx`;
}

//colleague url
export function payoffUrl() {
  return `/payoff`;
}

export function payoffReportUrl() {
  return `/payoff-report`;
}

export function colleagueSalePanelUrl() {
  return `/download-colleague-panel`;
}

export function movieColleagueSalePanelUrl() {
  return `/movie-colleague-panel`;
}

export function seriesColleagueSalePanelUrl(uid) {
  if (uid) {
    return `/series-colleague-panel?uid=${uid}`;
  } else {
    return `/series-colleague-panel`;
  }
}

export function colleagueReportUrl() {
  return `/report-colleague`;
}

export function colleagueLinkUrl() {
  return `/download-colleague-links`;
}
