export function moviePoster(poster) {
  try {
    if (poster) {
      return poster.replace("https://elitland.com", "https://api.salamcinama.ir/");
    }
    return require("public/images/default-movie.png");
  } catch (error) {
    console.error(error);
  }
}

export function roundMovieVote(vote) {
  return parseFloat((Math.round(vote * 100) / 100).toFixed(1));
}

export function cutString(text, strLength) {
  if (!text) {
    return;
  }

  if (text.length < strLength) {
    return text;
  }

  return `${text.substr(0, strLength + text.substr(strLength).indexOf(" "))} ...`;
}

export function nextDayFromNow(count) {
  const today = new Date();
  return new Date(today.getTime() + count * 24 * 60 * 60 * 1000);
}

export function toEnglishDigits(number) {
  if (typeof number === undefined || number === null) {
    return;
  }
  number = String(number);
  return number.replace(
    /([٠١٢٣٤٥٦٧٨٩])|([۰۱۲۳۴۵۶۷۸۹])/g,
    (m, $1) => m.charCodeAt(0) - ($1 ? 1632 : 1776)
  );
}

export function engToPerNumber(engNumber) {
  let prsNumber = engNumber;
  if (engNumber !== undefined) {
    const prs = "۰۱۲۳۴۵۶۷۸۹";
    prsNumber = prsNumber.toString().replace(/0|1|2|3|4|5|6|7|8|9/g, function myFunction(x) {
      return prs[x];
    });
  }
  return prsNumber;
}

export function redirect(url) {
  window.location.href = url;
}

export function fullname(name, family) {
  if (family) {
    return `${name} ${family}`;
  }
  return name;
}

export function beatifuleFileSize(size) {
  if (!size) {
    return size;
  } else if (size > 1000) {
    size = Math.round((size / 1024) * 100) / 100;
    return `${engToPerNumber(size)} GB`;
  }
  return `${engToPerNumber(size)} MB`;
}

export function roundNumber(number, count) {
  return Math.round(number * count) / count;
}

export function pageNumber(params) {
  if (params && params.query) {
    return params.query.page;
  } else {
    return 1;
  }
  // const page = params && params.query ? params.query.page : 1;
}

export function pageTitleWithPagination(total, active, perPage) {
  const count = Math.floor(total / perPage) + 1;
  let currentPageNumber = 1;
  if (active === undefined) {
    currentPageNumber = 1;
  } else if (active > 0) {
    currentPageNumber = active;
  }
  return `( صفحه ${engToPerNumber(currentPageNumber)} از ${engToPerNumber(count)} ) `;
}

export function copy_to_clipboard_text(text) {
  const x = document.createElement("INPUT");
  x.setAttribute("type", "text");
  x.setAttribute("value", text);
  x.setAttribute("id", "copy_element");
  // document.body.appendChild(x).setAttribute("style", 'visibility: hidden');
  document.body.appendChild(x);

  const copy_element = document.getElementById("copy_element");
  copy_element.select();
  document.execCommand("copy");
}
