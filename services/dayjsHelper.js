import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import fa from "dayjs/locale/fa";

import { engToPerNumber } from "./helper";

// set local region for use in timeDiffWord function
dayjs.locale("fa-farsi", fa);

// set relativeTime for use in timeDiffWord function
dayjs.extend(relativeTime);

/**
 * a simple function that using day.js library for time manipulation
 * using catch try block for catch all error
 *
 * @param gTime: standard full time ( example: "2014-07-14T12:20:44.000+03:30")
 * timeDiff: Number or String ۱ - ۱۰ or یک دو سه ...
 * unit: String ماه روز سال ساعت دقیقه ثانیه
 * @return String
 *
 */
const timeDiffWord = gTime => {
  try {
    const [timeDiff, unit] = dayjs()
      .from(dayjs(gTime), true)
      .split(" ");

    return timeDiff !== "NaN" && unit
      ? `${engToPerNumber(timeDiff)} ${unit} پیش `
      : "خطا در محاسبه زمان";
  } catch {
    return "خطا در parse زمان";
  }
};

export default timeDiffWord;
