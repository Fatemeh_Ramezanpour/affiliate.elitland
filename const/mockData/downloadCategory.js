const animation = [
  {
    id: 6370,
    uid: "MeBg",
    name: "جاسوسان نامحسوس",
    eng_name: "Spies in Disguise",
    movieType: "foreign_cinema",
    full_title: "فیلم جاسوسان نامحسوس",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6370/medium_cf0ca557-238b-4d5e-a1d7-d7b0e127fd24.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6370/large_cf0ca557-238b-4d5e-a1d7-d7b0e127fd24.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6370/small_cf0ca557-238b-4d5e-a1d7-d7b0e127fd24.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6370/medium_cf0ca557-238b-4d5e-a1d7-d7b0e127fd24.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6370/large_cf0ca557-238b-4d5e-a1d7-d7b0e127fd24.jpg"
    },
    story:
      "بهترین جاسوس دنیا که به یک کبوتر تبدیل شده است با کمک همکارش می خواهد دنیا را نجات دهد.",
    url: "https://www.salamcinama.ir/movie/MeBg/جاسوسان-نامحسوس",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 3,
        name: "اکشن"
      },
      {
        id: 8,
        name: "ماجراجویی"
      }
    ]
  },
  {
    id: 5402,
    uid: "PYgg",
    name: "باب اسفنجی - شلوار بلند",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - شلوار بلند",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5402/large_c489f1a6-89c1-41ba-88e5-c1dfde0fe0ed.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5402/large_c489f1a6-89c1-41ba-88e5-c1dfde0fe0ed.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5402/small_c489f1a6-89c1-41ba-88e5-c1dfde0fe0ed.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5402/large_c489f1a6-89c1-41ba-88e5-c1dfde0fe0ed.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5402/large_c489f1a6-89c1-41ba-88e5-c1dfde0fe0ed.jpg"
    },
    story:
      "باب با تعویض شلوارش به نظر خوشگل تر میشه و ظاهر بهتری به خودش میگیره و مغرورانه از رستوران آقای خرچنگ استعفا میده تا اینکه…",
    url: "http://salamcinama.local/movie/PYgg/باب-اسفنجی-شلوار-بلند",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 22,
        name: "خانوادگی"
      }
    ]
  },
  {
    id: 5401,
    uid: "xJ0E",
    name: "باب اسفنجی - پیک نیک اسفنجی",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - پیک نیک اسفنجی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5401/large_08c49c07-5840-4eac-bd3c-5e0b431f028f.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5401/large_08c49c07-5840-4eac-bd3c-5e0b431f028f.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5401/small_08c49c07-5840-4eac-bd3c-5e0b431f028f.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5401/large_08c49c07-5840-4eac-bd3c-5e0b431f028f.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5401/large_08c49c07-5840-4eac-bd3c-5e0b431f028f.jpg"
    },
    story:
      "فروش رستوران آقای خرچنگ پایین آمده و باب پیشنهاد می دهد که برای بازدهی بیشتر، کارمندان را به پیک نیک دعوت کند که…",
    url: "http://salamcinama.local/movie/xJ0E/باب-اسفنجی-پیک-نیک-اسفنجی",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 5687,
    uid: "NKe4",
    name: "باب اسفنجی - پروفسور بختاپوس",
    eng_name: "SpongeBob SquarePants Professor Squidward",
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - پروفسور بختاپوس",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5687/large_474140e4-06d0-470a-817b-7dcd21ae5a30.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5687/large_474140e4-06d0-470a-817b-7dcd21ae5a30.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5687/small_474140e4-06d0-470a-817b-7dcd21ae5a30.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5687/large_474140e4-06d0-470a-817b-7dcd21ae5a30.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5687/large_474140e4-06d0-470a-817b-7dcd21ae5a30.jpg"
    },
    story:
      "بختاپوس خود را بجای استاد موسیقی معرفی می کند و برای بقیه سو تفاهم هایی را بوجود می آورد که…",
    url: "http://salamcinama.local/movie/NKe4/باب-اسفنجی-پروفسور-بختاپوس",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 5400,
    uid: "DKgQ",
    name: "باب اسفنجی - نقاش می شود",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - نقاش می شود",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5400/large_9f512f84-488c-4a17-bd3c-6377ee00c4c3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5400/large_9f512f84-488c-4a17-bd3c-6377ee00c4c3.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5400/small_9f512f84-488c-4a17-bd3c-6377ee00c4c3.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5400/large_9f512f84-488c-4a17-bd3c-6377ee00c4c3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5400/large_9f512f84-488c-4a17-bd3c-6377ee00c4c3.jpg"
    },
    story:
      "آقای خرچنگ به پاتریک و باب اسفنجی مسئولیت رنگ کردن خانه اش را می سپارد و آن ها مشغول به کار می شوند که …",
    url: "http://salamcinama.local/movie/DKgQ/باب-اسفنجی-نقاش-می-شود",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 22,
        name: "خانوادگی"
      }
    ]
  },
  {
    id: 5465,
    uid: "5xg4",
    name: "باب اسفنجی - ماقبل تاریخ",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - ماقبل تاریخ",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5465/large_9c5d6e91-0937-403c-bf5f-8de79a9977f9.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5465/large_9c5d6e91-0937-403c-bf5f-8de79a9977f9.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5465/small_9c5d6e91-0937-403c-bf5f-8de79a9977f9.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5465/large_9c5d6e91-0937-403c-bf5f-8de79a9977f9.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5465/large_9c5d6e91-0937-403c-bf5f-8de79a9977f9.jpg"
    },
    story:
      "باب اسفنجی به همراه دوستانش در دوره ای از ماقبل تاریخ زندگی میکنند و با مشکلاتی روبه\u200cرو هستند که…",
    url: "http://salamcinama.local/movie/5xg4/باب-اسفنجی-ماقبل-تاریخ",
    state: "confirmed",
    genre: [
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 5399,
    uid: "vY0e",
    name: "باب اسفنجی - خانه آناناسی",
    eng_name: "spongebob squarepants pineapple house",
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - خانه آناناسی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5399/large_f4b9ce9b-51c2-4a89-a16a-17697c1e7bc3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5399/large_f4b9ce9b-51c2-4a89-a16a-17697c1e7bc3.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5399/small_f4b9ce9b-51c2-4a89-a16a-17697c1e7bc3.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5399/large_f4b9ce9b-51c2-4a89-a16a-17697c1e7bc3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5399/large_f4b9ce9b-51c2-4a89-a16a-17697c1e7bc3.jpg"
    },
    story:
      "خانه باب اسفنجی به طور ناگهانی ناپدید می شود و او برای ادامه زندگی ناچار است هم خانه ی دوستش پاتریک شود اما خانه پاتریک یک خانه عجیب و غریب است و…",
    url: "http://salamcinama.local/movie/vY0e/باب-اسفنجی-خانه-آناناسی",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 8,
        name: "ماجراجویی"
      }
    ]
  },
  {
    id: 5361,
    uid: "8Nl4",
    name: "باب اسفنجی - غار پلانکتون",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - غار پلانکتون",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5361/large_5d40302a-868d-47f1-9f94-cb9c5878250c.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5361/large_5d40302a-868d-47f1-9f94-cb9c5878250c.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5361/small_5d40302a-868d-47f1-9f94-cb9c5878250c.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5361/large_5d40302a-868d-47f1-9f94-cb9c5878250c.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5361/large_5d40302a-868d-47f1-9f94-cb9c5878250c.jpg"
    },
    story: "پلانکتون برای رقابت با اقای خرچنگ، غار ترسناکی در زیر زمین ایجاد میکند ...!",
    url: "http://salamcinama.local/movie/8Nl4/باب-اسفنجی-غار-پلانکتون",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 8,
        name: "ماجراجویی"
      }
    ]
  },
  {
    id: 5360,
    uid: "jez1",
    name: "باب اسفنجی - جاسوس بازی",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - جاسوس بازی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5360/large_e2225bbe-e13c-4f4a-a35f-909668e88ac6.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5360/large_e2225bbe-e13c-4f4a-a35f-909668e88ac6.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5360/small_e2225bbe-e13c-4f4a-a35f-909668e88ac6.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5360/large_e2225bbe-e13c-4f4a-a35f-909668e88ac6.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5360/large_e2225bbe-e13c-4f4a-a35f-909668e88ac6.jpg"
    },
    story:
      "باب اسفنجی شلوار مکعبی، باب و دوستش پاتریک تصمیم میگیرند برای یک عمیلات سری اقدام به جاسوس کنند اما…",
    url: "http://salamcinama.local/movie/jez1/باب-اسفنجی-جاسوس-بازی",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 8,
        name: "ماجراجویی"
      }
    ]
  },
  {
    id: 5327,
    uid: "A7oE",
    name: "باب اسفنجی - اختراع پلانکتون",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم باب اسفنجی - اختراع پلانکتون",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5327/large_487c0126-88f4-4779-8c2c-edd843db8d82.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5327/large_487c0126-88f4-4779-8c2c-edd843db8d82.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5327/small_487c0126-88f4-4779-8c2c-edd843db8d82.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5327/large_487c0126-88f4-4779-8c2c-edd843db8d82.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5327/large_487c0126-88f4-4779-8c2c-edd843db8d82.jpg"
    },
    story:
      "در این قسمت از مجموعه پر طرفدار باب اسفنجی پلانکتون تصمیم می گیرد تا یک کپی از باب اسفنجی را بسازد تا از این طریق به فرمول همبرگر خرچنگی دست پیدا کند که ...",
    url: "http://salamcinama.local/movie/A7oE/باب-اسفنجی-اختراع-پلانکتون",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 8,
        name: "ماجراجویی"
      }
    ]
  },
  {
    id: 5568,
    uid: "owy1",
    name: "باب اسفنجی - گلادیاتور می شود",
    eng_name: null,
    movieType: "foreign_cinema",
    full_title: "فیلم باب اسفنجی - گلادیاتور می شود",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5568/large_7ccf4772-190d-4b34-9e5b-84c544ea140e.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5568/large_7ccf4772-190d-4b34-9e5b-84c544ea140e.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5568/small_7ccf4772-190d-4b34-9e5b-84c544ea140e.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5568/large_7ccf4772-190d-4b34-9e5b-84c544ea140e.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5568/large_7ccf4772-190d-4b34-9e5b-84c544ea140e.jpg"
    },
    story: null,
    url: "http://salamcinama.local/movie/owy1/باب-اسفنجی-گلادیاتور-می-شود",
    state: "confirmed",
    genre: [
      {
        id: 20,
        name: "انیمیشن"
      },
      {
        id: 22,
        name: "خانوادگی"
      }
    ]
  },
  {
    id: 4488,
    uid: "DKyo",
    name: "هواپیماها 1",
    eng_name: "",
    movieType: "foreign_cinema",
    full_title: "فیلم هواپیماها 1",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4488/large_8adf6f09-06fd-48f5-91a8-d961ce045bdc.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4488/large_8adf6f09-06fd-48f5-91a8-d961ce045bdc.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4488/small_8adf6f09-06fd-48f5-91a8-d961ce045bdc.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4488/large_8adf6f09-06fd-48f5-91a8-d961ce045bdc.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4488/large_8adf6f09-06fd-48f5-91a8-d961ce045bdc.jpg"
    },
    story:
      "داستان درباره داستی یک هواپیمای کوچک ملخی است که در آرزوی شرکت در مسابقات بزرگ بین هواپیماهاست اما فقط یک مشکل کوچک وجود دارد. داستی از ارتفاع می\u200cترسد!",
    url: "http://salamcinama.local/movie/DKyo/هواپیماها-1",
    state: "confirmed",
    genre: []
  },
  {
    id: 4489,
    uid: "xJrd",
    name: "هواپیماها 2: آتش و نجات",
    eng_name: "Planes: Fire & Rescue",
    movieType: "foreign_cinema",
    full_title: "فیلم هواپیماها 2: آتش و نجات",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4489/large_052ad7f1-ff36-49ed-ba1a-ed57eb3cc6cc.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4489/large_052ad7f1-ff36-49ed-ba1a-ed57eb3cc6cc.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4489/small_052ad7f1-ff36-49ed-ba1a-ed57eb3cc6cc.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4489/large_052ad7f1-ff36-49ed-ba1a-ed57eb3cc6cc.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4489/large_052ad7f1-ff36-49ed-ba1a-ed57eb3cc6cc.jpg"
    },
    story:
      "وقتی که داستی متوجه می شود موتورش ضربه دیده و شاید دیگر نتواند مسابقه بدهد، تصمیم می گیرد که در تیم آتش نشانی جنگل عضو شود و در گروه نجات قرار بگیرد تا بتواند یک آتش نشان حرفه ای بشود و ...",
    url: "http://salamcinama.local/movie/xJrd/هواپیماها-2-آتش-و-نجات",
    state: "confirmed",
    genre: []
  }
];

const comedy = [
  {
    id: 4753,
    uid: "nwnK",
    name: "خداحافظ دختر شیرازی",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم خداحافظ دختر شیرازی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/medium_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/large_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg"
    },
    produceYear: 1397,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/small_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/medium_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/large_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg"
    },
    story:
      "«خداحافظ دخترشیرازی» یک عاشقانه‌ی مفرّح است. یکی از شیراز، یکی از آبادان، هردو در تهران! مجبورند فعلن باهم سر کنند تا محمودِ نارفیق را پیدا کنند.",
    url: "https://www.salamcinama.ir/movie/nwnK/خداحافظ-دختر-شیرازی",
    state: "confirmed",
    genre: [
      {
        id: 4,
        name: "عاشقانه"
      },
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 707,
    uid: "707",
    name: "مارمولک",
    eng_name: "The Lizard",
    movieType: "cinema",
    full_title: "فیلم مارمولک",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/707/medium_931cdba3-ecf9-4bdc-9a2f-eda00d51ac0e.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/707/large_931cdba3-ecf9-4bdc-9a2f-eda00d51ac0e.jpg"
    },
    produceYear: 1382,
    director: "کمال تبریزی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/707/small_931cdba3-ecf9-4bdc-9a2f-eda00d51ac0e.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/707/medium_931cdba3-ecf9-4bdc-9a2f-eda00d51ac0e.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/707/large_931cdba3-ecf9-4bdc-9a2f-eda00d51ac0e.jpg"
    },
    story:
      "رضا مثقالی معروف به رضا مارمولک دزد سابقه‌داری است که در زندان به سر می برد که با جا زدن خودش به عنوان یک آخوند موفق به فرار می شود اما برخلاف انتظارش باید مدت زمان زیادی نقش یک آخوند را بازی کند...",
    url: "https://www.salamcinama.ir/movie/707/مارمولک",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 403,
    uid: "403",
    name: "پارادایس",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم پارادایس",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/403/medium_5f14bcf6-3e6d-4ddc-ada7-6d2f563323f9.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/403/large_5f14bcf6-3e6d-4ddc-ada7-6d2f563323f9.jpg"
    },
    produceYear: 1394,
    director: "علی عطشانی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/403/small_5f14bcf6-3e6d-4ddc-ada7-6d2f563323f9.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/403/medium_5f14bcf6-3e6d-4ddc-ada7-6d2f563323f9.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/403/large_5f14bcf6-3e6d-4ddc-ada7-6d2f563323f9.jpg"
    },
    story:
      "داستان 2 طلبه جوان است که می خواهند برای شرکت در سمیناری دانشگاهی پیرامون ادیان به آلمان بروند اما رییس حوزه علمیه آن ها که حاج آقا فراستی نام دارد، راه سفر را بر آن ها سد می کند. طلبه های جوان با اغفال حاج آقا فراستی، ویزایی هم برای او می گیرند و وی را با خود به آلمان می برند و...",
    url: "https://www.salamcinama.ir/movie/403/پارادایس",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 4757,
    uid: "EbeD",
    name: "عزیز میلیون دلاری",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم عزیز میلیون دلاری",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/4757/medium_5fccc9a5-1d19-46d3-99ec-8d173a7de111.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/4757/large_5fccc9a5-1d19-46d3-99ec-8d173a7de111.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4757/small_5fccc9a5-1d19-46d3-99ec-8d173a7de111.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4757/medium_5fccc9a5-1d19-46d3-99ec-8d173a7de111.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4757/large_5fccc9a5-1d19-46d3-99ec-8d173a7de111.jpg"
    },
    story:
      "این فیلم درباره اردشیر مشکین ثروتمندترین مرد دنیاست که برای اموال خود به دنبال نوه گم شده خود عزیز می گردد و در این حین و بین ، زندگی او و اطرافیانش دستخوش اتفاقات عجیبی می شود.",
    url: "http://salamcinama.local/movie/EbeD/عزیز-میلیون-دلاری",
    state: "confirmed",
    genre: []
  },
  {
    id: 229,
    uid: "229",
    name: "سه بیگانه",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم سه بیگانه",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/229/medium_ffc67dbe-3276-4d84-a884-3d6a9f24a83d.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/229/large_ffc67dbe-3276-4d84-a884-3d6a9f24a83d.jpg"
    },
    produceYear: 1393,
    director: "مهدی مظلومی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/229/small_ffc67dbe-3276-4d84-a884-3d6a9f24a83d.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/229/medium_ffc67dbe-3276-4d84-a884-3d6a9f24a83d.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/229/large_ffc67dbe-3276-4d84-a884-3d6a9f24a83d.jpg"
    },
    story:
      "داستان سه مامور خارجی است که برای انجام عملیاتی با یک نقشه دقیق و از پیش تعیین شده وارد ایران می شوند اما از لحظه ورود با موقعیت های ویژه ای روبرو می شوند که برنامه های آن ها را به هم می ریزد.",
    url: "http://salamcinama.local/movie/229/سه-بیگانه",
    state: "confirmed",
    genre: []
  },
  {
    id: 4246,
    uid: "PYBq",
    name: "دم سرخ ها",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم دم سرخ ها",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/4246/medium_38748a4b-4740-4d92-b0f2-b8b385633999.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/4246/large_38748a4b-4740-4d92-b0f2-b8b385633999.jpg"
    },
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4246/small_38748a4b-4740-4d92-b0f2-b8b385633999.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4246/medium_38748a4b-4740-4d92-b0f2-b8b385633999.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4246/large_38748a4b-4740-4d92-b0f2-b8b385633999.jpg"
    },
    story: "برای فرار از حقیقت خوابیدیم و کابوس دیدیم ...",
    url: "http://salamcinama.local/movie/PYBq/دم-سرخ-ها",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 4651,
    uid: "d9A5",
    name: "لازانیا",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم لازانیا",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/4651/medium_a8c07412-ed20-4e48-a44b-37c3fcd5b24f.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/4651/large_a8c07412-ed20-4e48-a44b-37c3fcd5b24f.jpg"
    },
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4651/small_a8c07412-ed20-4e48-a44b-37c3fcd5b24f.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4651/medium_a8c07412-ed20-4e48-a44b-37c3fcd5b24f.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4651/large_a8c07412-ed20-4e48-a44b-37c3fcd5b24f.jpg"
    },
    story: "تو کفتم!...کف! ...می فهمی؟ تا نخورمت ولت نمی کنم!",
    url: "http://salamcinama.local/movie/d9A5/لازانیا",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 3949,
    uid: "2q8afkyx64yw1k0",
    name: "عشقولانس",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم عشقولانس",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/3949/medium_c2121756-d9af-4e7c-adab-487d5857590c.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/3949/large_c2121756-d9af-4e7c-adab-487d5857590c.jpg"
    },
    produceYear: 1395,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3949/small_c2121756-d9af-4e7c-adab-487d5857590c.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3949/medium_c2121756-d9af-4e7c-adab-487d5857590c.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3949/large_c2121756-d9af-4e7c-adab-487d5857590c.jpg"
    },
    story: "",
    url: "http://salamcinama.local/movie/2q8afkyx64yw1k0/عشقولانس",
    state: "confirmed",
    genre: []
  },
  {
    id: 3879,
    uid: "fa9afkwn4gbi1k0",
    name: "شاباش",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم شاباش",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/3879/medium_d45b3f31-4ff0-4b9a-9995-8768d7fb1a46.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/3879/large_d45b3f31-4ff0-4b9a-9995-8768d7fb1a46.jpg"
    },
    produceYear: 1391,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3879/small_d45b3f31-4ff0-4b9a-9995-8768d7fb1a46.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3879/medium_d45b3f31-4ff0-4b9a-9995-8768d7fb1a46.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3879/large_d45b3f31-4ff0-4b9a-9995-8768d7fb1a46.jpg"
    },
    story:
      "داستان زندگی حاج احمد عباچی (عبدی) را روایت می کند که پول پیش خانه اش را در عروسی خواهرزاده اش شاباش می دهد و...",
    url: "http://salamcinama.local/movie/fa9afkwn4gbi1k0/شاباش",
    state: "confirmed",
    genre: []
  },
  {
    id: 4874,
    uid: "36vY",
    name: "مشت آخر",
    eng_name: "last stroke",
    movieType: "cinema",
    full_title: "فیلم مشت آخر",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4874/medium_922fb7c1-338f-4427-90f0-27ecec7c2360.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4874/large_922fb7c1-338f-4427-90f0-27ecec7c2360.jpg"
    },
    produceYear: 1397,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4874/small_922fb7c1-338f-4427-90f0-27ecec7c2360.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4874/medium_922fb7c1-338f-4427-90f0-27ecec7c2360.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4874/large_922fb7c1-338f-4427-90f0-27ecec7c2360.jpg"
    },
    story:
      "داستان پیر مردی است که از سالهای دور تا کنون در کاراته تبحر بسیار بالایی را دارد ولی جهت امرار معاش تظاهر به\nفرتوتی میکند و در خیابان ها با دستیارش به صورت سوری معرکه دعوا به راه می اندازد و مردم جهت کمک سعی در جدا کردن\nآنها میکنند که در این میان دستیارش از شلوغی فضا کمک گرفته و جیب مردم را میزنند و ...",
    url: "https://www.salamcinama.ir/movie/36vY/مشت-آخر",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 3,
        name: "اکشن"
      }
    ]
  },
  {
    id: 6883,
    uid: "Jo8y",
    name: "لعنتی خنده دار",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم لعنتی خنده دار",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6883/medium_2425c6b0-c89f-4127-aabd-7a7ede5c78c8.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6883/large_2425c6b0-c89f-4127-aabd-7a7ede5c78c8.jpg"
    },
    produceYear: 1392,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6883/small_2425c6b0-c89f-4127-aabd-7a7ede5c78c8.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6883/medium_2425c6b0-c89f-4127-aabd-7a7ede5c78c8.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6883/large_2425c6b0-c89f-4127-aabd-7a7ede5c78c8.jpg"
    },
    story:
      "سیامک جوانیست که به تهیه داروهای کمیاب و دلالی قلب برای بیماران نیازمند پیوند پرداخته است. از همین رو وی همیشه و همه جا به دنبال یافتن مواردی است که شخصی بر اثر مرگ مغزی، مستعد اهدای اعضا باشد و در نتیجه او قلب فرد درگذشته را برای یکی از سفارش دهندگانش خریداری می‌نماید. این در حالیست که خانواده ی وی چه مادر و چه نامزدش که دختر خاله اش نیز می باشد، از شغل او اطلاعی ندارند و…",
    url: "https://www.salamcinama.ir/movie/Jo8y/لعنتی-خنده-دار",
    state: "confirmed",
    genre: []
  },
  {
    id: 6536,
    uid: "wGvm",
    name: "آواز در خواب",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم آواز در خواب",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6536/medium_3867e2cb-44fc-4cc7-adf5-381578a6a4d3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6536/large_3867e2cb-44fc-4cc7-adf5-381578a6a4d3.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6536/small_3867e2cb-44fc-4cc7-adf5-381578a6a4d3.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6536/medium_3867e2cb-44fc-4cc7-adf5-381578a6a4d3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6536/large_3867e2cb-44fc-4cc7-adf5-381578a6a4d3.jpg"
    },
    story:
      "داستان «آواز در خواب» درباره آقای فتحی صاحب یک مغازه‌ی عتیقه فروشی است که فردی بسیار خسیس است. او روح شریکش را در خواب می‌بیند که او را متوجه گذشته، حال و آینده‌اش می‌کند. فتحی که در خواب حس می‌کند متحول شده، از خواب بیدار می‌شود و آدم دیگری می‌شود که…",
    url: "https://www.salamcinama.ir/movie/wGvm/آواز-در-خواب",
    state: "confirmed",
    genre: []
  },
  {
    id: 4079,
    uid: "06wD",
    name: "همه چی عادیه",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم همه چی عادیه",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/4079/medium_29959c1b-14e3-4227-82be-9c1ed05d7365.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/4079/large_29959c1b-14e3-4227-82be-9c1ed05d7365.jpg"
    },
    produceYear: 1395,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4079/small_29959c1b-14e3-4227-82be-9c1ed05d7365.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4079/medium_29959c1b-14e3-4227-82be-9c1ed05d7365.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4079/large_29959c1b-14e3-4227-82be-9c1ed05d7365.jpg"
    },
    story:
      "«همه چی عادیه!» مضمونی اجتماعی با رگه هایی کمدی دارد و داستانی از مشکلات روز جامعه را روایت می کند. این فیلم، داستان زندگی جوانی روستایی (عباس غزالی) است که به پایتخت مهاجرت می\u200cکند. او مجبور به فعالیت در مشاغلی می\u200cشود که دانش و تجربه\u200cای از آن\u200cها ندارد.",
    url: "http://salamcinama.local/movie/06wD/همه-چی-عادیه",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 4638,
    uid: "7RvL",
    name: "کاتیوشا",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم کاتیوشا",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/4638/medium_bbeb88ef-c5da-4281-9693-14ac52f2bdaf.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/4638/large_bbeb88ef-c5da-4281-9693-14ac52f2bdaf.jpg"
    },
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4638/small_bbeb88ef-c5da-4281-9693-14ac52f2bdaf.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4638/medium_bbeb88ef-c5da-4281-9693-14ac52f2bdaf.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4638/large_bbeb88ef-c5da-4281-9693-14ac52f2bdaf.jpg"
    },
    story:
      "من اگه بخوام توی زندگیم موفق باشم، حتما نباید اونجوری به زندگی نگاه کنم که تو نگاه می\u200cکنی./ کاتیوشا(هادی حجازی فر) ماموریت دارد تا از ارشیا(احمد مهران فر) مراقب کند تا به سمت مواد مخدر نرود...",
    url: "http://salamcinama.local/movie/7RvL/کاتیوشا",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 338,
    uid: "338",
    name: "گذر موقت",
    eng_name: "Temporary Passage",
    movieType: "cinema",
    full_title: "فیلم گذر موقت",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/338/medium_30c8511e-3dfd-49f8-8b03-757e2e57ac41.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/338/large_30c8511e-3dfd-49f8-8b03-757e2e57ac41.jpg"
    },
    produceYear: 1394,
    director: "افشین هاشمی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/338/small_30c8511e-3dfd-49f8-8b03-757e2e57ac41.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/338/medium_30c8511e-3dfd-49f8-8b03-757e2e57ac41.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/338/large_30c8511e-3dfd-49f8-8b03-757e2e57ac41.jpg"
    },
    story:
      "داستان دو پیرمرد است که در شبی تاریک، تصمیمی مفرح می\u200cگیرند و سفری شبانه را در دل شهر آغاز می\u200cکنند.",
    url: "http://salamcinama.local/movie/338/گذر-موقت",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 911,
    uid: "911",
    name: "اجاره نشین ها",
    eng_name: "The Tenants",
    movieType: "cinema",
    full_title: "فیلم اجاره نشین ها",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/911/medium_14b0731a-e0d8-4101-aadb-fdf30899e5e1.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/911/large_14b0731a-e0d8-4101-aadb-fdf30899e5e1.jpg"
    },
    produceYear: 1365,
    director: "داریوش مهرجویی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/911/small_14b0731a-e0d8-4101-aadb-fdf30899e5e1.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/911/medium_14b0731a-e0d8-4101-aadb-fdf30899e5e1.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/911/large_14b0731a-e0d8-4101-aadb-fdf30899e5e1.jpg"
    },
    story:
      "خانه\u200cای که وارث یا وارثانش معلوم نیست، عده\u200cای مستاجر دارد. خانه نیازمند تعمیرات اساسی است ولی بی توجهی عباس آقا، مباشر که قصد تصاحب خانه را دارد، باعث می\u200cشود تا به رغم تلاش\u200cهای دیگر ساکنان، کار از کار بگذرد و خانه بر ساکنانش فروریزد.",
    url: "http://salamcinama.local/movie/911/اجاره-نشین-ها",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 767,
    uid: "767",
    name: "دخترعمو و پسرعمو",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم دخترعمو و پسرعمو",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/767/medium_ea1eaf2f-c855-4ff9-9128-cdc68858c360.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/767/large_ea1eaf2f-c855-4ff9-9128-cdc68858c360.jpg"
    },
    produceYear: 1396,
    director: "روح انگیز شمس",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/767/small_ea1eaf2f-c855-4ff9-9128-cdc68858c360.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/767/medium_ea1eaf2f-c855-4ff9-9128-cdc68858c360.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/767/large_ea1eaf2f-c855-4ff9-9128-cdc68858c360.jpg"
    },
    story:
      "داستان «پسر عمو دختر عمو» درباره زندگی یک پسر عمو و دختر عمو است که در زندگی خانوادگی شان اختلافات زیادی به وجود می\u200cآید. آنها با دستور و راهنمایی\u200cهای پدربزرگ خود مجبور می\u200cشوند تمامی اختلافات خانوادگی شان را حل کنند…",
    url: "http://salamcinama.local/movie/767/دخترعمو-و-پسرعمو",
    state: "confirmed",
    genre: []
  },

  {
    id: 4176,
    uid: "zJnq",
    name: "پا تو کفش من نکن",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم پا تو کفش من نکن",
    old_poster_which_should_remove: {
      medium:
        "http://api.salamcinama.local/uploads/movie/poster/4176/medium_c854beff-bb7c-424a-9b10-389ac0769249.jpg",
      large:
        "http://api.salamcinama.local/uploads/movie/poster/4176/large_c854beff-bb7c-424a-9b10-389ac0769249.jpg"
    },
    produceYear: 1395,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4176/small_c854beff-bb7c-424a-9b10-389ac0769249.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4176/medium_c854beff-bb7c-424a-9b10-389ac0769249.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4176/large_c854beff-bb7c-424a-9b10-389ac0769249.jpg"
    },
    story:
      "این کمدی-اجتماعی داستان دو زوج را روایت میکند که برای ثبت ازدواج به محضر می\u200cروند ولی شلوغی محضر عاملی می\u200cشود برای رخداد اتفاقی غیرمنتظره و در نهایت گره خوردن زندگی این دو زوج به یکدیگر!",
    url: "http://salamcinama.local/movie/zJnq/پا-تو-کفش-من-نکن",
    state: "confirmed",
    genre: []
  },
  {
    id: 5929,
    uid: "6LjE",
    name: "سرکوفت",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم سرکوفت",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5929/medium_bd39b8cc-70f5-4d8c-b01b-afdf080cea73.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5929/large_bd39b8cc-70f5-4d8c-b01b-afdf080cea73.jpg"
    },
    produceYear: 1397,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5929/small_bd39b8cc-70f5-4d8c-b01b-afdf080cea73.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5929/medium_bd39b8cc-70f5-4d8c-b01b-afdf080cea73.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5929/large_bd39b8cc-70f5-4d8c-b01b-afdf080cea73.jpg"
    },
    story:
      "مجید(وحید رحمتی) برای خواستگاری از فرشته(ویدا جوان) منتظر بازگشت پدر او از سفر خارج است،‌در صورتی که بر خلاف تصور او مظفرخان(سید مهرداد ضیایی) از زندان آزاد شده و به خانه باز می گردد. از آنجا که خانواده مجید کاملا موجه و  متشخص بوده و بر خلاف آنها پدر،‌برادر (محمد معماریان) و پسرخاله فرشته (محمد امین کریم پور) همگی خلاف کار هستند، مظفر خان نگران سرکوفت های احتمالی آینده شده و با این ازدواج مخالفت مینماید.بنابراین شرطی برای ازدواج آن ها می گذارد،سیاوش(محمدامین کریم پور) و فرهاد(محمد معماریان) جهت بانجام رساندن شرط مظفرخان نقشه های مختلفی دارند که اتفاقاتی را رقم زده و ...",
    url: "https://www.salamcinama.ir/movie/6LjE/سرکوفت",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 4,
        name: "عاشقانه"
      }
    ]
  }
];

const series = [
  {
    id: 6971,
    uid: "AEq6",
    name: "شام ایرانی",
    full_title: "فیلم شام ایرانی",
    produceYear: 1388,
    director: null,
    is_series: false,
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6971/large_c58b6037-1a2a-49e9-9510-69dd828c78ba.jpg"
    },
    url: "/shameirani"
  },
  {
    id: 6924,
    uid: "o4kR",
    name: "روز بازی",
    produceYear: 1398,
    director: null,
    is_series: false,
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6924/large_1c70319d-185f-47c5-aede-7832c86374fb.jpg"
    },
    url: "/roozebazi"
  },
  {
    id: 6302,
    uid: "PKo5",
    name: "قهوه تلخ",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم قهوه تلخ",
    produceYear: 1388,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6302/small_17e4d4d9-962f-4211-82dc-7d7b4074e9a7.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6302/medium_17e4d4d9-962f-4211-82dc-7d7b4074e9a7.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6302/large_17e4d4d9-962f-4211-82dc-7d7b4074e9a7.jpg"
    },
    story:
      "نیما که یک تاریخدان است با دختری آشنا می شود که در مورد دوره ای تاریخی پژوهش میکرد که در هیچ کتابی منتشر نشده است . نیما معتقد بود که چنین دوره ای اصلا وجود ندارد . برای ثابت کردن حرفش دختر جوان را همراهی میکند تا اینکه به این پی میبرد که دخترجوان راست میگوید و دوره ای که مدعی بود وجود دارد که پادشاه آن در تهران قرار داشته ولی در هیچ کتب تاریخی قید نشده است ! نیما برای تحقیقات کاملتر وارد کاخی میشود که در آن قهوه ی تلخی را می نوشد . نیما بعد از خوردن قهوه به دوره ای می رود که درباره ی آن تحقیق میکرد ! او اول تصور میکند که همه چیز خواب است و هیچ چیز واقعیت ندارد ولی بعد از مدتی با این موضوع کنار می آید و چون تاریخ را میداند سعی میکند تا به وسیله ی آن درباریان را کمک کند.",
    url: "/ghahvetalkh",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 6608,
    uid: "eZdk",
    name: "هم گناه",
    eng_name: null,
    movieType: "home_serial",
    full_title: "فیلم هم گناه",
    produceYear: 1398,
    director: null,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6608/small_7d3b8d9a-b2f7-4c5c-8d5b-e33ad4f311a3.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6608/medium_7d3b8d9a-b2f7-4c5c-8d5b-e33ad4f311a3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6608/large_7d3b8d9a-b2f7-4c5c-8d5b-e33ad4f311a3.jpg"
    },
    story:
      "خانواده صبوری خانواده‌ای بزرگ و قدیمی است که سال‌هاست در کار صادرات و واردات گل و گیاه است. آن‌ها در خانه‌ای قدیمی همچنان در آرامش با هم زندگی می‌کنند، ورود جوانی به این خانواده زندگی همه خانواده را دستخوش تغییراتی می‌کند که....",
    url: "/hamgonah",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      },
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 12,
        name: "اسرار آمیز"
      }
    ]
  },
  {
    id: 5322,
    uid: "XA7v",
    name: "خواب زده",
    eng_name: null,
    movieType: "home_serial",
    full_title: "سریال خواب زده ",
    produceYear: 1397,
    director: null,
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5322/large_0d7482c7-620b-4df8-8e5a-c68806b4d120.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5322/medium_0d7482c7-620b-4df8-8e5a-c68806b4d120.jpg",
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5322/small_0d7482c7-620b-4df8-8e5a-c68806b4d120.jpg"
    },
    story:
      "«رها» و «هما»ی قصه با دیدن یک خواب مشترک با هم اشنا میشن و این شروع ماجراهای عشقی و ورودشان به زندگی یکدیگر است.",
    url: "https://www.salamcinama.ir/khabzade",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      },
      {
        id: 22,
        name: "خانوادگی"
      },
      {
        id: 12,
        name: "اسرار آمیز"
      }
    ]
  },
  {
    id: 4642,
    uid: "yG63",
    name: "ساخت ایران 2",
    eng_name: null,
    movieType: "home_serial",
    full_title: "سریال ساخت ایران 2 ",
    produceYear: 1396,
    director: null,
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4642/large_740cd784-91a0-42ea-94bb-615a2d6b7da6.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4642/medium_740cd784-91a0-42ea-94bb-615a2d6b7da6.jpg",
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4642/small_740cd784-91a0-42ea-94bb-615a2d6b7da6.jpg"
    },
    story:
      "داستان فصل دوم این سریال هم در ادامه داستان فصل اول است؛ که در فصل اول جایی که دکتر مخترع تصادف می کند و ساک همراه او که حامل اختراعات جدید در علم بشریت است دست فرد لاتی به نام غلام می افتد و او به دلیل دست یافتن به این اطلاعات ارزشمند به خارج کشور می رود ولی هیچ کس نمی داند که او دکتر واقعی نیست…",
    url: "https://www.salamcinama.ir/download/A7O6",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 4768,
    uid: "owZR",
    name: "رقص روی شیشه",
    eng_name: null,
    movieType: "home_serial",
    full_title: "سریال رقص روی شیشه ",
    produceYear: 1397,
    director: null,
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4768/large_ca7fafbc-790b-4ac6-95ce-5006d169ccbd.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4768/medium_ca7fafbc-790b-4ac6-95ce-5006d169ccbd.jpg",
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4768/small_ca7fafbc-790b-4ac6-95ce-5006d169ccbd.jpg"
    },
    story: "مجموعه «رقص روی شیشه» یک ملودرام اجتماعی در مذمت مهاجرت و در ستایش خانواده است.",
    url: "https://www.salamcinama.ir/download/0pmJ",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      },
      {
        id: 4,
        name: "عاشقانه"
      }
    ]
  },
  {
    id: 4962,
    uid: "q8e0",
    name: "ریکاوری",
    eng_name: null,
    movieType: "home_serial",
    full_title: "سریال ریکاوری ",
    produceYear: 1397,
    director: null,
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4962/large_9b8f712d-1d6b-4fd4-b1c9-a11ffe84b72c.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4962/medium_9b8f712d-1d6b-4fd4-b1c9-a11ffe84b72c.jpg",
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4962/small_9b8f712d-1d6b-4fd4-b1c9-a11ffe84b72c.jpg"
    },
    story:
      "داستانی است درباره رفاقت، عشق و خیانت بین سه خانواده‌ی بابک، مسعود و سهیل که شرکای کاری و دوستان قدیمی هستند.↵بابک سردبیر مجله‌ی پرفروشی‌ است و سهیل و مسعود همکاران او در نشریه هستند، هرکدام در زندگی مشترکشان دچار مسائلی شده‌اند و برای  فرار زا زندگی روزمره و کاری و چند روز دوری از همسرانشان تصمیم می‌گیرند بدون اطلاع خانواده به سفر مجردی و به اصطلاح خودشان ریکاوری بروند.",
    url: "https://www.salamcinama.ir/download/NMXn",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      },
      {
        id: 4,
        name: "عاشقانه"
      }
    ]
  },
  {
    id: 4914,
    uid: "mvYN",
    name: " 13 شمالی ",
    eng_name: null,
    movieType: "home_serial",
    full_title: "سریال 13 شمالی ",
    produceYear: 1396,
    director: null,
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4914/large_e3fc75b7-80b9-4195-bc4e-04a886b2b573.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4914/medium_e3fc75b7-80b9-4195-bc4e-04a886b2b573.jpg",
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4914/small_e3fc75b7-80b9-4195-bc4e-04a886b2b573.jpg"
    },
    story:
      "این فیلم مسابقه بین دو گروه از بازیگران چهره مرد و زن است که در گروه آقایان کامبیز دیرباز، محمدرضا هدایتی، امیرحسین آرمان، هادی کاظمی و در گروه خانم ها، سحر قریشی، لیلا اوتادی، سمانه پاکدل و بهاره افشاری حضور دارند. این دو گروه با داوری حمید گودرزی در جنگل های بهشهر و نوشهر به رقابت می‌پردازند. این دو تیم در طی 10 روز با یکدیگر رقابت داشتند و در هر روز یک دور از مسابقات برگزار می شد. تیم بازنده باید برای دور بعد، یک نفر را از بین خودشان حذف می کرد تا به همین شکل تیم ها به مرحله نهایی برسند.",
    url: "https://www.salamcinama.ir/download/16z6",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 5,
        name: "هیجانی"
      },
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 4995,
    uid: "d9qd",
    name: "آشوب",
    eng_name: null,
    movieType: "home_serial",
    full_title: "سریال آشوب ",
    produceYear: 1394,
    director: null,
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4395/large_a4ab2d25-49bf-4e4b-b995-d409887f6da4.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4395/medium_a4ab2d25-49bf-4e4b-b995-d409887f6da4.jpg",
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4395/small_a4ab2d25-49bf-4e4b-b995-d409887f6da4.jpg"
    },
    story:
      "داستان این فیلم در دهه 1320 می گذرد و مضمون اصلی آن در رابطه با بحران هنرمندان در آن روزگار است. اصل داستان این فیلم مربوط به جوانی است که با نام مستعار آشوب در کافه های لاله زار خوانندگی می کند و آین آشوب با آشوب های تاریخی و واقعی دهه 1320 تلفیق می شود و این شخصیت را به یک آشوب خوفناک و تهدید آمیز تبدیل می کند.",
    url: "https://www.salamcinama.ir/download/xJr7",
    state: "confirmed",
    genre: [
      {
        id: 9,
        name: "کمدی"
      },
      {
        id: 5,
        name: "هیجانی"
      },
      {
        id: 1,
        name: "درام"
      }
    ]
  }
];

const documentary = [
  {
    id: 6949,
    uid: "BXog",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت چهارم: همسایه ها",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم روز بازی با روایت عادل فردوسی پور – قسمت چهارم: همسایه ها",
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6949/small_1570c479-0997-439e-9b0e-52276d71d037.jpeg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6949/medium_1570c479-0997-439e-9b0e-52276d71d037.jpeg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6949/large_1570c479-0997-439e-9b0e-52276d71d037.jpeg"
    },
    story:
      "هیچ لذتی شیرین‌تر از شکست دادن رقبای همشهری نیست، چیزی که طرفداران بارسا در چند فصل اخیر و در بازی مقابل اسپانیول به آن عادت کرده‌اند. با این حال داربی همیشه یک بازی ویژه است، بخصوص بخاطر تاریخچه‌ی بین دو تیم، مسئله‌ای که ارنستو والورده و فیلیپ کوتینیو به خوبی از آن آگاهند.",
    url:
      "https://www.salamcinama.ir/movie/BXog/روز-بازی-با-روایت-عادل-فردوسی-پور-قسمت-چهارم-همسایه-ها",
    state: "confirmed",
    genre: []
  },
  {
    id: 6926,
    uid: "MeZ3",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت سوم: طلسم",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم روز بازی با روایت عادل فردوسی پور – قسمت سوم: طلسم",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6926/medium_df168a22-cd53-4747-904b-9295b117b834.jpeg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6926/large_df168a22-cd53-4747-904b-9295b117b834.jpeg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6926/small_df168a22-cd53-4747-904b-9295b117b834.jpeg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6926/medium_df168a22-cd53-4747-904b-9295b117b834.jpeg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6926/large_df168a22-cd53-4747-904b-9295b117b834.jpeg"
    },
    story:
      "در چند فصل اخیر، بارسلونا تسلیم طلسم مرحله یک‌چهارم نهایی لیگ قهرمانان بوده. حالا وقتش رسیده که با شکست یک باشگاه تاریخی دیگر، یعنی منچستریونایتد، باز هم به مرحله نیمه‌نهایی برگردد. جرارد پیکه به خانه‌ی روزهای نوجوانیش برمی‌گردد و کلمان لنگله که در ترکیب اصلی این مسابقه قرار گرفته، با برادرانش در زمین تمرین ملاقات می‌کند، ملاقاتی که الهام‌بخش تیم برای موفقیت در اولدترافورد خواهد شد.",
    url: "https://www.salamcinama.ir/movie/MeZ3/روز-بازی-با-روایت-عادل-فردوسی-پور-قسمت-سوم-طلسم",
    state: "confirmed",
    genre: []
  },
  {
    id: 6924,
    uid: "o4kR",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت اول: فوتبال کلاسیک",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم روز بازی با روایت عادل فردوسی پور – قسمت اول: فوتبال کلاسیک",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6924/medium_1c70319d-185f-47c5-aede-7832c86374fb.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6924/large_1c70319d-185f-47c5-aede-7832c86374fb.jpg"
    },
    produceYear: 1398,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6924/small_1c70319d-185f-47c5-aede-7832c86374fb.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6924/medium_1c70319d-185f-47c5-aede-7832c86374fb.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6924/large_1c70319d-185f-47c5-aede-7832c86374fb.jpg"
    },
    story:
      "لوئیز سوارز کودکی بی‌دردسری را پشت سر نگذاشت. نداشتن پدر و مشکلاتی که مادرش در تأمین معاش خانواده با آن مواجه بود، او را تحت فشار می‌گذاشت که در فوتبال به موفقیت برسد. لحظه‌ی کلیدی زندگی او، زمانی بود که در اروگوئه و در نوجوانی با همسر آینده‌اش، سوفیا، ملاقات کرد. سوارز توضیح می‌دهد که کمی بعد از این ملاقات، سوفیا ناچار می‌شود با خانواده‌اش به بارسلونا نقل مکان کند اما علی‌رغم فاصله، رابطه‌ی آنها زنده می‌ماند. زمانی که بارسا به جذب او علاقه نشان داد، او متوجه شد که حالا می‌تواند رویای تمام زندگی‌اش را محقق کند: در بارسلونا و در کنار سوفیا زندگی کند و برای بهترین تیم دنیا به میدان برود.",
    url:
      "https://www.salamcinama.ir/movie/o4kR/روز-بازی-با-روایت-عادل-فردوسی-پور-قسمت-اول-فوتبال-کلاسیک",
    state: "confirmed",
    genre: []
  },
  {
    id: 6925,
    uid: "GpOk",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت دوم: محدوده‌ی دشمن",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم روز بازی با روایت عادل فردوسی پور – قسمت دوم: محدوده‌ی دشمن",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6925/medium_2b376a6e-ed4b-451d-893e-7bd5cc5489bf.jpeg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6925/large_2b376a6e-ed4b-451d-893e-7bd5cc5489bf.jpeg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6925/small_2b376a6e-ed4b-451d-893e-7bd5cc5489bf.jpeg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6925/medium_2b376a6e-ed4b-451d-893e-7bd5cc5489bf.jpeg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6925/large_2b376a6e-ed4b-451d-893e-7bd5cc5489bf.jpeg"
    },
    story:
      "چند هفته بعد از برد قاطعانه بارسا در کلاسیکو، قرعه‌کشی کوپا‌دل‌ری تقابل تازه‌ای با رئال مادرید را سر راهشان قرار داد: دو بازی رفت‌وبرگشت در مرحله‌ی نیمه‌نهایی. بازی رفت در نیوکمپ و با درخشش غیرمنتظره‌ی مالکوم همراه بود اما همه‌چیز به مسابقه برگشت در برنابئو کشیده شد. در آن مسابقه، دروازه‌بان آلمانی بارسا، ترشتگن در قامت یک قهرمان ظاهر می‌شود و خود را به عنوان یک دوست‌دار واقعی شهر بارسلونا معرفی می‌کند.",
    url:
      "https://www.salamcinama.ir/movie/GpOk/روز-بازی-با-روایت-عادل-فردوسی-پور-قسمت-دوم-محدوده-ی-دشمن",
    state: "confirmed",
    genre: []
  },
  {
    id: 5698,
    uid: "ZRY1",
    name: "فرشاد آقای گل",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم فرشاد آقای گل",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/medium_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/large_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg"
    },
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/small_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/medium_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/large_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg"
    },
    story:
      "عکاسی علاقمند به فوتبال به مرور شیفتۀ هنرنماییِ پسرکی با استعداد به نامِ فرشاد پیوس می شود. از دریچه نگاه این عکاس سفری به دلِ تاریخ با تصاویری بدیع و دیده نشده خواهیم داشت، مروری می کنیم بر زندگی بهترین گلزن تاریخِ فوتبال باشگاهی ایران و هنرنمایی کسی که عناوینِ آقای گلی را یکی پس از دیگری به تصاحب در می آورد را با حضور ستارگان تاریخ فوتبال ایران به نظاره می نشینیم.",
    url: "https://www.salamcinama.ir/movie/ZRY1/فرشاد-آقای-گل",
    state: "confirmed",
    genre: []
  },
  {
    id: 6820,
    uid: "YPDO",
    name: "اختاپوس",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم اختاپوس",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6820/medium_df4db348-b25d-4d4d-9f04-f7cfd7f4e784.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6820/large_df4db348-b25d-4d4d-9f04-f7cfd7f4e784.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6820/small_df4db348-b25d-4d4d-9f04-f7cfd7f4e784.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6820/medium_df4db348-b25d-4d4d-9f04-f7cfd7f4e784.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6820/large_df4db348-b25d-4d4d-9f04-f7cfd7f4e784.jpg"
    },
    story: "مستند «اختاپوس» به بررسی موضوع مافیای صنعت خودرو از ابتدای انقلاب تا امروز می‌پردازد.",
    url: "https://www.salamcinama.ir/movie/YPDO/اختاپوس",
    state: "confirmed",
    genre: []
  },
  {
    id: 6815,
    uid: "lq3l",
    name: "ایکسونامی",
    eng_name: "xsunami",
    movieType: "cinema",
    full_title: "فیلم ایکسونامی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6815/medium_6cf3adab-6c7b-45b5-bc79-179c6742612a.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6815/large_6cf3adab-6c7b-45b5-bc79-179c6742612a.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6815/small_6cf3adab-6c7b-45b5-bc79-179c6742612a.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6815/medium_6cf3adab-6c7b-45b5-bc79-179c6742612a.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6815/large_6cf3adab-6c7b-45b5-bc79-179c6742612a.jpg"
    },
    story:
      "در این مستند، زن آمریکایی هرزه‌نگاری تجربه رسیدن به آزادی‌های جنسی در ایالات متحده را برای مردم ایران روایت می‌کند. **تماشای این فیلم به افراد زیر 18 سال توصیه نمی‌شود.**",
    url: "https://www.salamcinama.ir/movie/lq3l/ایکسونامی",
    state: "confirmed",
    genre: []
  }
];

const drum = [
  {
    id: 4834,
    uid: "LNGg",
    name: "هزارتو",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم هزارتو",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4834/medium_87a21adf-a97c-43b8-9039-f9fc1dbd7c23.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4834/large_87a21adf-a97c-43b8-9039-f9fc1dbd7c23.jpg"
    },
    produceYear: 1397,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4834/small_87a21adf-a97c-43b8-9039-f9fc1dbd7c23.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4834/medium_87a21adf-a97c-43b8-9039-f9fc1dbd7c23.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4834/large_87a21adf-a97c-43b8-9039-f9fc1dbd7c23.jpg"
    },
    story:
      "زندگی یه هزارتووی پیچیده‌اس، همه آدما دنبال راهی برای فرار هستند اما فقط یه مسیر خروج وجود داره و تا موقعی که به مرکزش نَرسی متوجه‌اش نمی‌شی!",
    url: "https://www.salamcinama.ir/movie/LNGg/هزارتو",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      },
      {
        id: 12,
        name: "اسرار آمیز"
      }
    ]
  },
  {
    id: 4472,
    uid: "4DPG",
    name: "درخونگاه",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم درخونگاه",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4472/medium_ca1b066d-fe02-4826-83c7-59988f255267.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4472/large_ca1b066d-fe02-4826-83c7-59988f255267.jpg"
    },
    produceYear: 1396,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4472/small_ca1b066d-fe02-4826-83c7-59988f255267.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4472/medium_ca1b066d-fe02-4826-83c7-59988f255267.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4472/large_ca1b066d-fe02-4826-83c7-59988f255267.jpg"
    },
    story:
      "ظهری می اید\nتا بسازد!\nاگر نبازد...\nرضا (امین حیایی) پس از هشت سال کار کردن در ژاپن، به محله‌شان به نام «درخونگاه» بر‌می‌گردد که ...\n**تماشای این فیلم به افراد زیر 12 سال توصیه نمی‌شود**",
    url: "https://www.salamcinama.ir/movie/4DPG/درخونگاه",
    state: "confirmed",
    genre: []
  },
  {
    id: 137,
    uid: "137",
    name: "چاقی",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم چاقی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/137/medium_c9c3c740-0ef0-4400-945a-177489ddf3b6.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/137/large_c9c3c740-0ef0-4400-945a-177489ddf3b6.jpg"
    },
    produceYear: 1393,
    director: "راما قویدل",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/137/small_c9c3c740-0ef0-4400-945a-177489ddf3b6.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/137/medium_c9c3c740-0ef0-4400-945a-177489ddf3b6.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/137/large_c9c3c740-0ef0-4400-945a-177489ddf3b6.jpg"
    },
    story: "روایتی است از یک انتخاب؛ انتخابِ عشق یا وِجدان!",
    url: "https://www.salamcinama.ir/movie/137/چاقی",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 3728,
    uid: "um0afkupvlq91k0",
    name: "فصل باران های موسمی",
    eng_name: "Rainy Seasons",
    movieType: "honar_tajrobe",
    full_title: "فیلم فصل باران های موسمی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3728/medium_24086c35-52cc-407b-a16c-fdb8ceccbeda.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3728/large_24086c35-52cc-407b-a16c-fdb8ceccbeda.jpg"
    },
    produceYear: 1387,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3728/small_24086c35-52cc-407b-a16c-fdb8ceccbeda.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3728/medium_24086c35-52cc-407b-a16c-fdb8ceccbeda.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3728/large_24086c35-52cc-407b-a16c-fdb8ceccbeda.jpg"
    },
    story:
      "پسر نوجوانی در آستانه ی جدایی پدر ومادرش زندگی جدیدی را تجربه می کند. این زندگی ساده که در تنهایی و پرسه های طولانی می گذرد ، او را ناگزیر به انتخاب های جدید می کند.",
    url: "https://www.salamcinama.ir/movie/um0afkupvlq91k0/فصل-باران-های-موسمی",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 704,
    uid: "704",
    name: "روسری آبی",
    eng_name: "The Blue Veiled",
    movieType: "cinema",
    full_title: "فیلم روسری آبی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/704/medium_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/704/large_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg"
    },
    produceYear: 1373,
    director: "رخشان بنی‌اعتماد",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/704/small_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/704/medium_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/704/large_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg"
    },
    story:
      "«رسول رحمانی» مالک یک مزرعه گوجه‌فرنگی است و کارخانه‌ای هم در کنار آن دارد. او که چند سال پیش همسرش را از دست داده است، تنها زندگی می‌کند. «نوبر کردانی» زنی است که سرپرستی خانواده‌اش را به عهده دارد و ناچار است کار کند. او به همراه چند زن دیگر برای کار در مزرعه انتخاب می‌شود و...",
    url: "https://www.salamcinama.ir/movie/704/روسری-آبی",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 248,
    uid: "248",
    name: "درباره الی",
    eng_name: "About Elly",
    movieType: "cinema",
    full_title: "فیلم درباره الی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/248/medium_9fa2ec80-695a-4b30-ada0-e633b2dcec56.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/248/large_9fa2ec80-695a-4b30-ada0-e633b2dcec56.jpg"
    },
    produceYear: 1387,
    director: "اصغر فرهادی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/248/small_9fa2ec80-695a-4b30-ada0-e633b2dcec56.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/248/medium_9fa2ec80-695a-4b30-ada0-e633b2dcec56.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/248/large_9fa2ec80-695a-4b30-ada0-e633b2dcec56.jpg"
    },
    story: "روایت‌گر زندگی چند خانواده است که برای گذراندن تعطیلات به شمال کشور سفر کرده‌اند.",
    url: "https://www.salamcinama.ir/movie/248/درباره-الی",
    state: "confirmed",
    genre: []
  },
  {
    id: 442,
    uid: "442",
    name: "جاودانگی",
    eng_name: "immortality",
    movieType: "honar_tajrobe",
    full_title: "فیلم جاودانگی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/442/medium_ed9d86c5-c45f-4131-b1db-ad143a5e1b59.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/442/large_ed9d86c5-c45f-4131-b1db-ad143a5e1b59.jpg"
    },
    produceYear: 1393,
    director: "مهدی فردقادری",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/442/small_ed9d86c5-c45f-4131-b1db-ad143a5e1b59.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/442/medium_ed9d86c5-c45f-4131-b1db-ad143a5e1b59.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/442/large_ed9d86c5-c45f-4131-b1db-ad143a5e1b59.jpg"
    },
    story:
      "این فیلم به قصه زندگی شش خانواده در قطار، در شبی بارانی می‌پردازد، که این قصه‌ها با جابه جایی زمان روایت می‌شود.",
    url: "https://www.salamcinama.ir/movie/442/جاودانگی",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 233,
    uid: "233",
    name: "یتیم خانه ایران",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم یتیم خانه ایران",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/233/medium_e8b89547-1c05-4836-b7b8-8fdd2b90bddb.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/233/large_e8b89547-1c05-4836-b7b8-8fdd2b90bddb.jpg"
    },
    produceYear: 1394,
    director: "ابوالقاسم طالبی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/233/small_e8b89547-1c05-4836-b7b8-8fdd2b90bddb.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/233/medium_e8b89547-1c05-4836-b7b8-8fdd2b90bddb.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/233/large_e8b89547-1c05-4836-b7b8-8fdd2b90bddb.jpg"
    },
    story:
      "داستان قحطی و خشکسالی عظیم ایران که در سال‌های 1295 تا 1297 و در کوران جنگ اول جهانی و در زمان اشغال ایران به دست قوای بیگانه روس و انگلیس رخ داد را روایت می کند.",
    url: "https://www.salamcinama.ir/movie/233/یتیم-خانه-ایران",
    state: "confirmed",
    genre: []
  }
];
const social = [
  {
    id: 6962,
    uid: "q4LX",
    name: "سوم شخص غایب",
    eng_name: "Absent Third Person",
    movieType: "cinema",
    full_title: "فیلم سوم شخص غایب",
    produceYear: 1397,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6962/small_52d7cced-3d66-44e6-9771-e27a0934f237.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6962/medium_52d7cced-3d66-44e6-9771-e27a0934f237.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6962/large_52d7cced-3d66-44e6-9771-e27a0934f237.jpg"
    },
    story:
      "«سوم شخص غایب» فیلمی است که در فضای آموزشی و تربیتی ساخته شده و از زاویه دید کودکان مسائل بزرگ و مهمی همچون رفاقت، انسانیت و مهرورزی را بیان می کند.",
    url: "https://www.salamcinama.ir/movie/q4LX/سوم-شخص-غایب",
    state: "confirmed",
    genre: [
      {
        id: 22,
        name: "خانوادگی"
      }
    ]
  },
  {
    id: 5566,
    uid: "XAyN",
    name: "اسکی باز",
    eng_name: "The Skier",
    movieType: "cinema",
    full_title: "فیلم اسکی باز",
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5566/small_56889a07-15b4-4dd7-a0ad-ce7c12fbe3e7.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5566/medium_56889a07-15b4-4dd7-a0ad-ce7c12fbe3e7.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5566/large_56889a07-15b4-4dd7-a0ad-ce7c12fbe3e7.jpg"
    },
    story:
      "بز کوهی که به روستاه پناه آورده، قرار است قربانی یک رسم محلی شود، جولی پسربچه‌ای است که تا عصر، قبل از مسابقه اسکی، فرصت دارد او را  نجات دهد.",
    url: "https://www.salamcinama.ir/movie/XAyN/اسکی-باز",
    state: "confirmed",
    genre: []
  },
  {
    id: 4459,
    uid: "lvg4",
    name: "رقص پا",
    eng_name: "Foot Work",
    movieType: "cinema",
    full_title: "فیلم رقص پا",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4459/medium_357aa97d-73e8-4136-98e2-c147f48bd0d8.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4459/large_357aa97d-73e8-4136-98e2-c147f48bd0d8.jpg"
    },
    produceYear: null,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4459/small_357aa97d-73e8-4136-98e2-c147f48bd0d8.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4459/medium_357aa97d-73e8-4136-98e2-c147f48bd0d8.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4459/large_357aa97d-73e8-4136-98e2-c147f48bd0d8.jpg"
    },
    story:
      "محمدعلی وندی بازیگر، فیلمساز و معلم تئاتر، ناامید از مهاجرت به زادگاهش تهران بازگشته تا به همراه همسرش میترا صدری، نقاش، زندگی تازه ای را شروع کنند اما جستجو برای پیدا کردن خانه ای مناسب و کنجی آرام غیرممکن به نظر می رسد...",
    url: "https://www.salamcinama.ir/movie/lvg4/رقص-پا",
    state: "confirmed",
    genre: []
  },
  {
    id: 6726,
    uid: "Mejy",
    name: "پلکان مرگ",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم پلکان مرگ",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6726/medium_64182810-25fa-48f6-927b-dd4234a1578c.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6726/large_64182810-25fa-48f6-927b-dd4234a1578c.jpg"
    },
    produceYear: 1394,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6726/small_64182810-25fa-48f6-927b-dd4234a1578c.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6726/medium_64182810-25fa-48f6-927b-dd4234a1578c.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6726/large_64182810-25fa-48f6-927b-dd4234a1578c.jpg"
    },
    story:
      "کشته شدن جمشید، پیمان افسر آگاهی را با چالشی جدی روبرو می کند زیرا متهم ردیف اول این قتل نامزد او مهتاب است . پیمان میان عشق و وظیفه باید یکی را انتخاب کند.",
    url: "https://www.salamcinama.ir/movie/Mejy/پلکان-مرگ",
    state: "confirmed",
    genre: []
  },
  {
    id: 3761,
    uid: "acfafkv23ywb1k0",
    name: "پریناز",
    eng_name: "Parinaz",
    movieType: "honar_tajrobe",
    full_title: "فیلم پریناز",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3761/medium_88909c24-027f-4093-b7c2-d402ca4bfd8c.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3761/large_88909c24-027f-4093-b7c2-d402ca4bfd8c.jpg"
    },
    produceYear: 1389,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3761/small_88909c24-027f-4093-b7c2-d402ca4bfd8c.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3761/medium_88909c24-027f-4093-b7c2-d402ca4bfd8c.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3761/large_88909c24-027f-4093-b7c2-d402ca4bfd8c.jpg"
    },
    story:
      "دختر بچه‌ای مادر خود را از دست داده و قرار است از سوی خانواده مادری‌ نگهداری شود، اما در خانواده جدید ماجراهای خارق‌العاده‌ای برای وی رخ می‌دهد که زندگی خانواده را با چالش‌هایی روبرو می‌کند.",
    url: "https://www.salamcinama.ir/movie/acfafkv23ywb1k0/پریناز",
    state: "confirmed",
    genre: []
  },
  {
    id: 3689,
    uid: "7xgafkucdjvr1k0",
    name: "هفت و پنج دقیقه",
    eng_name: "",
    movieType: "honar_tajrobe",
    full_title: "فیلم هفت و پنج دقیقه",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3689/medium_527dd38d-8a68-47a0-8ca6-e9ceb790cb80.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3689/large_527dd38d-8a68-47a0-8ca6-e9ceb790cb80.jpg"
    },
    produceYear: 1387,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3689/small_527dd38d-8a68-47a0-8ca6-e9ceb790cb80.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3689/medium_527dd38d-8a68-47a0-8ca6-e9ceb790cb80.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3689/large_527dd38d-8a68-47a0-8ca6-e9ceb790cb80.jpg"
    },
    story:
      "سه زن در شهر کوچکی در فرانسه، هر کدام در مواجهه با جامعه و خانواده خود دست به انتخاب‎هایی می‌زنند که زندگی آن‎ها را تحت الشعاع قرار می‎دهد.",
    url: "https://www.salamcinama.ir/movie/7xgafkucdjvr1k0/هفت-و-پنج-دقیقه",
    state: "confirmed",
    genre: []
  },
  {
    id: 4247,
    uid: "R5dj",
    name: "کبریت سوخته",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم کبریت سوخته",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4247/medium_184d4e4a-ecff-4a3e-af94-737dc58d2336.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4247/large_184d4e4a-ecff-4a3e-af94-737dc58d2336.jpg"
    },
    produceYear: 1391,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4247/small_184d4e4a-ecff-4a3e-af94-737dc58d2336.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4247/medium_184d4e4a-ecff-4a3e-af94-737dc58d2336.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4247/large_184d4e4a-ecff-4a3e-af94-737dc58d2336.jpg"
    },
    story:
      "کاوه با عابر پیاده‌ای تصادف می‌کند و به زندان می‌رود. او به طور غیرمنتظره‌ای توسط پدر همسرش حاجی توسلی با قید ضمانت آزاد می‌شود. کاوه بعد از آزادی متوجه می شود برادرش کشته شده و...",
    url: "https://www.salamcinama.ir/movie/R5dj/کبریت-سوخته",
    state: "confirmed",
    genre: []
  },
  {
    id: 777,
    uid: "777",
    name: "رفقای خوب",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم رفقای خوب",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/777/medium_e650dfc2-cf59-492b-9e90-fd4c40b975bd.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/777/large_e650dfc2-cf59-492b-9e90-fd4c40b975bd.jpg"
    },
    produceYear: 1394,
    director: "مجید قاری‌زاده",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/777/small_e650dfc2-cf59-492b-9e90-fd4c40b975bd.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/777/medium_e650dfc2-cf59-492b-9e90-fd4c40b975bd.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/777/large_e650dfc2-cf59-492b-9e90-fd4c40b975bd.jpg"
    },
    story:
      "داستان زندگی یک بازیگر پیشکسوت را روایت می کند که سالهاست جلوی دوربین نرفته است و حالا که بناست مراسم تجلیلی برایش برگزار شود هم هیچ کس از محل زندگیش خبر ندارد. رفقای قدیمی این بازیگر درصدد برمی‌آیند تا با کمک نوه اش، او را پیدا کنند…",
    url: "https://www.salamcinama.ir/movie/777/رفقای-خوب",
    state: "confirmed",
    genre: []
  },
  {
    id: 991,
    uid: "991",
    name: "خرگیوش",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم خرگیوش",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/991/medium_2458b474-1914-46b2-a60c-d4e8c7efc5f3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/991/large_2458b474-1914-46b2-a60c-d4e8c7efc5f3.jpg"
    },
    produceYear: 1395,
    director: "مانی باغبانی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/991/small_2458b474-1914-46b2-a60c-d4e8c7efc5f3.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/991/medium_2458b474-1914-46b2-a60c-d4e8c7efc5f3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/991/large_2458b474-1914-46b2-a60c-d4e8c7efc5f3.jpg"
    },
    story:
      "فیلم خرگیوش دربارهٔ مردی به نام آرش (بابک حمیدیان) است. پدر آرش بیمار است  و برای همین به خارج از کشور رفته‌است. دوستان او بابک (سیامک انصاری) و بهنود (جواد عزتی) برای اینکه او  افسرده نشود قرص‌های شادی مصرف می‌کنند و ...",
    url: "https://www.salamcinama.ir/movie/991/خرگیوش",
    state: "confirmed",
    genre: []
  },
  {
    id: 5698,
    uid: "ZRY1",
    name: "فرشاد آقای گل",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم فرشاد آقای گل",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/medium_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/large_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg"
    },
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/small_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/medium_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5698/large_73f2d40e-f792-4c1a-a9b9-437a757bc74d.jpg"
    },
    story:
      "عکاسی علاقمند به فوتبال به مرور شیفتۀ هنرنماییِ پسرکی با استعداد به نامِ فرشاد پیوس می شود. از دریچه نگاه این عکاس سفری به دلِ تاریخ با تصاویری بدیع و دیده نشده خواهیم داشت، مروری می کنیم بر زندگی بهترین گلزن تاریخِ فوتبال باشگاهی ایران و هنرنمایی کسی که عناوینِ آقای گلی را یکی پس از دیگری به تصاحب در می آورد را با حضور ستارگان تاریخ فوتبال ایران به نظاره می نشینیم.",
    url: "https://www.salamcinama.ir/movie/ZRY1/فرشاد-آقای-گل",
    state: "confirmed",
    genre: []
  },
  {
    id: 737,
    uid: "737",
    name: "اسرافیل",
    eng_name: "israfil",
    movieType: "cinema",
    full_title: "فیلم اسرافیل",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/737/medium_4ae45e6a-9162-42c5-ad8a-556c969319c4.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/737/large_4ae45e6a-9162-42c5-ad8a-556c969319c4.jpg"
    },
    produceYear: 1395,
    director: "آیدا پناهنده",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/737/small_4ae45e6a-9162-42c5-ad8a-556c969319c4.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/737/medium_4ae45e6a-9162-42c5-ad8a-556c969319c4.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/737/large_4ae45e6a-9162-42c5-ad8a-556c969319c4.jpg"
    },
    story:
      "بیست سال از ماجرای عاشقانه اما ناکام ماهی و بهروز می گذرد. بهروز  بازگشته است تا با سارا، دختری جوان ازدواج کند اما عشق قدیمی دوباره زنده می شود...",
    url: "https://www.salamcinama.ir/movie/737/اسرافیل",
    state: "confirmed",
    genre: []
  },
  {
    id: 4895,
    uid: "d9vq",
    name: "اعتراف",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم اعتراف",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4895/medium_ecc84618-3007-4dc8-b416-a5266538ef65.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4895/large_ecc84618-3007-4dc8-b416-a5266538ef65.jpg"
    },
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4895/small_ecc84618-3007-4dc8-b416-a5266538ef65.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4895/medium_ecc84618-3007-4dc8-b416-a5266538ef65.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4895/large_ecc84618-3007-4dc8-b416-a5266538ef65.jpg"
    },
    story:
      "داستان اعتراف برگرفته از قصه‌ی اصلی نمایش اعتراف برد میرمن نویسنده و کارگردان مطرح آمریکایی است که شهاب حسینی این قصه را بازسازی کرده است.",
    url: "https://www.salamcinama.ir/movie/d9vq/اعتراف",
    state: "confirmed",
    genre: []
  },
  {
    id: 811,
    uid: "811",
    name: "هاری",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم هاری",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/811/medium_b9fbf9a7-f232-4359-a9df-116c10c8c3af.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/811/large_b9fbf9a7-f232-4359-a9df-116c10c8c3af.jpg"
    },
    produceYear: 1393,
    director: "امیر احمد انصاری",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/811/small_b9fbf9a7-f232-4359-a9df-116c10c8c3af.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/811/medium_b9fbf9a7-f232-4359-a9df-116c10c8c3af.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/811/large_b9fbf9a7-f232-4359-a9df-116c10c8c3af.jpg"
    },
    story:
      "من خطر کردم که یا همه چیز درست شه یا هیچی! هر کاری می‌کنم واسه خاطر بچمون که تو خانواده‌ای که من بزرگ شدم، بزرگ نشه",
    url: "https://www.salamcinama.ir/movie/811/هاری",
    state: "confirmed",
    genre: []
  },
  {
    id: 393,
    uid: "393",
    name: "وقتی برگشتم...",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم وقتی برگشتم...",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/393/medium_a465553e-6925-480e-9e7b-602267629c43.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/393/large_a465553e-6925-480e-9e7b-602267629c43.jpg"
    },
    produceYear: 1394,
    director: "وحید موساییان",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/393/small_a465553e-6925-480e-9e7b-602267629c43.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/393/medium_a465553e-6925-480e-9e7b-602267629c43.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/393/large_a465553e-6925-480e-9e7b-602267629c43.jpg"
    },
    story: "خانه‌ای قشنگ دارد و فرنگ. رویا دارد و واقعیت و عشقی که خانه را سرپا نگه می‌دارد.",
    url: "https://www.salamcinama.ir/movie/393/وقتی-برگشتم-",
    state: "confirmed",
    genre: []
  },
  {
    id: 402,
    uid: "402",
    name: "ربوده شده",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم ربوده شده",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/402/medium_7c1e22da-c0cc-4f7a-bd7d-5cc1c1d05fd7.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/402/large_7c1e22da-c0cc-4f7a-bd7d-5cc1c1d05fd7.jpg"
    },
    produceYear: 1394,
    director: "بيژن ميرباقری",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/402/small_7c1e22da-c0cc-4f7a-bd7d-5cc1c1d05fd7.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/402/medium_7c1e22da-c0cc-4f7a-bd7d-5cc1c1d05fd7.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/402/large_7c1e22da-c0cc-4f7a-bd7d-5cc1c1d05fd7.jpg"
    },
    story:
      "داستان زندگی روج جوانی(نیکی کریمی و روزبه بمانی) را روایت می‌کند که در آستانه مهاجرت از ایران، درگیر مشکلاتی می‌شوند که حوادث فیلم را رقم می‌زند.",
    url: "https://www.salamcinama.ir/movie/402/ربوده-شده",
    state: "confirmed",
    genre: []
  },
  {
    id: 3752,
    uid: "sg6afkv03t2z1k0",
    name: "بهشت گمشده",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم بهشت گمشده",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3752/medium_b12e4eb6-f944-4719-ac8d-52f8eef17fd2.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3752/large_b12e4eb6-f944-4719-ac8d-52f8eef17fd2.jpg"
    },
    produceYear: 1389,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3752/small_b12e4eb6-f944-4719-ac8d-52f8eef17fd2.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3752/medium_b12e4eb6-f944-4719-ac8d-52f8eef17fd2.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3752/large_b12e4eb6-f944-4719-ac8d-52f8eef17fd2.jpg"
    },
    story: "عیسی برای یافتن راه نجات آیدا، سفری را در قصه ای پرماجرا آغاز می کند ...",
    url: "https://www.salamcinama.ir/movie/sg6afkv03t2z1k0/بهشت-گمشده",
    state: "confirmed",
    genre: []
  },
  {
    id: 4033,
    uid: "bKz0",
    name: "آزاد به قید شرط",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم آزاد به قید شرط",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4033/medium_df04c88f-dd41-4bc5-9513-4609d0e484fd.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4033/large_df04c88f-dd41-4bc5-9513-4609d0e484fd.jpg"
    },
    produceYear: 1395,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4033/small_df04c88f-dd41-4bc5-9513-4609d0e484fd.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4033/medium_df04c88f-dd41-4bc5-9513-4609d0e484fd.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4033/large_df04c88f-dd41-4bc5-9513-4609d0e484fd.jpg"
    },
    story:
      "کمالی مرد 50 ساله که 10 سال زندان بوده است، هم اکنون به طور مشروط از زندان آزاد شده. او تلاش می کند تا رابطه اش با آدم ها منجر به هیچ مشکلی نگردد.",
    url: "https://www.salamcinama.ir/movie/bKz0/آزاد-به-قید-شرط",
    state: "confirmed",
    genre: []
  },
  {
    id: 4711,
    uid: "vYDJ",
    name: "عشق و خیانت",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم عشق و خیانت",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4711/medium_7746085e-b7b1-4001-be50-4954e72c2603.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4711/large_7746085e-b7b1-4001-be50-4954e72c2603.jpg"
    },
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4711/small_7746085e-b7b1-4001-be50-4954e72c2603.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4711/medium_7746085e-b7b1-4001-be50-4954e72c2603.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4711/large_7746085e-b7b1-4001-be50-4954e72c2603.jpg"
    },
    story:
      "داستان مردی به نام رضا است که به زنش خیانت کرده و رابطه ای پنهانی با دختری دارد این امر در زندگی متاهلی او تاثیر گذاشته و باعث شده اتفاقاتی برای او رخ دهد ...",
    url: "https://www.salamcinama.ir/movie/vYDJ/عشق-و-خیانت",
    state: "confirmed",
    genre: []
  },
  {
    id: 3927,
    uid: "2r5afkyeoxb11k0",
    name: "دو عروس",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم دو عروس",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3927/medium_e5f31b05-1c4e-42b5-bfc0-1a432cb02bc3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3927/large_e5f31b05-1c4e-42b5-bfc0-1a432cb02bc3.jpg"
    },
    produceYear: 1394,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3927/small_e5f31b05-1c4e-42b5-bfc0-1a432cb02bc3.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3927/medium_e5f31b05-1c4e-42b5-bfc0-1a432cb02bc3.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3927/large_e5f31b05-1c4e-42b5-bfc0-1a432cb02bc3.jpg"
    },
    story:
      "پونه همسرش اردلان را زیر فشار قرار میداد تا برایش ماشین خارجی بخرد ، اردلان که کارمند ساده ای بیش نیست استطاعت برآورده شدن چنین خواسته ای را ندارد و همین امر موجب می شود تا زندگی آنها دستخوش حوادثی گردیده و تا آستانه جدائی پیش بروند.",
    url: "https://www.salamcinama.ir/movie/2r5afkyeoxb11k0/دو-عروس",
    state: "confirmed",
    genre: []
  },
  {
    id: 3929,
    uid: "2ubafkyiaegc1k0",
    name: "غیرمجاز",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم غیرمجاز",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3929/medium_266b1db2-b390-4b56-8080-09974a354357.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3929/large_266b1db2-b390-4b56-8080-09974a354357.jpg"
    },
    produceYear: 1394,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3929/small_266b1db2-b390-4b56-8080-09974a354357.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3929/medium_266b1db2-b390-4b56-8080-09974a354357.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3929/large_266b1db2-b390-4b56-8080-09974a354357.jpg"
    },
    story:
      "غیر مجاز داستان دخترجوانی است که در پی یافتن نامزد خود به تهران می آید او در این راه با ناملایماتی روبرو می شود که توقع آن را ندارد.",
    url: "https://www.salamcinama.ir/movie/2ubafkyiaegc1k0/غیرمجاز",
    state: "confirmed",
    genre: []
  },
  {
    id: 3995,
    uid: "exoasqi8y3oq1k0",
    name: "شنل",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم شنل",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3995/medium_048bc94f-a446-4002-8c6d-4032eb63daa5.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3995/large_048bc94f-a446-4002-8c6d-4032eb63daa5.jpg"
    },
    produceYear: 1395,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3995/small_048bc94f-a446-4002-8c6d-4032eb63daa5.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3995/medium_048bc94f-a446-4002-8c6d-4032eb63daa5.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3995/large_048bc94f-a446-4002-8c6d-4032eb63daa5.jpg"
    },
    story: "خیلی طول میکشه باور کنی که دیگه منو نمیبینی. شنیدی چی گفتم صحرا؟",
    url: "https://www.salamcinama.ir/movie/exoasqi8y3oq1k0/شنل",
    state: "confirmed",
    genre: []
  },
  {
    id: 3933,
    uid: "11aafkykjvzm1k0",
    name: "ماحی",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم ماحی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3933/medium_d2cdc54d-e770-4ad0-8df6-e1a09b38faef.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3933/large_d2cdc54d-e770-4ad0-8df6-e1a09b38faef.jpg"
    },
    produceYear: 1394,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3933/small_d2cdc54d-e770-4ad0-8df6-e1a09b38faef.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3933/medium_d2cdc54d-e770-4ad0-8df6-e1a09b38faef.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3933/large_d2cdc54d-e770-4ad0-8df6-e1a09b38faef.jpg"
    },
    story:
      "داستان این فیلم که بر اساس اتفاقات واقعی است، بین سال های 91 تا 92 می گذرد که روایتگر شرایط شخصیت زن فیلم به نام «ماحی» است. شرایط وی بازگوکننده معنای کلمه ماحی است که از آن به محو کننده یا نابود کننده یاد می شود.",
    url: "https://www.salamcinama.ir/movie/11aafkykjvzm1k0/ماحی",
    state: "confirmed",
    genre: []
  },
  {
    id: 434,
    uid: "434",
    name: "مالیخولیا",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم مالیخولیا",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/434/medium_05f4f5f6-e82c-4e93-8d24-2667a353e71a.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/434/large_05f4f5f6-e82c-4e93-8d24-2667a353e71a.jpg"
    },
    produceYear: 1394,
    director: "مرتضی آتش ‌زمزم",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/434/small_05f4f5f6-e82c-4e93-8d24-2667a353e71a.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/434/medium_05f4f5f6-e82c-4e93-8d24-2667a353e71a.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/434/large_05f4f5f6-e82c-4e93-8d24-2667a353e71a.jpg"
    },
    story: "داستان این فیلم درباره آدم هایی است که دچار دوگانگی هویتی شده اند…",
    url: "https://www.salamcinama.ir/movie/434/مالیخولیا",
    state: "confirmed",
    genre: []
  },
  {
    id: 408,
    uid: "408",
    name: "نقطه کور",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم نقطه کور",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/408/medium_c8cd63d3-997b-47ae-94d6-ad2ca4aa19dd.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/408/large_c8cd63d3-997b-47ae-94d6-ad2ca4aa19dd.jpg"
    },
    produceYear: 1394,
    director: "مهدی گلستانه",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/408/small_c8cd63d3-997b-47ae-94d6-ad2ca4aa19dd.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/408/medium_c8cd63d3-997b-47ae-94d6-ad2ca4aa19dd.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/408/large_c8cd63d3-997b-47ae-94d6-ad2ca4aa19dd.jpg"
    },
    story:
      "خسرو که 23 روز بر پهنه دریا و به دور از خانواده اش گذرانده است. او با دنیایی از ابهامات فکری که تمام مدت همراه او بوده وارد خانه اش در تهران می شود اما تمامی ذهنیت هایش عینیت یافته و قادر به مقابله با مسائل پیش آمده نیست تا اینکه...",
    url: "https://www.salamcinama.ir/movie/408/نقطه-کور",
    state: "confirmed",
    genre: []
  },
  {
    id: 411,
    uid: "411",
    name: "امتحان نهایی",
    eng_name: "The Final Exam",
    movieType: "cinema",
    full_title: "فیلم امتحان نهایی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/411/medium_62e05d3e-acbe-4f0f-a224-a4a0b4ac4241.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/411/large_62e05d3e-acbe-4f0f-a224-a4a0b4ac4241.jpg"
    },
    produceYear: 1394,
    director: "عادل یراقی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/411/small_62e05d3e-acbe-4f0f-a224-a4a0b4ac4241.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/411/medium_62e05d3e-acbe-4f0f-a224-a4a0b4ac4241.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/411/large_62e05d3e-acbe-4f0f-a224-a4a0b4ac4241.jpg"
    },
    story:
      "داستان یک معلم ریاضی است که برای تدریس خصوصی به خانه شاگردش می‌رود و همین امر باعث آشنایی او با مادر شاگرد می‌شود و ...",
    url: "https://www.salamcinama.ir/movie/411/امتحان-نهایی",
    state: "confirmed",
    genre: []
  },
  {
    id: 392,
    uid: "392",
    name: "افسونگر",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم افسونگر",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/392/medium_ac84bda2-ace8-4486-aaf2-4b262cf30df6.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/392/large_ac84bda2-ace8-4486-aaf2-4b262cf30df6.jpg"
    },
    produceYear: 1394,
    director: "حسین تبریزی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/392/small_ac84bda2-ace8-4486-aaf2-4b262cf30df6.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/392/medium_ac84bda2-ace8-4486-aaf2-4b262cf30df6.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/392/large_ac84bda2-ace8-4486-aaf2-4b262cf30df6.jpg"
    },
    story:
      "روایت انسان و انسان - اعمال و کردار - رویارویی خویش با خویشتن - یافتن خود از بیخودی و گذشتن از ناحق به حق - بودن برای زیستن – زیستن برای عشق",
    url: "https://www.salamcinama.ir/movie/392/افسونگر",
    state: "confirmed",
    genre: []
  }
];

const foreign = [
  {
    id: 5686,
    uid: "yGZb",
    name: "مرد ایرلندی",
    eng_name: "The Irishman",
    movieType: "foreign_cinema",
    full_title: "فیلم مرد ایرلندی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5686/medium_9d576c23-79a8-414f-a568-fbac3ebbd464.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5686/large_9d576c23-79a8-414f-a568-fbac3ebbd464.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5686/small_9d576c23-79a8-414f-a568-fbac3ebbd464.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5686/medium_9d576c23-79a8-414f-a568-fbac3ebbd464.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5686/large_9d576c23-79a8-414f-a568-fbac3ebbd464.jpg"
    },
    story:
      "«فرانک شیرن» ادعا می کند که با دار و دسته بافالینو در ارتباط است و «جیمی هوفا» را کشته است.",
    url: "https://www.salamcinama.ir/movie/yGZb/مرد-ایرلندی",
    state: "confirmed",
    genre: [
      {
        id: 14,
        name: "بیوگرافی"
      },
      {
        id: 10,
        name: "جنایی"
      },
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 5312,
    uid: "pARR",
    name: "جوکر",
    eng_name: "Joker",
    movieType: "foreign_cinema",
    full_title: "فیلم جوکر",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5312/medium_da1a02cc-9924-4799-ab32-19f898b752ad.jpeg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5312/large_da1a02cc-9924-4799-ab32-19f898b752ad.jpeg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5312/small_da1a02cc-9924-4799-ab32-19f898b752ad.jpeg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5312/medium_da1a02cc-9924-4799-ab32-19f898b752ad.jpeg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5312/large_da1a02cc-9924-4799-ab32-19f898b752ad.jpeg"
    },
    story: "یک کمدین ناموفق دیوانه شده و تبدیل به یک قاتل روانی می شود.",
    url: "https://www.salamcinama.ir/movie/pARR/جوکر",
    state: "confirmed",
    genre: [
      {
        id: 10,
        name: "جنایی"
      },
      {
        id: 1,
        name: "درام"
      },
      {
        id: 5,
        name: "هیجانی"
      }
    ]
  },
  {
    id: 4741,
    uid: "K14x",
    name: "فرامحرک",
    eng_name: "Overdrive",
    movieType: "foreign_cinema",
    full_title: "فیلم فرامحرک",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4741/medium_a658b2ba-20dc-4927-b53c-28e41b0ded42.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4741/large_a658b2ba-20dc-4927-b53c-28e41b0ded42.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4741/small_a658b2ba-20dc-4927-b53c-28e41b0ded42.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4741/medium_a658b2ba-20dc-4927-b53c-28e41b0ded42.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4741/large_a658b2ba-20dc-4927-b53c-28e41b0ded42.jpg"
    },
    story:
      "اندرو و گرت، دو برادر هستند که کارشون سرقت ماشین‌های گران قیمت است. آن‌ها تصمیم می گیرند برای اجرایی نقشه جدیدشان به فرانسه بروند و چند ماشین خاص و لوکس را بدزدند…",
    url: "https://www.salamcinama.ir/movie/K14x/فرامحرک",
    state: "confirmed",
    genre: []
  },
  {
    id: 4742,
    uid: "yGR0",
    name: "یاغی ها",
    eng_name: "Renegades",
    movieType: "foreign_cinema",
    full_title: "فیلم یاغی ها",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4742/medium_3e6f8aa2-48c3-4ec2-ab57-f5cdb4aaa195.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4742/large_3e6f8aa2-48c3-4ec2-ab57-f5cdb4aaa195.jpg"
    },
    produceYear: null,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4742/small_3e6f8aa2-48c3-4ec2-ab57-f5cdb4aaa195.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4742/medium_3e6f8aa2-48c3-4ec2-ab57-f5cdb4aaa195.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4742/large_3e6f8aa2-48c3-4ec2-ab57-f5cdb4aaa195.jpg"
    },
    story:
      "یک گروه از نیروی دریایی درحالی که به دنبال گنج در یک دریاچه در صربستان جنگ‌زده هستند، تلاش می‌کنند تا رمز و راز فراموش شده آنجا را هم حل کنند اما...",
    url: "https://www.salamcinama.ir/movie/yGR0/یاغی-ها",
    state: "confirmed",
    genre: []
  }
];
const childTeenager = [
  {
    id: 762,
    uid: "762",
    name: "شکلاتی",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم شکلاتی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/762/medium_5ef753df-67c4-4a7c-b7ce-f76943c10ebd.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/762/large_5ef753df-67c4-4a7c-b7ce-f76943c10ebd.jpg"
    },
    produceYear: 1395,
    director: "سھیل موفق",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/762/small_5ef753df-67c4-4a7c-b7ce-f76943c10ebd.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/762/medium_5ef753df-67c4-4a7c-b7ce-f76943c10ebd.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/762/large_5ef753df-67c4-4a7c-b7ce-f76943c10ebd.jpg"
    },
    story:
      "همه چیز از یک جعبه شکلات شروع شد. یعنی من بچه بودم ...بچه هم که باشیم بازم شکلات عاشقانه ترین مزه دنیاست اما...",
    url: "https://www.salamcinama.ir/movie/762/شکلاتی",
    state: "confirmed",
    genre: []
  }
];
const romance = [
  {
    id: 4753,
    uid: "nwnK",
    name: "خداحافظ دختر شیرازی",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم خداحافظ دختر شیرازی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/medium_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/large_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg"
    },
    produceYear: 1397,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/small_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/medium_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4753/large_df77e1fc-77a0-4517-b077-ac633e0fe879.jpg"
    },
    story:
      "«خداحافظ دخترشیرازی» یک عاشقانه‌ی مفرّح است. یکی از شیراز، یکی از آبادان، هردو در تهران! مجبورند فعلن باهم سر کنند تا محمودِ نارفیق را پیدا کنند.",
    url: "https://www.salamcinama.ir/movie/nwnK/خداحافظ-دختر-شیرازی",
    state: "confirmed",
    genre: [
      {
        id: 4,
        name: "عاشقانه"
      },
      {
        id: 9,
        name: "کمدی"
      }
    ]
  },
  {
    id: 704,
    uid: "704",
    name: "روسری آبی",
    eng_name: "The Blue Veiled",
    movieType: "cinema",
    full_title: "فیلم روسری آبی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/704/medium_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/704/large_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg"
    },
    produceYear: 1373,
    director: "رخشان بنی‌اعتماد",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/704/small_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/704/medium_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/704/large_7c46eaf4-7ebe-4df4-9d11-f77b63193318.jpg"
    },
    story:
      "«رسول رحمانی» مالک یک مزرعه گوجه‌فرنگی است و کارخانه‌ای هم در کنار آن دارد. او که چند سال پیش همسرش را از دست داده است، تنها زندگی می‌کند. «نوبر کردانی» زنی است که سرپرستی خانواده‌اش را به عهده دارد و ناچار است کار کند. او به همراه چند زن دیگر برای کار در مزرعه انتخاب می‌شود و...",
    url: "https://www.salamcinama.ir/movie/704/روسری-آبی",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      }
    ]
  },
  {
    id: 6689,
    uid: "xokd",
    name: "گذر لوطی هاشم",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم گذر لوطی هاشم",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6689/medium_23a4e444-89ba-4756-ba42-6d33a55cab32.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6689/large_23a4e444-89ba-4756-ba42-6d33a55cab32.jpg"
    },
    produceYear: 1397,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6689/small_23a4e444-89ba-4756-ba42-6d33a55cab32.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6689/medium_23a4e444-89ba-4756-ba42-6d33a55cab32.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6689/large_23a4e444-89ba-4756-ba42-6d33a55cab32.jpg"
    },
    story:
      "این نمایش یک روایت عاشقانه است که در اوائل دوره پهلوی اول می‌گذرد و داستان عشق لیلا را روایت می‌کند.",
    url: "https://www.salamcinama.ir/movie/xokd/گذر-لوطی-هاشم",
    state: "confirmed",
    genre: [
      {
        id: 4,
        name: "عاشقانه"
      }
    ]
  },
  {
    id: 4083,
    uid: "JKBw",
    name: "ماهورا",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم ماهورا",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/medium_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/large_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg"
    },
    produceYear: 1395,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/small_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/medium_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/large_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg"
    },
    story:
      "در زندگی تو زنی است با چشمانی شکوهمند که لبانش خوشه انگور و لبخندش موسیقی و گل، اما هرکس بخواهد به او نزدیک شود ناپدید می شود.",
    url: "https://www.salamcinama.ir/movie/JKBw/ماهورا",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      },
      {
        id: 18,
        name: "جنگی"
      }
    ]
  },
  {
    id: 3928,
    uid: "h1zafkyexmn41k0",
    name: "دو لکه ابر",
    eng_name: "Two patches of clouds",
    movieType: "honar_tajrobe",
    full_title: "فیلم دو لکه ابر",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3928/medium_d1c06b68-12f3-4b61-99f5-c7f33696fb6e.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3928/large_d1c06b68-12f3-4b61-99f5-c7f33696fb6e.jpg"
    },
    produceYear: 1394,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3928/small_d1c06b68-12f3-4b61-99f5-c7f33696fb6e.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3928/medium_d1c06b68-12f3-4b61-99f5-c7f33696fb6e.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3928/large_d1c06b68-12f3-4b61-99f5-c7f33696fb6e.jpg"
    },
    story:
      "ابری برسر شهر سایه افکنده است،لکه ابری به دنبال لکه گمشده خود می گردد... کسری به دنبال خواهرزاده گمشده‌اش ، شهر را جست و جو می کند. در همین حال، مروا زخم خورده از زندان آزاد می شود.",
    url: "https://www.salamcinama.ir/movie/h1zafkyexmn41k0/دو-لکه-ابر",
    state: "confirmed",
    genre: [
      {
        id: 4,
        name: "عاشقانه"
      }
    ]
  },
  {
    id: 4530,
    uid: "364N",
    name: "شاید عشق نبود",
    eng_name: "",
    movieType: "honar_tajrobe",
    full_title: "فیلم شاید عشق نبود",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4530/medium_63caca72-ec22-46c6-9a0f-490801fadc2f.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4530/large_63caca72-ec22-46c6-9a0f-490801fadc2f.jpg"
    },
    produceYear: 1395,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4530/small_63caca72-ec22-46c6-9a0f-490801fadc2f.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4530/medium_63caca72-ec22-46c6-9a0f-490801fadc2f.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4530/large_63caca72-ec22-46c6-9a0f-490801fadc2f.jpg"
    },
    story:
      "عروسک‌فروشی در پارک ماجراهایی که بر سر دوست سینماگرش آمده است را برای دختر جوانی بازگو می‎کند. دختر جوان که از عوامل فیلمی است که در آن‌سوی پارک مشغول فیلم‌برداری آن هستند، در انتها به هویت مرد عروسک فروش پی می‌برد.",
    url: "https://www.salamcinama.ir/movie/364N/شاید-عشق-نبود",
    state: "confirmed",
    genre: []
  },
  {
    id: 4644,
    uid: "DKkv",
    name: "نازلی",
    eng_name: "Nazli",
    movieType: "cinema",
    full_title: "فیلم نازلی",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4644/medium_e1410b0a-daa5-432f-bb7a-f7b2d7a2562d.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4644/large_e1410b0a-daa5-432f-bb7a-f7b2d7a2562d.jpg"
    },
    produceYear: 1395,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4644/small_e1410b0a-daa5-432f-bb7a-f7b2d7a2562d.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4644/medium_e1410b0a-daa5-432f-bb7a-f7b2d7a2562d.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4644/large_e1410b0a-daa5-432f-bb7a-f7b2d7a2562d.jpg"
    },
    story:
      "داستان شیرینی از یک روستای دلگرم و سنت پذیری و عشق دییرینه و کهنه ای که به علت اتفاقاتی، دوباره نو و تکرار می شود، است.",
    url: "https://www.salamcinama.ir/movie/DKkv/نازلی",
    state: "confirmed",
    genre: []
  }
];
const actionCriminal = [
  {
    id: 6096,
    uid: "eZjY",
    name: "شانتاژ",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم شانتاژ",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6096/medium_d53f8a74-1b67-4710-9863-4eff6d83114a.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6096/large_d53f8a74-1b67-4710-9863-4eff6d83114a.jpg"
    },
    produceYear: 1397,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6096/small_d53f8a74-1b67-4710-9863-4eff6d83114a.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6096/medium_d53f8a74-1b67-4710-9863-4eff6d83114a.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6096/large_d53f8a74-1b67-4710-9863-4eff6d83114a.jpg"
    },
    story:
      "داستان این اثر درباره مردی سرشناس و ثروتمند است که همسرش توسط یک گروه تبهکار مورد اذیت و آزار قرار می گیرد و آن گروه با هدف باج گیری و اخاذی از طریق فضای مجازی وارد زندگی این زوج شده و زندگی آنها را به مخاطره می اندازند ؛ اما ...",
    url: "https://www.salamcinama.ir/movie/eZjY/شانتاژ",
    state: "confirmed",
    genre: [
      {
        id: 3,
        name: "اکشن"
      },
      {
        id: 10,
        name: "جنایی"
      }
    ]
  }
];
const war = [
  {
    id: 6964,
    uid: "YPjJ",
    name: "روزگار مجنون",
    eng_name: null,
    movieType: "cinema",
    full_title: "فیلم روزگار مجنون",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6964/medium_8bf81802-7805-4a84-8069-e29eff608f8a.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6964/large_8bf81802-7805-4a84-8069-e29eff608f8a.jpg"
    },
    produceYear: 1396,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/6964/small_8bf81802-7805-4a84-8069-e29eff608f8a.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/6964/medium_8bf81802-7805-4a84-8069-e29eff608f8a.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6964/large_8bf81802-7805-4a84-8069-e29eff608f8a.jpg"
    },
    story:
      "در زمان حمله منافقین، ستاره پرستار یکی از بیمارستان های صحرایی غرب کشور است. او به همراه مجروحان و مردم محلی، در حال فرار هستند که خبر سقوط اسلام آباد را می شنوند. آنها به یک قلعه پناه می برند و این تازه شروع ماجراست…",
    url: "https://www.salamcinama.ir/movie/YPjJ/روزگار-مجنون",
    state: "confirmed",
    genre: []
  },
  {
    id: 5354,
    uid: "ZRjm",
    name: "23 نفر",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم 23 نفر",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5354/medium_77d1f89b-d059-483b-bf72-7a0d66460dea.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5354/large_77d1f89b-d059-483b-bf72-7a0d66460dea.jpg"
    },
    produceYear: 1397,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/5354/small_77d1f89b-d059-483b-bf72-7a0d66460dea.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/5354/medium_77d1f89b-d059-483b-bf72-7a0d66460dea.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/5354/large_77d1f89b-d059-483b-bf72-7a0d66460dea.jpg"
    },
    story:
      "روایت گروهی از رزمندگان نوجوان ایرانی است که در جریان جنگ ایران و عراق در سال 1361 به اسارت نیروهای عراقی درآمدند.",
    url: "https://www.salamcinama.ir/movie/ZRjm/23-نفر",
    state: "confirmed",
    genre: [
      {
        id: 18,
        name: "جنگی"
      },
      {
        id: 16,
        name: "تاریخی"
      }
    ]
  },
  {
    id: 4083,
    uid: "JKBw",
    name: "ماهورا",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم ماهورا",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/medium_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/large_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg"
    },
    produceYear: 1395,
    director: "",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/small_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/medium_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/4083/large_eec15f75-2cb4-499c-8105-a7eb83efa893.jpg"
    },
    story:
      "در زندگی تو زنی است با چشمانی شکوهمند که لبانش خوشه انگور و لبخندش موسیقی و گل، اما هرکس بخواهد به او نزدیک شود ناپدید می شود.",
    url: "https://www.salamcinama.ir/movie/JKBw/ماهورا",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      },
      {
        id: 18,
        name: "جنگی"
      }
    ]
  },
  {
    id: 3854,
    uid: "u4oafkwdj6yz1k0",
    name: "578 روز انتظار",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم 578 روز انتظار",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3854/medium_8dcb90ae-95f7-4dbd-a4d1-544028cf43e5.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3854/large_8dcb90ae-95f7-4dbd-a4d1-544028cf43e5.jpg"
    },
    produceYear: 1390,
    director: null,
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/3854/small_8dcb90ae-95f7-4dbd-a4d1-544028cf43e5.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/3854/medium_8dcb90ae-95f7-4dbd-a4d1-544028cf43e5.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/3854/large_8dcb90ae-95f7-4dbd-a4d1-544028cf43e5.jpg"
    },
    story:
      "در رابطه با عملیات بزرگ بیت المقدس و فتح خرمشهر است؛ در این فیلم قبل و زمان آزادسازی به تصویر کشیده شده است.",
    url: "https://www.salamcinama.ir/movie/u4oafkwdj6yz1k0/578-روز-انتظار",
    state: "confirmed",
    genre: [
      {
        id: 1,
        name: "درام"
      },
      {
        id: 18,
        name: "جنگی"
      }
    ]
  },
  {
    id: 578,
    uid: "578",
    name: "ویلایی‌ ها",
    eng_name: "",
    movieType: "cinema",
    full_title: "فیلم ویلایی‌ ها",
    old_poster_which_should_remove: {
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/578/medium_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/578/large_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg"
    },
    produceYear: 1395,
    director: "منیره قیدی",
    is_series: false,
    poster: {
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/578/small_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/578/medium_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg",
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/578/large_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg"
    },
    story:
      "در سال های جنگ، خانواده برخی رزمندگان در مجتمع های ویلایی در حوالی خط مقدم ساکن شده اند...",
    url: "https://www.salamcinama.ir/movie/578/ویلایی-ها",
    state: "confirmed",
    genre: []
  }
  // {
  //   id: 578,
  //   uid: "578",
  //   name: "ویلایی‌ ها",
  //   eng_name: "",
  //   movieType: "cinema",
  //   full_title: "فیلم ویلایی‌ ها",
  //   old_poster_which_should_remove: {
  //     medium:
  //       "https://api.salamcinama.ir/uploads/movie/poster/578/medium_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg",
  //     large:
  //       "https://api.salamcinama.ir/uploads/movie/poster/578/large_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg"
  //   },
  //   produceYear: 1395,
  //   director: "منیره قیدی",
  //   is_series: false,
  //   poster: {
  //     small:
  //       "https://api.salamcinama.ir/uploads/movie/poster/578/small_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg",
  //     medium:
  //       "https://api.salamcinama.ir/uploads/movie/poster/578/medium_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg",
  //     large:
  //       "https://api.salamcinama.ir/uploads/movie/poster/578/large_f8ee5782-f058-46de-bb3c-b32ffbfe9b32.jpg"
  //   },
  //   story:
  //     "در سال های جنگ، خانواده برخی رزمندگان در مجتمع های ویلایی در حوالی خط مقدم ساکن شده اند...",
  //   url: "https://www.salamcinama.ir/movie/578/ویلایی-ها",
  //   state: "confirmed",
  //   genre: []
  // },
];
export { animation, comedy, series, drum, social, romance, war, foreign, documentary };
