const ghahvetalkhInfo = [
  {
    id: 6968,
    uid: "o47R",
    name: "قهوه تلخ - قسمت 15",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6968/large_2273086b-1c4e-45e2-b58f-4c5902a5dc1d.jpg"
    }
  },
  {
    id: 6959,
    uid: "lqL8",
    name: "قهوه تلخ - قسمت 14",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6959/large_bf3afc84-9241-4222-87a1-cece5ffe73a2.jpg"
    }
  },
  {
    id: 6958,
    uid: "mpLw",
    name: "قهوه تلخ - قسمت 13",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6958/large_3931c579-66a6-43f0-b560-fa50ae33c33b.jpg"
    }
  },
  {
    id: 6957,
    uid: "E0XD",
    name: "قهوه تلخ - قسمت 12",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6957/large_a38f749f-eee9-4932-9a0f-b751623c71a7.jpg"
    }
  },
  {
    id: 6956,
    uid: "p4L6",
    name: "قهوه تلخ - قسمت 11",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6956/large_6b98d43f-ac5c-457b-a3d0-69a253d3677d.jpg"
    }
  },
  {
    id: 6955,
    uid: "vJLJ",
    name: "قهوه تلخ - قسمت 10",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6955/large_d7b94daa-be6e-4620-9cce-382e4a765be1.jpg"
    }
  },
  {
    id: 6954,
    uid: "Zk8R",
    name: "قهوه تلخ - قسمت 9",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6954/large_8d54ab67-582a-4f8e-9865-f813e87c7bd9.jpg"
    }
  },
  {
    id: 6934,
    uid: "LJnR",
    name: "قهوه تلخ - قسمت 8",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6934/large_fa6406b0-7949-403d-ab07-fd5a75cdbfa3.jpg"
    }
  },
  {
    id: 6933,
    uid: "bz11",
    name: "قهوه تلخ - قسمت 7",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6933/large_558de1e2-a8a5-407c-8edf-896468b0c8c6.jpg"
    }
  },
  {
    id: 6932,
    uid: "zbB8",
    name: "قهوه تلخ - قسمت 6",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6932/large_b08418ae-ebab-4c40-bf20-d834c134fc45.jpg"
    }
  },
  {
    id: 6931,
    uid: "r6XP",
    name: "قهوه تلخ - قسمت 5",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6931/large_9d0ee4f2-c385-4645-a8d1-651863957d9e.jpg"
    }
  },
  {
    id: 6930,
    uid: "3Jnv",
    name: "قهوه تلخ - قسمت 4",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6930/large_774a3536-0579-423e-acb6-943dc94a4156.jpg"
    }
  },
  {
    id: 6929,
    uid: "6Lp9",
    name: "قهوه تلخ - قسمت 3",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6929/large_fbd7d019-aba4-4488-a4b3-f45a3b7a2eb4.jpg"
    }
  },
  {
    id: 6928,
    uid: "4jZA",
    name: "قهوه تلخ - قسمت 2",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6928/large_c4ea2f43-9681-4336-946e-f7a0a33e6cd0.jpg"
    }
  },
  {
    id: 6927,
    uid: "AEQ6",
    name: "قهوه تلخ - قسمت 1",

    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6927/large_f69314bb-e7aa-4a80-9264-f5081c70ab06.jpg"
    }
  }
];

const shameIraniInfo = [
  {
    id: 6972,
    uid: "4jBA",
    name: "شام ایرانی 10 - میزبان شب دوم: شبنم قلی خانی",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6972/large_2b87dfa1-275c-4011-939b-397fce8779aa.jpg"
    }
  },
  {
    id: 6971,
    uid: "AEq6",
    name: "شام ایرانی 10 - میزبان شب اول: سیما تیرانداز",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6971/large_c58b6037-1a2a-49e9-9510-69dd828c78ba.jpg"
    }
  },
  {
    id: 6963,
    uid: "k8Lr",
    name: "شام ایرانی - میزبان شب چهارم: امره تتیکل",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6972/large_2b87dfa1-275c-4011-939b-397fce8779aa.jpg"
    }
  },
  {
    id: 6960,
    uid: "j4Lk",
    name: "شام ایرانی - میزبان شب سوم: پوریا پورسرخ",

    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6960/large_f2a2473f-275b-48f1-be55-01d8c17f7f9c.jpg"
    }
  },
  {
    id: 6922,
    uid: "Xx51",
    name: "شام ایرانی - میزبان شب دوم: نیما شاهرخ شاهی",

    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6922/large_cf94141e-a982-4e77-bfc9-40c267c1bccc.jpg"
    }
  },
  {
    id: 6921,
    uid: "59vR",
    name: "شام ایرانی - میزبان شب اول: سامان گوران",

    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6921/large_b889b7d9-1ac9-4f09-81b3-402ce472813a.jpg"
    }
  }
];

const roozeBaziInfo = [
  {
    id: 6952,
    uid: "eZLO",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت هفتم: طوفان هجده روزه",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6952/large_c54c7566-3fd5-4b5b-95f8-d64df1e93d50.jpeg"
    }
  },
  {
    id: 6951,
    uid: "dEe4",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت ششم: شب اروپایی",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6951/large_99d8a949-12c1-4402-84c2-0f0fc62b4453.jpeg"
    }
  },
  {
    id: 6950,
    uid: "1Edm",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت پنجم: امتیاز نهایی",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6950/large_3e0f5107-5d66-47a1-a45f-7a09bb826dae.jpeg"
    }
  },
  {
    id: 6949,
    uid: "BXog",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت چهارم: همسایه ها",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6949/large_1570c479-0997-439e-9b0e-52276d71d037.jpeg"
    }
  },
  {
    id: 6926,
    uid: "MeZ3",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت سوم: طلسم",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6926/large_df168a22-cd53-4747-904b-9295b117b834.jpeg"
    }
  },
  {
    id: 6925,
    uid: "GpOk",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت دوم: محدوده‌ی دشمن",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6925/large_2b376a6e-ed4b-451d-893e-7bd5cc5489bf.jpeg"
    }
  },
  {
    id: 6924,
    uid: "o4kR",
    name: "روز بازی با روایت عادل فردوسی پور – قسمت اول: فوتبال کلاسیک",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6924/large_1c70319d-185f-47c5-aede-7832c86374fb.jpg"
    }
  }
];
const hamgonahInfo = [
  {
    id: 6896,
    uid: "eZY7",
    name: "هم گناه - قسمت 1",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6896/large_0ea86619-f71d-41e1-9290-7a705c063c2a.jpg"
    }
  },
  {
    id: 6903,
    uid: "RMx4",
    name: "هم گناه - قسمت 2",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6903/large_4ae78793-4308-4b51-8e83-1f73e830801d.jpg"
    }
  },
  {
    id: 6911,
    uid: "vJxJ",
    name: "هم گناه - قسمت 3",
    poster: {
      large:
        "https://api.salamcinama.ir//uploads/movie/poster/6911/large_835ffcfb-294f-4d55-a1c4-26442a25addf.jpg"
    }
  },
  {
    id: 6919,
    uid: "k8lr",
    name: "هم گناه - قسمت 4",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6919/large_89b15805-0967-47e9-8b33-f2f95276ae8e.jpg"
    }
  },
  {
    id: 6965,
    uid: "591R",
    name: "هم گناه - قسمت 5",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6965/large_b0ea612b-5ddf-4c02-8008-67b7dde24314.jpg"
    }
  },
  {
    id: 6979,
    uid: "0pxJ",
    name: "هم گناه - قسمت 6",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6979/large_be623c58-3520-4345-9f52-9d0aa76ce177.jpg"
    }
  },
  {
    id: 6992,
    uid: "Oe1l",
    name: "هم گناه - قسمت 7",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6979/large_be623c58-3520-4345-9f52-9d0aa76ce177.jpg"
    }
  },
  {
    id: 6999,
    uid: "vJQJ",
    name: "هم گناه - قسمت 8",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6999/large_97224c2a-b16b-4ef1-9e27-a76b25a29dfc.jpg"
    }
  }
];

const khabzadeInfo = [
  {
    id: 7004,
    uid: "OevO",
    name: "خواب زده - قسمت 13",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/7004/large_3e9d8b80-ae1b-46ff-9e55-2835be188df7.jpg"
    }
  },
  {
    id: 6995,
    uid: "dEL4",
    name: "خواب زده - قسمت 12",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6995/large_a0a3681e-36ba-4bac-890d-1ea1fb62446a.jpeg"
    }
  },
  {
    id: 6987,
    uid: "NMBw",
    name: "خواب زده - قسمت 11",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6987/large_6c4b0f27-86f8-4568-9294-0368b30c7e76.jpg"
    }
  },
  {
    id: 6966,
    uid: "Xxl1",
    name: "خواب زده - قسمت 10",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6966/large_bd5b3b82-09b9-46d8-b342-d02cdbc9d148.jpeg"
    }
  },
  {
    id: "Oepl",
    uid: "Oepl",
    name: "خواب زده - قسمت 9",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6948/large_d0aa0b3e-e197-4ad7-966f-07f49944bca8.jpg"
    }
  },
  {
    id: "YPXJ",
    uid: "YPXJ",
    name: "خواب زده - قسمت 8",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6920/large_81c66a85-47bc-4666-9d72-8f44d1e1715d.jpg"
    }
  },
  {
    id: 6920,
    uid: "npqK",
    name: "خواب زده - قسمت 7",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6909/large_27b6a32e-e961-4580-9799-1f123766cb36.jpg"
    }
  },
  {
    id: 6906,
    uid: "1Ejm",
    name: "خواب زده - قسمت 6",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6906/large_c4f3dd92-9a2d-4ea7-ae4c-5dd0f552f463.jpg"
    }
  },
  {
    id: "vJEk",
    uid: "vJEk",
    name: "خواب زده - قسمت 5",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6899/large_ab764199-15f6-4469-a885-426634ec98e8.jpg"
    }
  },
  {
    id: 6890,
    uid: "PKQd",
    name: "خواب زده - قسمت 4",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6890/large_bac6f6f0-aef1-4332-9cfd-e1570a38d86b.jpeg"
    }
  },
  {
    id: 6884,
    uid: "9oDA",
    name: "خواب زده - خلاصه قسمت های 1 تا 3 ",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6884/large_11bba915-11df-47e5-baa2-1d5fda252a8c.jpeg"
    }
  },
  {
    id: 6882,
    uid: "76XL",
    name: "خواب زده - قسمت 3",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6882/large_11533a49-3822-4b55-bd5f-c109f0312583.jpg"
    }
  },
  {
    id: "59Nj",
    uid: "59Nj",
    name: "خواب زده - قسمت 2",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6865/large_315d6583-4b74-411f-82ad-570e7e1c7b6f.jpg"
    }
  },
  {
    id: "59Zj",
    uid: "59Zj",
    name: "خواب زده - قسمت 1",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/6821/large_cddec726-dd17-4305-ab23-2e6d97f61a9c.jpg"
    }
  }
];
export { ghahvetalkhInfo, shameIraniInfo, roozeBaziInfo, hamgonahInfo, khabzadeInfo };
