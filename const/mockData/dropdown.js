import {
  payoffUrl,
  payoffReportUrl,
  colleagueReportUrl,
  seriesColleagueSalePanelUrl,
  movieColleagueSalePanelUrl
} from "services/url";
const settlement = [
  {
    link: payoffUrl(),

    title: "درخواست تسویه",
    dropdown: true
  },
  {
    link: payoffReportUrl(),

    title: "گزارش تسویه"
  }
];

const report = [
  {
    link: colleagueReportUrl(),

    title: "گزارش کامل فروش",
    dropdown: true
  },
  {
    link: seriesColleagueSalePanelUrl(),

    title: "درآمد حاصل از سریال‌ها"
  },
  {
    link: movieColleagueSalePanelUrl(),
    title: "درآمد حاصل از فیلم‌ها"
  }
];
export { settlement, report };
