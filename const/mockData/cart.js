const cartInfo = {
  id: 123,
  movie: {
    id: 409,
    uid: "409",
    name: "ابد و یک روز",
    poster: {
      large:
        "https://api.salamcinama.ir/uploads/movie/poster/409/large_28b95ac4-7d36-4dc4-8ba9-55db21da68db.jpg",
      medium:
        "https://api.salamcinama.ir/uploads/movie/poster/409/medium_28b95ac4-7d36-4dc4-8ba9-55db21da68db.jpg",
      small:
        "https://api.salamcinama.ir/uploads/movie/poster/409/small_28b95ac4-7d36-4dc4-8ba9-55db21da68db.jpg"
    }
  },
  purchase: {
    cost: "3000"
  }
};

export { cartInfo };
