/**
 * @type {import('next').NextConfig}
 */
const nextConfig = {
  /* config options here */
  images: {
    domains: [
      "admin.elitland.local",
      "api.salamcinama.local",
      "elitland.local/api",
      "127.0.0.1",
      "www.salamcinama.ir",
      "api.salamcinama.ir",
      "elitland.com/api"
    ]
    // loader: "imgix",
    // path: ""
  },
  experimental: {
    outputStandalone: true
  }
};

module.exports = nextConfig;
