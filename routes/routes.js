const nextRoutes = require("next-routes");
const routes = (module.exports = nextRoutes());

// series
routes.add("series", "/series/:uid/:colleague_uid*", "series");

// download
routes.add("download-details", "/download/:uid/:colleague_uid*", "download-details");
routes.add("download-details-f", "/downloadf/:uid/:fquality/:colleague_uid*", "download-details-f");
routes.add("hamgonah", "/hamgonah/:colleague_uid*", "hamgonah");
routes.add("download-colleague-panel", "/download-cooleague-panel", "download-colleague-panel");
routes.add("movie-colleague-panel", "/movie-cooleague-panel", "movie-colleague-panel");
routes.add("download-colleague-links", "/download-cooleague-links", "download-colleague-links");
routes.add(
  "download-colleague-dashboard",
  "download-colleague-dashboard",
  "download-colleague-dashboard"
);
// routes.add(
//   "series-colleague-panel",
//   "/series-colleague-panel?uid=/:uid&page=/:page",
//   "series-colleague-panel"
// );
routes.add({
  name: "download-colleague-moment-report",
  pattern: "/download-colleague-moment-report?uid=/:uid&page=/:page",
  page: "download-colleague-moment-report"
});
routes.add("download-buy-info", "/download-buy-info?page=/:page", "download-buy-info");
routes.add("download", "/download", "download");
routes.add("download-company-panel", "/download-company-panel", "download-company-panel");
routes.add("download-category", "/download-category", "download-category");

routes.add("payoff", "/payoff", "payoff");
{
  /* <Route path="/payoff" component={PanelDownloadPayoff}/> */
}

//payoff
//payoff-report
routes.add("ghahvetalkh", "/ghahvetalkh/:colleague_uid*", "ghahvetalkh");
routes.add("roozebazi", "/roozebazi/:colleague_uid*", "roozebazi");
routes.add("shameirani", "/shameirani/:colleague_uid*", "shameirani");
routes.add({
  name: "download-company-moment-report",
  pattern: "/download-company-moment-report?uid=/:uid&page=/:page",
  page: "download-company-moment-report"
});

//hamgonah campaign
routes.add({
  name: "HamgonahCampaign",
  pattern: "/hamgonah-campaign/:colleague_uid*",
  page: "campaign/hamgonah/HamgonahCampaign"
});

routes.add({
  name: "HamgonahAwards",
  pattern: "/hamgonah-awards",
  page: "campaign/hamgonah/HamgonahAwards"
});
routes.add({
  name: "HamgonahBuy",
  pattern: "/hamgonah-buy/:uid/:colleague_uid*",
  page: "campaign/hamgonah/HamgonahBuy"
});
routes.add({
  name: "HamgonahSelect",
  pattern: "/hamgonah-select-quality/:uid*",
  page: "campaign/hamgonah/HamgonahSelect"
});

routes.add(
  "download-company-dashboard",
  "/download-company-dashboard",
  "download-company-dashboard"
);
routes.add(
  "company-general-moment",
  "/company-general-moment?start=/:start*&end=/:end*&page=/:page",
  "company-general-moment"
);

//Cart
routes.add("cart", "/cart", "cart");
