import React, { Component } from "react";

import styles from "./Loading.module.scss";

export default class Loading extends Component {
  render() {
    const { width, height } = this.props;
    // const loadingImg = require("public/icons/loading.gif");
    return (
      <div className="text-center">
        <img
          src={process.env.NEXT_PUBLIC_DOMAIN + "/icons/loading.gif"}
          className={styles.loadingParent}
          width={width || "50"}
          height={height || "50"}
          alt="در حال دریافت اطلاعات"
        />
      </div>
    );
  }
}
