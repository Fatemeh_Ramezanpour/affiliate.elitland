import React from "react";
import LazyLoad from "react-lazyload";

const ImageCheck = ({ urlImg, classNameCom }) => {
  const defaultImage = "public/images/default-user.jpeg";

  return (
    <LazyLoad>
      <img src={urlImg !== null ? urlImg : defaultImage} className={classNameCom} alt={urlImg} />
    </LazyLoad>
  );
};

export default ImageCheck;
