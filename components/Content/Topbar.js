import React from "react";

import { elitlandUrl } from "services/url";

import { FaAlignJustify } from "react-icons/fa";

import styles from "./Topbar.module.scss";

const Topbar = ({ toggleSidebar }) => {
  return (
    <nav
      color="light"
      light
      className="navbar shadow-sm p-3 bg-white rounded"
      // expand="md"
    >
      <button color="info" onClick={toggleSidebar}>
        <FaAlignJustify />
        {/* <FontAwesomeIcon icon={faAlignLeft} /> */}
      </button>
      {/* <NavbarToggler onClick={toggleTopbar} /> */}
      <ul className={[styles.changeFlex, "ml-auto mr-4"].join(" ")} navbar>
        <li className={styles.changeEffect}>
          <a href={elitlandUrl()}>بازگشت به Elitland</a>
        </li>
        {/* <NavItem>
        </NavItem>
        <NavItem>
          <NavLink>page 3</NavLink>
        </NavItem>
        <NavItem>
          <NavLink>page 4</NavLink>
        </NavItem> */}
      </ul>
      {/* <Collapse isOpen={topbarIsOpen} navbar>
        <Nav className="ml-auto" navbar>
          <NavItem>
            <NavLink >
              page 1
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink >
              page 2
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink >
              page 3
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink >
              page 4
            </NavLink>
          </NavItem>
        </Nav>
      </Collapse> */}
    </nav>
  );
};

export default Topbar;
