import React from "react";

import styles from "./Content.module.scss";

const Content = ({ sidebarIsOpen, children }) => (
  <div className={[styles.content, `${sidebarIsOpen ? styles.isOpen : ""}`].join(" ")}>
    <div style={{ width: "100%" }} className="p-2">
      {children}
    </div>
  </div>
);

export default Content;
