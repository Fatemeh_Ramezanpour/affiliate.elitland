import React, { Component } from "react";

import classNames from "classnames";

import styles from "./BoxHeader.module.scss";

export default class BoxHeader extends Component {
  render() {
    const {
      text,
      Header,
      rightText,
      leftText,
      hrClassName,
      children,
      boxClassName,
      textClassName,
      description,
      desClassName
    } = this.props;
    return (
      <div>
        <div className={classNames(styles.mainBoxStyle, boxClassName)}>
          {Header && (
            <div>
              <Header className={classNames(styles.mainTextStyle, textClassName)}>{text}</Header>
              {description && (
                <div className={classNames(styles.mainTextStyle, desClassName)}>{description}</div>
              )}
            </div>
          )}
          {(rightText || leftText) && (
            <div>
              {rightText && <span className={classNames(styles.rightTextStyle)}>{rightText}</span>}
              {leftText && <span className={styles.leftTextStyle}>{leftText}</span>}
            </div>
          )}
        </div>
        <hr className={hrClassName ? hrClassName : styles.hrStyle} />
        {children}
      </div>
    );
  }
}
