import React from "react";

import { Card, CardBody, CardTitle, CardSubtitle } from "reactstrap";

import styles from "./FlatBox.module.scss";

const FlatBox = ({ headerTitle, headerSubtitle, children }) => {
  return (
    <Card className={styles.cardEdit}>
      <CardBody className={styles.cardHeder}>
        <CardTitle>{headerTitle}</CardTitle>
        <CardSubtitle>{headerSubtitle}</CardSubtitle>
      </CardBody>
      <CardBody>{children}</CardBody>
    </Card>
  );
};

export default FlatBox;
