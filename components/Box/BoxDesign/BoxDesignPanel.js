import React, { Component } from "react";

export default class BoxDesignPanel extends Component {
  render() {
    const { children, PanelTitle } = this.props;

    return (
      <div>
        {PanelTitle}

        <span>{children}</span>
      </div>
    );
  }
}
