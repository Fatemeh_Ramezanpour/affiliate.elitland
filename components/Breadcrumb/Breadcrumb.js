import React from "react";

import { BreadcrumbItem, Breadcrumb as Breadcrumbs } from "reactstrap";

import { Link } from "routes/routes";

import classNames from "classnames";
import styles from "./Breadcrumb.module.scss";

const Breadcrumb = ({ breadcrumbs }) => {
  let breadcrumbItems = [];

  if (breadcrumbs && breadcrumbs.length > 0) {
    breadcrumbItems = breadcrumbs.map((breadcrumb, index) => {
      return (
        // <li key={index}>
        <React.Fragment key={index}>
          {breadcrumb.link !== undefined && (
            <BreadcrumbItem className={styles.breadItem}>
              <Link to={breadcrumb.link}>
                <a>{breadcrumb.text}</a>
              </Link>
            </BreadcrumbItem>
          )}
          {breadcrumb.href !== undefined && (
            <BreadcrumbItem className={styles.breadItem}>
              <a href={breadcrumb.href}>{breadcrumb.text}</a>
            </BreadcrumbItem>
          )}
          {breadcrumb.href === undefined && breadcrumb.link === undefined && (
            <BreadcrumbItem className={classNames(styles.breadItem, "active")}>
              <span>{breadcrumb.text}</span>
            </BreadcrumbItem>
          )}
        </React.Fragment>
      );
    });
  }

  return (
    <Breadcrumbs className={["d-none d-sm-block", styles.breadcrumbBox].join(" ")}>
      {breadcrumbItems}
    </Breadcrumbs>
  );
};

export default Breadcrumb;
