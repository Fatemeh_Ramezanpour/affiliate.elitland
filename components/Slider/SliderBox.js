import React, { Component } from "react";
import Swiper from "react-id-swiper";

import { BasicLoading, BoxDesignPanel } from "components";
import styles from "./SliderBox.module.scss";
export default class SliderBox extends Component {
  render() {
    const {
      item,
      panelTitle,
      slideItemCount,
      slideMobile,
      className,
      disableBorder,
      iscritic,
      Header
    } = this.props;
    const nextBtnCustomized = styles.nextButtonCustomized;
    const prevBtnCustomized = styles.prevButtonCustomized;
    const custmSwiperPagination = styles.customizedSwiperPagination;
    const params = {
      rtl: true,
      slidesPerView: slideItemCount,
      slidesPerGroup: slideItemCount || 4,
      spaceBetween: 5,
      pagination: {
        el: ".swiper-pagination",
        type: "custom",
        clickable: true
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      },
      nextButtonCustomizedClass: nextBtnCustomized, // Add your class name for next button
      prevButtonCustomizedClass: prevBtnCustomized, // Add your class name for prev button
      paginationCustomizedClass: custmSwiperPagination,
      breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 40
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 30
        },
        640: {
          slidesPerView: slideMobile || 1,
          slidesPerGroup: slideMobile || 1,
          spaceBetween: 20
        },
        320: {
          slidesPerView: slideMobile || 1,
          slidesPerGroup: slideMobile || 1,
          spaceBetween: 10
        }
      }
    };

    const innerView = (
      <div className={styles.container}>
        <Swiper {...params}>{item}</Swiper>
      </div>
    );

    const outerView = disableBorder ? (
      innerView
    ) : (
      <BoxDesignPanel
        PanelTitle={panelTitle}
        className={styles.sliderBasic}
        titleClassName={className}
        iscritic={iscritic}
        PanelHeader={Header}
      >
        {innerView}
      </BoxDesignPanel>
    );

    return (
      <div>
        {item && outerView}
        {!item && <BasicLoading />}
      </div>
    );
  }
}
