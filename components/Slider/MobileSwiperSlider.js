import React, { memo } from "react";
import Swiper from "react-id-swiper";

import styles from "./MobileSwiperSlider.module.scss";

const MobileSwiperSlider = ({ children, title }) => {
  const params = {
    lazy: true,
    spaceBetween: 10,
    slidesPerView: "auto",
    slidesPerGroup: 5,
    freeMode: true,
    breakpoints: {
      1024: {
        slidesPerView: 5,
        slidesPerGroup: 5,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 5,
        slidesPerGroup: 5,
        spaceBetween: 10
      },
      640: {
        slidesPerView: 3,
        slidesPerGroup: 3,
        spaceBetween: 10,
        navigation: false
      },
      320: {
        slidesPerView: 2.2,
        slidesPerGroup: 2,
        spaceBetween: 10,
        navigation: false
      }
    }
  };

  return (
    <>
      <div
        className={[styles.header, "d-block d-sm-block d-md-none d-lg-none d-xl-none"].join(" ")}
      >
        <div className={styles.title}>{title}</div>
        <Swiper {...params}>{children}</Swiper>
      </div>
    </>
  );
};

export default memo(MobileSwiperSlider);
