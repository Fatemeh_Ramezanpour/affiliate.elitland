import React, { memo } from "react";
import Swiper from "react-id-swiper";

import { FaAngleLeft, FaAngleRight } from "react-icons/fa";

import styles from "./SwiperSlider.module.scss";

const SwiperSlider = ({ children, title }) => {
  const params = {
    // pagination: {
    //   el: '.swiper-pagination',
    //   type: 'bullets',
    //   clickable: true
    // },
    lazy: true,
    spaceBetween: 10,
    slidesPerView: "auto",
    slidesPerGroup: 5,
    freeMode: true,
    breakpoints: {
      1024: {
        slidesPerView: 5,
        slidesPerGroup: 5,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 5,
        slidesPerGroup: 5,
        spaceBetween: 10
      },
      640: {
        slidesPerView: 3,
        slidesPerGroup: 3,
        spaceBetween: 10,
        navigation: false
      },
      320: {
        slidesPerView: 2.2,
        slidesPerGroup: 2,
        spaceBetween: 10,
        navigation: false
      }
    },
    navigation: {
      nextEl: `.${styles.prevButton}`,
      prevEl: `.${styles.nextButton}`
    },
    renderPrevButton: () => (
      <div className={styles.prevBtnBorder}>
        <button title={"Previous Button"} className={[styles.button, styles.prevButton].join(" ")}>
          <FaAngleLeft />
        </button>
      </div>
    ),
    renderNextButton: () => (
      <div className={styles.nextBtnBorder}>
        <button title={"Next Button"} className={[styles.button, styles.nextButton].join(" ")}>
          <FaAngleRight />
        </button>
      </div>
    )
  };

  return (
    <>
      <div
        className={[styles.header, "d-none d-sm-none d-md-block d-lg-block d-xl-block"].join(" ")}
      >
        <div className={styles.title}>{title}</div>
        <Swiper {...params} containerClass={["swiper-container", styles.alignContent].join(" ")}>
          {children}
        </Swiper>
      </div>
    </>
  );
};

export default memo(SwiperSlider);
