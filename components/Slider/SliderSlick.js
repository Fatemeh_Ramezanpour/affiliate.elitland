// import React, { Component } from "react";
// import Slider from "react-slick";

// export default class SliderSlick extends Component {
//   render() {
//     const { data, slidesToShow, slidesToScroll } = this.props;
//     const settings = {
//       dots: true,
//       infinite: false,
//       speed: 500,
//       slidesToShow: slidesToShow,
//       slidesToScroll: slidesToScroll,
//       arrows: false,
//       swipeToSlide: true,
//       initialSlide: data.length - slidesToShow,
//       responsive: [
//         {
//           breakpoint: 1024,
//           settings: {
//             slidesToShow: slidesToShow > 1 ? slidesToShow - 1 : slidesToShow,
//             slidesToScroll: slidesToScroll > 1 ? slidesToScroll - 1 : slidesToScroll,
//             infinite: false,
//             dots: true,
//             initialSlide:
//               slidesToShow > 1
//                 ? data.length - (slidesToShow - 1)
//                 : data.length - slidesToShow
//           }
//         },
//         {
//           breakpoint: 700,
//           settings: {
//             slidesToShow: 1,
//             slidesToScroll: slidesToScroll > 2 ? slidesToScroll - 2 : 1,
//             initialSlide:
//               slidesToShow > 2 ? data.length - (slidesToShow - 2) : data.length - 1
//           }
//         },
//         {
//           breakpoint: 480,
//           settings: {
//             slidesToShow: 1,
//             slidesToScroll: 1,
//             initialSlide: data.length - 1
//           }
//         }
//       ]
//     };

//     return (
//       <Slider ref={c => (this.slider = c)} {...settings} className="container">
//         {data}
//       </Slider>
//     );
//   }
// }

import React, { memo } from "react";
// import * as PropTypes from "prop-types";
import Slider from "react-slick";

// import { FaAngleLeft, FaAngleRight } from "react-icons/fa";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";

// import styles from "./SectionLayoutSlider.scss";
import styles from "./SliderSlick.module.scss";

const SliderSlick = ({ slidesToShow, slidesToScroll, children, title, removeArrow, autoplay }) => {
  const settings = {
    dots: false,
    infinite: autoplay,
    speed: 500,
    slidesToShow: slidesToShow,
    slidesToScroll: slidesToScroll,
    arrows: false,
    swipeToSlide: false,
    autoplay: autoplay,
    initialSlide: children ? children.length - slidesToShow : 1,
    // rtl: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: slidesToShow > 1 ? slidesToShow - 1 : slidesToShow,
          slidesToScroll: slidesToScroll > 1 ? slidesToScroll - 1 : slidesToScroll,
          infinite: false,
          dots: false,
          initialSlide:
            slidesToShow > 1 ? children.length - (slidesToShow - 1) : children.length - slidesToShow
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 3.5,
          slidesToScroll: slidesToScroll > 2 ? slidesToScroll - 2 : 1,
          initialSlide:
            slidesToShow > 2 ? children.length - (slidesToShow - 2) : children.length - 1
        }
      },
      {
        swipe: false,
        breakpoint: 480,
        settings: {
          slidesToShow: 2.1,
          slidesToScroll: 2,
          initialSlide: children.length - 2.1
        }
      }
    ]
  };
  // create a slider object for use it's methods
  let slider = null;

  return (
    <div className={[styles.sectionLayoutSliderWrapper].join(" ")}>
      <div className={styles.header}>
        <div className={styles.title}>{title}</div>
        {children && children.length > 1 && !removeArrow && (
          <div className={[styles.buttonsContainer, "d-none d-sm-block"].join(" ")}>
            <button
              title={"Next Button"}
              className={[styles.button, styles.nextButton].join(" ")}
              onClick={() => slider.slickNext()}
              data-testid={"nextButton"}
            >
              <FaAngleRight />
            </button>
            <button
              title={"Previous Button"}
              className={[styles.button, styles.prevButton].join(" ")}
              onClick={() => slider.slickPrev()}
              data-testid={"prevButton"}
            >
              <FaAngleLeft />
            </button>
          </div>
        )}
      </div>
      <Slider ref={c => (slider = c)} {...settings}>
        {children}
      </Slider>
    </div>
  );
};

// SectionLayoutSlider.defaultProps = {
//   title: "عنوان پیش فرض",
//   slidesToShow: 4,
//   slidesToScroll: 4,
//   children: "<div>کامپوننت فرزند پیش فرض</div>",
// };

// SectionLayoutSlider.propTypes = {
//   title: PropTypes.string,
//   slidesToShow: PropTypes.number,
//   slidesToScroll: PropTypes.number,
//   children: PropTypes.array,
// };

export default memo(SliderSlick);
