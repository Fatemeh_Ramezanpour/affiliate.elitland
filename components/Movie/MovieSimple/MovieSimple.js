import React, { memo } from "react";

import MoviePoster from "components/MoviePoster/MoviePoster";

const MovieSimple = ({ movie, posterWidth }) => {
  return (
    <>
      {movie && (
        <>
          <div className="text-center">
            <MoviePoster src={movie.poster} width={posterWidth || "200px"} large={true} />
          </div>
          <div className={"text-center"} style={{ margin: "15px" }}>
            <strong>{movie.name}</strong>
          </div>
          <hr />
          <p>{movie.story}</p>
        </>
      )}
    </>
  );
};

export default memo(MovieSimple);
