import React, { Component } from "react";

import styles from "./Pagination.module.scss";

export class PaginationItemData extends Component {
  render() {
    const { href, as, text, active } = this.props;
    return (
      <li active={active} className={[""].join(" ")}>
        <a
          href={href}
          as={as}
          className="py-2 px-3 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
        >
          {/* <PaginationItem >
        <PaginationLink href={href} as={as}> */}
          {text}
          {/* </PaginationLink>
      </PaginationItem> */}
        </a>
      </li>
    );
  }
}

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lastPageToShow: props.perPage,
      number: 1,
      showNext: true
    };
  }

  render() {
    const { active, total, perPage, url } = this.props;
    const itemsPages = [];
    const activePage = Number(active || 1);
    const endPage = Math.ceil(total / perPage);
    let lastPageToShow = activePage <= 6 ? 10 : activePage + 4;
    lastPageToShow = lastPageToShow > endPage ? endPage : lastPageToShow;
    const firstPageToShow = activePage <= 6 ? 1 : activePage - 5;
    let seperateParams;
    if (url && url.includes("?")) {
      seperateParams = "&";
    } else {
      seperateParams = "?";
    }

    const prevUrl = activePage === 2 ? url : `${url}${seperateParams}page=${activePage - 1}`;
    const nextUrl = `${url}${seperateParams}page=${activePage + 1}`;
    // href={`${url}?page=${activePage + 1}`}

    for (let number = firstPageToShow; number <= lastPageToShow; number++) {
      const url_show = number === 1 ? url : `${url}${seperateParams}page=${number}`;
      itemsPages.push(
        <PaginationItemData
          key={`page_${number}`}
          active={number === activePage}
          styleClassName={styles.paginationItem}
          href={url_show}
          as={`${url}/${number}`}
          text={number}
        />
      );
    }

    return (
      <nav className="p-2 mt-3">
        <ul className="inline-flex">
          {/* <ul
          aria-label="Page navigation example"
          className={`${styles.responsivePg} ${styles.pagination}`}
        > */}
          {activePage !== 1 && (
            <PaginationItemData
              key={"page_first"}
              styleClassName={styles.paginationItem}
              href={url}
              as={`${url}/${1}`}
              text={"صفحه نخست"}
            />
          )}
          {activePage !== 1 && (
            <PaginationItemData
              key={"page_before"}
              styleClassName={styles.paginationItem}
              href={prevUrl}
              as={`${url}/${activePage - 1}`}
              text={"قبلی"}
            />
          )}
          {itemsPages}
          {activePage !== endPage && (
            <PaginationItemData
              key={"page_after"}
              styleClassName={styles.paginationItem}
              // href={`${url}?page=${activePage + 1}`}
              href={nextUrl}
              as={`${url}/${activePage + 1}`}
              text={"بعدی"}
            />
          )}
          {activePage !== endPage && (
            <PaginationItemData
              key={"page_last"}
              styleClassName={styles.paginationItem}
              // href={`${url}?page=${endPage}`}
              href={`${url}${seperateParams}page=${endPage}`}
              as={`${url}/${endPage}`}
              text={"صفحه آخر"}
            />
          )}
        </ul>
      </nav>
    );
  }
}

export default Pagination;
