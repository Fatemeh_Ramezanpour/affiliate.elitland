import React, { memo } from "react";

import { engToPerNumber } from "services/helper";

import styles from "./NoticePanel.module.scss";

const NoticePanel = ({ title, txtClass, total, month, week, day, table, notification }) => {
  return (
    <div className={styles.boxDesignStyle}>
      <>
        <div className={table ? styles.tableFontStl : `${styles.text} ${txtClass}`}>{title}</div>
        {!notification && (
          <>
            {table && (
              <div className={styles.price}>
                <span>رتبه کل</span>{" "}
                <span className={styles.rank}>
                  {engToPerNumber(total !== null ? total : "میزان فروش شما ۰ می‌باشد")}
                </span>
              </div>
            )}
            {!table && (
              <div className={styles.price}>
                <span>رتبه ماهانه</span>{" "}
                <span className={styles.rank}>
                  {engToPerNumber(month !== null ? month : "میزان فروش شما ۰ می‌باشد")}
                </span>
              </div>
            )}
            {!table && (
              <div className={styles.price}>
                <span>رتبه هفتگی</span>{" "}
                <span className={styles.rank}>
                  {engToPerNumber(week !== null ? week : "میزان فروش شما ۰ می‌باشد")}
                </span>
              </div>
            )}
            {!table && (
              <div className={styles.price}>
                <span>رتبه روزانه</span>{" "}
                <span className={styles.rank}>
                  {engToPerNumber(day !== null ? day : "میزان فروش شما ۰ می‌باشد")}
                </span>
              </div>
            )}
          </>
        )}
        {notification && <div className="text-right">{notification}</div>}
      </>
    </div>
  );
};

export default memo(NoticePanel);
