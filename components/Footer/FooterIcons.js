import React from "react";

const FooterIcons = ({ href, src, className }) => (
  <a target="_blank" href={href} rel="noreferrer noopener">
    <img className={className} src={src} alt={src} />
  </a>
);
export default FooterIcons;
