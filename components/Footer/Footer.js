import React, { Component } from "react";

import AlertList from "components/Alert/AlertList/AlertList";
import Image from "next/image";

import classNames from "classnames";
import styles from "./Footer.module.scss";

const FooterIcons = ({ href, src, className }) => (
  <a target="_blank" href={href} rel="noreferrer noopener">
    <Image className={className} src={src} width="50" height="50" />
  </a>
);
export default class Footer extends Component {
  render() {
    const { className } = this.props;
    const facebookIcon = require("public/icons/facebook.svg");
    const twiterIcon = require("public/icons/twitter.svg");
    const whatssapp = require("public/icons/whatsapp.svg");
    const telegram = require("public/icons/telegram.svg");
    const insta = require("public/icons/instagram.svg");
    const linkedin = require("public/icons/linkedin.svg");
    // const question = require("public/icons/q.svg");
    const PersianTexts = {
      tel: "در صورت نیاز از طریق یکی از روش‌های زیر با پشتیبانی تماس بگیرید",
      permition: " ۱. ارسال پیام به آدرس تلگرام",
      telNum: "۲. ارسال پیام به واتس آپ یا تلگرام به شماره 09218121989",
      copyright: "۳. ارسال ایمیل به آدرس support@salamcinama.ir"
      // connection: "",
    };

    return (
      <footer className={[styles.mainStyle, className, "p-4"].join(" ")}>
        <div className="grid grid-cols-3 gap-4">
          {/* <div className="col-span-1" className={classNames("midTPadding")}> */}
          <div>
            <div className={styles.copyRight}>
              <p className={styles.telpermision}>{PersianTexts.tel} </p>
              <p className={styles.permision}>
                {PersianTexts.permition}
                <a
                  target="_blank"
                  className={styles.phoneStyle}
                  rel="noopener noreferrer"
                  href="http://t.me/salamcinema_help"
                >
                  salamcinema_help
                </a>
              </p>
              <p className={styles.tel}>{PersianTexts.telNum}</p>
              <p className={styles.permision}>{PersianTexts.copyright}</p>
            </div>
          </div>
          <div className="text-center">
            {/* <div className={styles.icons}>
              <div className={styles.ways}>
                <p className={styles.connection}>{PersianTexts.connection}</p>
              </div> */}
            {/* <div className={styles.ways}> */}
            <FooterIcons
              href="https://www.instagram.com/salamcinama/"
              src={insta}
              className={classNames(styles.iconMargin, styles.insta, "inline-block")}
            />
            <FooterIcons
              href="https://twitter.com/salamcinama"
              src={twiterIcon}
              className={classNames(styles.iconMargin, styles.twiterIcon, "inline-block")}
            />
            <FooterIcons
              href="whatsapp://send?!&phone=0989218121989"
              src={whatssapp}
              className={classNames(styles.iconMargin, styles.whatssapp, "inline-block")}
            />
            <FooterIcons
              href="https://t.me/salamcinema_help/"
              src={telegram}
              className={classNames(styles.iconMargin, styles.telegram, "inline-block")}
            />
            {/* </div> */}
            {/* <div className={styles.ways}>
                <FooterIcons
                  href="https://www.facebook.com/salamcinama/"
                  src={facebookIcon}
                  className={classNames(styles.iconMargin, styles.facebookIcon)}
                />
                <FooterIcons
                  href="https://www.linkedin.com/company/salam-cinema/"
                  src={linkedin}
                  className={classNames(styles.iconMargin, styles.linkedin)}
                />
            </div> */}
            <div className="text-center">
              <FooterIcons
                href="https://www.facebook.com/salamcinama/"
                src={facebookIcon}
                className={classNames(styles.iconMargin, styles.facebookIcon, "inline-block")}
              />
              <FooterIcons
                href="https://www.linkedin.com/company/salam-cinema/"
                src={linkedin}
                className={classNames(styles.iconMargin, styles.linkedin, "inline-block")}
              />
            </div>
          </div>
          <div></div>
          <AlertList />
        </div>
        <style jsx>{`
          .hideFooter {
            display: none;
          }
          .showFooter {
            display: block;
          }
        `}</style>
      </footer>
    );
  }
}
