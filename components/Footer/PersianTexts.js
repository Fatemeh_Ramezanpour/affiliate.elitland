const PersianTexts = {
  tel: "تلفن تماس:",
  telNum: "۶۶۰۶۶۱۴۳-۰۲۱",
  permition: "استفاده از مطالب سلام سینما با ذکر منبع مجاز است.",
  copyright: "کلیه حقوق این سایت برای سلام سینما محفوظ می‌باشد.",
  connection: "راه های ارتباطی با ما در شبکه های اجتماعی و پاسخ به سوالات شما:"
};
export default PersianTexts;
