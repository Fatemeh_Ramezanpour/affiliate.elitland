import React, { Component } from "react";
import { connect } from "react-redux";

import Sidebar from "components/Sidebar/Sidebar";
import { setSidebarOpenAction, addSubMenuAction } from "posts/actions/menu";
import {
  payoffUrl,
  payoffReportUrl,
  seriesColleagueSalePanelUrl,
  movieColleagueSalePanelUrl,
  colleagueReportUrl
} from "services/url";

import Content from "components/Content/Content";
import Topbar from "components/Content/Topbar";
// import { faImage, faCopy } from "@fortawesome/free-solid-svg-icons";
import { FaShoppingBasket, FaFileInvoiceDollar } from "react-icons/fa";

import styles from "./OriginalWrapper.module.scss";

class OriginalWrapper extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    const submenus = [
      {
        id: 1,
        collapsed: true,
        mainTitle: "تسویه",
        icon: FaShoppingBasket,
        subItem: [
          {
            title: "درخواست تسویه",
            target: payoffUrl()
          },
          {
            title: "گزارش تسویه",
            target: payoffReportUrl()
          }
        ]
      },
      {
        id: 2,
        collapsed: true,
        mainTitle: "درآمدها",
        icon: FaFileInvoiceDollar,
        subItem: [
          {
            title: "گزارش کامل فروش",
            target: colleagueReportUrl()
          },
          {
            title: "درآمد حاصل از سریال‌ها",
            target: seriesColleagueSalePanelUrl()
          },
          {
            title: "درآمد حاصل از فیلم‌ها",
            // target: persianMvColleaguePanelUrl()
            target: movieColleagueSalePanelUrl()
          }
          // {
          //   title: "فیلم‌های خارجی",
          //   target: colleagueSalePanelUrl()
          // }
        ]
      }
    ];
    dispatch(addSubMenuAction(submenus));
  }
  render() {
    const { children, sidebarIsOpen, data, user, params } = this.props;
    const toggleSidebar = () => {
      const { dispatch } = this.props;
      dispatch(setSidebarOpenAction(!sidebarIsOpen));
    };
    return (
      <>
        <Topbar toggleSidebar={toggleSidebar} user={user} />
        <div className={styles.sidebarWrapper}>
          {/* <div
          className={`${
            sidebarIsOpen ? ["grid grid-cols-5 gap-3"].join(" ") : ["grid gap-3"].join(" ")
          }`}
        > */}
          {/* <div className="col-span-1"> */}
          <Sidebar
            user={user}
            toggle={toggleSidebar}
            isOpen={sidebarIsOpen}
            submenuItems={data}
            params={params}
          />
          {/* </div> */}
          {/* <div className="col-span-4 p-4"> */}
          <Content toggleSidebar={toggleSidebar} sidebarIsOpen={sidebarIsOpen}>
            {children}
          </Content>
          {/* </div> */}
          {/* </div> */}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  sidebarIsOpen: state.menu.isOpenSidebar,
  data: state.menu.data,
  user: state.auth.user
});

export default connect(mapStateToProps)(OriginalWrapper);
