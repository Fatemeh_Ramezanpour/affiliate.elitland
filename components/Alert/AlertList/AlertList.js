import React, { Component } from "react";
import { connect } from "react-redux";

import AlertItem from "components/Alert/AlertItem/AlertItem";

import styles from "./AlertList.module.scss";

class AlertList extends Component {
  render() {
    const { alerts, visible } = this.props;
    return (
      <div className={styles.alertBox}>
        {alerts &&
          alerts.map(alert => <AlertItem key={alert.id} alert={alert} visible={visible} />)}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  visible: state.alert.visible,
  alerts: state.alert.data
});
export default connect(mapStateToProps)(AlertList);
