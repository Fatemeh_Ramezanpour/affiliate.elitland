import React, { Component } from "react";
import LazyLoad from "react-lazyload";

export default class UserImage extends Component {
  render() {
    const { src, width, height, className, lazyClass, alt } = this.props;
    const defaultUser = process.env.NEXT_PUBLIC_DOMAIN + "/images/userpic.jpeg";
    const srcM = src || defaultUser;
    const cWidth = Number.parseInt(height || width || "40") + 10;
    return (
      <LazyLoad height={height || width || "40"} width={cWidth} className={lazyClass}>
        <img
          src={srcM}
          width={width || "40"}
          height={height || width || "40"}
          className={["rounded-circle", className].join(" ")}
          alt={alt}
        />
      </LazyLoad>
    );
  }
}
