import React, { Component, PureComponent } from "react";

import MoviePoster from "components/MoviePoster/MoviePoster";
import Pagination from "components/Pagination/Pagination";
import Loading from "components/Loading/Loading";

import { engToPerNumber } from "services/helper";
import { seriesColleagueSalePanelUrl } from "services/url";
import Router from "next/router";

import Row from "reactstrap/lib/Row";
import Col from "reactstrap/lib/Col";

class DownloadSalePanelInfo extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      moreSaleInfo: false,
      showColleagueLinks: false,
      showColleageSale: false
    };
  }

  toggle = () => {
    const { item } = this.props;
    this.setState({
      moreSaleInfo: true,
      showColleageSale: true,
      showColleagueLinks: false
    });
    // loadDownloadColleaguePanelDetails(item.movie.uid);
    Router.push(seriesColleagueSalePanelUrl(item.movie.series_uid));
  };

  getLink = () => {
    const { loadDownloadDetailsListAction, item } = this.props;
    this.setState({
      moreSaleInfo: true,
      showColleagueLinks: true,
      showColleageSale: false
    });
    loadDownloadDetailsListAction(item.movie.uid);
  };

  render() {
    const { item } = this.props;

    return (
      <>
        <tr>
          <td className="text-right">
            <MoviePoster src={item.movie.poster} width={"40px"} className="inline-block" />
            <span style={{ margin: "10px" }}>{engToPerNumber(item.movie.name)}</span>
          </td>
          <td className="text-center">
            <span>{engToPerNumber(item.sale.last_day_payment_summary.count)}</span>
          </td>
          <td className="text-center">
            <span>{engToPerNumber(item.sale.total_payment_summary.count)}</span>
          </td>
          <td className="text-center">
            <span>{engToPerNumber(item.sale.total_payment_summary.amount)}</span>
          </td>
          <td className="text-center">
            <button
              onClick={() => this.toggle()}
              className={"bg-purple-700 p-2 text-sm text-white rounded"}
            >
              سایر قسمت‌ها
            </button>
          </td>
        </tr>
        <tr style={{ display: this.state.moreSaleInfo ? "" : "none" }}></tr>
      </>
    );
  }
}

class SeriesColleaguePanel extends Component {
  render() {
    const { colleagueSale, url, title } = this.props;
    let listItems = [];
    if (colleagueSale && colleagueSale.data) {
      listItems = colleagueSale.data.movies.map((item, index) => {
        return <DownloadSalePanelInfo key={`sale_panel_${index}`} item={item} {...this.props} />;
      });
    }

    return (
      <div style={{ marginTop: "20px" }}>
        <h1>{title}</h1>
        <hr />
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 table-auto">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr className="text-right">
                <td className="px-6 py-3">فیلم</td>
                <td className="px-6 py-3">۲۴ ساعت گذشته</td>
                <td className="px-6 py-3">کل</td>
                <td className="px-6 py-3">درآمد شما</td>
                <td colSpan="2" className="px-6 py-3">
                  جزئیات بیشتر
                </td>
              </tr>
            </thead>
            <tbody>
              {listItems}
              {!colleagueSale.data && (
                <tr>
                  <td colSpan="5">
                    <Loading />
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        {colleagueSale.data && (
          <>
            <Row>
              <Col xs="12" sm="12" md="12" lg="12">
                <Pagination
                  active={colleagueSale.page}
                  total={colleagueSale.data.total_movies}
                  perPage={10}
                  url={url}
                />
              </Col>
            </Row>
          </>
        )}
      </div>
    );
  }
}

export default SeriesColleaguePanel;
