import React, { Component } from "react";
import { useForm } from "react-hook-form";

import { engToPerNumber } from "services/helper";

import styles from "./DownloadPayoffForm.module.scss";

const required = value => (value ? undefined : "این فیلد نمیتواند خالی باشد.");

const checkSheba = value => {
  let error = "";
  if (value.includes(" ") || value.includes("-")) {
    error += "* کاراکتر فاصله و - را وارد نکنید";
    return error;
  }
  if (isNaN(Number(value))) {
    error += "* تنها عددهای 0 تا 9 انگلیسی وارد کنید";
    return error;
  }
  if (value.length < 24 || value.length > 24) {
    error += "* شماره شبا می‌بایست ۲۴ رقم باشد";
    return error;
  }
  return;
};

const DownloadPayoffForm = ({ submitData, accountMony, fields, syncError }) => {
  const {
    register,
    formState: { errors },
    handleSubmit
  } = useForm();
  // const onSubmit = data => console.log(data);
  const onSubmit = data => {
    submitData(data);
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <div className={styles.balance}>
          <span className={styles.account}>موجودی حساب شما</span>
          <span className={styles.accountMony}>{`${engToPerNumber(accountMony)} ` + `تومان`} </span>
        </div>
        <div className="mb-2">
          {/* <Field
            name="amount"
            component="input"
            type="text"
            placeholder="مبلغ درخواستی (تومان)"
            className={styles.textFieldStyle}
            validate={required}
          /> */}

          <input
            name="amount"
            placeholder="مبلغ درخواستی (تومان)"
            {...register("amount", { required: true })}
            className={styles.textFieldStyle}
          />
          {errors.amount && <span className="text-red-600">این فیلد نمیتواند خالی باشد.</span>}
        </div>
        {/* <div>
          {fields?.amount?.touched && syncError?.amount && (
            <span style={{ color: "red" }}>{syncError.amount}</span>
          )}
        </div> */}
        <div className="mb-2">
          {/* <Field
            name="accountNo"
            component="input"
            type="text"
            placeholder="شماره شبا"
            className={styles.textFieldStyle}
            validate={[required, checkSheba]}
          /> */}
          <input
            // aria-invalid={errors.accountNo ? "true" : "false"}
            name="accountNo"
            placeholder="شماره شبا"
            {...register("accountNo", {
              validate: value => checkSheba(value),
              required: "این فیلد نمیتواند خالی باشد."
              // pattern: {
              //   value: /^\d+$/,
              //   minLength: 24,
              //   message: "invalid email address"
              // }
            })}
            // validate={[required, checkSheba]}
            className={styles.textFieldStyle}
          />
          {errors.accountNo?.message && (
            <span className="text-red-600">{errors.accountNo?.message}</span>
          )}
        </div>
        {/* <div>
          {fields?.accountNo?.touched && syncError?.accountNo && (
            <span style={{ color: "red" }}>{syncError.accountNo}</span>
          )}
        </div> */}
        <div className={styles.buttonPosition}>
          <input type="submit" value=" درخواست تسویه حساب" className={styles.settlementButton} />

          {/* <Button onClick={handleSubmit} className={styles.settlementButton}>
            درخواست تسویه حساب
          </Button> */}
        </div>
      </div>
    </form>
  );
};

export default DownloadPayoffForm;
// class DownloadPayoffForm extends Component {
//   render() {
//     const { handleSubmit, accountMony, fields, syncError } = this.props;
//     return (
//       <form>
//         <div className="form-group">
//           <div className={styles.balance}>
//             <span className={styles.account}>موجودی حساب شما</span>
//             <span className={styles.accountMony}>
//               {`${engToPerNumber(accountMony)} ` + `تومان`}{" "}
//             </span>
//           </div>
//           <div>
//             <Field
//               name="amount"
//               component="input"
//               type="text"
//               placeholder="مبلغ درخواستی (تومان)"
//               className={styles.textFieldStyle}
//               validate={required}
//             />
//           </div>
//           <div>
//             {fields?.amount?.touched && syncError?.amount && (
//               <span style={{ color: "red" }}>{syncError.amount}</span>
//             )}
//           </div>
//           <div>
//             <Field
//               name="accountNo"
//               component="input"
//               type="text"
//               placeholder="شماره شبا"
//               className={styles.textFieldStyle}
//               validate={[required, checkSheba]}
//             />
//           </div>
//           <div>
//             {fields?.accountNo?.touched && syncError?.accountNo && (
//               <span style={{ color: "red" }}>{syncError.accountNo}</span>
//             )}
//           </div>
//           <div className={styles.buttonPosition}>
//             <Button onClick={handleSubmit} className={styles.settlementButton}>
//               درخواست تسویه حساب
//             </Button>
//           </div>
//         </div>
//       </form>
//     );
//   }
// }

// DownloadPayoffForm = reduxForm({
//   enableReinitialize: true,
//   form: "DownloadPayoffForm" // a unique identifier for this form
// })(DownloadPayoffForm);

// DownloadPayoffForm = connect()(DownloadPayoffForm);

// export default DownloadPayoffForm;
