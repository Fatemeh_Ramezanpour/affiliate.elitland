import React, { Component } from "react";

// import { MovieDownloadItem } from 'components';
import BoxHeader from "components/Box/BoxHeader/BoxHeader";
import toPersianDate from "services/localDate";
import { engToPerNumber } from "services/helper";

import Badge from "reactstrap/lib/Badge";
import Table from "reactstrap/lib/Table";

import styles from "./PayoffReport.module.scss";

export default class PayoffReport extends Component {
  render() {
    const { reportInfo } = this.props;
    let reportItem = [];
    if (reportInfo) {
      reportItem = reportInfo.map(item => {
        return (
          <tr key={item.created_at}>
            <td className="text-right">{engToPerNumber(item.amount)}</td>
            <td className="text-center">
              {item.state === "pending" && <span className="leading-9"> درحال بررسی </span>}
              {item.state === "canceled" && <span className="leading-9"> لغو شده </span>}
              {item.state === "approved" && <span className="leading-9"> تایید شده </span>}
            </td>
            <td className="text-center">{toPersianDate(item.created_at)}</td>
          </tr>
        );
      });
    }
    return (
      <BoxHeader
        text="گزارش تسویه حساب"
        Header="h1"
        textClassName={styles.ToptextStyle}
        hrClassName="noLine"
      >
        {/* <Table striped bordered responsive className={styles.tableDir}> */}
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 table-auto">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="text-center">
              <td className="px-6 py-3">مبلغ / تومان</td>
              <td className="px-6 py-3">وضعیت</td>
              <td className="px-6 py-3">تاریخ</td>
            </tr>
          </thead>
          <tbody>{reportItem}</tbody>
        </table>
      </BoxHeader>
    );
  }
}
