import React, { Component } from "react";

// import { Pagination, DownlaodMenu, MoviePoster, Loading } from "components";
// import MoviePoster from "components/MoviePoster/MoviePoster";
import Pagination from "components/Pagination/Pagination";
import Loading from "components/Loading/Loading";
import DownloadSalePanelInfo from "components/Download/DownloadColleaguePanel/DownloadSalePanelInfo";

// import { engToPerNumber } from "services/helper";
import NoticePanel from "components/NoticePanel/NoticePanel";
// import DownloadColleagueLinks from "components/Download/DownloadColleagueLinks/DownloadColleagueLinks";
import { seriesColleagueSalePanelUrl } from "services/url";
import { Link } from "routes/routes";
// import Router from "next/router";

// import styles from "./DownloadColleaguePanel.module.scss";
// class DownloadSalePanelInfo extends PureComponent {
//   constructor(props) {
//     super(props);

//     this.state = {
//       moreSaleInfo: false,
//       showColleagueLinks: false,
//       showColleageSale: false,
//       rankFlag: false
//     };
//   }

//   toggle = () => {
//     const { loadDownloadColleaguePanelDetails, item } = this.props;
//     this.setState({
//       moreSaleInfo: true,
//       showColleageSale: true,
//       showColleagueLinks: false
//     });
//     loadDownloadColleaguePanelDetails(item.movie.uid);
//   };
//   toggleRank = rankFlag => {
//     const { loadColleagueMovieRankAction, item } = this.props;
//     this.setState({
//       rankFlag: rankFlag
//     });
//     loadColleagueMovieRankAction(item.movie.uid);
//   };

//   getLink = moreSaleInfo => {
//     const { loadDownloadDetailsListAction, item } = this.props;
//     this.setState({
//       moreSaleInfo: moreSaleInfo,
//       showColleagueLinks: true,
//       showColleageSale: false
//     });
//     loadDownloadDetailsListAction(item.movie.uid);
//   };

//   otherEpisode = () => {
//     const { item } = this.props;
//     Router.push(seriesColleagueSalePanelUrl(item.movie.series_uid));
//   };

//   render() {
//     const { item, panelDetails, downloadInfo, user, movieRank, series, paramsUid } = this.props;
//     const { moreSaleInfo, rankFlag } = this.state;
//     let moreInfo = [];
//     if (
//       panelDetails &&
//       panelDetails[item.movie.uid]?.data &&
//       !panelDetails[item.movie.uid].Loading
//     ) {
//       moreInfo = panelDetails[item.movie.uid].data.qualities.map(data => (
//         <tr key={`sale_info_${item.movie.uid}_${data.ratio}`}>
//           <td className="text-center">{engToPerNumber(data.ratio)}</td>
//           <td>{engToPerNumber(data.last_day_payment_summary.count)}</td>
//           <td>{engToPerNumber(data.last_month_payment_summary.count)}</td>
//           <td>{engToPerNumber(data.total_payment_summary.count)}</td>
//         </tr>
//       ));
//     }

//     return (
//       <>
//         <tr>
//           <td className="text-right" width="25%">
//             <MoviePoster src={item.movie.poster} width={"40px"} className="inline-block" />
//             <span style={{ margin: "10px" }}>{engToPerNumber(item.movie.name)}</span>
//           </td>
//           <td className="text-center">
//             <span>{engToPerNumber(item.sale.last_day_payment_summary.count)}</span>
//           </td>
//           <td className="text-center">
//             <span>{engToPerNumber(item.sale.total_payment_summary.count)}</span>
//           </td>
//           <td className="text-center">
//             <span>{engToPerNumber(item.sale.total_payment_summary.amount)}</span>
//           </td>
//           <td className="text-center">
//             <button className="bg-blue-500 p-2 text-sm text-white rounded" onClick={this.toggle}>
//               اطلاعات بیشتر
//             </button>
//           </td>
//           <td className="text-center">
//             <button
//               className="bg-green-600 p-2 text-sm text-white rounded"
//               href={downloadColleagueMomentRepoUrl(item.movie.uid, 1)}
//             >
//               گزارش لحظه‌ای
//             </button>
//           </td>
//           <td className="text-center">
//             <button className="bg-sky-500 p-2 text-sm text-white rounded" onClick={this.getLink}>
//               لینک فروش
//             </button>
//           </td>
//           <td>
//             {(!item?.movie?.series_uid || paramsUid) && (
//               <button
//                 className={"bg-orange-600	p-2 text-sm text-white rounded"}
//                 onClick={() => this.toggleRank(!rankFlag)}
//               >
//                 رتبه فروش
//               </button>
//             )}
//           </td>
//           <td>
//             {item?.movie?.series_uid && !series && (
//               <button
//                 className={"bg-purple-700 p-2 text-sm text-white rounded"}
//                 onClick={this.otherEpisode}
//               >
//                 سایر قسمت‌ها
//               </button>
//             )}
//           </td>
//         </tr>
//         <tr style={{ display: this.state.moreSaleInfo ? "" : "none" }}>
//           <td colSpan="9">
//             <div style={{ padding: "25px", display: this.state.showColleagueLinks ? "" : "none" }}>
//               <h5>لینک های فروش {item.movie.name}</h5>
//               <div>
//                 {downloadInfo[item.movie.uid] && (
//                   <DownloadColleagueLinks
//                     downloadUid={item.movie.uid}
//                     downloadInfo={downloadInfo[item.movie.uid].data}
//                     user={user}
//                   />
//                 )}
//               </div>
//             </div>

//             {panelDetails && panelDetails[item.movie.uid] && !panelDetails[item.movie.uid].Loading && (
//               <div style={{ padding: "25px", display: this.state.showColleageSale ? "" : "none" }}>
//                 <h5>آمار فروش {item.movie.name}</h5>
//                 <table style={{ backgroundColor: "lightgoldenrodyellow" }} responsive>
//                   <thead>
//                     <tr>
//                       <td>کیفیت</td>
//                       <td>۲۴ ساعت</td>
//                       <td>۳۰ روز</td>
//                       <td>کل</td>
//                     </tr>
//                   </thead>
//                   <tbody>
//                     {moreInfo.length > 0 && moreInfo}
//                     {moreInfo.length === 0 && (
//                       <tr>
//                         <td colSpan="4">
//                           <Loading />
//                         </td>
//                       </tr>
//                     )}
//                   </tbody>
//                 </table>
//               </div>
//             )}
//           </td>
//         </tr>

//         <tr className={rankFlag ? " " : "d-none"}>
//           <td colSpan="9">
//             <>
//               {movieRank?.[item.movie.uid]?.total === null && (
//                 <NoticePanel notification="میزان فروش شما در این فیلم ۰ می‌باشد" />
//               )}
//               {movieRank?.[item.movie.uid]?.total && (
//                 <NoticePanel
//                   title="رتبه شما در بین همکاران فروش"
//                   className={styles.fontSizeStl}
//                   total={movieRank[item.movie.uid].total}
//                   table
//                 />
//               )}
//             </>
//           </td>
//         </tr>
//       </>
//     );
//   }
// }

class DownloadColleaguePanel extends Component {
  render() {
    const { colleagueSale, url, title, rank, params, rankLoading } = this.props;
    let listItems = [];
    if (colleagueSale && colleagueSale.data) {
      listItems = colleagueSale.data.movies.map((item, index) => {
        return (
          <DownloadSalePanelInfo
            key={`sale_panel_${index}`}
            item={item}
            {...this.props}
            index={index}
          />
        );
      });
    }

    return (
      <>
        <div className="d-flex flex-row justify-content-between align-items-center">
          <h1>{title}</h1>
          {colleagueSale?.data?.series_uid && (
            <span style={{ color: "#4726bf" }}>
              <Link route={seriesColleagueSalePanelUrl()}>
                <a> رفتن به لیست سریال‌ها</a>
              </Link>
            </span>
          )}
        </div>
        <hr className="mt-0" />
        {params?.asPath && params.asPath === "/" && rank && (
          <NoticePanel
            title="رتبه شما در بین همکاران فروش"
            month={rank.month}
            week={rank.week}
            day={rank.day}
            loading={rankLoading}
          />
        )}

        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 table-auto">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr className="text-right">
                <td className="px-6 py-3">فیلم</td>
                <td className="px-6 py-3">۲۴ ساعت گذشته</td>
                <td className="px-6 py-3">کل</td>
                <td className="px-6 py-3">درآمد شما</td>
                <td colSpan="2" className="px-6 py-3">
                  جزئیات بیشتر
                </td>
                <td className="px-6 py-3">لینک فروش</td>
                <td className="px-6 py-3">رتبه فروش فیلم</td>
                <td className="px-6 py-3">سایر قسمت‌ها</td>
              </tr>
            </thead>
            <tbody>
              {listItems}
              {!colleagueSale.data && (
                <tr>
                  <td colSpan="8">
                    <Loading />
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        {colleagueSale.data && (
          <>
            {/* <Row>
              <Col xs="12" sm="12" md="12" lg="12"> */}
            <div className="grid grid-cols-12 gap-3 ">
              <Pagination
                active={colleagueSale.page}
                total={colleagueSale.data.total_movies}
                perPage={10}
                url={url}
              />
            </div>
            {/* </Col>
            </Row> */}
          </>
        )}
      </>
      // </Container>
    );
  }
}

export default DownloadColleaguePanel;
