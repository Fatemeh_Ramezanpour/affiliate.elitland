import React from "react";
import { useDispatch } from "react-redux";
import Router from "next/router";

import MoviePoster from "components/MoviePoster/MoviePoster";
import Loading from "components/Loading/Loading";
import DownloadColleagueLinks from "components/Download/DownloadColleagueLinks/DownloadColleagueLinks";
import NoticePanel from "components/NoticePanel/NoticePanel";
// import { loadDownloadColleaguePanelDetails } from 'posts/actions/download';
import { loadColleagueMovieRankAction } from "posts/actions/download-details";
import { downloadColleagueMomentRepoUrl, seriesColleagueSalePanelUrl } from "services/url";
import { engToPerNumber } from "services/helper";

import styles from "./DownloadColleaguePanel.module.scss";

const DownloadSalePanelInfo = ({
  item,
  panelDetails,
  downloadInfo,
  user,
  movieRank,
  series,
  paramsUid,
  ...props
}) => {
  const [moreSaleInfo, setMoreSaleInfo] = React.useState(false);
  const [showColleagueLinks, setShowColleagueLinks] = React.useState(false);
  const [showColleageSale, setShowColleageSale] = React.useState(false);
  const [rankFlag, setRankFlag] = React.useState(false);
  const dispatch = useDispatch();

  const toggle = () => {
    setMoreSaleInfo(true);
    setShowColleagueLinks(false);
    setShowColleageSale(true);
    // const { loadDownloadColleaguePanelDetails, item } = props;
    // this.setState({
    //   moreSaleInfo: true,
    //   showColleageSale: true,
    //   showColleagueLinks: false
    // });
    props.loadDownloadColleaguePanelDetails(item.movie.uid);
  };

  const toggleRank = () => {
    // const { loadColleagueMovieRankAction, item } = this.props;
    // this.setState({
    //   rankFlag: rankFlag
    // });
    setRankFlag(!rankFlag);
    dispatch(loadColleagueMovieRankAction(item.movie.uid));
  };

  const getLink = moreSaleInfo => {
    // const { loadDownloadDetailsListAction, item } = this.props;
    // this.setState({
    //   moreSaleInfo: moreSaleInfo,
    //   showColleagueLinks: true,
    //   showColleageSale: false
    // });
    setMoreSaleInfo(moreSaleInfo);
    setShowColleagueLinks(!showColleagueLinks);
    setShowColleageSale(false);

    props.loadDownloadDetailsListAction(item.movie.uid);
  };

  const otherEpisode = () => {
    // const { item } = this.props;
    Router.push(seriesColleagueSalePanelUrl(item.movie.series_uid));
  };

  const changeRoute = (uid, number) => {
    Router.push(downloadColleagueMomentRepoUrl(uid, number));
  };
  let moreInfo = [];
  if (panelDetails && panelDetails[item.movie.uid]?.data && !panelDetails[item.movie.uid].Loading) {
    moreInfo = panelDetails[item.movie.uid].data.qualities.map(data => (
      <tr key={`sale_info_${item.movie.uid}_${data.ratio}`}>
        <td className="text-center">{engToPerNumber(data.ratio)}</td>
        <td className="px-6 py-3">{engToPerNumber(data.last_day_payment_summary.count)}</td>
        <td className="px-6 py-3">{engToPerNumber(data.last_month_payment_summary.count)}</td>
        <td className="px-6 py-3">{engToPerNumber(data.total_payment_summary.count)}</td>
      </tr>
    ));
  }
  return (
    <>
      <tr>
        <td className="text-right" width="25%">
          <MoviePoster src={item.movie.poster} width={"40px"} className="inline-block" />
          <span style={{ margin: "10px" }}>{engToPerNumber(item.movie.name)}</span>
        </td>
        <td className="text-center">
          <span>{engToPerNumber(item.sale.last_day_payment_summary.count)}</span>
        </td>
        <td className="text-center">
          <span>{engToPerNumber(item.sale.total_payment_summary.count)}</span>
        </td>
        <td className="text-center">
          <span>{engToPerNumber(item.sale.total_payment_summary.amount)}</span>
        </td>
        <td className="text-center">
          <button className="bg-blue-500 p-2 text-sm text-white rounded" onClick={toggle}>
            اطلاعات بیشتر
          </button>
        </td>
        <td className="text-center">
          <button
            className="bg-green-600 p-2 text-sm text-white rounded"
            onClick={() => changeRoute(item.movie.uid, 1)}
          >
            گزارش لحظه‌ای
          </button>
        </td>
        <td className="text-center">
          <button className="bg-sky-500 p-2 text-sm text-white rounded" onClick={getLink}>
            لینک فروش
          </button>
        </td>
        <td>
          {(!item?.movie?.series_uid || paramsUid) && (
            <button className={"bg-orange-600	p-2 text-sm text-white rounded"} onClick={toggleRank}>
              رتبه فروش
            </button>
          )}
        </td>
        <td>
          {item?.movie?.series_uid && !series && (
            <button
              className={"bg-purple-700 p-2 text-sm text-white rounded"}
              onClick={otherEpisode}
            >
              سایر قسمت‌ها
            </button>
          )}
        </td>
      </tr>
      <tr style={{ display: moreSaleInfo ? "" : "none" }}>
        <td colSpan="9">
          <div style={{ padding: "25px", display: showColleagueLinks ? "" : "none" }}>
            <h5 className="text-right mb-3 font-normal">لینک های فروش {item.movie.name}</h5>
            <div>
              {downloadInfo[item.movie.uid] && (
                <DownloadColleagueLinks
                  downloadUid={item.movie.uid}
                  downloadInfo={downloadInfo[item.movie.uid].data}
                  user={user}
                />
              )}
            </div>
          </div>

          {panelDetails && panelDetails[item.movie.uid] && !panelDetails[item.movie.uid].Loading && (
            <div style={{ padding: "25px", display: showColleageSale ? "" : "none" }}>
              <h5 className="text-right font-normal">آمار فروش {item.movie.name}</h5>
              {/* <table style={{ backgroundColor: "lightgoldenrodyellow" }} responsive> */}
              <div className="relative overflow-x-auto shadow-md">
                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 table-auto">
                  <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                      <td className="px-6 py-3">کیفیت</td>
                      <td className="px-6 py-3">۲۴ ساعت</td>
                      <td className="px-6 py-3">۳۰ روز</td>
                      <td className="px-6 py-3">کل</td>
                    </tr>
                  </thead>
                  <tbody>
                    {moreInfo.length > 0 && moreInfo}
                    {moreInfo.length === 0 && (
                      <tr>
                        <td colSpan="4" className="px-6 py-3">
                          <Loading />
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          )}
        </td>
      </tr>

      <tr className={rankFlag ? " " : "hidden"}>
        <td colSpan="9">
          <>
            {movieRank?.[item.movie.uid]?.total === null && (
              <NoticePanel notification="میزان فروش شما در این فیلم ۰ می‌باشد" />
            )}
            {movieRank?.[item.movie.uid]?.total && (
              <NoticePanel
                title="رتبه شما در بین همکاران فروش"
                className={[styles.fontSizeStl].join(" ")}
                total={movieRank[item.movie.uid].total}
                table
              />
            )}
          </>
        </td>
      </tr>
    </>
  );
};

export default DownloadSalePanelInfo;
