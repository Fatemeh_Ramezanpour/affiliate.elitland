import React, { Component } from "react";

import { engToPerNumber } from "services/helper";

import styles from "./DownloadTotalPayment.module.scss";
class TotalPaymentItem extends Component {
  render() {
    const { count, amount, title, pureIncome, txtClass, priceClass } = this.props;

    return (
      <div className={styles.boxDesignStyle}>
        <div className={`${styles.text} ${txtClass}`}>{title}</div>
        {pureIncome && (
          <div className={styles.remaining}>
            <div className={styles.remainingInfo}>
              <span className={styles.remainingText}>مقدار تسویه نشده</span>
              <span> {engToPerNumber(pureIncome)} </span> <span> تومان</span>
            </div>
          </div>
        )}
        <div className={`${styles.price} ${priceClass}`}>
          <div className={styles.priceInfo}>
            <span> {engToPerNumber(amount)} </span> <span> تومان</span>
          </div>
          <div className={styles.saleCount}>
            <span>تعداد </span>
            <span> {engToPerNumber(count)} </span>
          </div>
        </div>
      </div>
    );
  }
}

export default class DownloadTotalPayment extends Component {
  render() {
    const { accounting } = this.props;

    return (
      <>
        {accounting && (
          <>
            <div className="col-span-2 md:col-span-1 lg:col-span-1 xl:col-span-1 2xl:col-span-1 py-2">
              <TotalPaymentItem
                amount={accounting.daily.amount}
                count={accounting.daily.count}
                title="مجموع فروش ۲۴ ساعت گذشته"
              />
            </div>
            <div className="col-span-2 md:col-span-1 lg:col-span-1 xl:col-span-1 2xl:col-span-1 py-2">
              <TotalPaymentItem
                amount={accounting.total.amount}
                count={accounting.total.count}
                payOff={accounting.total.pay_off}
                pureIncome={accounting.total.pure_income}
                title="مجموع کل فروش"
              />
            </div>
          </>
        )}
      </>
    );
  }
}
