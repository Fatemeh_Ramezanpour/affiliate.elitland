import React, { Component } from "react";

import { engToPerNumber } from "services/helper";
import { downloadColleagueUrl } from "services/url";
import styles from "./DownloadColleagueLinks.module.scss";
class DownloadColleagueLinks extends Component {
  render() {
    const { downloadInfo, user, downloadUid } = this.props;
    let links = [];
    if (
      user &&
      downloadInfo &&
      downloadInfo.partner_links &&
      downloadInfo.partner_links.length > 0
    ) {
      links = downloadInfo.partner_links.map((linkInfo, index) => (
        <tr key={`links_${downloadUid}_${index}_${linkInfo.quality}`} className="m-2">
          <td className="px-6 py-3 text-right">
            {engToPerNumber(linkInfo.quality.replace("_", ""))}
          </td>
          <td className="px-6 py-3 text-right">
            <a
              href={downloadColleagueUrl(downloadUid, user.uid, linkInfo.quality)}
              style={{ lineHeight: "45px" }}
            >
              {downloadColleagueUrl(downloadUid, user.uid, linkInfo.quality)}
            </a>
          </td>
        </tr>
      ));
    }

    return (
      <>
        {user &&
          downloadInfo &&
          downloadInfo.partner_links &&
          downloadInfo.partner_links.length > 0 && (
            <div className="relative overflow-x-auto shadow-md ">
              <table
                className={[
                  "w-full text-sm text-left text-gray-500 dark:text-gray-400 table-auto",
                  styles.tableStyle
                ].join(" ")}
              >
                <tbody style={{ backgroundColor: "rgba(180, 215, 240, 1)" }}>
                  <tr key={`links_${downloadUid}_userchoice`}>
                    <td className="px-6 py-3 text-right">انتخابی کاربر</td>
                    <td className="px-6 py-3 text-right">
                      <a
                        href={downloadColleagueUrl(downloadUid, user.uid)}
                        style={{ lineHeight: "50px" }}
                      >
                        {downloadColleagueUrl(downloadUid, user.uid)}
                      </a>
                    </td>
                  </tr>
                  {links}
                </tbody>
              </table>
            </div>
          )}
      </>
    );
  }
}

export default DownloadColleagueLinks;
