import React, { Component, PureComponent } from "react";

import Pagination from "components/Pagination/Pagination";
import MoviePoster from "components/MoviePoster/MoviePoster";
import Loading from "components/Loading/Loading";

import { engToPerNumber } from "services/helper";
import DownloadColleagueLinks from "components/Download/DownloadColleagueLinks/DownloadColleagueLinks";
class DownloadSalePanelInfo extends PureComponent {
  render() {
    const { item, user } = this.props;

    return (
      <>
        <tr>
          <td className="text-right">
            <MoviePoster src={item.poster} width={"100px"} large={true} className="inline-block" />
            <span style={{ margin: "10px" }}>{engToPerNumber(item.name)}</span>
          </td>
          <td>
            <DownloadColleagueLinks downloadUid={item.uid} downloadInfo={item} user={user} />
          </td>
        </tr>
      </>
    );
  }
}

class DownloadColleagueLinksDashboard extends Component {
  render() {
    const { colleagueLinks } = this.props;
    let listItems = [];
    if (colleagueLinks?.links?.movies) {
      listItems = colleagueLinks.links.movies.map((item, index) => {
        return (
          <DownloadSalePanelInfo key={`colleague_links_${index}`} item={item} {...this.props} />
        );
      });
    }

    return (
      <div style={{ marginTop: "20px" }}>
        <h1>لینک‌های همکار فروش </h1>
        {/* <DownlaodMenu user={user} /> */}
        <hr />
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 table-auto">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr className="text-right">
                <td>فیلم</td>
                <td>لینک همکاری مخصوص شما</td>
              </tr>
            </thead>
            <tbody>
              {listItems}
              {!colleagueLinks?.links && (
                <tr>
                  <td colSpan="5">
                    <Loading />
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        {colleagueLinks?.links && (
          <>
            <div>
              <Pagination
                active={colleagueLinks.page}
                total={colleagueLinks?.links?.total_movies}
                perPage={10}
                url={"/download-colleague-links"}
              />
            </div>
          </>
        )}
      </div>
    );
  }
}

export default DownloadColleagueLinksDashboard;
