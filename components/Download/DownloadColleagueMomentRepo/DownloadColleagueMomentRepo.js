import React from "react";

import Pagination from "components/Pagination/Pagination";
import toPersianDate from "services/localDate";
import { downloadColleagueMomentRepoUrl } from "services/url";

const CompanyMomentItem = ({ item, index, page }) => {
  const dateVal = item.time;
  const stateColor = state => {
    if (state === "verified") {
      return "success";
    } else if (state === "rejected") {
      return "danger";
    } else if (state === "pending") {
      return "warning";
    } else {
      return "secondary";
    }
  };

  return (
    <>
      <tr>
        <td>{50 * (page - 1) + index + 1}</td>
        <td>
          <span className={`badge badge-${stateColor(item.state)}`}>{item.state}</span>
        </td>
        <td>
          <span>{item.user ? item.user.mobile : ""}</span>
        </td>
        <td>
          <span>{item.user ? item.user.email : ""}</span>
        </td>
        <td>
          <span>{item.quality.replace("_", "")}</span>
        </td>
        <td>
          <span>{item.amount} تومان</span>
        </td>
        <td>
          <span>{toPersianDate(dateVal)}</span>
        </td>
      </tr>
    </>
  );
};
const DownloadColleagueMomentRepo = ({ momentData, movie, page, totalRecord, movieUid }) => {
  let momentItem = [];
  if (momentData && momentData.length > 0) {
    momentItem = momentData.map((item, index) => {
      return (
        <React.Fragment key={index}>
          <CompanyMomentItem item={item} index={index} page={page} />
        </React.Fragment>
      );
    });
  }

  return (
    <div fluid={true} style={{ marginTop: "20px" }}>
      <h1> {`پنل گزارش لحظه‌ای فروش فیلم ${movie ? movie.name : ""}`}</h1>
      {/* <Table className="table table-striped" responsive> */}
      <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 table-auto">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            {/* <thead className="table-dark"> */}
            <tr>
              <td className="px-6 py-3">ردیف</td>
              <td className="px-6 py-3">وضعیت</td>
              <td className="px-6 py-3">شماره موبایل</td>
              <td className="px-6 py-3">ایمیل</td>
              <td className="px-6 py-3">کیفیت</td>
              <td className="px-6 py-3">قیمت</td>
              <td className="px-6 py-3">تاریخ</td>
            </tr>
          </thead>
          <tbody>
            {momentItem}
            {!momentData ||
              (momentData.length === 0 && (
                <tr>
                  <td colSpan="7" className="px-6 py-3">
                    <span style={{ margin: "10px 30px", fontWeight: "bold" }}>
                      {" "}
                      رکورد فروش برای این فیلم وجود ندارد
                    </span>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
      {momentData && momentData.length > 0 && (
        <div>
          <Pagination
            active={page}
            total={totalRecord}
            perPage={50}
            url={downloadColleagueMomentRepoUrl(movieUid)}
          />
        </div>
      )}
    </div>
  );
};
export default DownloadColleagueMomentRepo;
