import React, { memo } from "react";

import DownloadTotalPayment from "components/Download/DownloadTotalPayment/DownloadTotalPayment";
import Loading from "components/Loading/Loading";

const ReportColleaguePanel = ({ colleagueSale, loading }) => {
  return (
    <>
      <div className="grid grid-cols-2 gap-3 ">
        <div className="col-span-2 pr-3 divide-y-4 divide-teal-600  my-4">
          <h1 className="">پنل آمار فروش</h1>
          {/* <hr /> */}
          {loading && <Loading />}
        </div>
        {!loading && colleagueSale && <DownloadTotalPayment accounting={colleagueSale} />}
      </div>
    </>
  );
};

export default memo(ReportColleaguePanel);
