import React, { Component } from "react";
import { moviePoster } from "services/helper";
import Image from "next/image";
export default class MoviePoster extends Component {
  render() {
    const { small, medium, large, src, width, height, className } = this.props;
    const restWidth = ((small, medium, large, width) => {
      if (width) {
        return width;
      }
      if (small) {
        return 40;
      }
      if (medium) {
        return 60;
      }
      if (large) {
        return 150;
      }
      return 40;
    })(small, medium, large, width);
    const resHeight = ((small, medium, large, height) => {
      if (height) {
        return height;
      }
      if (small) {
        return 59;
      }
      if (medium) {
        return 88;
      }
      if (large) {
        return 221;
      }
      return 59;
    })(small, medium, large, height);

    const resSrc = ((small, medium, large, src) => {
      if (small) {
        return moviePoster(src && src.small ? src.small : "");
      }
      if (medium) {
        return moviePoster(src && src.medium ? src.medium : "");
      }
      if (large) {
        return moviePoster(src && src.large ? src.large : "");
      }
      return moviePoster(src);
    })(small, medium, large, src);

    const addDefaultSrc = ev => {
      ev.target.src = moviePoster("");
    };

    return (
      <>
        <Image
          onError={addDefaultSrc}
          src={resSrc}
          width={restWidth}
          height={resHeight}
          className={className}
        />
      </>
    );
  }
}
