import React from "react";
import Link from "next/link";

const SidebarDropdownItem = ({ toggle, data, toggleSidebar }) => {
  return (
    <ul className={toggle ? "" : "hidden"}>
      {data?.map((item, index) => {
        return (
          <li className="mr-3" onClick={toggleSidebar} key={index}>
            <Link href={item.link}>
              <a className="flex items-center p-2 text-xs font-normal text-slate-300 dark:text-white hover:bg-sky-500 dark:hover:bg-gray-700">
                {item.icon}
                <span className="flex-1 ml-3 whitespace-nowrap">{item.title}</span>
              </a>
            </Link>
          </li>
        );

        // return <SidebarItem link={item.link} icon={item.icon} title={item.title} dropdown={item.dropdown} />;
      })}
    </ul>
  );
};
export default SidebarDropdownItem;
