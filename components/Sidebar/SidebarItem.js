import React from "react";
import { useDispatch } from "react-redux";
import Link from "next/link";

import SidebarDropdownItem from "components/Sidebar/SidebarDropdownItem";
import { setSidebarOpenAction } from "posts/actions/menu";

const SidebarItem = ({ id, link, icon, title, dropdown, dropdownItems }) => {
  const [toggle, setToggle] = React.useState(false);
  const dispatch = useDispatch();

  const handleClick = () => {
    setToggle(!toggle);
  };
  const toggleSidebar = ({ isOpen }) => {
    dispatch(setSidebarOpenAction(!isOpen));
  };
  return (
    <li>
      {dropdown && (
        <>
          <button
            type="button"
            className="flex items-center p-2 w-full text-base font-normal text-gray-900 transition duration-75 group hover:bg-sky-500 dark:text-white dark:hover:bg-gray-700"
            aria-controls={id}
            // data-collapse-toggle={toggle}
            onClick={handleClick}
          >
            <div
              // href={link}
              className="flex items-center text-sm font-normal text-slate-300 dark:text-white hover:bg-sky-500 dark:hover:bg-gray-700"
            >
              {icon}
              <span className="flex-1 ml-3 whitespace-nowrap">{title}</span>
              <svg
                sidebar-toggle-item=""
                className="w-6 h-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </div>
          </button>
          <SidebarDropdownItem
            id={id}
            data={dropdownItems}
            toggle={toggle}
            toggleSidebar={toggleSidebar}
          />
        </>
      )}
      {!dropdown && (
        <Link href={link}>
          <a className="flex items-center p-2 text-sm font-normal text-slate-300 dark:text-white hover:bg-sky-500 dark:hover:bg-gray-700">
            {icon}
            <span className="flex-1 ml-3 whitespace-nowrap">{title}</span>
          </a>
        </Link>
      )}
    </li>
  );
};
export default SidebarItem;
