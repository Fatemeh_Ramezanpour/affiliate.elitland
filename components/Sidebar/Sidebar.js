import React, { Component } from "react";
import { connect } from "react-redux";

import UserImage from "components/User/UserImage";
import { colleagueLinkUrl } from "services/url";
import { logout } from "posts/actions/auth";

import SidebarItem from "components/Sidebar/SidebarItem";
// import { FaLink, FaHome, FaTelegramPlane } from "react-icons/fa";
import {
  FaShoppingBasket,
  FaFileInvoiceDollar,
  FaLink,
  FaHome,
  FaTelegramPlane
} from "react-icons/fa";

import styles from "./Sidebar.module.scss";
// import style from "./Sidebar.module.css";
import { settlement, report } from "const/mockData/dropdown";

class Sidebar extends Component {
  logOut = () => {
    const { dispatch } = this.props;
    dispatch(logout());
  };

  render() {
    // console.log('=============settlementsettlement=======================');
    // console.log(settlement);
    // console.log('====================================');
    const { isOpen, toggle, user } = this.props;
    // let subMenuItem = [];
    // subMenuItem = submenuItems.map(item => {
    //   return (
    //     <React.Fragment key={item.id}>
    //       <SubMenu item={item} SubIcon={item.icon} sumMenuData={submenuItems} params={params} />
    //     </React.Fragment>
    //   );
    // });

    return (
      <div
        className={`${
          isOpen
            ? [
                styles.sidebar,
                styles.isOpen,
                "hidden md:block lg:block xl:block 2xl:block  bg-slate-800 dark:bg-gray-800"
              ].join(" ")
            : [
                styles.sidebar,
                styles.isOpen,
                "block sm:block md:hidden lg:hidden xl:hidden 2xl:hidden  bg-slate-800 dark:bg-gray-800"
              ].join(" ")
        }`}
      >
        {/* <div */}
        {/* //         className={`${ */}
        {/* //           isOpen
//             ? [styles.sidebar, styles.isOpen, "d-none d-sm-none d-lg-block d-xl-block "].join(" ")
//             : [styles.sidebar, styles.isOpen, "d-block d-sm-block d-lg-none d-xl-none "].join(" ")
//         }`}
//       > */}
        <aside className="md:w-64" aria-label="Sidebar">
          <div className="overflow-y-auto py-4 px-3 ">
            <div className={styles.sidebarHeader}>
              <span color="info" onClick={toggle} style={{ color: "#fff" }}>
                &times;
              </span>
              {/* <h3></h3> */}
              {user && (
                <div className={styles.userInfo}>
                  <UserImage src={user.user_img} className={styles.userInfoItem} />
                  <span className={["ml-2", styles.flexItem].join(" ")}>{user.fullname}</span>
                  {/* <span className={["ml-2", styles.flexItem].join(" ")}>خروج</span> */}
                  <button
                    color="link"
                    onClick={this.logOut}
                    className={["ml-2", styles.flexItem, styles.btnStl].join(" ")}
                  >
                    خروج
                  </button>
                </div>
              )}
            </div>
            <ul className="space-y-2">
              <SidebarItem
                link={"/"}
                icon={<FaHome className="ml-2" width="13px" height="13px" />}
                title="داشبورد"
              />
              <SidebarItem
                link={"#"}
                icon={<FaShoppingBasket className="ml-2" width="13px" height="13px" />}
                title="تسویه"
                dropdown={true}
                dropdownItems={settlement}
                isOpen={isOpen}
              />
              <SidebarItem
                link={"#"}
                icon={<FaFileInvoiceDollar className="ml-2" width="13px" height="13px" />}
                title="درآمدها"
                dropdown={true}
                dropdownItems={report}
                isOpen={isOpen}
              />
              <SidebarItem
                link={colleagueLinkUrl()}
                icon={<FaLink className="ml-2" width="13px" height="13px" />}
                title="لینک‌ها"
              />
              <SidebarItem
                link={"https://t.me/salamcinema_download"}
                icon={<FaTelegramPlane className="ml-2" width="13px" height="13px" />}
                title="کانال تلگرام"
              />
            </ul>
          </div>
        </aside>
      </div>
    );
  }
}
const mapStateToProps = () => ({
  // isOpen: state.menu.isOpen
});
export default connect(mapStateToProps)(Sidebar);

// import React, { Component } from "react";
// import { connect } from "react-redux";
// import Link from "next/link";

// import SubMenu from "components/Sidebar/SubMenu";
// import UserImage from "components/User/UserImage";
// import { homeUrl, colleagueLinkUrl } from "services/url";
// import { logout } from "posts/actions/auth";

// import { FaLink, FaHome, FaTelegramPlane } from "react-icons/fa";

// import NavLink from "reactstrap/lib/NavLink";
// import NavItem from "reactstrap/lib/NavItem";
// import Nav from "reactstrap/lib/Nav";
// import Button from "reactstrap/lib/Button";
// import styles from "./Sidebar.module.scss";
// // import style from "./Sidebar.module.css";

// class Sidebar extends Component {
//   logOut = () => {
//     const { dispatch } = this.props;
//     dispatch(logout());
//   };

//   render() {
//     const { isOpen, toggle, submenuItems, user, params } = this.props;
//     let subMenuItem = [];
//     subMenuItem = submenuItems.map(item => {
//       return (
//         <React.Fragment key={item.id}>
//           <SubMenu item={item} SubIcon={item.icon} sumMenuData={submenuItems} params={params} />
//         </React.Fragment>
//       );
//     });

//     return (
//       <div
//         className={`${
//           isOpen
//             ? [styles.sidebar, styles.isOpen, "d-none d-sm-none d-lg-block d-xl-block "].join(" ")
//             : [styles.sidebar, styles.isOpen, "d-block d-sm-block d-lg-none d-xl-none "].join(" ")
//         }`}
//       >
//         <div className={styles.sidebarHeader}>
//           <span color="info" onClick={toggle} style={{ color: "#fff" }}>
//             &times;
//           </span>
//           {/* <h3></h3> */}
//           {user && (
//             <div className={styles.userInfo}>
//               <UserImage src={user.user_img} className={styles.userInfoItem} />
//               <span className={["ml-2", styles.flexItem].join(" ")}>{user.fullname}</span>
//               {/* <span className={["ml-2", styles.flexItem].join(" ")}>خروج</span> */}
//               <Button
//                 color="link"
//                 onClick={this.logOut}
//                 className={["ml-2", styles.flexItem, styles.btnStl].join(" ")}
//               >
//                 خروج
//               </Button>
//             </div>
//           )}
//         </div>
//         <div className={styles.sideMenu}>
//           <Nav vertical className={[styles.listUnstyled, "pb-3"].join(" ")}>
//             {/* <p>Dummy Heading</p> */}
//             <NavItem>
//               <NavLink href={homeUrl()} className={styles.alignNav}>
//                 <FaHome className="ml-2" width="13px" height="13px" />
//                 {/* <FontAwesomeIcon icon={faBriefcase}  /> */}
//                 داشبورد
//               </NavLink>
//             </NavItem>
//             {subMenuItem}
//             {/* <SubMenu title="تسویه" icon={faCopy} items={submenus[0]} toggleMenu={toggle} sumMenuData={submenus} />
//             <SubMenu title="" icon={faImage} items={submenus[1]} toggleMenu={toggle} sumMenuData={submenus}/> */}
//             {/* <NavItem>
//           <NavLink href="#" >
//           <FontAwesomeIcon icon={faImage} className="mr-2" />
//             Portfolio
//           </NavLink>
//         </NavItem> */}
//             <NavItem className="nav-link">
//               <NavLink tag={Link} href={colleagueLinkUrl()}>
//                 <a>
//                   <FaLink className="ml-2" width="13px" height="13px" />
//                   {/* <FontAwesomeIcon icon={faLink}  /> */}
//                   لینک‌ها
//                 </a>
//               </NavLink>
//             </NavItem>
//             <NavItem>
//               <NavLink href={"https://t.me/salamcinema_download"}>
//                 <FaTelegramPlane className="ml-2" width="13px" height="13px" />
//                 کانال تلگرام
//               </NavLink>
//             </NavItem>
//           </Nav>
//         </div>
//       </div>
//     );
//   }
// }
// const mapStateToProps = () => ({
//   // isOpen: state.menu.isOpen
// });
// export default connect(mapStateToProps)(Sidebar);
