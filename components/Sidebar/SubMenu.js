import React, { Component } from "react";
import { connect } from "react-redux";
import Link from "next/link";

import { setSidebarOpenAction, setSubmenuOpenAction } from "posts/actions/menu";

// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Collapse from "reactstrap/lib/Collapse";
import NavItem from "reactstrap/lib/NavItem";
import NavLink from "reactstrap/lib/NavLink";

// import styles from "./Sidebar.module.scss";
import styles from "./SubMenu.module.scss";

class SubMenu extends Component {
  toggleSidebar = () => {
    const { dispatch, isOpen } = this.props;
    dispatch(setSidebarOpenAction(!isOpen));
  };

  toggle = id => {
    const { dispatch, item } = this.props;
    dispatch(setSubmenuOpenAction(id, !item.collapsed));
  };

  render() {
    const { item, SubIcon } = this.props;
    return (
      <div>
        <NavItem
          onClick={() => this.toggle(item.id)}
          className={[styles.dropdownToggle, `${!item.collapsed ? styles.menuOpen : ""}`].join(" ")}
        >
          <NavLink>
            <SubIcon className="ml-2" />
            {item.mainTitle}
          </NavLink>
        </NavItem>
        <Collapse
          isOpen={!item.collapsed}
          navbar
          className={[styles.itemsMenu, `${!item.collapsed ? "mb-1" : ""}`].join(" ")}
        >
          {item.subItem.map((item, index) => (
            <NavItem
              key={index}
              className={["pl-4", "nav-link"].join(" ")}
              onClick={this.toggleSidebar}
            >
              <NavLink tag={Link} href={item.target}>
                <a>{item.title}</a>
              </NavLink>
            </NavItem>
          ))}
        </Collapse>
      </div>
    );
  }
}
const mapStateToProps = () => ({});

export default connect(mapStateToProps)(SubMenu);
