import React, { Component } from "react";

import { IoIosClose } from "react-icons/io";
import classNames from "classnames";
import { closeLoginModal } from "posts/actions/dialogLogin";
import { connect } from "react-redux";
import styles from "./LoginModal.module.scss";

class LoginModal extends Component {
  handleCloseModal = () => {
    const { dispatch } = this.props;
    dispatch(closeLoginModal());
  };

  render() {
    const { open, contentClass, backgroundClass } = this.props;

    return (
      <div className={open ? styles.display : styles.displayNone}>
        <div
          className={classNames(
            styles.background,
            { [backgroundClass]: backgroundClass },
            { [styles.modalBgColor]: !backgroundClass }
          )}
        >
          <div className={classNames(styles.modalBox, contentClass)}>{this.props.children}</div>
          <IoIosClose className={styles.place} onClick={() => this.handleCloseModal()} />
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  open: state.dialog.openLoginModal
});
export default connect(mapStateToProps)(LoginModal);
