/* eslint-disable react/no-string-refs */
import React, { Component } from "react";
import Dropzone from "react-dropzone";
import { TiPlus } from "react-icons/ti";

export default class UploadImage extends Component {
  constructor() {
    super();
    this.state = { files: [] };
  }

  onDrop = files => {
    this.setState({
      files
    });
    if (this.props.input) {
      this.props.input.onChange(files);
    } else {
      this.props.onDrop(files);
    }
  };

  render() {
    const { files } = this.state;
    return (
      <section>
        <div className="dropzone">
          <Dropzone onDrop={this.onDrop.bind(this)} accept="image/*" ref="dropzone">
            <p style={{ textAlign: "center", marginTop: "20%" }}>برای آپلود عکس کلیک کنید</p>
            {files.length === 0 && (
              <TiPlus
                style={{
                  fontSize: "50px",
                  color: "#00a152",
                  position: "absolute",
                  top: "12%",
                  bottom: "0",
                  right: "0",
                  left: "0",
                  margin: "auto"
                }}
              />
            )}
            {files.length > 0 && (
              <div className="selectImgPreviewBox">
                {files.map((f, index) => (
                  <span key={index}>
                    <img className="selectImgPreview" src={f.preview} alt="upload" />
                  </span>
                ))}
                <style jsx>{`
                  // .selectImgPreviewBox{
                  //   width: 75px;
                  //   height: 75px;
                  //   display: inline-block;
                  //   margin: 10px;
                  //   border-radius: 5px;
                  //   position: relative;
                  // }
                  .selectImgPreview {
                    width: 75px;
                    height: 75px;
                    border-radius: 5px;
                    position: absolute;
                    top: 12%;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    margin: auto;
                  }
                `}</style>
              </div>
            )}
          </Dropzone>
        </div>
      </section>
    );
  }
}
