module.exports = {
  presets: ["next/babel"],
  plugins: [
    ["module-resolver", { root: ["./"] }],
    ["babel-plugin-styled-components", { ssr: true }],
    ["@babel/plugin-proposal-optional-chaining"]
  ]
};
