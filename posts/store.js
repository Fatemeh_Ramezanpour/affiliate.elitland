// /* eslint-disable no-use-before-define */
// import { applyMiddleware, createStore } from "redux";

// import api from "services/api";
// import createSagaMiddleware from "redux-saga";
// import rootReducer from "./reducers/rootReducer";
// import rootSaga from "./sagas/index";
// import { actionTypes } from "posts/actions/auth";

// const sagaMiddleware = createSagaMiddleware();

// const customMiddleWare = () => next => action => {
//   if (action.type === "LOGIN_SUCCESS") {
//     api.setToken(action.data.token);
//     // Dispatch(setToken(cookies(ctx).token_elitland));
//   }

//   next(action);
//   // Return next(action);
// };

// const bindMiddleware = middleware => {
//   if (process.env.NODE_ENV !== "production") {
//     const { composeWithDevTools } = require("redux-devtools-extension");
//     return composeWithDevTools(applyMiddleware(...middleware));
//   }

//   return applyMiddleware(...middleware);
// };

// function configureStore(initialState) {
//   const store = createStore(
//     rootReducer,
//     initialState,
//     bindMiddleware([sagaMiddleware, customMiddleWare])
//   );

//   store.runSagaTask = () => {
//     store.sagaTask = sagaMiddleware.run(rootSaga);
//   };

//   store.runSagaTask();
//   return store;
// }

// export default configureStore;

import { applyMiddleware, createStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { createWrapper } from "next-redux-wrapper";

import rootReducer from "posts/reducers/rootReducer";
import rootSaga from "posts/sagas";

import api from "services/api";
// import apiCall from 'services/apiConf/apiCall';
import { actionTypes } from "posts/actions/auth";
const customMiddleWare = () => next => action => {
  if (action.type === actionTypes.LOGIN_SUCCESS) {
    api.setToken(action.data.token);
  }
  next(action);
  // Return next(action);
};

const bindMiddleware = middleware => {
  if (process.env.NODE_ENV !== "production") {
    const { composeWithDevTools } = require("redux-devtools-extension");
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};
export const makeStore = (initialState = initialState) => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducer,
    initialState,
    bindMiddleware([sagaMiddleware, customMiddleWare])
  );

  store.sagaTask = sagaMiddleware.run(rootSaga);

  return store;
};

export const wrapper = createWrapper(makeStore, { debug: false });
