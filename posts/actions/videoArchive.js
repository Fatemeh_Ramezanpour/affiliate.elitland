export const LOAD_VIDEO = "LOAD_VIDEO";
export const LOAD_VIDEO_SUCCESS = "LOAD_VIDEO_SUCCESS";
export const LOAD_VIDEO_FAIL = "LOAD_VIDEO_FAIL";

export const LOAD_VIDEO_DETAIL = "LOAD_VIDEO_DETAIL";
export const LOAD_VIDEO_DETAIL_SUCCESS = "LOAD_VIDEO_DETAIL_SUCCESS";
export const LOAD_VIDEO_DETAIL_FAIL = "LOAD_VIDEO_DETAIL_FAIL";

//video archive
export function loadVideoAction(page, perpage, videoType) {
  return {
    type: LOAD_VIDEO,
    page,
    perpage,
    videoType
  };
}

export function loadVideoActionSuccess(data, videoType) {
  return {
    type: LOAD_VIDEO_SUCCESS,
    data,
    videoType
  };
}

export function loadVideoActionFail(error, videoType) {
  return {
    type: LOAD_VIDEO_FAIL,
    error,
    videoType
  };
}

// Get Video Detail

export function loadVideoDetail(data) {
  return {
    type: LOAD_VIDEO_DETAIL,
    data
  };
}

export function loadVideoDetailSuccess(data) {
  return {
    type: LOAD_VIDEO_DETAIL_SUCCESS,
    data
  };
}

export function loadVideoDetailFail() {
  return {
    type: LOAD_VIDEO_DETAIL_FAIL
  };
}
