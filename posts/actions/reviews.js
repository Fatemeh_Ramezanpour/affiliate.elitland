export const LOAD_PERSIAN_REVIEWS_ARCHIVE = "LOAD_PERSIAN_REVIEWS_ARCHIVE";
export const LOAD_PERSIAN_REVIEWS_ARCHIVE_SUCCESS = "LOAD_PERSIAN_REVIEWS_ARCHIVE_SUCCESS";
export const LOAD_PERSIAN_REVIEWS_ARCHIVE_FAIL = "LOAD_PERSIAN_REVIEWS_ARCHIVE_FAIL";

export const LOAD_FOREIGN_REVIEWS_ARCHIVE = "LOAD_FOREIGN_REVIEWS_ARCHIVE";
export const LOAD_FOREIGN_REVIEWS_ARCHIVE_SUCCESS = "LOAD_FOREIGN_REVIEWS_ARCHIVE_SUCCESS";
export const LOAD_FOREIGN_REVIEWS_ARCHIVE_FAIL = "LOAD_FOREIGN_REVIEWS_ARCHIVE_FAIL";

export const LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE = "LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE";
export const LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_SUCCESS = "LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_SUCCESS";
export const LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_FAIL = "LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_FAIL";

export const LOAD_PEOPLE_REVIEWS_ARCHIVE = "LOAD_PEOPLE_REVIEWS_ARCHIVE";
export const LOAD_PEOPLE_REVIEWS_ARCHIVE_SUCCESS = "LOAD_PEOPLE_REVIEWS_ARCHIVE_SUCCESS";
export const LOAD_PEOPLE_REVIEWS_ARCHIVE_FAIL = "LOAD_PEOPLE_REVIEWS_ARCHIVE_FAIL";

export const LOAD_ARTICLE_REVIEWS_ARCHIVE = "LOAD_ARTICLE_ARCHIVE";
export const LOAD_ARTICLE_REVIEWS_ARCHIVE_SUCCESS = "LOAD_ARTICLE_ARCHIVE_SUCCESS";
export const LOAD_ARTICLE_REVIEWS_ARCHIVE_FAIL = "LOAD_ARTICLE_ARCHIVE_FAIL";

export const LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE = "LOAD_ALL_ARTICLE_ARCHIVE";
export const LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE_SUCCESS = "LOAD_ALL_ARTICLE_ARCHIVE_SUCCESS";
export const LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE_FAIL = "LOAD_ALL_ARTICLE_ARCHIVE_FAIL";

export const LOAD_RECENT_REVIEWS_ARCHIVE = "LOAD_RECENT_REVIEWS_ARCHIVE";
export const LOAD_RECENT_REVIEWS_ARCHIVE_SUCCESS = "LOAD_RECENT_REVIEWS_ARCHIVE_SUCCESS";
export const LOAD_RECENT_REVIEWS_ARCHIVE_FAIL = "LOAD_RECENT_REVIEWS_ARCHIVE_FAIL";

export const LOAD_RECENT_ARTICLES_ARCHIVE = "LOAD_RECENT_ARTICLES_ARCHIVE";
export const LOAD_RECENT_ARTICLES_ARCHIVE_SUCCESS = "LOAD_RECENT_ARTICLES_ARCHIVE_SUCCESS";
export const LOAD_RECENT_ARTICLES_ARCHIVE_FAIL = "LOAD_RECENT_ARTICLES_ARCHIVE_FAIL";

// Persian reviews archive
export function loadPersianReviewsArchive(data) {
  return {
    type: LOAD_PERSIAN_REVIEWS_ARCHIVE,
    data
  };
}

export function loadPersianReviewsArchiveSuccess(data) {
  return {
    type: LOAD_PERSIAN_REVIEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadPersianReviewsArchiveFail(error) {
  return {
    type: LOAD_PERSIAN_REVIEWS_ARCHIVE_FAIL,
    error
  };
}

// Foreign reviews archive
export function loadForeignReviewsArchive(data) {
  return {
    type: LOAD_FOREIGN_REVIEWS_ARCHIVE,
    data
  };
}

export function loadForeignReviewsArchiveSuccess(data) {
  return {
    type: LOAD_FOREIGN_REVIEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadForeignReviewsArchiveFail(error) {
  return {
    type: LOAD_FOREIGN_REVIEWS_ARCHIVE_FAIL,
    error
  };
}

// People reviews archive
export function loadPeopleReviewsArchive(data) {
  return {
    type: LOAD_PEOPLE_REVIEWS_ARCHIVE,
    data
  };
}

export function loadPeopleReviewsArchiveSuccess(data) {
  return {
    type: LOAD_PEOPLE_REVIEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadPeopleReviewsArchiveFail() {
  return {
    type: LOAD_PEOPLE_REVIEWS_ARCHIVE_FAIL
  };
}

// All people reviews archive
export function loadAllPeopleReviewsArchive(data) {
  return {
    type: LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE,
    data
  };
}

export function loadAllPeopleReviewsArchiveSuccess(data) {
  return {
    type: LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadAllPeopleReviewsArchiveFail(error) {
  return {
    type: LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_FAIL,
    error
  };
}

// Article reviews archive
export function loadArticleReviewsArchive(data) {
  return {
    type: LOAD_ARTICLE_REVIEWS_ARCHIVE,
    data
  };
}

export function loadArticleReviewsArchiveSuccess(data) {
  return {
    type: LOAD_ARTICLE_REVIEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadArticleReviewsArchiveFail() {
  return {
    type: LOAD_ARTICLE_REVIEWS_ARCHIVE_FAIL
  };
}

// All article reviews archive
export function loadAllArticleReviewsArchive(data) {
  return {
    type: LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE,
    data
  };
}

export function loadAllArticleReviewsArchiveSuccess(data) {
  return {
    type: LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadAllArticleReviewsArchiveFail(error) {
  return {
    type: LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE_FAIL,
    error
  };
}

// All recent reviews archive
export function loadRecentReviewsArchive(data) {
  return {
    type: LOAD_RECENT_REVIEWS_ARCHIVE,
    data
  };
}

export function loadRecentReviewsArchiveSuccess(data) {
  return {
    type: LOAD_RECENT_REVIEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadRecentReviewsArchiveFail() {
  return {
    type: LOAD_RECENT_REVIEWS_ARCHIVE_FAIL
  };
}

// All recent articles archive
export function loadRecentArticlesArchive(data) {
  return {
    type: LOAD_RECENT_ARTICLES_ARCHIVE,
    data
  };
}

export function loadRecentArticlesArchiveSuccess(data) {
  return {
    type: LOAD_RECENT_ARTICLES_ARCHIVE_SUCCESS,
    data
  };
}

export function loadRecentArticlesArchiveFail() {
  return {
    type: LOAD_RECENT_ARTICLES_ARCHIVE_FAIL
  };
}
