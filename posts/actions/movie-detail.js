/* eslint-disable no-undef */
export const LOAD_MOVIE_DETAIL_DATA = "LOAD_MOVIE_DETAIL_DATA";
export const LOAD_MOVIE_DETAIL_DATA_SUCCESS = "LOAD_MOVIE_DETAIL_DATA_SUCCESS";
export const LOAD_MOVIE_DETAIL_DATA_FAIL = "LOAD_MOVIE_DETAIL_DATA_FAIL";

export const LOAD_FIND_BY_MOVIE_DATA = "LOAD_FIND_BY_MOVIE_DATA";
export const LOAD_FIND_BY_MOVIE_DATA_SUCCESS = "LOAD_FIND_BY_MOVIE_DATA_SUCCESS";
export const LOAD_FIND_BY_MOVIE_DATA_FAIL = "LOAD_FIND_BY_MOVIE_DATA_FAIL";

// export const LOAD_COMMENTS_MOVIE_DATA = "LOAD_COMMENTS_MOVIE_DATA";
// export const LOAD_COMMENTS_MOVIE_DATA_SUCCESS = "LOAD_COMMENTS_MOVIE_DATA_SUCCESS";
// export const LOAD_COMMENTS_MOVIE_DATA_FAIL = "LOAD_COMMENTS_MOVIE_DATA_FAIL";

export const SAVE_MOVIE_DETAIL_VOTE = "SAVE_MOVIE_DETAIL_VOTE";
export const SAVE_MOVIE_DETAIL_VOTE_SUCCESS = "SAVE_MOVIE_DETAIL_VOTE_SUCCESS";
export const SAVE_MOVIE_DETAIL_VOTE_FAIL = "SAVE_MOVIE_DETAIL_VOTE_FAIL";

export const UPDATE_MOVIE_DETAIL_VOTE = "UPDATE_MOVIE_DETAIL_VOTE";
export const UPDATE_MOVIE_DETAIL_VOTE_SUCCESS = "UPDATE_MOVIE_DETAIL_VOTE_SUCCESS";
export const UPDATE_MOVIE_DETAIL_VOTE_FAIL = "UPDATE_MOVIE_DETAIL_VOTE_FAIL";

export const FIND_VIDEO = "FIND_VIDEO";

// Load data movie
export function loadMovieDetailData(data) {
  return {
    type: LOAD_MOVIE_DETAIL_DATA,
    data
  };
}

export function loadMovieDetailDataSuccess(data) {
  return {
    type: LOAD_MOVIE_DETAIL_DATA_SUCCESS,
    data
  };
}

export function loadMovieDetailDataFail() {
  return {
    type: LOAD_MOVIE_DETAIL_DATA_FAIL
  };
}

// Load data vidoes of movie
export function loadFindByMovieData(data) {
  return {
    type: LOAD_FIND_BY_MOVIE_DATA,
    data
  };
}

export function loadFindByMovieDataSuccess(data) {
  return {
    type: LOAD_FIND_BY_MOVIE_DATA_SUCCESS,
    data
  };
}

export function loadFindByMovieDataFail() {
  return {
    type: LOAD_FIND_BY_MOVIE_DATA_FAIL
  };
}

// Load data comments of movie
// export function loadCommentsMovieData(data) {
//   return {
//     type: LOAD_COMMENTS_MOVIE_DATA,
//     data
//   };
// }

// export function loadCommentsMovieDataSuccess(data) {
//   return {
//     type: LOAD_COMMENTS_MOVIE_DATA_SUCCESS,
//     data
//   };
// }

// export function loadCommentsMovieDataFail() {
//   return {
//     type: LOAD_COMMENTS_MOVIE_DATA_FAIL
//   };
// }

// Save Vote For Movie
export function saveMovieDetailVote(data, uid) {
  return {
    type: SAVE_MOVIE_DETAIL_VOTE,
    data,
    uid
  };
}

export function saveMovieDetailVoteSuccess(data) {
  return {
    type: SAVE_MOVIE_DETAIL_VOTE_SUCCESS,
    data
  };
}

export function saveMovieDetailVoteFail() {
  return {
    type: SAVE_MOVIE_DETAIL_VOTE_FAIL,
    err
  };
}

// Update Vote For Movie
export function updateMovieDetailVote(data, uid) {
  return {
    type: UPDATE_MOVIE_DETAIL_VOTE,
    data,
    uid
  };
}

export function updateMovieDetailVoteSuccess(data) {
  return {
    type: UPDATE_MOVIE_DETAIL_VOTE_SUCCESS,
    data
  };
}

export function updateMovieDetailVoteFail() {
  return {
    type: UPDATE_MOVIE_DETAIL_VOTE_FAIL,
    err
  };
}

// Find video
export function findVideo(id) {
  return {
    type: FIND_VIDEO,
    id
  };
}
