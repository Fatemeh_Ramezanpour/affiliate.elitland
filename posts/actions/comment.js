export const LOAD_COMMENTS_MOVIE_DATA = "LOAD_COMMENTS_MOVIE_DATA";
export const LOAD_COMMENTS_MOVIE_DATA_SUCCESS = "LOAD_COMMENTS_MOVIE_DATA_SUCCESS";
export const LOAD_COMMENTS_MOVIE_DATA_FAIL = "LOAD_COMMENTS_MOVIE_DATA_FAIL";

export const SAVE_COMMENTS_MOVIE_DATA = "SAVE_COMMENTS_MOVIE_DATA";
export const SAVE_COMMENTS_MOVIE_DATA_SUCCESS = "SAVE_COMMENTS_MOVIE_DATA_SUCCESS";
export const SAVE_COMMENTS_MOVIE_DATA_FAIL = "SAVE_COMMENTS_MOVIE_DATA_FAIL";

export const DELETE_COMMENTS_MOVIE_DATA = "DELETE_COMMENTS_MOVIE_DATA";
export const DELETE_COMMENTS_MOVIE_DATA_SUCCESS = "DELETE_COMMENTS_MOVIE_DATA_SUCCESS";
export const DELETE_COMMENTS_MOVIE_DATA_FAIL = "DELETE_COMMENTS_MOVIE_DATA_FAIL";
export const OPEN_REPLY_FORM = "OPEN_REPLY_FORM";

// Load data comments of movie
export function loadCommentsMovieData(data) {
  return {
    type: LOAD_COMMENTS_MOVIE_DATA,
    data
  };
}

export function loadCommentsMovieDataSuccess(data) {
  return {
    type: LOAD_COMMENTS_MOVIE_DATA_SUCCESS,
    data
  };
}

export function loadCommentsMovieDataFail() {
  return {
    type: LOAD_COMMENTS_MOVIE_DATA_FAIL
  };
}

// Save data comments of movie
export function saveCommentsMovieData(data, uid) {
  return {
    type: SAVE_COMMENTS_MOVIE_DATA,
    data,
    uid
  };
}

export function saveCommentsMovieDataSuccess(data, uid) {
  return {
    type: SAVE_COMMENTS_MOVIE_DATA_SUCCESS,
    data,
    uid
  };
}

export function saveCommentsMovieDataFail() {
  return {
    type: SAVE_COMMENTS_MOVIE_DATA_FAIL
  };
}

// Delete data comments of movie
export function deleteCommentsMovieData(data, uid) {
  return {
    type: DELETE_COMMENTS_MOVIE_DATA,
    data,
    uid
  };
}

export function deleteCommentsMovieDataSuccess(data) {
  return {
    type: DELETE_COMMENTS_MOVIE_DATA_SUCCESS,
    data
  };
}

export function deleteCommentsMovieDataFail() {
  return {
    type: DELETE_COMMENTS_MOVIE_DATA_FAIL
  };
}

// Open Replay From
export function openReplayForm(CommentId) {
  return { type: OPEN_REPLY_FORM, CommentId };
}
