export const LOAD_PERSIAN_PROFILE_ARCHIVE = "LOAD_PERSIAN_PROFILE_ARCHIVE";
export const LOAD_PERSIAN_PROFILE_ARCHIVE_SUCCESS = "LOAD_PERSIAN_PROFILE_ARCHIVE_SUCCESS";
export const LOAD_PERSIAN_PROFILE_ARCHIVE_FAIL = "LOAD_PERSIAN_PROFILE_ARCHIVE_FAIL";

export const LOAD_FOREIGN_PROFILE_ARCHIVE = "LOAD_FOREIGN_PROFILE_ARCHIVE";
export const LOAD_FOREIGN_PROFILE_ARCHIVE_SUCCESS = "LOAD_FOREIGN_PROFILE_ARCHIVE_SUCCESS";
export const LOAD_FOREIGN_PROFILE_ARCHIVE_FAIL = "LOAD_FOREIGN_PROFILE_ARCHIVE_FAIL";

// Persian profile archive
export function loadPersianProfileArchive(data) {
  return {
    type: LOAD_PERSIAN_PROFILE_ARCHIVE,
    data
  };
}

export function loadPersianProfileArchiveSuccess(data) {
  return {
    type: LOAD_PERSIAN_PROFILE_ARCHIVE_SUCCESS,
    data
  };
}

export function loadPersianProfileArchiveFail(error) {
  return {
    type: LOAD_PERSIAN_PROFILE_ARCHIVE_FAIL,
    error
  };
}

// Foreign profile archive
export function loadForeignProfileArchive(data) {
  return {
    type: LOAD_FOREIGN_PROFILE_ARCHIVE,
    data
  };
}

export function loadForeignProfileArchiveSuccess(data) {
  return {
    type: LOAD_FOREIGN_PROFILE_ARCHIVE_SUCCESS,
    data
  };
}

export function loadForeignProfileArchiveFail(error) {
  return {
    type: LOAD_FOREIGN_PROFILE_ARCHIVE_FAIL,
    error
  };
}
