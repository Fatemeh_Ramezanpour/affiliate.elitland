export const actionTypes = {
  PANEL_PAYOFF: "PANEL_PAYOFF",
  PANEL_PAYOFF_SUCCESS: "PANEL_PAYOFF_SUCCESS",
  PANEL_PAYOFF_FAIL: "PANEL_PAYOFF_FAIL",

  PAYOFF_REPORT: "PAYOFF_REPORT",
  PAYOFF_REPORT_SUCCESS: "PAYOFF_REPORT_SUCCESS",
  PAYOFF_REPORT_FAIL: "PAYOFF_REPORT_FAIL",

  PAYOFF_ACCOUNTING: "PAYOFF_ACCOUNTING",
  PAYOFF_ACCOUNTING_SUCCESS: "PAYOFF_ACCOUNTING_SUCCESS",
  PAYOFF_ACCOUNTING_FAIL: "PAYOFF_ACCOUNTING_FAIL"
};

export function panelPayoffAction(data) {
  return {
    type: actionTypes.PANEL_PAYOFF,
    data
  };
}

export function panelPayoffActionSuccess(data) {
  return {
    type: actionTypes.PANEL_PAYOFF_SUCCESS,
    data
  };
}

export function panelPayoffActionFail(err) {
  return {
    type: actionTypes.PANEL_PAYOFF_FAIL,
    err
  };
}

//payoff report
export function payoffReportAction() {
  return {
    type: actionTypes.PAYOFF_REPORT
  };
}
export function payoffReportActionSuccess(data) {
  return {
    type: actionTypes.PAYOFF_REPORT_SUCCESS,
    data
  };
}

export function payoffReportActionFail(err) {
  return {
    type: actionTypes.PAYOFF_REPORT_FAIL,
    err
  };
}

//payoff report
export function loadPayoffAccountingAction() {
  return {
    type: actionTypes.PAYOFF_ACCOUNTING
  };
}
export function loadPayoffAccountingActionSuccess(data) {
  return {
    type: actionTypes.PAYOFF_ACCOUNTING_SUCCESS,
    data
  };
}

export function loadPayoffAccountingActionFail(err) {
  return {
    type: actionTypes.PAYOFF_ACCOUNTING_FAIL,
    err
  };
}
