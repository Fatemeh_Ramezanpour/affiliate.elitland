export const OPEN_LOGIN_MODAL = "OPEN_LOGIN_MODAL";
export const CLOSE_LOGIN_MODAL = "CLOSE_LOGIN_MODAL";
export const SHOW_VIDEO_MODAL_MOVIE = " SHOW_VIDEO_MODAL_MOVIE";

export function openLoginModal(mssg) {
  return {
    type: OPEN_LOGIN_MODAL,
    mssg
  };
}

export function closeLoginModal(mssg) {
  return {
    type: CLOSE_LOGIN_MODAL,
    mssg
  };
}

export function showVideoMdalInMovie() {
  return {
    type: SHOW_VIDEO_MODAL_MOVIE
  };
}
