export const actionTypes = {
  LOAD_DOWNLOAD_LIST: "LOAD_DOWNLOAD_LIST",
  LOAD_DOWNLOAD_LIST_SUCCESS: "LOAD_DOWNLOAD_LIST_SUCCESS",
  LOAD_DOWNLOAD_LIST_FAIL: "LOAD_DOWNLOAD_LIST_FAIL",
  LOAD_DOWNLOAD_COLLEAGUE_PANEL: "LOAD_DOWNLOAD_COLLEAGUE_PANEL",
  LOAD_DOWNLOAD_COLLEAGUE_PANEL_SUCCESS: "LOAD_DOWNLOAD_COLLEAGUE_PANEL_SUCCESS",
  LOAD_DOWNLOAD_COLLEAGUE_PANEL_FAIL: "LOAD_DOWNLOAD_COLLEAGUE_PANEL_FAIL",

  LOAD_DOWNLOAD_COMPANY_PANEL: "LOAD_DOWNLOAD_OMPANY_PANEL",
  LOAD_DOWNLOAD_COMPANY_PANEL_SUCCESS: "LOAD_DOWNLOAD_OMPANY_PANEL_SUCCESS",
  LOAD_DOWNLOAD_COMPANY_PANEL_FAIL: "LOAD_DOWNLOAD_COMPANY_PANEL_FAIL",

  LOAD_DOWNLOAD_COLLEAGUE_LINKS: "LOAD_DOWNLOAD_COLLEAGUE_LINKS",
  LOAD_DOWNLOAD_COLLEAGUE_LINKS_SUCCESS: "LOAD_DOWNLOAD_COLLEAGUE_LINKS_SUCCESS",
  LOAD_DOWNLOAD_COLLEAGUE_LINKS_FAIL: "LOAD_DOWNLOAD_COLLEAGUE_LINKS_FAIL",
  LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS: "LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS",
  LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS_SUCCESS: "LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS_SUCCESS",
  LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS_FAIL: "LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS_FAIL",

  LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS: "LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS",
  LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS_SUCCESS: "LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS_SUCCESS",
  LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS_FAIL: "LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS_FAIL",

  LOAD_DOWNLOAD_BUY_INFO: "LOAD_DOWNLOAD_BUY_INFO",
  LOAD_DOWNLOAD_BUY_INFO_SUCCESS: "LOAD_DOWNLOAD_BUY_INFO_SUCCESS",
  LOAD_DOWNLOAD_BUY_INFO_FAIL: "LOAD_DOWNLOAD_BUY_INFO_FAIL",

  LOAD_DOWNLOAD_MOMENT_INFO: "LOAD_DOWNLOAD_MOMENT_INFO",
  LOAD_DOWNLOAD_COLLEAGUE_MOMENT_INFO: "LOAD_DOWNLOAD_COLLEAGUE_MOMENT_INFO",
  LOAD_DOWNLOAD_MOMENT_INFO_SUCCESS: "LOAD_DOWNLOAD_MOMENT_INFO_SUCCESS",
  LOAD_DOWNLOAD_MOMENT_INFO_FAIL: "LOAD_DOWNLOAD_MOMENT_INFO_FAIL",

  LOAD_RECOMM_DOWNLOAD: "LOAD_RECOMM_DOWNLOAD",
  LOAD_RECOMM_DOWNLOAD_SUCCESS: "LOAD_RECOMM_DOWNLOAD_SUCCESS",
  LOAD_RECOMM_DOWNLOAD_FAIL: "LOAD_RECOMM_DOWNLOAD_FAIL",

  LOAD_CAMPAIGN_DOWNLOAD: "LOAD_CAMPAIGN_DOWNLOAD",
  LOAD_CAMPAIGN_DOWNLOAD_SUCCESS: "LOAD_CAMPAIGN_DOWNLOAD_SUCCESS",
  LOAD_CAMPAIGN_DOWNLOAD_FAIL: "LOAD_CAMPAIGN_DOWNLOAD_FAIL",

  LOAD_CAMPAIGN_DOWNLOAD_PURCHASED: "LOAD_CAMPAIGN_DOWNLOAD_PURCHASED",
  LOAD_CAMPAIGN_DOWNLOAD_PURCHASED_SUCCESS: "LOAD_CAMPAIGN_DOWNLOAD_PURCHASED_SUCCESS",
  LOAD_CAMPAIGN_DOWNLOAD_PURCHASED_FAIL: "LOAD_CAMPAIGN_DOWNLOAD_PURCHASED_FAIL",

  LOAD_COLLEAGUE_INFO: "LOAD_COLLEAGUE_INFO",
  LOAD_COLLEAGUE_INFO_SUCCESS: "LOAD_COLLEAGUE_INFO_SUCCESS",
  LOAD_COLLEAGUE_INFO_FAIL: "LOAD_COLLEAGUE_INFO_FAIL",

  LOAD_COMPANY_GENERAL_LOAD_DOWNLOAD_LIST: "LOAD_DOWNLOAD_LIST",

  LOAD_COMPANY_GENERAL_MOMENT_INFO: "LOAD_COMPANY_GENERAL_MOMENT_INFO",
  LOAD_COMPANY_GENERAL_MOMENT_INFO_SUCCESS: "LOAD_COMPANY_GENERAL_MOMENT_INFO_SUCCESS",
  LOAD_COMPANY_GENERAL_MOMENT_INFO_FAIL: "LOAD_COMPANY_GENERAL_MOMENT_INFO_FAIL",

  LOAD_DOWNLOAD_CATEGORY: "LOAD_DOWNLOAD_CATEGORY",
  LOAD_DOWNLOAD_CATEGORY_SUCCESS: "LOAD_DOWNLOAD_CATEGORY_SUCCESS",
  LOAD_DOWNLOAD_CATEGORY_FAIL: "LOAD_DOWNLOAD_CATEGORY_FAIL",

  LOAD_COLLEAGUE_REPORT: "LOAD_COLLEAGUE_REPORT",
  LOAD_COLLEAGUE_REPORT_SUCCESS: "LOAD_COLLEAGUE_REPORT_SUCCESS",
  LOAD_COLLEAGUE_REPORT_FAIL: "LOAD_COLLEAGUE_REPORT_FAIL",

  LOAD_MOVIE_COLLEAGUE_PANEL: "LOAD_MOVIE_COLLEAGUE_PANEL",
  LOAD_MOVIE_COLLEAGUE_PANEL_SUCCESS: "LOAD_MOVIE_COLLEAGUE_PANEL_SUCCESS",
  LOAD_MOVIE_COLLEAGUE_PANEL_FAIL: "LOAD_MOVIE_COLLEAGUE_PANEL_FAIL",

  LOAD_SERIES_COLLEAGUE_PANEL: "LOAD_SERIES_COLLEAGUE_PANEL",
  LOAD_SERIES_COLLEAGUE_PANEL_SUCCESS: "LOAD_SERIES_COLLEAGUE_PANEL_SUCCESS",
  LOAD_SERIES_COLLEAGUE_PANEL_FAIL: "LOAD_SERIES_COLLEAGUE_PANEL_FAIL"
  // MOMENT_INFO_FAIL: "LOAD_COMPANY_GENERAL_MOMENT_INFO_FAIL",
};

export function loadDownloadList(page, series_uid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_LIST,
    page,
    series_uid
  };
}

export function loadDownloadListSuccess(data, page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_LIST_SUCCESS,
    data,
    page
  };
}

export function loadDownloadListFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_LIST_FAIL,
    err
  };
}

export function loadDownloadColleaguePanel(page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL,
    page
  };
}

export function loadDownloadColleaguePanelSuccess(data, page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_SUCCESS,
    data,
    page
  };
}

export function loadDownloadColleaguePanelFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_FAIL,
    err
  };
}

export function loadMovieColleaguePanel(page) {
  return {
    type: actionTypes.LOAD_MOVIE_COLLEAGUE_PANEL,
    page
  };
}

export function loadMovieColleaguePanelSuccess(data, page) {
  return {
    type: actionTypes.LOAD_MOVIE_COLLEAGUE_PANEL_SUCCESS,
    data,
    page
  };
}

export function loadMovieColleaguePanelFail(err) {
  return {
    type: actionTypes.LOAD_MOVIE_COLLEAGUE_PANEL_FAIL,
    err
  };
}
//series loading
export function loadSeriesColleaguePanelAction(page, uid) {
  return {
    type: actionTypes.LOAD_SERIES_COLLEAGUE_PANEL,
    page,
    uid
  };
}

export function loadSeriesColleaguePanelActionSuccess(data, page) {
  return {
    type: actionTypes.LOAD_SERIES_COLLEAGUE_PANEL_SUCCESS,
    data,
    page
  };
}

export function loadSeriesColleaguePanelActionFail(err) {
  return {
    type: actionTypes.LOAD_SERIES_COLLEAGUE_PANEL_FAIL,
    err
  };
}

export function loadDownloadCompanyPanel(page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL,
    page
  };
}

export function loadDownloadCompanyPanelSuccess(data, page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_SUCCESS,
    data,
    page
  };
}

export function loadDownloadCompanyPanelFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_FAIL,
    err
  };
}
export function loadColleagueReport() {
  return {
    type: actionTypes.LOAD_COLLEAGUE_REPORT
  };
}

export function loadColleagueReportSuccess(data) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_REPORT_SUCCESS,
    data
  };
}

export function loadColleagueReportFail(err) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_REPORT_FAIL,
    err
  };
}

export function loadDownloadColleagueLinks(page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_LINKS,
    page
  };
}

export function loadDownloadColleagueLinksSuccess(data, page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_LINKS_SUCCESS,
    data,
    page
  };
}

export function loadDownloadColleagueLinksFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_LINKS_FAIL,
    err
  };
}

export function loadDownloadColleaguePanelDetails(uid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS,
    uid
  };
}

export function loadDownloadColleaguePanelDetailsSuccess(data, uid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS_SUCCESS,
    data,
    uid
  };
}

export function loadDownloadColleaguePanelDetailsFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS_FAIL,
    err
  };
}

export function loadDownloadCompanyPanelDetails(uid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS,
    uid
  };
}

export function loadDownloadCompanyPanelDetailsSuccess(data, uid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS_SUCCESS,
    data,
    uid
  };
}

export function loadDownloadCompanyPanelDetailsFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS_FAIL,
    err
  };
}

export function loadDownloadBuyInfo(page, userId) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_BUY_INFO,
    page,
    userId
  };
}

export function loadDownloadBuyInfoSuccess(data, page, userId) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_BUY_INFO_SUCCESS,
    data,
    page,
    userId
  };
}

export function loadDownloadBuyInfoFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_BUY_INFO_FAIL,
    err
  };
}

export function loadDownloadColleagueMomentInfoAction(page, movieUid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_COLLEAGUE_MOMENT_INFO,
    page,
    movieUid
  };
}

//download moment
export function loadDownloadMomentInfoAction(page, movieUid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_MOMENT_INFO,
    page,
    movieUid
  };
}

export function loadDownloadMomentInfoActionSuccess(data, page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_MOMENT_INFO_SUCCESS,
    data,
    page
  };
}

export function loadDownloadMomentInfoActionFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_MOMENT_INFO_FAIL,
    err
  };
}
//company general moment
export function companyGeneralMomentAction(start, end, page) {
  return {
    type: actionTypes.LOAD_COMPANY_GENERAL_MOMENT_INFO,
    start,
    end,
    page
  };
}

export function companyGeneralMomentActionSuccess(data, page) {
  return {
    type: actionTypes.LOAD_COMPANY_GENERAL_MOMENT_INFO_SUCCESS,
    data,
    page
  };
}

export function companyGeneralMomentActionFail(err) {
  return {
    type: actionTypes.LOAD_COMPANY_GENERAL_MOMENT_INFO_FAIL,
    err
  };
}

//recomm download movie
export function loadRecommDownloadAction() {
  return {
    type: actionTypes.LOAD_RECOMM_DOWNLOAD
  };
}

export function loadRecommDownloadActionSuccess(data) {
  return {
    type: actionTypes.LOAD_RECOMM_DOWNLOAD_SUCCESS,
    data
  };
}

export function loadRecommDownloadActionFail(err) {
  return {
    type: actionTypes.LOAD_RECOMM_DOWNLOAD_FAIL,
    err
  };
}

// download category
export function loadDownloadCategoryAction(page) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_CATEGORY,
    page
  };
}

export function loadDownloadCategoryActionSuccess(data) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_CATEGORY_SUCCESS,
    data
  };
}

export function loadDownloadCategoryActionFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_CATEGORY_FAIL,
    err
  };
}

// CAMPAIGN
export function loadCampaignDownloadAction() {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD
  };
}

export function loadCampaignDownloadActionSuccess(data) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SUCCESS,
    data
  };
}

export function loadCampaignDownloadActionFail(err) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_FAIL,
    err
  };
}

export function loadCampaignDownloadPurchasedAction() {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_PURCHASED
  };
}

export function loadCampaignDownloadPurchasedActionSuccess(data) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_PURCHASED_SUCCESS,
    data
  };
}

export function loadCampaignDownloadPurchasedActionFail(err) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_PURCHASED_FAIL,
    err
  };
}

//download payoff
export function colleagueAction() {
  return {
    type: actionTypes.LOAD_COLLEAGUE_INFO
  };
}

export function colleagueActionSuccess(data) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_INFO_SUCCESS,
    data
  };
}

export function colleagueActionFail(err) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_INFO_FAIL,
    err
  };
}
