export const actionTypes = {
  LOAD_DOWNLOAD_DETAILS: "LOAD_DOWNLOAD_DETAILS",
  LOAD_DOWNLOAD_DETAILS_SUCCESS: "LOAD_DOWNLOAD_DETAILS_SUCCESS",
  LOAD_DOWNLOAD_DETAILS_FAIL: "LOAD_DOWNLOAD_DETAILS_FAIL",
  LOAD_DOWNLOAD_BUY: "LOAD_DOWNLOAD_BUY",
  LOAD_DOWNLOAD_BUY_SUCCESS: "LOAD_DOWNLOAD_BUY_SUCCESS",
  LOAD_DOWNLOAD_BUY_FAIL: "LOAD_DOWNLOAD_BUY_FAIL",
  LOAD_DOWNLOAD_DETAILS_LIST: "LOAD_DOWNLOAD_DETAILS_LIST",
  LOAD_DOWNLOAD_DETAILS_LIST_SUCCESS: "LOAD_DOWNLOAD_DETAILS_LIST_SUCCESS",
  LOAD_DOWNLOAD_DETAILS_LIST_FAIL: "LOAD_DOWNLOAD_DETAILS_LIST_FAIL",

  LOAD_CAMPAIGN_DOWNLOAD_DETAILS: "LOAD_CAMPAIGN_DOWNLOAD_DETAILS",
  LOAD_CAMPAIGN_DOWNLOAD_DETAILS_SUCCESS: "LOAD_CAMPAIGN_DOWNLOAD_DETAILS_SUCCESS",
  LOAD_CAMPAIGN_DOWNLOAD_DETAILS_FAIL: "LOAD_CAMPAIGN_DOWNLOAD_DETAILS_FAIL",

  LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS: "LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS",
  LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS_SUCCESS: "LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS_SUCCESS",
  LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS_FAIL: "LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS_FAIL",
  LOAD_COLLEAGUE_RANK: "LOAD_COLLEAGUE_RANK",
  LOAD_COLLEAGUE_RANK_SUCCESS: "LOAD_COLLEAGUE_RANK_SUCCESS",
  LOAD_COLLEAGUE_RANK_FAIL: "LOAD_COLLEAGUE_RANK_FAIL",

  LOAD_COLLEAGUE_MOVIE_RANK: "LOAD_COLLEAGUE_MOVIE_RANK",
  LOAD_COLLEAGUE_MOVIE_RANK_SUCCESS: "LOAD_COLLEAGUE_MOVIE_RANK_SUCCESS",
  LOAD_COLLEAGUE_MOVIE_RANK_FAIL: "LOAD_COLLEAGUE_MOVIE_RANK_FAIL"
};

export function loadDownloadDetailsAction(uid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_DETAILS,
    uid
  };
}
export function loadDownloadDetailsActionSuccess(data) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_DETAILS_SUCCESS,
    data
  };
}

export function loadDownloadDetailsActionFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_DETAILS_FAIL,
    err
  };
}

export function loadDownloadBuyAction(buyInfo) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_BUY,
    ...buyInfo
  };
}
export function loadDownloadBuyActionSuccess(data) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_BUY_SUCCESS,
    data
  };
}

export function loadDownloadBuyActionFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_BUY_FAIL,
    err
  };
}

export function loadDownloadDetailsListAction(uid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_DETAILS_LIST,
    uid
  };
}
export function loadDownloadDetailsListActionSuccess(data, uid) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_DETAILS_LIST_SUCCESS,
    data,
    uid
  };
}

export function loadDownloadDetailsListActionFail(err) {
  return {
    type: actionTypes.LOAD_DOWNLOAD_DETAILS_LIST_FAIL,
    err
  };
}

//campaign actions
export function loadCampaignDownloadDetailsAction(uid) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_DETAILS,
    uid
  };
}
export function loadCampaignDownloadDetailsActionSuccess(data) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_DETAILS_SUCCESS,
    data
  };
}

export function loadCampaignDownloadDetailsActionFail(err) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_DETAILS_FAIL,
    err
  };
}

export function loadCampaignDownloadSecondDetailsAction(uid) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS,
    uid
  };
}
export function loadCampaignDownloadSecondDetailsActionSuccess(data) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS_SUCCESS,
    data
  };
}

export function loadCampaignDownloadSecondDetailsActionFail(err) {
  return {
    type: actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS_FAIL,
    err
  };
}

//colleague Rank
export function loadcolleagueRankAction() {
  return {
    type: actionTypes.LOAD_COLLEAGUE_RANK
  };
}
export function loadcolleagueRankActionSuccess(data) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_RANK_SUCCESS,
    data
  };
}

export function loadcolleagueRankActionFail(err) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_RANK_FAIL,
    err
  };
}

//colleague movie Rank
export function loadColleagueMovieRankAction(uid) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_MOVIE_RANK,
    uid
  };
}
export function loadColleagueMovieRankActionSuccess(data, uid) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_MOVIE_RANK_SUCCESS,
    data,
    uid
  };
}

export function loadColleagueMovieRankActionFail(err) {
  return {
    type: actionTypes.LOAD_COLLEAGUE_MOVIE_RANK_FAIL,
    err
  };
}
