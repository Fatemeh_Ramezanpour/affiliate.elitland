export const actionTypes = {
  TOGGLE_MENU: "TOGGLE_MENU",
  SET_SIDEBAR_OPEN: "SET_SIDEBAR_OPEN",
  SET_SUBMENU_OPEN: "SET_SUBMENU_OPEN",
  ADD_SUBMENU_ITEMS: "ADD_SUBMENU_ITEMS"
};

export function toggleMenu(data) {
  return {
    type: actionTypes.TOGGLE_MENU,
    data
  };
}

export function setSidebarOpenAction(data) {
  return {
    type: actionTypes.SET_SIDEBAR_OPEN,
    data
  };
}

export function addSubMenuAction(data) {
  return {
    type: actionTypes.ADD_SUBMENU_ITEMS,
    data
  };
}

export function setSubmenuOpenAction(id, collapsed) {
  return {
    type: actionTypes.SET_SUBMENU_OPEN,
    id,
    collapsed
  };
}
