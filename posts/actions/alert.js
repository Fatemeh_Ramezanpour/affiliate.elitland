export const SUCCESS = "success";
export const REMOVE = "REMOVE";
export const ERROR = "danger";
export const ADD = "ADD";

// Export function successAlert (mssg) {
//   return{
//     type: SUCCESS_ALERT,
//     mssg<
//   }
// }

var lastId = 1;
function alertFactory({ title, body, timeout = 5000 }, type) {
  return {
    type: ADD,
    alert: {
      id: lastId++,
      timeout,
      title,
      body,
      type
    }
  };
}
export function remove({ id }) {
  return {
    type: REMOVE,
    id
  };
}

// Export function dismiss() {
//   return{
//     type: SUCCESS_DISMISS,
//   }
// }
export function addSuccess({ title, body, timeout }) {
  return alertFactory({ title, body, timeout }, SUCCESS);
}

export function addError({ title, body, timeout }) {
  return alertFactory({ title, body, timeout }, ERROR);
}

// Const types = {
//   SUCCESS: 'success',
//   ERROR: 'danger',
//   WARNING: 'warning',
//   INFO: 'info',
// };
