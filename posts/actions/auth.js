export const actionTypes = {
  LOAD_USER_INFO: "LOAD_USER_INFO",
  LOAD_USER_INFO_SUCCESS: "LOAD_USER_INFO_SUCCESS",
  REGISTER_GUEST_USER: "REGISTER_GUEST_USER",
  REGISTER_GUEST_USER_SUCCESS: "REGISTER_GUEST_USER_SUCCESS",
  REGISTER_GUEST_USER_FAILURE: "REGISTER_GUEST_USER_FAILURE",
  SAVE_MOBILE_FOR_CONFIRM: "SAVE_MOBILE_FOR_CONFIRM",
  LOGIN: "LOGIN",
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGIN_FAIL: "LOGIN_FAIL",
  OTP_CREATION: "OTP_CREATION",
  OTP_CREATION_SUCCESS: "OTP_CREATION_SUCCESS",
  OTP_CREATION_FAIL: "OTP_CREATION_FAIL",
  VERIFY_MOBILE: "VERIFY_MOBILE",
  VERIFY_MOBILE_SUCCESS: "VERIFY_MOBILE_SUCCESS",
  VERIFY_MOBILE_FAIL: "VERIFY_MOBILE_FAIL",
  SAVE_VERIFY_CODE: "SAVE_VERIFY_CODE",
  CHANGE_PASSWORD: "CHANGE_PASSWORD",
  CHANGE_PASSWORD_SUCCESS: "CHANGE_PASSWORD_SUCCESS",
  CHANGE_PASSWORD_FAIL: "CHANGE_PASSWORD_FAIL",
  SET_PASSWORD_SIGNUP: "SET_PASSWORD_SIGNUP",
  SET_PASSWORD_SIGNUP_SUCCESS: "SET_PASSWORD_SIGNUP_SUCCESS",
  SET_PASSWORD_SIGNUP_FAIL: "SET_PASSWORD_SIGNUP_FAIL",
  LOGOUT: "LOGOUT",
  LOGOUT_SUCCESS: "LOGOUT_SUCCESS",
  LOGOUT_FAIL: "LOGOUT_FAIL",
  SET_TOKEN: "SET_TOKEN",
  FORGOT_PASSWORD_In_OLD_SYSTEM: "FORGOT_PASSWORD_In_OLD_SYSTEM",
  FORGOT_PASSWORD_In_OLD_SYSTEM_SUCCESS: "FORGOT_PASSWORD_In_OLD_SYSTEM_SUCCESS",
  FORGOT_PASSWORD_In_OLD_SYSTEM_FAIL: "FORGOT_PASSWORD_In_OLD_SYSTEM_FAIL"
};

export function loadUserinfo() {
  return {
    type: actionTypes.LOAD_USER_INFO
  };
}

export function loadUserinfoSuccess(data) {
  return {
    type: actionTypes.LOAD_USER_INFO_SUCCESS,
    data
  };
}

export function hasAdmin(admin) {
  return admin && admin.token;
}

export function switchToAdmin() {
  return {
    // eslint-disable-next-line no-undef
    type: actionTypes.SWITCH_TO_ADMIN
  };
}

export function saveMobileForConfirm(mobile) {
  return {
    type: actionTypes.SAVE_MOBILE_FOR_CONFIRM,
    mobile
  };
}

export function registerGuestUser(signupInfo) {
  return {
    type: actionTypes.REGISTER_GUEST_USER,
    signupInfo
  };
}

export function registerGuestUserSuccess(data) {
  return {
    type: actionTypes.REGISTER_GUEST_USER_SUCCESS,
    data
  };
}

export function registerGuestUserFailure(err) {
  return {
    type: actionTypes.REGISTER_GUEST_USER_FAILURE,
    err
  };
}

export function login(userData, url) {
  return {
    type: actionTypes.LOGIN,
    userData,
    url
  };
}

export function loginSuccess(data) {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    data
  };
}

export function loginFailure(err) {
  return {
    type: actionTypes.LOGIN_FAIL,
    err
  };
}

export function OTPCreation(data) {
  return {
    type: actionTypes.OTP_CREATION,
    data
  };
}

export function OTPCreationSuccess(data) {
  return {
    type: actionTypes.OTP_CREATION_SUCCESS,
    data
  };
}

export function OTPCreationFailure(err) {
  return {
    type: actionTypes.OTP_CREATION_FAIL,
    err
  };
}

export function saveVerifyCode(code) {
  return {
    type: actionTypes.SAVE_VERIFY_CODE,
    code
  };
}

export function verifyMobile(mobile, code) {
  return {
    type: actionTypes.VERIFY_MOBILE,
    mobile,
    code
  };
}

export function verifyMobileSuccess(data) {
  return {
    type: actionTypes.VERIFY_MOBILE_SUCCESS,
    data
  };
}

export function verifyMobileFailure(err) {
  return {
    type: actionTypes.VERIFY_MOBILE_FAIL,
    err
  };
}

export function changePassword(mobile, data) {
  return {
    type: actionTypes.CHANGE_PASSWORD,
    mobile,
    data
  };
}

export function changePasswordSuccess(data) {
  return {
    type: actionTypes.CHANGE_PASSWORD_SUCCESS,
    data
  };
}

export function changePasswordFailure(err) {
  return {
    type: actionTypes.CHANGE_PASSWORD_FAIL,
    err
  };
}

export function setPasswordSignup(data, uid) {
  return {
    type: actionTypes.SET_PASSWORD_SIGNUP,
    data,
    uid
  };
}

export function setPasswordSignupSuccess(data) {
  return {
    type: actionTypes.SET_PASSWORD_SIGNUP_SUCCESS,
    data
  };
}

export function setPasswordSignupFailure(err) {
  return {
    type: actionTypes.SET_PASSWORD_SIGNUP_FAIL,
    err
  };
}

export function logout(params) {
  return {
    type: actionTypes.LOGOUT,
    params
  };
}

export function logoutSuccess(data) {
  return {
    type: actionTypes.LOGOUT_SUCCESS,
    data
  };
}

export function logoutFailure(err) {
  return {
    type: actionTypes.LOGOUT_FAIL,
    err
  };
}

export function isAdmin(user) {
  return user && user.is_admin;
}

export function isCompany(user) {
  return user && user.is_company;
}

export function isColleague(user) {
  return user && user.is_colleague;
}

export function setToken(data) {
  return {
    type: actionTypes.SET_TOKEN,
    data
  };
}

export function forgotPasswordInOldSystem(data) {
  return {
    type: actionTypes.FORGOT_PASSWORD_In_OLD_SYSTEM,
    data
  };
}

export function forgotPasswordInOldSystemSuccess(data) {
  return {
    type: actionTypes.FORGOT_PASSWORD_In_OLD_SYSTEM_SUCCESS,
    data
  };
}

export function forgotPasswordInOldSystemFailure(err) {
  return {
    type: actionTypes.FORGOT_PASSWORD_In_OLD_SYSTEM_FAIL,
    err
  };
}
