/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
export const LOAD_WATCHLIST = "LOAD_WATCHLIST";
export const LOAD_WATCHLIST_SUCCESS = "LOAD_WATCHLIST_SUCCESS";

export const LOAD_WATCHLIST_TOP = "LOAD_WATCHLIST_TOP";
export const LOAD_WATCHLIST_TOP_SUCCESS = "LOAD_WATCHLIST_TOP_SUCCESS";

export const DELETE_WATCHLIST = "DELETE_WATCHLIST";
export const DELETE_WATCHLIST_FAIL = "DELETE_WATCHLIST_FAIL";
export const DELETE_WATCHLIST_SUCCESS = " DELETE_WATCHLIST_SUCCESS";

export const REORDER_WATCHLIST = "REORDER_WATCHLIST";
export const REORDER_WATCHLIST_SUCCESS = "REORDER_WATCHLIST_SUCCESS";
export const REORDER_WATCHLIST_FAIL = "REORDER_WATCHLIST_FAIL";

export const MORE_REORDER_WATCHLIST = "MORE_REORDER_WATCHLIST";
export const MORE_REORDER_WATCHLIST_SUCCESS = "MORE_REORDER_WATCHLIST_SUCCESS";
export const MORE_REORDER_WATCHLIST_FAIL = "MORE_REORDER_WATCHLIST_FAIL";

export const ORDER_BY_VOTE = "ORDER_BY_VOTE";
export const ORDER_BY_VOTE_SUCCESS = "ORDER_BY_VOTE_SUCCESS";

export const ORDER_BY_YEAR = "ORDER_BY_YEAR";
export const ORDER_BY_YEAR_SUCCESS = "ORDER_BY_YEAR_SUCCESS";
export const ORDER_BY_YEAR_FAIL = "ORDER_BY_YEAR_FAIL";

export const ORDER_BY_ALPHABET = "ORDER_BY_ALPHABET";
export const ORDER_BY_ALPHABET_SUCCESS = "ORDER_BY_ALPHABET_SUCCESS";
export const ORDER_BY_ALPHABET_FAIL = "ORDER_BY_ALPHABET_FAIL";

export const SAVE_MOVIE_VOTE = "SAVE_MOVIE_VOTE";
export const SAVE_MOVIE_VOTE_SUCCESS = "SAVE_MOVIE_VOTE_SUCCESS";
export const SAVE_MOVIE_VOTE_FAIL = "SAVE_MOVIE_VOTE_FAIL";

export const UPDATE_VOTE = "UPDATE_VOTE";
export const UPDATE_VOTE_SUCCESS = "UPDATE_VOTE_SUCCESS";

export const ADD_SELECTED_ITEM = "ADD_SELECTED_ITEM";
export const SELECTED_ORDER = "SELECTED_ORDER";
export const ORDER_BY_VOTE_FAIL = "ORDER_BY_VOTE_FAIL";

export function loadWatchlist(page) {
  return {
    type: LOAD_WATCHLIST,
    page
  };
}

export function getWatchlistSuccess(data) {
  return {
    type: LOAD_WATCHLIST_SUCCESS,
    data
  };
}

export function loadWatchlistTop(userUid) {
  return {
    type: LOAD_WATCHLIST_TOP,
    userUid
  };
}

export function getWatchlistTopSuccess(data) {
  return {
    type: LOAD_WATCHLIST_TOP_SUCCESS,
    data
  };
}

export function deleteWatchlistItem(MovieUid) {
  return {
    type: DELETE_WATCHLIST,
    MovieUid
  };
}

export function WatchlistDataDeleteSuccess(data) {
  return {
    type: DELETE_WATCHLIST_SUCCESS,
    data
  };
}

export function WatchlistDataDeleteFail(err) {
  return {
    type: DELETE_WATCHLIST_FAIL,
    err
  };
}

export function reOrderWatchlist(page, typeOfOrder) {
  return {
    type: REORDER_WATCHLIST,
    page,
    typeOfOrder
  };
}

export function reOrderWatchlistSuccess(data, orderType) {
  return {
    type: REORDER_WATCHLIST_SUCCESS,
    data,
    orderType
  };
}

export function moreReOrderWatchlist(page, typeOfOrder) {
  return {
    type: MORE_REORDER_WATCHLIST,
    page,
    typeOfOrder
  };
}

export function moreReOrderWatchlistSuccess(data, orderType) {
  return {
    type: MORE_REORDER_WATCHLIST_SUCCESS,
    data,
    orderType
  };
}

export function reOrderWatchlistFail(typeOfOrder) {
  return {
    type: REORDER_WATCHLIST_FAIL,
    err
  };
}

export function saveMovieVote(vote, MovieUid) {
  return {
    type: SAVE_MOVIE_VOTE,
    MovieUid,
    vote
  };
}

export function saveMovieVoteSuccess(data) {
  return {
    type: SAVE_MOVIE_VOTE_SUCCESS,
    data
  };
}

export function saveMovieVoteFail() {
  return {
    type: SAVE_MOVIE_VOTE_FAIL,
    err
  };
}
