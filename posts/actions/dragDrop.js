export const DRAG_DROP_REORDER = "DRAG_DROP_REORDER";
// Export const DRAG_DROP_INITIAL_DATA = 'DRAG_DROP_INITIAL_DATA';
export const SET_SEARCH_KEY_DOWN = "SET_SEARCH_KEY_DOWN";

// Export function dragDropReorder(data){
//   return{
//     type: DRAG_DROP_REORDER,
//     data
//   }
// }
// export function dragdropInitial(data){
//   return{
//     type: DRAG_DROP_INITIAL_DATA,
//     data
//   }
// }
export function setSearchKeyDown() {
  return {
    type: SET_SEARCH_KEY_DOWN
  };
}
