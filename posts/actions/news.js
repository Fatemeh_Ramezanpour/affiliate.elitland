export const LOAD_PERSIAN_NEWS_ARCHIVE = "LOAD_PERSIAN_NEWS_ARCHIVE";
export const LOAD_PERSIAN_NEWS_ARCHIVE_SUCCESS = "LOAD_PERSIAN_NEWS_ARCHIVE_SUCCESS";
export const LOAD_PERSIAN_NEWS_ARCHIVE_FAIL = "LOAD_PERSIAN_NEWS_ARCHIVE_FAIL";

export const LOAD_FOREIGN_NEWS = "LOAD_FOREIGN_NEWS";
export const LOAD_FOREIGN_NEWS_SUCCESS = "LOAD_FOREIGN_NEWS_SUCCESS";
export const LOAD_FOREIGN_NEWS_FAIL = "LOAD_FOREIGN_NEWS_FAIL";

export const LOAD_PERSIAN_NEWS = "LOAD_PERSIAN_NEWS";
export const LOAD_PERSIAN_NEWS_SUCCESS = "LOAD_PERSIAN_NEWS_SUCCESS";
export const LOAD_PERSIAN_NEWS_FAIL = "LOAD_PERSIAN_NEWS_FAIL";

export const LOAD_FOREIGN_NEWS_ARCHIVE = "LOAD_FOREIGN_NEWS_ARCHIVE_ARCHIVE";
export const LOAD_FOREIGN_NEWS_ARCHIVE_SUCCESS = "LOAD_FOREIGN_NEWS_ARCHIVE_SUCCESS";
export const LOAD_FOREIGN_NEWS_ARCHIVE_FAIL = "LOAD_FOREIGN_NEWS_ARCHIVE_FAIL";

export const LOAD_POPULAR_NEWS = "LOAD_POPULAR_NEWS_ARCHIVE";
export const LOAD_POPULAR_NEWS_SUCCESS = "LOAD_POPULAR_NEWS_SUCCESS";
export const LOAD_POPULAR_NEWS_FAIL = "LOAD_POPULAR_NEWS_FAIL";

// Persian news archive
export function loadPersianNewsArchive(data) {
  return {
    type: LOAD_PERSIAN_NEWS_ARCHIVE,
    data
  };
}

export function loadPersianNewsArchiveSuccess(data) {
  return {
    type: LOAD_PERSIAN_NEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadPersianNewsArchiveFail(error) {
  return {
    type: LOAD_PERSIAN_NEWS_ARCHIVE_FAIL,
    error
  };
}

// Foreign news archive
export function loadForiegnNewsArchive(data) {
  return {
    type: LOAD_FOREIGN_NEWS_ARCHIVE,
    data
  };
}

export function loadForiegnNewsArchiveSuccess(data) {
  return {
    type: LOAD_FOREIGN_NEWS_ARCHIVE_SUCCESS,
    data
  };
}

export function loadForiegnNewsArchiveFail(error) {
  return {
    type: LOAD_FOREIGN_NEWS_ARCHIVE_FAIL,
    error
  };
}

// Foreign news
export function loadForiegnNews(data) {
  return {
    type: LOAD_FOREIGN_NEWS,
    data
  };
}

export function loadForiegnNewsSuccess(data) {
  return {
    type: LOAD_FOREIGN_NEWS_SUCCESS,
    data
  };
}

export function loadForiegnNewsFail() {
  return {
    type: LOAD_FOREIGN_NEWS_FAIL
  };
}

// Persian news
export function loadPersianNews(data) {
  return {
    type: LOAD_PERSIAN_NEWS,
    data
  };
}

export function loadPersianNewsSuccess(data) {
  return {
    type: LOAD_PERSIAN_NEWS_SUCCESS,
    data
  };
}

export function loadPersianNewsFail() {
  return {
    type: LOAD_PERSIAN_NEWS_FAIL
  };
}

// Popular news
export function loadPopularNews() {
  return {
    type: LOAD_POPULAR_NEWS
  };
}

export function loadPopularNewsSuccess(data) {
  return {
    type: LOAD_POPULAR_NEWS_SUCCESS,
    data
  };
}

export function loadPopularNewsFail() {
  return {
    type: LOAD_POPULAR_NEWS_FAIL
  };
}
