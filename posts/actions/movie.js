export const LOAD_PERSIAN_MOVIE_TOP_VIEW = "LOAD_PERSIAN_MOVIE_TOP_VIEW";
export const LOAD_PERSIAN_MOVIE_TOP_VIEW_SUCCESS = "LOAD_PERSIAN_MOVIE_TOP_VIEW_SUCCESS";
export const LOAD_PERSIAN_MOVIE_TOP_VIEW_FAIL = "LOAD_PERSIAN_MOVIE_TOP_VIEW_FAIL";

export const LOAD_FOREIGN_MOVIE_TOP_VIEW = "LOAD_FOREIGN_MOVIE_TOP_VIEW";
export const LOAD_FOREIGN_MOVIE_TOP_VIEW_SUCCESS = "LOAD_FOREIGN_MOVIE_TOP_VIEW_SUCCESS";
export const LOAD_FOREIGN_MOVIE_TOP_VIEW_FAIL = "LOAD_FOREIGN_MOVIE_TOP_VIEW_FAIL";

export const LOAD_PERSIAN_MOVIE_ARCHIVE = "LOAD_PERSIAN_MOVIE_ARCHIVE";
export const LOAD_PERSIAN_MOVIE_ARCHIVE_SUCCESS = "LOAD_PERSIAN_MOVIE_ARCHIVE_SUCCESS";
export const LOAD_PERSIAN_MOVIE_ARCHIVE_FAIL = "LOAD_PERSIAN_MOVIE_ARCHIVE_FAIL";

export const LOAD_FOREIGN_MOVIE_ARCHIVE = "LOAD_FOREIGN_MOVIE_ARCHIVE";
export const LOAD_FOREIGN_MOVIE_ARCHIVE_SUCCESS = "LOAD_FOREIGN_MOVIE_ARCHIVE_SUCCESS";
export const LOAD_FOREIGN_MOVIE_ARCHIVE_FAIL = "LOAD_FOREIGN_MOVIE_ARCHIVE_FAIL";

// Export const LOAD_PERSIAN_NEWS = "LOAD_PERSIAN_NEWS";
// export const LOAD_PERSIAN_NEWS_SUCCESS = "LOAD_PERSIAN_NEWS_SUCCESS";
// export const LOAD_PERSIAN_NEWS_FAIL = "LOAD_PERSIAN_NEWS_FAIL";

// export const LOAD_FOREIGN_NEWS_ARCHIVE = "LOAD_FOREIGN_NEWS_ARCHIVE_ARCHIVE";
// export const LOAD_FOREIGN_NEWS_ARCHIVE_SUCCESS = "LOAD_FOREIGN_NEWS_ARCHIVE_SUCCESS";
// export const LOAD_FOREIGN_NEWS_ARCHIVE_FAIL = "LOAD_FOREIGN_NEWS_ARCHIVE_FAIL";

// export const LOAD_POPULAR_NEWS = "LOAD_POPULAR_NEWS_ARCHIVE";
// export const LOAD_POPULAR_NEWS_SUCCESS = "LOAD_POPULAR_NEWS_SUCCESS";
// export const LOAD_POPULAR_NEWS_FAIL = "LOAD_POPULAR_NEWS_FAIL";

// persian movie top view
export function loadPersianMovieTopView(data) {
  return {
    type: LOAD_PERSIAN_MOVIE_TOP_VIEW,
    data
  };
}

export function loadPersianMovieTopViewSuccess(data) {
  return {
    type: LOAD_PERSIAN_MOVIE_TOP_VIEW_SUCCESS,
    data
  };
}

export function loadPersianMovieTopViewFail() {
  return {
    type: LOAD_PERSIAN_MOVIE_TOP_VIEW_FAIL
  };
}

// Foreign movie top view
export function loadForiegnMovieTopView(data) {
  return {
    type: LOAD_FOREIGN_MOVIE_TOP_VIEW,
    data
  };
}

export function loadForiegnMovieTopViewSuccess(data) {
  return {
    type: LOAD_FOREIGN_MOVIE_TOP_VIEW_SUCCESS,
    data
  };
}

export function loadForiegnMovieTopViewFail() {
  return {
    type: LOAD_FOREIGN_MOVIE_TOP_VIEW_FAIL
  };
}

// Persian movie archive
export function loadPersianMoviesArchive(data) {
  return {
    type: LOAD_PERSIAN_MOVIE_ARCHIVE,
    data
  };
}

export function loadPersianMoviesArchiveSuccess(data) {
  return {
    type: LOAD_PERSIAN_MOVIE_ARCHIVE_SUCCESS,
    data
  };
}

export function loadPersianMoviesArchiveFail(error) {
  return {
    type: LOAD_PERSIAN_MOVIE_ARCHIVE_FAIL,
    error
  };
}

// Foreign movie archive
export function loadForeignMoviesArchive(data) {
  return {
    type: LOAD_FOREIGN_MOVIE_ARCHIVE,
    data
  };
}

export function loadForeignMoviesArchiveSuccess(data) {
  return {
    type: LOAD_FOREIGN_MOVIE_ARCHIVE_SUCCESS,
    data
  };
}

export function loadForeignMoviesArchiveFail(error) {
  return {
    type: LOAD_FOREIGN_MOVIE_ARCHIVE_FAIL,
    error
  };
}
// Foreign news
// export function loadForiegnNews(data) {
//   return {
//     type: LOAD_FOREIGN_NEWS,
//     data
//   };
// }
// export function loadForiegnNewsSuccess(data) {
//   return {
//     type: LOAD_FOREIGN_NEWS_SUCCESS,
//     data
//   };
// }
// export function loadForiegnNewsFail() {
//   return {
//     type: LOAD_FOREIGN_NEWS_FAIL
//   };
// }

// //persian news
// export function loadPersianNews(data) {
//   return {
//     type: LOAD_PERSIAN_NEWS,
//     data
//   };
// }
// export function loadPersianNewsSuccess(data) {
//   return {
//     type: LOAD_PERSIAN_NEWS_SUCCESS,
//     data
//   };
// }
// export function loadPersianNewsFail() {
//   return {
//     type: LOAD_PERSIAN_NEWS_FAIL
//   };
// }

// //popular news
// export function loadPopularNews() {
//   return {
//     type: LOAD_POPULAR_NEWS,
//   };
// }

// export function loadPopularNewsSuccess(data) {
//   return {
//     type: LOAD_POPULAR_NEWS_SUCCESS,
//     data
//   };
// }

// export function loadPopularNewsFail() {
//   return {
//     type: LOAD_POPULAR_NEWS_FAIL
//   };
// }
