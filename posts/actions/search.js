export const LOAD_COMPLETE_SEARCH = "LOAD_COMPLETE_SEARCH";
export const LOAD_COMPLETE_SEARCH_FAILURE = "LOAD_COMPLETE_SEARCH_FAILURE";
export const LOAD_COMPLETE_SEARCH_SUCCESS = "LOAD_COMPLETE_SEARCH_SUCCESS";
export const ADD_SELECTED_ITEM = "ADD_SELECTED_ITEM";
export const SET_SEARCH_INPUT_VALUE = "SET_SEARCH_INPUT_VALUE";
export const LOAD_MOVIE_SEARCH_SUCCESS = "LOAD_MOVIE_SEARCH_SUCCESS";
export const LOAD_MOVIE_SEARCH_FAILURE = "LOAD_MOVIE_SEARCH_FAILURE";
export const SET_MOVIE_SEARCH_INPUT_VALUE = "SET_MOVIE_SEARCH_INPUT_VALUE";

export const LOAD_MOVIE_SEARCH = "LOAD_MOVIE_SEARCH";

export const LOAD_NEWS_SEARCH = "LOAD_NEWS_SEARCH";
export const SET_NEWS_SEARCH_INPUT_VALUE = "SET_NEWS_SEARCH_INPUT_VALUE";
export const LOAD_NEWS_SEARCH_SUCCESS = "LOAD_NEWS_SEARCH_SUCCESS";
export const LOAD_NEWS_SEARCH_FAILURE = "LOAD_NEWS_SEARCH_FAILURE";

export function getCompleteSearch(data) {
  return {
    type: LOAD_COMPLETE_SEARCH,
    data
  };
}

export function setNewsSearchInputValue(data) {
  return {
    type: SET_NEWS_SEARCH_INPUT_VALUE,
    data
  };
}

export function setMovieSearchInputValue(data) {
  return {
    type: SET_MOVIE_SEARCH_INPUT_VALUE,
    data
  };
}

export function loadCompleteSearchSuccess(data) {
  return {
    type: LOAD_COMPLETE_SEARCH_SUCCESS,
    data
  };
}

export function loadCompleteSearchFailure(err) {
  return {
    type: LOAD_COMPLETE_SEARCH_FAILURE,
    err
  };
}

export function getMovieSearch(data) {
  return {
    type: LOAD_MOVIE_SEARCH,
    data
  };
}

export function loadMovieSearchSuccess(data) {
  return {
    type: LOAD_MOVIE_SEARCH_SUCCESS,
    data
  };
}

export function loadMovieSearchFailure(err) {
  return {
    type: LOAD_MOVIE_SEARCH_FAILURE,
    err
  };
}

export function addSelectedItem(data) {
  return {
    type: ADD_SELECTED_ITEM,
    data
  };
}

// News search

export function getNewsSearch(data) {
  return {
    type: LOAD_NEWS_SEARCH,
    data
  };
}

export function setSearchInputValue(data) {
  return {
    type: SET_SEARCH_INPUT_VALUE,
    data
  };
}

export function loadNewsSearchSuccess(data) {
  return {
    type: LOAD_NEWS_SEARCH_SUCCESS,
    data
  };
}

export function loadNewsSearchFailure(err) {
  return {
    type: LOAD_NEWS_SEARCH_FAILURE,
    err
  };
}
