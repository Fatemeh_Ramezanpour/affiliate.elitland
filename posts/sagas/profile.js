import "isomorphic-unfetch";

import {
  LOAD_FOREIGN_PROFILE_ARCHIVE,
  LOAD_PERSIAN_PROFILE_ARCHIVE,
  loadForeignProfileArchiveFail,
  loadForeignProfileArchiveSuccess,
  loadPersianProfileArchiveFail,
  loadPersianProfileArchiveSuccess
} from "posts/actions/profile";

import { call, fork, put, takeEvery } from "redux-saga/effects";

import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

function* loadPersianProfile(page) {
  try {
    const url = page.data
      ? `share/v2/profiles?page=${page.data}&domestic=${1}&per_page=10`
      : "share/v2/profiles?page=1&domestic=1&per_page=10";
    const res = yield call(api.fetchGet, url);

    yield put(loadPersianProfileArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadPersianProfileArchiveFail(error.response.status));
    }
  }
}

function* loadForeignProfile(page) {
  try {
    const url = page.data
      ? `share/v2/profiles?page=${page.data}&domestic=${0}&per_page=10`
      : "share/v2/profiles?page=1&domestic=0&per_page=10";
    const res = yield call(api.fetchGet, url);
    yield put(loadForeignProfileArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadForeignProfileArchiveFail(error.response.status));
    }
  }
}

function* watchLoadPersianProfile() {
  yield takeEvery(LOAD_PERSIAN_PROFILE_ARCHIVE, loadPersianProfile);
}

function* watchLoadForeignProfile() {
  yield takeEvery(LOAD_FOREIGN_PROFILE_ARCHIVE, loadForeignProfile);
}

export default function* profileSagas() {
  yield fork(watchLoadPersianProfile);
  yield fork(watchLoadForeignProfile);
}
