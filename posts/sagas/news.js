import "isomorphic-unfetch";

import {
  LOAD_FOREIGN_NEWS,
  LOAD_FOREIGN_NEWS_ARCHIVE,
  LOAD_PERSIAN_NEWS,
  LOAD_PERSIAN_NEWS_ARCHIVE,
  LOAD_POPULAR_NEWS,
  loadForiegnNewsArchiveFail,
  loadForiegnNewsArchiveSuccess,
  loadForiegnNewsFail,
  loadForiegnNewsSuccess,
  loadPersianNewsArchiveFail,
  loadPersianNewsArchiveSuccess,
  loadPersianNewsFail,
  loadPersianNewsSuccess,
  loadPopularNewsFail,
  loadPopularNewsSuccess
} from "posts/actions/news";

import { call, fork, put, takeEvery } from "redux-saga/effects";

import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

function* persianNewsArchive(page) {
  try {
    const url = `/share/v2/news/iran_daily.json?page=${page.data}`;

    const res = yield call(api.fetchGet, url);
    yield put(loadPersianNewsArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadPersianNewsArchiveFail(error.response.status));
    }
  }
}

function* loadPersian() {
  try {
    const url = "/share/v2/news/iran_daily.json?page=1";

    const res = yield call(api.fetchGet, url);

    yield put(loadPersianNewsSuccess(res.data));
  } catch (error) {
    yield put(loadPersianNewsFail(error));
  }
}

function* foreignNewsArchive(page) {
  try {
    const url = `/share/v2/news/foreign_daily.json?page=${page.data}`;

    const res = yield call(api.fetchGet, url);

    yield put(loadForiegnNewsArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadForiegnNewsArchiveFail(error.response.status));
    }
  }
}

function* loadForiegn() {
  try {
    const url = "/share/v2/news/foreign_daily.json?page=1";

    const res = yield call(api.fetchGet, url);

    yield put(loadForiegnNewsSuccess(res.data));
  } catch (error) {
    yield put(loadForiegnNewsFail(error));
  }
}

function* loadPopularNews() {
  try {
    const url = "share/v2/news/popular.json";

    const res = yield call(api.fetchGet, url);
    yield put(loadPopularNewsSuccess(res.data));
  } catch (error) {
    yield put(loadPopularNewsFail(error));
  }
}

function* watchPersianNewsloader() {
  yield takeEvery(LOAD_PERSIAN_NEWS_ARCHIVE, persianNewsArchive);
}

function* watchForiegnNewsloader() {
  yield takeEvery(LOAD_FOREIGN_NEWS_ARCHIVE, foreignNewsArchive);
}

function* watchPopularNews() {
  yield takeEvery(LOAD_POPULAR_NEWS, loadPopularNews);
}

function* loadForiegnNews() {
  yield takeEvery(LOAD_FOREIGN_NEWS, loadForiegn);
}

function* loadPersianNews() {
  yield takeEvery(LOAD_PERSIAN_NEWS, loadPersian);
}

export default function* newsSagas() {
  yield fork(loadForiegnNews);
  yield fork(loadPersianNews);
  yield fork(watchPersianNewsloader);
  yield fork(watchForiegnNewsloader);
  yield fork(watchPopularNews);
}
