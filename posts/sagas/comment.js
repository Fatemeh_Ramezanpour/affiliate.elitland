/* eslint-disable no-undef */
import "isomorphic-unfetch";

import {
  DELETE_COMMENTS_MOVIE_DATA,
  LOAD_COMMENTS_MOVIE_DATA,
  SAVE_COMMENTS_MOVIE_DATA,
  deleteCommentsMovieDataFail,
  deleteCommentsMovieDataSuccess,
  loadCommentsMovieDataFail,
  loadCommentsMovieDataSuccess,
  saveCommentsMovieDataSuccess
} from "posts/actions/comment";
import { addError, addSuccess } from "posts/actions/alert";
import { call, fork, put, takeEvery } from "redux-saga/effects";

import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

function* loadCommentsMovieDetailData(page) {
  try {
    const url = page.data ? `share/v2/movie/${page.data}/comment.json?type=movie` : "";
    const res = yield call(api.fetchGet, url);
    yield put(loadCommentsMovieDataSuccess(res.data));
  } catch (error) {
    yield put(loadCommentsMovieDataFail(error));
  }
}

function* saveCommentsMovieDetailData(action) {
  try {
    const body = { body: action.data.body };
    const url = `/share/v2/movie/${action.uid}/comment.json`;
    const res = yield call(api.fetchPost, url, body);
    yield put(saveCommentsMovieDataSuccess(res.data, action.uid));
  } catch (err) {
    yield put(
      addError({
        title: "ثبت با خطا مواجه شد",
        body: `${
          err.response && err.response.status === 404 ? "فیلم برای نظر دادن تایید نشده" : ""
        }`
      })
    );
  }
}

function* deleteMovieCommentDetailData(action) {
  try {
    const url = action.data ? `/social/v1/comment/${action.data}.json` : "";
    const res = yield call(api.fetchDelete, url, action.data);
    yield put(deleteCommentsMovieDataSuccess(res.data));
    yield put(addSuccess({ title: " کامنت با موفقیت حذف شد" }));
  } catch (error) {
    yield put(deleteCommentsMovieDataFail(err));
    yield put(
      addError({
        title: "خطا هنگام حذف کامنت",
        body: `${
          err.response && err.response.status === 404 ? "فیلم برای نظر دادن تایید نشده" : ""
        }`
      })
    );
  }
}

function* watchLoadCommentsMovieData() {
  yield takeEvery(LOAD_COMMENTS_MOVIE_DATA, loadCommentsMovieDetailData);
}

function* watchSaveCommentsMovieData() {
  yield takeEvery(SAVE_COMMENTS_MOVIE_DATA, saveCommentsMovieDetailData);
}

function* watchDeleteCommentsMovieData() {
  yield takeEvery(DELETE_COMMENTS_MOVIE_DATA, deleteMovieCommentDetailData);
}

export default function* movieDetailSagas() {
  yield fork(watchLoadCommentsMovieData);
  yield fork(watchSaveCommentsMovieData);
  yield fork(watchDeleteCommentsMovieData);
}
