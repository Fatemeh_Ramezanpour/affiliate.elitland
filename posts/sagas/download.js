import { call, put, fork, takeEvery } from "redux-saga/effects";
import es6promise from "es6-promise";
import "isomorphic-unfetch";
import api from "services/api";

import {
  actionTypes,
  loadDownloadListSuccess,
  loadDownloadListFail,
  loadDownloadColleaguePanelSuccess,
  loadDownloadColleaguePanelDetailsSuccess,
  loadDownloadColleaguePanelDetailsFail,
  loadDownloadBuyInfoSuccess,
  loadDownloadBuyInfoFail,
  loadDownloadColleagueLinksSuccess,
  loadDownloadColleagueLinksFail,
  loadCampaignDownloadActionSuccess,
  loadCampaignDownloadActionFail,
  loadCampaignDownloadPurchasedActionSuccess,
  loadCampaignDownloadPurchasedActionFail,
  loadDownloadCompanyPanelSuccess,
  loadDownloadCompanyPanelDetailsSuccess,
  loadDownloadCompanyPanelDetailsFail,
  loadDownloadMomentInfoActionSuccess,
  loadDownloadMomentInfoActionFail,
  colleagueActionSuccess,
  colleagueActionFail,
  companyGeneralMomentActionSuccess,
  companyGeneralMomentActionFail,
  loadRecommDownloadActionSuccess,
  loadRecommDownloadActionFail,
  loadDownloadCategoryActionSuccess,
  loadDownloadCategoryActionFail,
  loadColleagueReportSuccess,
  loadColleagueReportFail,
  loadMovieColleaguePanelSuccess,
  loadMovieColleaguePanelFail,
  loadSeriesColleaguePanelActionSuccess,
  loadSeriesColleaguePanelActionFail
} from "posts/actions/download";

es6promise.polyfill();

function* loadDownloadListSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    let url;
    if (data.series_uid) {
      url = `/share/v2/download/downloade_series_episode.json?series_uid=${data.series_uid}`;
    } else {
      url = `share/v2/download/downloadable_list.json?page=${page}`;
    }
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadListSuccess(res.data, page));
  } catch (error) {
    yield put(loadDownloadListFail(error));
  }
}

function* loadDownloadColleaguePanelSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    const url = `/share/colleague/finance/simple2.json?page=${page}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadColleaguePanelSuccess(res.data, page));
  } catch (error) {
    yield put(loadDownloadListFail(error));
  }
}

function* loadMovieColleagueSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    const url = `/share/colleague/finance/movie.json?page=${page}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadMovieColleaguePanelSuccess(res.data, page));
  } catch (error) {
    yield put(loadMovieColleaguePanelFail(error));
  }
}

function* loadColleagueReportSaga() {
  try {
    // const page = data.page ? data.page : 1;
    const url = `/share/colleague/finance/report.json`;
    const res = yield call(api.fetchGet, url);
    yield put(loadColleagueReportSuccess(res.data));
  } catch (error) {
    yield put(loadColleagueReportFail(error));
  }
}

function* loadDownloadColleaguePanelDetailsSaga(data) {
  try {
    const url = `/share/colleague/finance/details.json?uid=${data.uid}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadColleaguePanelDetailsSuccess(res.data, data.uid));
  } catch (error) {
    yield put(loadDownloadColleaguePanelDetailsFail(error));
  }
}

function* loadDownloadBuyInfoSaga(data) {
  try {
    const url = `/customers_dashboard/get_payments.json?page=${data.page}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadBuyInfoSuccess(res.data, data.page, data.userId));
  } catch (error) {
    yield put(loadDownloadBuyInfoFail(error));
  }
}

function* loadDownloadColleagueLinksSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    const url = `/share/v2/download/downloadable_movies_colleage_page.json?page=${page}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadColleagueLinksSuccess(res.data, data.page));
  } catch (error) {
    yield put(loadDownloadColleagueLinksFail(error));
  }
}

function* loadCampaignDownloadSaga() {
  try {
    const url = `/share/v2/download/campaign_user_info.json`;
    const res = yield call(api.fetchGet, url);
    yield put(loadCampaignDownloadActionSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadCampaignDownloadActionFail(error.response.status));
    }
  }
}

function* loadCampaignDownloadPurchasedSaga() {
  try {
    const url = `/share/v2/download/campaign_user_info_purchased.json`;
    const res = yield call(api.fetchGet, url);
    yield put(loadCampaignDownloadPurchasedActionSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadCampaignDownloadPurchasedActionFail(error.response.status));
    }
  }
}

function* loadDownloadCompanyPanelDetailsSaga(data) {
  try {
    const url = `/share/company/finance/details.json?uid=${data.uid}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadCompanyPanelDetailsSuccess(res.data, data.uid));
  } catch (error) {
    yield put(loadDownloadCompanyPanelDetailsFail(error));
  }
}

function* loadDownloadCompanyPanelSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    const url = `/share/company/finance/simple.json?page=${page}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadCompanyPanelSuccess(res.data, page));
  } catch (error) {
    yield put(loadDownloadListFail(error));
  }
}

function* loadDownloadMomentInfoSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    const url = `/share/company/finance/details_online?uid=${data.movieUid}&page=${page}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadMomentInfoActionSuccess(res.data, page));
  } catch (error) {
    yield put(loadDownloadMomentInfoActionFail(error));
  }
}

function* companyGeneralMomentInfoSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    const start = data.start ? data.start : null;
    const end = data.end ? data.end : null;
    let url = `/share/company/finance/details_online_general`;

    if (start && end) {
      url += `?start_date=${start}&end_date=${end}&page=${page}`;
    } else {
      url += `?page=${page}`;
    }

    const res = yield call(api.fetchGet, url);
    yield put(companyGeneralMomentActionSuccess(res.data, page));
  } catch (error) {
    yield put(companyGeneralMomentActionFail(error));
  }
}

function* loadDownloadColleagueMomentInfoSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    const url = `/share/colleague/finance/details_online?uid=${data.movieUid}&page=${page}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadMomentInfoActionSuccess(res.data, page));
  } catch (error) {
    yield put(loadDownloadMomentInfoActionFail(error));
  }
}

function* loadColleagueInfoSaga() {
  try {
    const url = `/share/colleague/finance.json`;
    const res = yield call(api.fetchGet, url);
    yield put(colleagueActionSuccess(res.data));
  } catch (error) {
    yield put(colleagueActionFail(error));
  }
}

function* loadRecommDownloadSaga() {
  try {
    // const url = `/share/v2/movie/0/recommend.json`;
    const url = `https://elitland.com/api/share/v2/movie/0/recommend.json`;
    const res = yield call(api.fetchGet, url);
    yield put(loadRecommDownloadActionSuccess(res.data));
  } catch (error) {
    yield put(loadRecommDownloadActionFail(error));
  }
}

function* loadDownloadCategorySaga(data) {
  try {
    const page = data.page ? data.page : 1;
    const url = `/share/v2/list/download.json?page=${page}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadCategoryActionSuccess(res.data));
  } catch (error) {
    yield put(loadDownloadCategoryActionFail(error));
  }
}

function* loadSeriesColleagueSaga(data) {
  try {
    const page = data.page ? data.page : 1;
    let url = `/share/colleague/finance/series.json?page=${page}`;
    if (data.uid) {
      url = `/share/colleague/finance/series.json?series_uid=${data.uid}&page=${page}`;
    } else {
      url = `/share/colleague/finance/series.json?page=${page}`;
    }
    const res = yield call(api.fetchGet, url);
    yield put(loadSeriesColleaguePanelActionSuccess(res.data, page));
  } catch (error) {
    yield put(loadSeriesColleaguePanelActionFail(error));
  }
}

function* watchLoadDownloadList() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_LIST, loadDownloadListSaga);
}

function* watchLoadDownloadColleaguePanel() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL, loadDownloadColleaguePanelSaga);
}

function* watchLoadDownloadColleaguePanelDetails() {
  yield takeEvery(
    actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS,
    loadDownloadColleaguePanelDetailsSaga
  );
}

function* watchloadDownloadBuyInfoSaga() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_BUY_INFO, loadDownloadBuyInfoSaga);
}

function* watchloadDownloadColleagueLinksSaga() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_COLLEAGUE_LINKS, loadDownloadColleagueLinksSaga);
}

function* watchLoadCampaignDownloadSaga() {
  yield takeEvery(actionTypes.LOAD_CAMPAIGN_DOWNLOAD, loadCampaignDownloadSaga);
}

function* watchLoadCampaignDownloadPurchasedSaga() {
  yield takeEvery(actionTypes.LOAD_CAMPAIGN_DOWNLOAD_PURCHASED, loadCampaignDownloadPurchasedSaga);
}

function* watchLoadDownloadCompanyPanel() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL, loadDownloadCompanyPanelSaga);
}

function* watchLoadDownloadCompanyPanelDetails() {
  yield takeEvery(
    actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS,
    loadDownloadCompanyPanelDetailsSaga
  );
}

function* watchLoadDownloadMomentInfoSaga() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_MOMENT_INFO, loadDownloadMomentInfoSaga);
}
function* watchCompanyGeneralMomentInfo() {
  yield takeEvery(actionTypes.LOAD_COMPANY_GENERAL_MOMENT_INFO, companyGeneralMomentInfoSaga);
}

function* watchLoadDownloadColleagueMomentInfoSaga() {
  yield takeEvery(
    actionTypes.LOAD_DOWNLOAD_COLLEAGUE_MOMENT_INFO,
    loadDownloadColleagueMomentInfoSaga
  );
}

function* watchLoadcolleagueInfoSaga() {
  yield takeEvery(actionTypes.LOAD_COLLEAGUE_INFO, loadColleagueInfoSaga);
}

function* watchLoadRecommDownloadSaga() {
  yield takeEvery(actionTypes.LOAD_RECOMM_DOWNLOAD, loadRecommDownloadSaga);
}

function* watchLoadDownloadCategorySaga() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_CATEGORY, loadDownloadCategorySaga);
}

function* watchLoadColleagueReportSaga() {
  yield takeEvery(actionTypes.LOAD_COLLEAGUE_REPORT, loadColleagueReportSaga);
}

function* watchLoadMovieColleagueSaga() {
  yield takeEvery(actionTypes.LOAD_MOVIE_COLLEAGUE_PANEL, loadMovieColleagueSaga);
}

function* watchLoadSeriesColleagueSaga() {
  yield takeEvery(actionTypes.LOAD_SERIES_COLLEAGUE_PANEL, loadSeriesColleagueSaga);
}

export default function* downloadSagas() {
  yield fork(watchLoadDownloadList);
  yield fork(watchLoadDownloadColleaguePanel);
  yield fork(watchLoadDownloadColleaguePanelDetails);
  yield fork(watchloadDownloadBuyInfoSaga);
  yield fork(watchloadDownloadColleagueLinksSaga);
  yield fork(watchLoadCampaignDownloadSaga);
  yield fork(watchLoadCampaignDownloadPurchasedSaga);
  yield fork(watchLoadDownloadCompanyPanel);
  yield fork(watchLoadDownloadCompanyPanelDetails);
  yield fork(watchLoadDownloadMomentInfoSaga);
  yield fork(watchLoadDownloadColleagueMomentInfoSaga);
  yield fork(watchLoadcolleagueInfoSaga);
  yield fork(watchCompanyGeneralMomentInfo);
  yield fork(watchLoadRecommDownloadSaga);
  yield fork(watchLoadDownloadCategorySaga);
  yield fork(watchLoadColleagueReportSaga);
  yield fork(watchLoadMovieColleagueSaga);
  yield fork(watchLoadSeriesColleagueSaga);
}
