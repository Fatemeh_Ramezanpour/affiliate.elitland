/* eslint-disable no-undef */
import "isomorphic-unfetch";

import {
  actionTypes,
  OTPCreationFailure,
  OTPCreationSuccess,
  changePasswordFailure,
  changePasswordSuccess,
  loadUserinfoSuccess,
  loginFailure,
  loginSuccess,
  logoutFailure,
  logoutSuccess,
  registerGuestUserFailure,
  registerGuestUserSuccess,
  setPasswordSignupFailure,
  setPasswordSignupSuccess,
  verifyMobileFailure,
  verifyMobileSuccess,
  forgotPasswordInOldSystemSuccess,
  forgotPasswordInOldSystemFailure
} from "posts/actions/auth";
import { call, fork, put, takeEvery } from "redux-saga/effects";

import { addError, addSuccess } from "posts/actions/alert";
import api from "services/api";
import { closeLoginModal } from "posts/actions/dialogLogin";
import { homeUrl, backToPageFromLogin } from "services/url";
// import { Router } from "routes/routes";
import Router from "next/router";

import es6promise from "es6-promise";
es6promise.polyfill();

function* userInfo() {
  try {
    const url = "/share/user/profile.json";
    const res = yield call(api.fetchGet, url);
    yield put(loadUserinfoSuccess(res.data));
  } catch (err) {
    // Alert(err);
    // yield put(AdminGetFeatureListFail(err));
  }
}

function* registerGuestUser(action) {
  try {
    const url = "/registration/mobiles.json";
    const res = yield call(api.fetchPost, url, action.signupInfo);
    yield put(registerGuestUserSuccess(res.data));
  } catch (err) {
    // Alert(err);
    yield put(registerGuestUserFailure(err));
  }
}
// Export const setCookie = (data) => {
//   const cook = new Cookies();
//   // cook.set("token_elitland", data.token,{ path: '/' });
//   // if(req){

//   //   req.universalCookies.set("token_elitland", data.token);
//   // }
//   // if(res){
//   //   res.redirect("/");
//   // }
// };

function* login(action) {
  try {
    const url = "/share/user/login.json";
    const res = yield call(api.fetchPost, url, action.userData);

    yield put(loginSuccess(res.data));
    yield put(closeLoginModal());
    if (action.url) {
      window.location = backToPageFromLogin(action.url);
    } else {
      window.location = homeUrl();
    }
    // Yield call(setCookie, res.data);
  } catch (err) {
    yield put(loginFailure(err));
    yield put(
      addError({
        title: "خطای ورود",
        body: `${err.response && err.response.status === 422 ? "رمز عبور اشتباه است" : ""}`
      })
    );
  }
}

function* OTPCreation(action) {
  try {
    const url = "/verification/mobiles.json";
    const res = yield call(api.fetchPost, url, action.data);

    yield put(OTPCreationSuccess(res.data));
  } catch (err) {
    yield put(OTPCreationFailure(err));
  }
}

function* verifyMobile(action) {
  try {
    const url = `/verification/mobiles/${action.mobile}.json`;
    const res = yield call(api.fetchPut, url, action.code);
    yield put(verifyMobileSuccess(res.data));
  } catch (err) {
    yield put(verifyMobileFailure(err));
  }
}

function* changePassword(action) {
  try {
    const url = `/password/mobiles/${action.mobile}.json`;
    const res = yield call(api.fetchPut, url, action.data);
    yield put(changePasswordSuccess(res.data));
    yield put(closeLoginModal());
  } catch (err) {
    yield put(changePasswordFailure(err));
    yield put(addError({ title: "خطای تغییر رمز عبور" }));
  }
}

function* setPasswordSignup(action) {
  try {
    const url = `/users/${action.uid}/profiles.json`;
    const res = yield call(api.fetchPost, url, action.data);
    yield put(setPasswordSignupSuccess(res.data));
    yield put(closeLoginModal());
  } catch (err) {
    yield put(setPasswordSignupFailure(err));
  }
}

export const removeCookie = data => {
  const { req, res } = data;
  if (req) {
    req.universalCookies.remove("token_elitland", {
      path: "/",
      domain: cookieDomain
    });
  }

  if (res) {
    res.redirect("/");
  }
};

function* logout(action) {
  try {
    const url = "/share/user/logout.json";
    const res = yield call(api.fetchDelete, url);
    yield put(logoutSuccess(res.data));
    yield call(removeCookie, res.data);
    if (action && action.params) {
      yield call(Router.push, backToPageFromLogin(action.params));
    } else {
      window.location = homeUrl();
    }
    // eslint-disable-next-line no-undef
    // window.location = homeUrl();
    // Yield put(closeLoginModal());
  } catch (err) {
    yield put(logoutFailure(err));
  }
}
function* forgotPasswordInOldSystemSaga(action) {
  try {
    const url = `share/change_password/forgot_password.json`;
    const res = yield call(api.fetchPost, url, action.data);
    yield put(forgotPasswordInOldSystemSuccess(res.data));
    yield put(
      addSuccess({
        title: "ایمیل تغییر رمز عبور برای شما ارسال شد "
      })
    );

    // yield put(closeLoginModal());
  } catch (err) {
    if (err.response) {
      yield put(forgotPasswordInOldSystemFailure(err.response.status));
      yield put(
        addError({
          title: "خطای تغییر رمز عبور",
          body: `${
            err.response && err.response.status === 400 ? "تغییر رمز عبور با مشکل مواجه شد" : ""
          }`
        })
      );
    }
  }
}
function* watchLoadUserInfo() {
  yield takeEvery(actionTypes.LOAD_USER_INFO, userInfo);
}

function* watchregisterGuestUser() {
  yield takeEvery(actionTypes.REGISTER_GUEST_USER, registerGuestUser);
}

function* watchLogin() {
  yield takeEvery(actionTypes.LOGIN, login);
}

function* watchOTP() {
  yield takeEvery(actionTypes.OTP_CREATION, OTPCreation);
}

function* watchVerifyMobile() {
  yield takeEvery(actionTypes.VERIFY_MOBILE, verifyMobile);
}

function* watchChangePassword() {
  yield takeEvery(actionTypes.CHANGE_PASSWORD, changePassword);
}

function* watchSetPasswordSignup() {
  yield takeEvery(actionTypes.SET_PASSWORD_SIGNUP, setPasswordSignup);
}

function* watchLogout() {
  yield takeEvery(actionTypes.LOGOUT, logout);
}
function* watchForgotPasswordInOldSystem() {
  yield takeEvery(actionTypes.FORGOT_PASSWORD_In_OLD_SYSTEM, forgotPasswordInOldSystemSaga);
}

export default function* authSagas() {
  yield fork(watchLoadUserInfo);
  yield fork(watchregisterGuestUser);
  yield fork(watchLogin);
  yield fork(watchOTP);
  yield fork(watchVerifyMobile);
  yield fork(watchChangePassword);
  yield fork(watchSetPasswordSignup);
  yield fork(watchLogout);
  yield fork(watchForgotPasswordInOldSystem);
}
