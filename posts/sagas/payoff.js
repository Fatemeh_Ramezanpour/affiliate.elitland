import { call, put, fork, takeEvery } from "redux-saga/effects";
import es6promise from "es6-promise";
import "isomorphic-unfetch";
import api from "services/api";
// import { Router } from "routes/routes";
import Router from "next/router";

import { panelLink } from "services/url";
import {
  actionTypes,
  panelPayoffActionSuccess,
  panelPayoffActionFail,
  payoffReportActionSuccess,
  payoffReportActionFail,
  loadPayoffAccountingActionSuccess,
  loadPayoffAccountingActionFail
} from "posts/actions/payoff";
import { addError } from "posts/actions/alert";

es6promise.polyfill();

function* panelPayoffSaga(action) {
  try {
    const url = `/share/v2/pay_off.json`;
    const res = yield call(api.fetchPost, url, action.data);
    yield put(panelPayoffActionSuccess(res.data));
    yield call(Router.push, panelLink("payoff-report"));
  } catch (error) {
    if (error.response) {
      yield put(
        addError({ title: "درخواست تسویه با مشکل مواجه شد", body: error.response.data[0] })
      );
      yield put(panelPayoffActionFail(error.response.status));
    }
  }
}

function* payoffReportSaga() {
  try {
    const url = `/sales_partner/payoffs.json`;
    const res = yield call(api.fetchGet, url);
    yield put(payoffReportActionSuccess(res.data));
    // yield call(Router.push, panelLink('payoff-report'));
  } catch (error) {
    if (error.response) {
      yield put(addError({ title: "    ", body: error.response.data[0] }));
      yield put(payoffReportActionFail(error.response.status));
    }
  }
}

function* loadPayoffAccountingSaga() {
  try {
    const url = `/share/colleague/finance/payoff_report.json`;
    const res = yield call(api.fetchGet, url);
    yield put(loadPayoffAccountingActionSuccess(res.data));
    // yield call(Router.push, panelLink('payoff-report'));
  } catch (error) {
    if (error.response) {
      yield put(addError({ title: "    ", body: error.response.data[0] }));
      yield put(loadPayoffAccountingActionFail(error.response.status));
    }
  }
}

function* watchPanelPayoffSaga() {
  yield takeEvery(actionTypes.PANEL_PAYOFF, panelPayoffSaga);
}
function* watchPayoffReportSaga() {
  yield takeEvery(actionTypes.PAYOFF_REPORT, payoffReportSaga);
}
function* watchPayoffAccountingSaga() {
  yield takeEvery(actionTypes.PAYOFF_ACCOUNTING, loadPayoffAccountingSaga);
}

export default function* payoffSagas() {
  yield fork(watchPanelPayoffSaga);
  yield fork(watchPayoffReportSaga);
  yield fork(watchPayoffAccountingSaga);
}
