import "isomorphic-unfetch";

import {
  LOAD_COMPLETE_SEARCH,
  LOAD_MOVIE_SEARCH,
  LOAD_NEWS_SEARCH,
  loadCompleteSearchFailure,
  loadCompleteSearchSuccess,
  loadMovieSearchFailure,
  loadMovieSearchSuccess,
  loadNewsSearchFailure,
  loadNewsSearchSuccess
} from "posts/actions/search";
import { call, fork, put, takeEvery } from "redux-saga/effects";

import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

function* completeSearchData(action) {
  try {
    const url = `/share/v1/search?query=${action.data}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadCompleteSearchSuccess(res.data));
  } catch (err) {
    yield put(loadCompleteSearchFailure(err));
  }
}

function* getMovieSearch(action) {
  try {
    const movieUrl = `/share/v1/search/movie.json?query=${action.data}`;
    const res = yield call(api.fetchGet, movieUrl);
    yield put(loadMovieSearchSuccess(res.data));
  } catch (err) {
    yield put(loadMovieSearchFailure(err));
  }
}

function* getNewSearch(action) {
  try {
    const movieUrl = `/share/v1/search/news.json?query=${action.data}`;
    const res = yield call(api.fetchGet, movieUrl);
    yield put(loadNewsSearchSuccess(res.data));
  } catch (err) {
    yield put(loadNewsSearchFailure(err));
  }
}

function* watchCompleteSearch() {
  yield takeEvery(LOAD_COMPLETE_SEARCH, completeSearchData);
}

function* watchMovieSearch() {
  yield takeEvery(LOAD_MOVIE_SEARCH, getMovieSearch);
}

function* watchNewsSearch() {
  yield takeEvery(LOAD_NEWS_SEARCH, getNewSearch);
}
// Function* watchActorMovie(){
//   yield takeEvery(LOAD_ACTOR_MOVIE_SEARCH, ActorMovieSearch);
// }

export default function* searchSagas() {
  yield fork(watchCompleteSearch);
  yield fork(watchMovieSearch);
  yield fork(watchNewsSearch);
  // Yield fork(watchActorMovie);
}
