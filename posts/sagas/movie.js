import "isomorphic-unfetch";

import {
  LOAD_FOREIGN_MOVIE_ARCHIVE,
  LOAD_FOREIGN_MOVIE_TOP_VIEW,
  LOAD_PERSIAN_MOVIE_ARCHIVE,
  LOAD_PERSIAN_MOVIE_TOP_VIEW,
  loadForeignMoviesArchiveFail,
  loadForeignMoviesArchiveSuccess,
  loadForiegnMovieTopViewFail,
  loadForiegnMovieTopViewSuccess,
  loadPersianMovieTopViewFail,
  loadPersianMovieTopViewSuccess,
  loadPersianMoviesArchiveFail,
  loadPersianMoviesArchiveSuccess
} from "posts/actions/movie";
import { call, fork, put, takeEvery } from "redux-saga/effects";

import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

function* loadForiegnTopViewMovie() {
  try {
    const url = "/share/v2/movie.json?page=1&per_page=4&movie_types[0]=foreign_cinema";

    const res = yield call(api.fetchGet, url);
    yield put(loadForiegnMovieTopViewSuccess(res.data));
  } catch (error) {
    yield put(loadForiegnMovieTopViewFail(error));
  }
}

function* loadPersianTopViewMovie() {
  try {
    const url = "/share/v2/movie.json?page=1&per_page=4&movie_types[0]=cinema";

    const res = yield call(api.fetchGet, url);
    yield put(loadPersianMovieTopViewSuccess(res.data));
  } catch (error) {
    yield put(loadPersianMovieTopViewFail(error));
  }
}

function* loadPersianArchiveMovie(page) {
  try {
    const url = `/share/v2/movie.json?page=${page.data}&per_page=5&movie_types[0]=cinema`;

    const res = yield call(api.fetchGet, url);
    yield put(loadPersianMoviesArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadPersianMoviesArchiveFail(error.response.status));
    }
  }
}

function* loadForeignArchiveMovie(page) {
  try {
    const url = `/share/v2/movie.json?page=${page.data}&per_page=5&movie_types[0]=foreign_cinema`;

    const res = yield call(api.fetchGet, url);
    yield put(loadForeignMoviesArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadForeignMoviesArchiveFail(error.response.status));
    }
  }
}

function* loadPersianTopMovie() {
  yield takeEvery(LOAD_PERSIAN_MOVIE_TOP_VIEW, loadPersianTopViewMovie);
}

function* loadForiegnTopMovie() {
  yield takeEvery(LOAD_FOREIGN_MOVIE_TOP_VIEW, loadForiegnTopViewMovie);
}

function* loadPersianArchive() {
  yield takeEvery(LOAD_PERSIAN_MOVIE_ARCHIVE, loadPersianArchiveMovie);
}

// eslint-disable-next-line generator-star-spacing
function* loadForeignArchive() {
  yield takeEvery(LOAD_FOREIGN_MOVIE_ARCHIVE, loadForeignArchiveMovie);
}

// eslint-disable-next-line generator-star-spacing
export default function* movieSagas() {
  yield fork(loadPersianTopMovie);
  yield fork(loadForiegnTopMovie);
  yield fork(loadPersianArchive);
  yield fork(loadForeignArchive);
}
