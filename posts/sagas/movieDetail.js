import "isomorphic-unfetch";

import {
  // LOAD_COMMENTS_MOVIE_DATA,
  LOAD_FIND_BY_MOVIE_DATA,
  LOAD_MOVIE_DETAIL_DATA,
  // loadCommentsMovieDataFail,
  // loadCommentsMovieDataSuccess,
  loadFindByMovieDataFail,
  loadFindByMovieDataSuccess,
  loadMovieDetailDataFail,
  loadMovieDetailDataSuccess,
  SAVE_MOVIE_DETAIL_VOTE,
  saveMovieDetailVoteSuccess
} from "posts/actions/movie-detail";
import { call, fork, put, takeEvery } from "redux-saga/effects";

import { addError } from "posts/actions/alert";
import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

function* loadMovieDetailData(page) {
  try {
    const url = page.data ? `share/v2/movie/${page.data}/complete.json` : "";
    const res = yield call(api.fetchGet, url);
    yield put(loadMovieDetailDataSuccess(res.data));
  } catch (error) {
    yield put(loadMovieDetailDataFail(error));
  }
}

function* loadFindByMovieData(action) {
  try {
    const obj = {
      movies: [action.data]
    };
    const url = "share/v1/video/find_by_movie.json";
    const res = yield call(api.fetchPost, url, obj);
    yield put(loadFindByMovieDataSuccess(res.data));
  } catch (error) {
    yield put(loadFindByMovieDataFail(error));
  }
}

// function* loadCommentsMovieDetailData(page) {
//   try {
//     const url = page.data ? `share/v2/movie/${page.data}/comment.json?type=movie` : "";
//     const res = yield call(api.fetchGet, url);
//     yield put(loadCommentsMovieDataSuccess(res.data));
//   } catch (error) {
//     yield put(loadCommentsMovieDataFail(error));
//   }
// }

function* loadSaveVoteMovieDetailData(action) {
  try {
    const body = { rate: action.data };

    const url = `/share/v3/movie/${action.uid}/rate.json`;

    const res = yield call(api.fetchPost, url, body);
    yield put(saveMovieDetailVoteSuccess(res, action.MovieUid));
  } catch (err) {
    yield put(
      addError({
        title: "ثبت با خطا مواجه شد",
        body: `${
          err.response && err.response.status === 404 ? "فیلم برای رای دادن تایید نشده" : ""
        }`
      })
    );
  }
}

function* watchLoadMovieDetailData() {
  yield takeEvery(LOAD_MOVIE_DETAIL_DATA, loadMovieDetailData);
}

function* watchLoadFindByMovieData() {
  yield takeEvery(LOAD_FIND_BY_MOVIE_DATA, loadFindByMovieData);
}

// function* watchLoadCommentsMovieData() {
//   yield takeEvery(LOAD_COMMENTS_MOVIE_DATA, loadCommentsMovieDetailData);
// }

function* watchLoadSaveVoteMovieDetailData() {
  yield takeEvery(SAVE_MOVIE_DETAIL_VOTE, loadSaveVoteMovieDetailData);
}

export default function* movieDetailSagas() {
  yield fork(watchLoadMovieDetailData);
  yield fork(watchLoadFindByMovieData);
  // yield fork(watchLoadCommentsMovieData);
  yield fork(watchLoadSaveVoteMovieDetailData);
}
