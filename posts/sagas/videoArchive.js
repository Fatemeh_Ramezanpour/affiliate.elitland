import "isomorphic-unfetch";

import {
  LOAD_VIDEO,
  loadVideoActionSuccess,
  loadVideoActionFail,
  LOAD_VIDEO_DETAIL,
  loadVideoDetailSuccess,
  loadVideoDetailFail
} from "posts/actions/videoArchive";
import { call, fork, put, takeEvery } from "redux-saga/effects";

import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

export const videoType = data => {
  const type = data.videoType.replace(/persian-/g, "").replace(/foreign-/g, "");
  const domestic = data.videoType.includes("persian") ? "1" : "0";
  let url = `share/v1/video.json?video_type=${type}&domestic=${domestic}&per_page=${data.perpage}`;
  if (data.page) {
    url += `&page=${data.page}`;
  }
  return url;
};
function* loadVideosSaga(data) {
  try {
    const url = yield call(videoType, data);
    const res = yield call(api.fetchGet, url);
    yield put(loadVideoActionSuccess(res.data, data.videoType));
  } catch (error) {
    if (error.response) {
      yield put(loadVideoActionFail(error.response.status, data.videoType));
    }
  }
}

function* loadVideoDetailSaga(data) {
  const id = data.data;
  try {
    const url = `/share/v1/video/${id}.json`;

    const res = yield call(api.fetchGet, url);

    yield put(loadVideoDetailSuccess(res.data));
  } catch (error) {
    if (error) {
      yield put(loadVideoDetailFail(error));
    }
  }
}

function* watchLoadVideos() {
  yield takeEvery(LOAD_VIDEO, loadVideosSaga);
}
function* watchLoadVideoDetail() {
  yield takeEvery(LOAD_VIDEO_DETAIL, loadVideoDetailSaga);
}

export default function* videosArchiveSagas() {
  yield fork(watchLoadVideos);
  yield fork(watchLoadVideoDetail);
}
