import "isomorphic-unfetch";

import {
  DELETE_WATCHLIST,
  LOAD_WATCHLIST,
  LOAD_WATCHLIST_TOP,
  MORE_REORDER_WATCHLIST,
  REORDER_WATCHLIST,
  SAVE_MOVIE_VOTE,
  WatchlistDataDeleteFail,
  WatchlistDataDeleteSuccess,
  getWatchlistSuccess,
  getWatchlistTopSuccess,
  moreReOrderWatchlistSuccess,
  reOrderWatchlistSuccess,
  saveMovieVoteSuccess
} from "posts/actions/watchlist";
import { addError, addSuccess } from "posts/actions/alert";
import { call, fork, put, takeEvery } from "redux-saga/effects";

// Import apiCleint from "services/api";
import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

function* getWatchlistData(action) {
  try {
    const url = `/movie/watchlists.json?page=${action.page}`;
    const res = yield call(api.fetchGet, url);
    yield put(getWatchlistSuccess(res.data));
  } catch (err) {
    yield put(addError({ title: "خطای عدم لاگین بودن" }));
  }
}

function* getWatchlistTopMovies() {
  try {
    const url = "/movie/watchlists/top_movies.json";
    const res = yield call(api.fetchGet, url);
    yield put(getWatchlistTopSuccess(res.data));
  } catch (err) {
    addError(err);
  }
}

function* WatchlistDataDeleteItem(action) {
  try {
    const body = { movie_uid: action.MovieUid };
    const url = "/movie/watchlists/destroy.json";
    const res = yield call(api.fetchDelete, url, body);

    yield put(WatchlistDataDeleteSuccess(res.data));
    yield put(addSuccess({ title: "حذف با موفقیت انجام شد" }));
  } catch (error) {
    yield put(addError({ title: "حذف با خطا مواجه شد" }));

    yield put(WatchlistDataDeleteFail(error));
  }
}

function* getReOrderWatchlist(action) {
  if (action.typeOfOrder === 1) {
    try {
      const url = `/movie/watchlists.json?list_type=avg_vote&page=${action.page}`;
      const res = yield call(api.fetchGet, url);

      yield put(reOrderWatchlistSuccess(res.data, action.typeOfOrder));
    } catch (err) {
      addError(err);
    }
  } else if (action.typeOfOrder === 2) {
    try {
      const url = `/movie/watchlists.json?list_type=produce_year&page=${action.page}`;
      const res = yield call(api.fetchGet, url);
      yield put(reOrderWatchlistSuccess(res.data, action.typeOfOrder));
    } catch (err) {
      addError(err);
    }
  } else {
    try {
      const url = `/movie/watchlists.json?page=${action.page}&list_type=alphabet`;
      const res = yield call(api.fetchGet, url);
      yield put(reOrderWatchlistSuccess(res.data, action.typeOfOrder));
    } catch (err) {
      addError(err);
    }
  }
}

function* getMoreReorderWatchlist(action) {
  if (action.typeOfOrder === 1) {
    try {
      const url = `/movie/watchlists.json?list_type=avg_vote&page=${action.page}`;
      const res = yield call(api.fetchGet, url);

      yield put(moreReOrderWatchlistSuccess(res.data, action.typeOfOrder));
    } catch (err) {
      addError(err);
    }
  } else if (action.typeOfOrder === 2) {
    try {
      const url = `/movie/watchlists.json?list_type=produce_year&page=${action.page}`;
      const res = yield call(api.fetchGet, url);
      yield put(moreReOrderWatchlistSuccess(res.data, action.typeOfOrder));
    } catch (err) {
      addError(err);
    }
  } else {
    try {
      const url = `/movie/watchlists.json?page=${action.page}&list_type=alphabet`;
      const res = yield call(api.fetchGet, url);
      yield put(moreReOrderWatchlistSuccess(res.data, action.typeOfOrder));
    } catch (err) {
      addError(err);
    }
  }
}

function* rateWatchlistMovie(action) {
  try {
    const body = { rate: action.vote };
    const url = `/share/v3/movie/${action.MovieUid}/rate.json`;
    const res = yield call(api.fetchPost, url, body);
    yield put(saveMovieVoteSuccess(res, action.MovieUid));
    yield put(addSuccess({ title: "ثبت رای با موفقیت انجام شد" }));
  } catch (err) {
    yield put(
      addError({
        title: "ثبت با خطا مواجه شد",
        body: `${
          err.response && err.response.status === 404 ? "فیلم برای رای دادن تایید نشده" : ""
        }`
      })
    );
  }
}

function* watchLoadWatchlist() {
  yield takeEvery(LOAD_WATCHLIST, getWatchlistData);
}

function* watchRateWatchlist() {
  yield takeEvery(SAVE_MOVIE_VOTE, rateWatchlistMovie);
}

function* watchLoadTopWatchlist() {
  yield takeEvery(LOAD_WATCHLIST_TOP, getWatchlistTopMovies);
}

function* watchDeleteWatchlist() {
  yield takeEvery(DELETE_WATCHLIST, WatchlistDataDeleteItem);
}

function* watchReorderWatchlist() {
  yield takeEvery(REORDER_WATCHLIST, getReOrderWatchlist);
}

function* watchMoreReorderWatchlist() {
  yield takeEvery(MORE_REORDER_WATCHLIST, getMoreReorderWatchlist);
}

export default function* listSagas() {
  yield fork(watchLoadWatchlist);
  yield fork(watchRateWatchlist);
  yield fork(watchLoadTopWatchlist);
  yield fork(watchDeleteWatchlist);
  yield fork(watchReorderWatchlist);
  yield fork(watchMoreReorderWatchlist);
}
