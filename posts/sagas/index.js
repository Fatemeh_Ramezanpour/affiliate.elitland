import { all } from "redux-saga/effects";
import searchSagas from "./search";
import listSagas from "./list";
import authSagas from "./auth";
import newsSagas from "./news";
import movieSagas from "./movie";
import reviewSagas from "./review";
import profileSagas from "./profile";
import movieDetailSagas from "./movieDetail";
import comment from "./comment";
import downloadSagas from "./download";
import downloadDetailsSagas from "./download-details";
import videosArchiveSagas from "./videoArchive";
import payoffSagas from "./payoff";

// eslint-disable-next-line generator-star-spacing
export default function* rootSaga() {
  yield all([
    searchSagas(),
    listSagas(),
    authSagas(),
    newsSagas(),
    movieSagas(),
    reviewSagas(),
    profileSagas(),
    movieDetailSagas(),
    downloadSagas(),
    downloadDetailsSagas(),
    comment(),
    videosArchiveSagas(),
    payoffSagas()
  ]);
}
