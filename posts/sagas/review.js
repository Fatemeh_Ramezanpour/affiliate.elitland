import "isomorphic-unfetch";

import {
  LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE,
  LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE,
  LOAD_ARTICLE_REVIEWS_ARCHIVE,
  LOAD_FOREIGN_REVIEWS_ARCHIVE,
  LOAD_PEOPLE_REVIEWS_ARCHIVE,
  LOAD_PERSIAN_REVIEWS_ARCHIVE,
  LOAD_RECENT_ARTICLES_ARCHIVE,
  LOAD_RECENT_REVIEWS_ARCHIVE,
  loadAllArticleReviewsArchiveFail,
  loadAllArticleReviewsArchiveSuccess,
  loadAllPeopleReviewsArchiveFail,
  loadAllPeopleReviewsArchiveSuccess,
  loadArticleReviewsArchiveFail,
  loadArticleReviewsArchiveSuccess,
  loadForeignReviewsArchiveFail,
  loadForeignReviewsArchiveSuccess,
  loadPeopleReviewsArchiveFail,
  loadPeopleReviewsArchiveSuccess,
  loadPersianReviewsArchiveFail,
  loadPersianReviewsArchiveSuccess,
  loadRecentArticlesArchiveFail,
  loadRecentArticlesArchiveSuccess,
  loadRecentReviewsArchiveFail,
  loadRecentReviewsArchiveSuccess
} from "posts/actions/reviews";
import { call, fork, put, takeEvery } from "redux-saga/effects";

import api from "services/api";
import es6promise from "es6-promise";

es6promise.polyfill();

function* loadPeopleReviews() {
  try {
    const url = "share/v2/movie/people_critic.json";

    const res = yield call(api.fetchGet, url);
    yield put(loadPeopleReviewsArchiveSuccess(res.data));
  } catch (error) {
    yield put(loadPeopleReviewsArchiveFail(error));
  }
}

function* loadArticleReviews() {
  try {
    const url = "share/v2/movie/article.json";

    const res = yield call(api.fetchGet, url);
    yield put(loadArticleReviewsArchiveSuccess(res.data));
  } catch (error) {
    yield put(loadArticleReviewsArchiveFail(error));
  }
}

function* loadPersianReviews(page) {
  try {
    const url = page.data
      ? `share/v2/movie/long_critic.json?page=${page.data}&type=iran&per_page=10`
      : "share/v2/movie/long_critic.json&per_page=10";

    const res = yield call(api.fetchGet, url);

    yield put(loadPersianReviewsArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadPersianReviewsArchiveFail(error.response.status));
    }
  }
}

function* loadForeignReviews(page) {
  try {
    const url = page.data
      ? `share/v2/movie/long_critic.json?page=${page.data}&type=world&per_page=10`
      : "share/v2/movie/long_critic.json&per_page=10";

    const res = yield call(api.fetchGet, url);

    yield put(loadForeignReviewsArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadForeignReviewsArchiveFail(error.response.status));
    }
  }
}

function* loadAllPeopleReviews(page) {
  try {
    const url = page.data
      ? `share/v2/movie/long_critic.json?page=${page.data}&type=people&per_page=10`
      : "share/v2/movie/long_critic.json&per_page=10";

    const res = yield call(api.fetchGet, url);

    yield put(loadAllPeopleReviewsArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadAllPeopleReviewsArchiveFail(error.response.status));
    }
  }
}

function* loadAllArticleReviews(page) {
  try {
    const url = page.data
      ? `share/v2/movie/article?page=${page.data}&per_page=10`
      : "share/v2/movie/article?&per_page=10";

    const res = yield call(api.fetchGet, url);

    yield put(loadAllArticleReviewsArchiveSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(loadAllArticleReviewsArchiveFail(error.response.status));
    }
  }
}

function* loadRecentReviewsArchive() {
  try {
    const url = "share/v2/movie/long_critic.json?top_posts=true";

    const res = yield call(api.fetchGet, url);

    yield put(loadRecentReviewsArchiveSuccess(res.data));
  } catch (error) {
    yield put(loadRecentReviewsArchiveFail(error));
  }
}

function* loadRecentArticlesArchive() {
  try {
    const url = "share/v2/movie/article?top_articles=true";

    const res = yield call(api.fetchGet, url);

    yield put(loadRecentArticlesArchiveSuccess(res.data));
  } catch (error) {
    yield put(loadRecentArticlesArchiveFail(error));
  }
}

function* watchLoadPeopleReviews() {
  yield takeEvery(LOAD_PEOPLE_REVIEWS_ARCHIVE, loadPeopleReviews);
}

function* watchLoadArticleReviews() {
  yield takeEvery(LOAD_ARTICLE_REVIEWS_ARCHIVE, loadArticleReviews);
}

function* watchLoadPersianReviews() {
  yield takeEvery(LOAD_PERSIAN_REVIEWS_ARCHIVE, loadPersianReviews);
}

function* watchLoadForeignReviews() {
  yield takeEvery(LOAD_FOREIGN_REVIEWS_ARCHIVE, loadForeignReviews);
}

function* watchLoadAllPeopleReviews() {
  yield takeEvery(LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE, loadAllPeopleReviews);
}

function* watchLoadAllArticleReviews() {
  yield takeEvery(LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE, loadAllArticleReviews);
}

function* watchLoadRecentReviewsArchive() {
  yield takeEvery(LOAD_RECENT_REVIEWS_ARCHIVE, loadRecentReviewsArchive);
}

function* watchLoadRecentArticlesArchive() {
  yield takeEvery(LOAD_RECENT_ARTICLES_ARCHIVE, loadRecentArticlesArchive);
}

export default function* reviewSagas() {
  yield fork(watchLoadPeopleReviews);
  yield fork(watchLoadArticleReviews);
  yield fork(watchLoadPersianReviews);
  yield fork(watchLoadForeignReviews);
  yield fork(watchLoadAllPeopleReviews);
  yield fork(watchLoadAllArticleReviews);
  yield fork(watchLoadRecentReviewsArchive);
  yield fork(watchLoadRecentArticlesArchive);
}
