import { call, put, fork, takeEvery } from "redux-saga/effects";
import es6promise from "es6-promise";
import "isomorphic-unfetch";
import api from "services/api";
import { redirect } from "services/helper";
import { addError } from "posts/actions/alert";

import {
  actionTypes,
  loadDownloadDetailsActionSuccess,
  loadDownloadDetailsActionFail,
  loadDownloadBuyActionSuccess,
  loadDownloadBuyActionFail,
  loadDownloadDetailsListActionSuccess,
  loadDownloadDetailsListActionFail,
  loadCampaignDownloadDetailsActionSuccess,
  loadCampaignDownloadDetailsActionFail,
  loadCampaignDownloadSecondDetailsActionSuccess,
  loadCampaignDownloadSecondDetailsActionFail,
  LOAD_COLLEAGUE_RANK,
  loadcolleagueRankActionSuccess,
  loadcolleagueRankActionFail,
  LOAD_COLLEAGUE_MOVIE_RANK,
  loadColleagueMovieRankActionSuccess,
  loadColleagueMovieRankActionFail
} from "posts/actions/download-details";

es6promise.polyfill();

function* loadDownloadDetailsSaga(data) {
  try {
    const url = `share/v2/download/${data.uid}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadDetailsActionSuccess(res.data));
  } catch (error) {
    if (error.response) {
      yield put(addError({ title: "خطا ", body: error.response.data[0] }));
    }
    yield put(loadDownloadDetailsListActionFail(error));
  }
}
// Buy Movie
function* loadDownloadBuySaga(data) {
  try {
    const url = `/share/v2/download/buy`;
    const res = yield call(api.fetchPost, url, data);
    yield put(loadDownloadBuyActionSuccess(res.data));
    redirect(res.data.redirect_url);
  } catch (error) {
    yield put(addError({ title: "خرید ناموفق", body: error.response.data[0] }));
    yield put(loadDownloadBuyActionFail(error));
  }
}

function* loadDownloadDetailsListSaga(data) {
  try {
    const url = `share/v2/download/${data.uid}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadDownloadDetailsListActionSuccess(res.data, data.uid));
  } catch (error) {
    yield put(loadDownloadDetailsActionFail(error));
  }
}

function* loadCampaignDownloadDetailsSaga(data) {
  try {
    const url = `share/v2/download/${data.uid}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadCampaignDownloadDetailsActionSuccess(res.data));
  } catch (error) {
    yield put(loadCampaignDownloadDetailsActionFail(error));
  }
}

function* loadCampaignDownloadSecondDetailsSaga(data) {
  try {
    const url = `share/v2/download/${data.uid}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadCampaignDownloadSecondDetailsActionSuccess(res.data));
  } catch (error) {
    yield put(loadCampaignDownloadSecondDetailsActionFail(error));
  }
}

function* loadcolleagueRankSaga() {
  try {
    const url = `/share/colleague/finance/colleague_rank.json`;
    const res = yield call(api.fetchGet, url);
    yield put(loadcolleagueRankActionSuccess(res.data));
  } catch (error) {
    yield put(loadcolleagueRankActionFail(error));
  }
}

function* loadColleagueMovieRankSaga(data) {
  try {
    const url = `/share/colleague/finance/colleague_movie_rank.json?uid=${data.uid}`;
    const res = yield call(api.fetchGet, url);
    yield put(loadColleagueMovieRankActionSuccess(res.data, data.uid));
  } catch (error) {
    yield put(loadColleagueMovieRankActionFail(error));
  }
}

function* watchLoadDownloadDetails() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_DETAILS, loadDownloadDetailsSaga);
}

function* watchLoadDownloadBuy() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_BUY, loadDownloadBuySaga);
}

function* watchLoadDownloadDetailsList() {
  yield takeEvery(actionTypes.LOAD_DOWNLOAD_DETAILS_LIST, loadDownloadDetailsListSaga);
}

function* watchLoadCampaignDownloadDetails() {
  yield takeEvery(actionTypes.LOAD_CAMPAIGN_DOWNLOAD_DETAILS, loadCampaignDownloadDetailsSaga);
}

function* watchLoadCampaignDownloadSecondDetails() {
  yield takeEvery(
    actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS,
    loadCampaignDownloadSecondDetailsSaga
  );
}

function* watchLoadcolleagueRank() {
  yield takeEvery(actionTypes.LOAD_COLLEAGUE_RANK, loadcolleagueRankSaga);
}

function* watchLoadColleagueMovieRank() {
  yield takeEvery(actionTypes.LOAD_COLLEAGUE_MOVIE_RANK, loadColleagueMovieRankSaga);
}

export default function* downloadDetailsSagas() {
  yield fork(watchLoadDownloadDetails);
  yield fork(watchLoadDownloadBuy);
  yield fork(watchLoadDownloadDetailsList);
  yield fork(watchLoadCampaignDownloadDetails);
  yield fork(watchLoadCampaignDownloadSecondDetails);
  yield fork(watchLoadcolleagueRank);
  yield fork(watchLoadColleagueMovieRank);
}
