import {
  LOAD_FOREIGN_PROFILE_ARCHIVE,
  LOAD_FOREIGN_PROFILE_ARCHIVE_FAIL,
  LOAD_FOREIGN_PROFILE_ARCHIVE_SUCCESS,
  LOAD_PERSIAN_PROFILE_ARCHIVE,
  LOAD_PERSIAN_PROFILE_ARCHIVE_FAIL,
  LOAD_PERSIAN_PROFILE_ARCHIVE_SUCCESS
} from "posts/actions/profile";

export const initialState = {
  persianData: [],
  persianTotal: 0,
  persianIsLoading: false,
  persianPage: 1,
  foreignData: [],
  foreignTotal: 0,
  foreignIsLoading: false,
  foreignPage: 1,
  statusCodePerPro: "",
  statusCodeForePro: ""
};

export default function profile(state = initialState, action) {
  switch (action.type) {
    case LOAD_PERSIAN_PROFILE_ARCHIVE:
      return {
        persianData: [],
        persianIsLoading: true
      };

    case LOAD_PERSIAN_PROFILE_ARCHIVE_SUCCESS:
      return {
        ...state,
        persianPage: state.persianPage + 1,
        persianIsLoading: false,
        persianData: action.data.cast,
        persianTotal: action.data.total
      };

    case LOAD_PERSIAN_PROFILE_ARCHIVE_FAIL:
      return {
        ...state,
        persianIsLoading: false,
        ...{ statusCodePerPro: action.error }
      };

    case LOAD_FOREIGN_PROFILE_ARCHIVE:
      return {
        foreignData: [],
        foreignIsLoading: true
      };

    case LOAD_FOREIGN_PROFILE_ARCHIVE_SUCCESS:
      return {
        ...state,
        foreignPage: state.foreignPage + 1,
        foreignIsLoading: false,
        foreignData: action.data.cast,
        foreignTotal: action.data.total
      };

    case LOAD_FOREIGN_PROFILE_ARCHIVE_FAIL:
      return {
        ...state,
        foreignIsLoading: false,
        ...{ statusCodeForePro: action.error }
      };

    default:
      return state;
  }
}
