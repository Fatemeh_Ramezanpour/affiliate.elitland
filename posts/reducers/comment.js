import {
  DELETE_COMMENTS_MOVIE_DATA,
  DELETE_COMMENTS_MOVIE_DATA_FAIL,
  DELETE_COMMENTS_MOVIE_DATA_SUCCESS,
  LOAD_COMMENTS_MOVIE_DATA,
  LOAD_COMMENTS_MOVIE_DATA_FAIL,
  LOAD_COMMENTS_MOVIE_DATA_SUCCESS,
  OPEN_REPLY_FORM,
  SAVE_COMMENTS_MOVIE_DATA,
  SAVE_COMMENTS_MOVIE_DATA_FAIL,
  SAVE_COMMENTS_MOVIE_DATA_SUCCESS
} from "posts/actions/comment";

const initialState = {
  commentsMovie: null,
  commentsMovieIsLoading: false,
  totalCommentsMovie: 0,
  saveCommentsMovieIsLoading: false,
  deleteCommentMovieIsLoading: false,
  commentId: null
};

const comment = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_COMMENTS_MOVIE_DATA:
      return {
        commentsMovie: null,
        commentsMovieIsLoading: true,
        totalCommentsMovie: 0
      };
    case LOAD_COMMENTS_MOVIE_DATA_SUCCESS:
      return {
        ...state,
        commentsMovieIsLoading: false,
        commentsMovie: action.data.comments,
        totalCommentsMovie: action.data.comment_count
      };
    case LOAD_COMMENTS_MOVIE_DATA_FAIL:
      return {
        ...state,
        commentsMovieIsLoading: false,
        ...{
          error: action.err
        }
      };
    case DELETE_COMMENTS_MOVIE_DATA:
      return {
        deleteCommentMovieIsLoading: true
      };
    case DELETE_COMMENTS_MOVIE_DATA_SUCCESS:
      return {
        ...state,
        commentsMovie: state.commentsMovie.filter(item => item.id !== action.deleteId),
        deleteCommentMovieIsLoading: false,
        totalCommentsMovie: state.totalCommentsMovie - 1
      };
    case DELETE_COMMENTS_MOVIE_DATA_FAIL:
      return {
        ...state,
        deleteCommentMovieIsLoading: false,
        ...{
          error: action.err
        }
      };
    case SAVE_COMMENTS_MOVIE_DATA:
      return {
        saveCommentsMovieIsLoading: true
      };
    case SAVE_COMMENTS_MOVIE_DATA_SUCCESS:
      return {
        ...state,
        commentsMovie: [...state.commentsMovie, action.result],
        saveCommentsMovieIsLoading: false,
        totalCommentsMovie: state.totalCommentsMovie + 1
      };
    case SAVE_COMMENTS_MOVIE_DATA_FAIL:
      return {
        ...state,
        saveCommentsMovieIsLoading: false,
        ...{
          error: action.err
        }
      };
    case OPEN_REPLY_FORM:
      return {
        ...state,
        commentId: action.CommentId
      };
    default:
      return state;
  }
};

export default comment;
