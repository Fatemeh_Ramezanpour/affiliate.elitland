import {
  LOAD_FOREIGN_NEWS,
  LOAD_FOREIGN_NEWS_ARCHIVE,
  LOAD_FOREIGN_NEWS_ARCHIVE_FAIL,
  LOAD_FOREIGN_NEWS_ARCHIVE_SUCCESS,
  LOAD_FOREIGN_NEWS_FAIL,
  LOAD_FOREIGN_NEWS_SUCCESS,
  LOAD_PERSIAN_NEWS,
  LOAD_PERSIAN_NEWS_ARCHIVE,
  LOAD_PERSIAN_NEWS_ARCHIVE_FAIL,
  LOAD_PERSIAN_NEWS_ARCHIVE_SUCCESS,
  LOAD_PERSIAN_NEWS_FAIL,
  LOAD_PERSIAN_NEWS_SUCCESS,
  LOAD_POPULAR_NEWS_SUCCESS
} from "posts/actions/news";

export const initialState = {
  completeArchive: {
    persianArchive: [],
    foreignArchive: [],
    message: ""
  },
  news: {
    foreign: [],
    persian: []
  },
  foreignPage: 0,
  persianPage: 0,
  newsisloading: false,
  foreNewsloading: false,
  perNewsloading: false,
  popularNews: [],
  statusCode: "",
  statusCodeFore: ""
};

export default function news(state = initialState, action) {
  switch (action.type) {
    case LOAD_FOREIGN_NEWS:
      return {
        completeArchive: {
          persianArchive: [],
          foreignArchive: []
        },
        foreNewsloading: true
      };
    case LOAD_FOREIGN_NEWS_SUCCESS:
      return {
        ...state,
        newsisloading: false,
        news: {
          ...state.news,
          foreign: action.data.foreign_daily_news
        },
        foreignPage: 0,
        foreNewsloading: false
      };
    case LOAD_FOREIGN_NEWS_FAIL:
      return {
        ...state,
        foreNewsloading: false,
        ...{ error: action.err }
      };
    case LOAD_PERSIAN_NEWS:
      return {
        ...state,
        perNewsloading: true
      };
    case LOAD_PERSIAN_NEWS_SUCCESS:
      return {
        ...state,
        // Newsisloading: false,
        news: {
          ...state.news,
          persian: action.data.iran_daily_news
        },
        persianPage: 0,
        perNewsloading: false
      };
    case LOAD_PERSIAN_NEWS_FAIL:
      return {
        ...state,
        completeArchive: {
          persianArchive: [],
          foreignArchive: []
        },
        perNewsloading: false,
        ...{ error: action.err }
      };
    case LOAD_PERSIAN_NEWS_ARCHIVE:
      return {
        ...state,
        newsisloading: true
      };
    case LOAD_PERSIAN_NEWS_ARCHIVE_SUCCESS:
      return {
        ...state,
        newsisloading: false,
        completeArchive: {
          ...state.completeArchive,
          persianArchive: {
            data: action.data.iran_daily_news,
            total: action.data.total
          }
        }
      };
    case LOAD_PERSIAN_NEWS_ARCHIVE_FAIL:
      return {
        ...state,
        newsisloading: false,
        ...{ statusCode: action.error }
      };
    case LOAD_FOREIGN_NEWS_ARCHIVE:
      return {
        ...state,
        newsisloading: true
      };
    case LOAD_FOREIGN_NEWS_ARCHIVE_SUCCESS:
      return {
        ...state,
        newsisloading: false,
        completeArchive: {
          ...state.completeArchive,
          foreignArchive: {
            data: action.data.foreign_daily_news,
            total: action.data.total
          }
        }
      };
    case LOAD_FOREIGN_NEWS_ARCHIVE_FAIL:
      return {
        ...state,
        newsisloading: false,
        ...{ statusCodeFore: action.error }
      };
    case LOAD_POPULAR_NEWS_SUCCESS:
      return {
        ...state,
        popularNews: action.data
      };
    default:
      return state;
  }
}
