/* eslint-disable no-case-declarations */
import {
  FIND_VIDEO,
  // LOAD_COMMENTS_MOVIE_DATA,
  // LOAD_COMMENTS_MOVIE_DATA_FAIL,
  // LOAD_COMMENTS_MOVIE_DATA_SUCCESS,
  LOAD_FIND_BY_MOVIE_DATA,
  LOAD_FIND_BY_MOVIE_DATA_FAIL,
  LOAD_FIND_BY_MOVIE_DATA_SUCCESS,
  LOAD_MOVIE_DETAIL_DATA,
  LOAD_MOVIE_DETAIL_DATA_FAIL,
  LOAD_MOVIE_DETAIL_DATA_SUCCESS,
  SAVE_MOVIE_DETAIL_VOTE,
  SAVE_MOVIE_DETAIL_VOTE_SUCCESS,
  SAVE_MOVIE_DETAIL_VOTE_FAIL,
  UPDATE_MOVIE_DETAIL_VOTE,
  UPDATE_MOVIE_DETAIL_VOTE_SUCCESS,
  UPDATE_MOVIE_DETAIL_VOTE_FAIL
} from "posts/actions/movie-detail";

export const initialState = {
  movieDetailData: null,
  movieDetailDataIsLoading: false,
  videos: null,
  videoIsLoading: false,
  comments: null,
  commentsIsLoading: false,
  totalComments: 0,
  movieVote: null,
  saveVoteMovieIsLoading: false,
  updateVoteMovieIsLoading: false
};

export default function movieDetail(state = initialState, action) {
  switch (action.type) {
    case LOAD_MOVIE_DETAIL_DATA:
      return {
        movieDetailData: null,
        movieDetailDataIsLoading: true
      };

    case LOAD_MOVIE_DETAIL_DATA_SUCCESS:
      return {
        ...state,
        movieDetailDataIsLoading: false,
        movieDetailData: action.data
      };

    case LOAD_MOVIE_DETAIL_DATA_FAIL:
      return {
        ...state,
        movieDetailDataIsLoading: false,
        ...{
          error: action.err
        }
      };

    case LOAD_FIND_BY_MOVIE_DATA:
      return {
        videos: null,
        videoIsLoading: true
      };

    case LOAD_FIND_BY_MOVIE_DATA_SUCCESS:
      return {
        ...state,
        videoIsLoading: false,
        videos: action.data
      };

    case LOAD_FIND_BY_MOVIE_DATA_FAIL:
      return {
        ...state,
        videoIsLoading: false,
        ...{
          error: action.err
        }
      };

    // case LOAD_COMMENTS_MOVIE_DATA:
    //   return {
    //     comments: null,
    //     commentsIsLoading: true,
    //     totalComments: 0
    //   };

    // case LOAD_COMMENTS_MOVIE_DATA_SUCCESS:
    //   return {
    //     ...state,
    //     commentsIsLoading: false,
    //     comments: action.data.comments,
    //     totalComments: action.data.comment_count
    //   };

    // case LOAD_COMMENTS_MOVIE_DATA_FAIL:
    //   return {
    //     ...state,
    //     commentsIsLoading: false,
    //     ...{
    //       error: action.err
    //     }
    //   };

    case SAVE_MOVIE_DETAIL_VOTE:
      return {
        ...state,
        saveVoteMovieIsLoading: true
      };
    case SAVE_MOVIE_DETAIL_VOTE_SUCCESS:
      return {
        ...state,
        saveVoteMovieIsLoading: false
      };
    case SAVE_MOVIE_DETAIL_VOTE_FAIL:
      return {
        ...state,
        saveVoteMovieIsLoading: false,
        ...{
          error: action.err
        }
      };

    case UPDATE_MOVIE_DETAIL_VOTE:
      return {
        updateVoteMovieIsLoading: true
      };

    case UPDATE_MOVIE_DETAIL_VOTE_SUCCESS:
      const movieVote = { ...state.movieDetailData };
      movieVote.summary.movieRating = {
        current_user_rate: action.data.yourVote,
        users_rate_count: action.data.userVoteCount,
        users_rate_avg: action.data.userVoteAvg,
        criticizers_rate_count: action.data.criticerVoteCount,
        criticizers_rate_avg: action.data.criticerVoteAvg
      };

      return {
        ...state,
        updateVoteMovieIsLoading: false,
        movieDetailData: movieVote
      };
    case UPDATE_MOVIE_DETAIL_VOTE_FAIL:
      return {
        ...state,
        updateVoteMovieIsLoading: false,
        ...{
          error: action.err
        }
      };

    case FIND_VIDEO:
      const info = [...state.videos];
      let selected = {};
      for (let i = 0; i < info.length; i++) {
        if (action.id === info[i].id) {
          selected = {
            id: info[i].id,
            file_url: info[i].file_url,
            thumbnails: {
              small: info[i].thumbnails.small,
              medium: info[i].thumbnails.medium,
              large: info[i].thumbnails.large
            },
            title: info[i].title,
            about: info[i].about
          };
        }
      }

      return {
        ...state,
        video: selected,
        muteVideo: true
      };
    default:
      return state;
  }
}
