import { actionTypes } from "posts/actions/payoff";
import { HYDRATE } from "next-redux-wrapper";

export const initialState = {
  payoff: {},
  // payoffReport: {},
  loading: false,
  payoffReport: false,
  accounting: { loading: false, data: null }
};

export default function payoff(state = initialState, action) {
  switch (action.type) {
    case HYDRATE: {
      return { ...state, ...action.payload.payoff };
    }
    case actionTypes.PANEL_PAYOFF:
      return {
        ...state,
        loading: true,
        submiting: true
      };
    case actionTypes.PANEL_PAYOFF_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        payoff: action.data,
        error: null,
        submiting: false
      };
    case actionTypes.PANEL_PAYOFF_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        payoff: null,
        error: action.err,
        submiting: false
      };
    case actionTypes.PAYOFF_REPORT:
      return {
        ...state,
        reportLoading: true,
        payoffReport: null
      };
    case actionTypes.PAYOFF_REPORT_SUCCESS:
      return {
        ...state,
        reportLoading: false,
        error: null,
        payoffReport: action.data
      };
    case actionTypes.PAYOFF_REPORT_FAIL:
      return {
        ...state,
        reportLoading: false,
        error: action.err,
        payoffReport: null
      };
    case actionTypes.PAYOFF_ACCOUNTING:
      return {
        ...state,
        accounting: {
          loading: true
        }
      };
    case actionTypes.PAYOFF_ACCOUNTING_SUCCESS:
      return {
        ...state,
        accounting: {
          loading: false,
          data: action.data
        }
      };
    case actionTypes.PAYOFF_ACCOUNTING_FAIL:
      return {
        ...state,
        accounting: {
          loading: false,
          data: null,
          error: action.err
        }
      };
    default:
      return state;
  }
}
