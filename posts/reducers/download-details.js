import { actionTypes } from "posts/actions/download-details";
import { HYDRATE } from "next-redux-wrapper";

export const initialState = {
  downloadDetails: null,
  downloadDetailsLoading: false,
  downloadBuyActionId: null,
  downloadDetailsList: {},
  secondDownloadDetails: null,
  secondDownloadDetailsLoading: false,
  rank: null,
  mRankLoading: false
};
let downloadList;
let colMovieRank;

export default function downloadDetails(state = initialState, action) {
  switch (action.type) {
    case HYDRATE: {
      return { ...state, ...action.payload.downloadDetails };
    }
    case actionTypes.LOAD_DOWNLOAD_DETAILS:
      return {
        ...state,
        downloadDetails: null,
        downloadDetailsLoading: true
      };

    case actionTypes.LOAD_DOWNLOAD_DETAILS_SUCCESS:
      action.data.partner_links.sort((a, b) => {
        const f1 = a.filesize;
        const f2 = b.filesize;
        return (f1 === null) - (f2 === null) || +(f1 > f2) || -(f1 < f2);
      });
      return {
        ...state,
        downloadDetails: action.data,
        downloadDetailsLoading: false
      };

    case actionTypes.LOAD_DOWNLOAD_DETAILS_FAIL:
      return {
        ...state,
        downloadDetailsLoading: false,
        ...{
          error: action.err
        }
      };
    case actionTypes.LOAD_DOWNLOAD_BUY:
      return {
        ...state,
        downloadBuyLoading: true,
        downloadBuyActionId: action.download_item_id
      };

    case actionTypes.LOAD_DOWNLOAD_BUY_SUCCESS:
      return {
        ...state,
        downloadBuyLoading: false,
        downloadBuyActionId: null
      };

    case actionTypes.LOAD_DOWNLOAD_BUY_FAIL:
      return {
        ...state,
        downloadBuyLoading: false,
        downloadBuyActionId: null,
        ...{
          error: action.err
        }
      };

    case actionTypes.LOAD_DOWNLOAD_DETAILS_LIST:
      downloadList = {
        ...state.downloadDetailsList
      };
      downloadList[action.uid] = {
        loading: true,
        data: null
      };
      return {
        ...state,
        downloadDetailsList: downloadList
      };

    case actionTypes.LOAD_DOWNLOAD_DETAILS_LIST_SUCCESS:
      action.data.partner_links.sort((a, b) => {
        const f1 = a.filesize;
        const f2 = b.filesize;
        return (f1 === null) - (f2 === null) || +(f1 > f2) || -(f1 < f2);
      });
      downloadList = {
        ...state.downloadDetailsList
      };
      downloadList[action.uid] = {
        loading: false,
        data: action.data
      };

      return {
        ...state,
        downloadDetailsList: downloadList
      };

    case actionTypes.LOAD_DOWNLOAD_DETAILS_LIST_FAIL:
      downloadList = {
        ...state.downloadDetailsList
      };
      downloadList[action.uid] = {
        loading: false
      };
      return {
        ...state,
        downloadDetailsList: downloadList,
        ...{
          error: action.err
        }
      };
    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_DETAILS:
      return {
        ...state,
        downloadDetails: null,
        downloadDetailsLoading: true
      };

    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_DETAILS_SUCCESS:
      action.data.partner_links.sort((a, b) => {
        const f1 = a.filesize;
        const f2 = b.filesize;
        return (f1 === null) - (f2 === null) || +(f1 > f2) || -(f1 < f2);
      });
      return {
        ...state,
        downloadDetails: action.data,
        downloadDetailsLoading: false
      };

    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_DETAILS_FAIL:
      return {
        ...state,
        downloadDetailsLoading: false,
        ...{
          error: action.err
        }
      };
    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS:
      return {
        ...state,
        secondDownloadDetails: null,
        secondDownloadDetailsLoading: true
      };

    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS_SUCCESS:
      action.data.partner_links.sort((a, b) => {
        const f1 = a.filesize;
        const f2 = b.filesize;
        return (f1 === null) - (f2 === null) || +(f1 > f2) || -(f1 < f2);
      });
      return {
        ...state,
        secondDownloadDetails: action.data,
        secondDownloadDetailsLoading: false
      };

    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SECOND_DETAILS_FAIL:
      return {
        ...state,
        secondDownloadDetailsLoading: false,
        ...{
          error: action.err
        }
      };
    case actionTypes.LOAD_COLLEAGUE_RANK:
      return {
        ...state,
        rankLoading: true
      };
    case actionTypes.LOAD_COLLEAGUE_RANK_SUCCESS:
      return {
        ...state,
        rankLoading: false,
        ...action.data
      };
    case actionTypes.LOAD_COLLEAGUE_RANK_FAIL:
      return {
        ...state,
        rankLoading: false,
        error: action.err
      };
    case actionTypes.LOAD_COLLEAGUE_MOVIE_RANK:
      return {
        ...state,
        mRankLoading: true
      };
    case actionTypes.LOAD_COLLEAGUE_MOVIE_RANK_SUCCESS:
      colMovieRank = {
        ...state.data
      };

      colMovieRank[action.uid] = action.data.movie_rank;
      // console.log("colMovieRank",colMovieRank);
      return {
        ...state,
        // mRankLoading: false,
        data: colMovieRank

        // downloadList = {
        //   ...state.downloadDetailsList
        // };
        // downloadList[action.uid] = {
        //   loading: false,
        //   data: action.data
        // };

        // return {
        //   ...state,
        //   downloadDetailsList: downloadList
      };

    case actionTypes.LOAD_COLLEAGUE_MOVIE_RANK_FAIL:
      return {
        ...state,
        mRankLoading: false,
        error: action.err
      };
    default:
      return state;
  }
}
