import {
  LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE,
  LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE_FAIL,
  LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE_SUCCESS,
  LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE,
  LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_FAIL,
  LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_SUCCESS,
  LOAD_ARTICLE_REVIEWS_ARCHIVE,
  LOAD_ARTICLE_REVIEWS_ARCHIVE_FAIL,
  LOAD_ARTICLE_REVIEWS_ARCHIVE_SUCCESS,
  LOAD_FOREIGN_REVIEWS_ARCHIVE,
  LOAD_FOREIGN_REVIEWS_ARCHIVE_FAIL,
  LOAD_FOREIGN_REVIEWS_ARCHIVE_SUCCESS,
  LOAD_PEOPLE_REVIEWS_ARCHIVE,
  LOAD_PEOPLE_REVIEWS_ARCHIVE_FAIL,
  LOAD_PEOPLE_REVIEWS_ARCHIVE_SUCCESS,
  LOAD_PERSIAN_REVIEWS_ARCHIVE,
  LOAD_PERSIAN_REVIEWS_ARCHIVE_FAIL,
  LOAD_PERSIAN_REVIEWS_ARCHIVE_SUCCESS,
  LOAD_RECENT_ARTICLES_ARCHIVE,
  LOAD_RECENT_ARTICLES_ARCHIVE_FAIL,
  LOAD_RECENT_ARTICLES_ARCHIVE_SUCCESS,
  LOAD_RECENT_REVIEWS_ARCHIVE,
  LOAD_RECENT_REVIEWS_ARCHIVE_FAIL,
  LOAD_RECENT_REVIEWS_ARCHIVE_SUCCESS
} from "posts/actions/reviews";

export const initialState = {
  peopleReviewIsLoading: false,
  articleReviewIsLoading: false,
  peopleReview: [],
  articleReview: [],

  typeReview: null,
  persianData: [],
  foreignData: [],
  peopleData: [],
  articleData: [],
  persianTotal: 0,
  foreignTotal: 0,
  peopleTotal: 0,
  articleTotal: 0,
  persianIsLoading: false,
  foreignIsLoading: false,
  peopleIsLoading: false,
  articleIsLoading: false,
  persianPage: 1,
  peoplePage: 1,
  foreignPage: 1,
  articlePage: 1,
  recentReviewsArchive: [],
  recentReviewsArchiveIsLoading: false,
  recentArticlesArchive: [],
  recentArticlesArchiveIsLoading: false,
  statusCodePerReviw: "",
  statusCodeForeReviw: "",
  statusCodeArticle: "",
  statusCodePeopReview: ""
};

export default function reviews(state = initialState, action) {
  switch (action.type) {
    case LOAD_PEOPLE_REVIEWS_ARCHIVE:
      return {
        peopleReview: [],
        peopleReviewIsLoading: true
      };

    case LOAD_PEOPLE_REVIEWS_ARCHIVE_SUCCESS:
      return {
        ...state,
        peopleReviewIsLoading: false,
        peopleReview: action.data.quotes
      };

    case LOAD_PEOPLE_REVIEWS_ARCHIVE_FAIL:
      return {
        ...state,
        peopleReviewIsLoading: false,
        ...{
          error: action.err
        }
      };

    case LOAD_ARTICLE_REVIEWS_ARCHIVE:
      return {
        articleReview: [],
        articleReviewIsLoading: true
      };

    case LOAD_ARTICLE_REVIEWS_ARCHIVE_SUCCESS:
      return {
        ...state,
        articleReviewIsLoading: false,
        articleReview: action.data.articles
      };

    case LOAD_ARTICLE_REVIEWS_ARCHIVE_FAIL:
      return {
        ...state,
        articleReviewIsLoading: false
        // ...{ statusCodeArticle: action.error },
      };

    case LOAD_PERSIAN_REVIEWS_ARCHIVE:
      return {
        ...state,
        typeReview: "iran",
        persianIsLoading: true
      };
    case LOAD_PERSIAN_REVIEWS_ARCHIVE_SUCCESS:
      return {
        ...state,
        persianPage: state.persianPage + 1,
        persianIsLoading: false,
        persianData: action.data.longCritics,
        persianTotal: action.data.total
      };

    case LOAD_PERSIAN_REVIEWS_ARCHIVE_FAIL:
      return {
        ...state,
        persianIsLoading: false,
        ...{ statusCodePerReviw: action.error }
      };

    case LOAD_FOREIGN_REVIEWS_ARCHIVE:
      return {
        ...state,
        typeReview: "foreign",
        foreignIsLoading: true
      };
    case LOAD_FOREIGN_REVIEWS_ARCHIVE_SUCCESS:
      // Const MoreForeignReview = state.foreignData.concat(
      //   action.data.longCritics
      // );
      return {
        ...state,
        foreignPage: state.foreignPage + 1,
        foreignIsLoading: false,
        foreignData: action.data.longCritics,
        foreignTotal: action.data.total
      };

    case LOAD_FOREIGN_REVIEWS_ARCHIVE_FAIL:
      return {
        ...state,
        foreignIsLoading: false,
        ...{ statusCodeForeReviw: action.error }
      };

    case LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE:
      return {
        ...state,
        typeReview: "iran",
        peopleIsLoading: true
      };
    case LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_SUCCESS:
      return {
        ...state,
        peoplePage: state.peoplePage + 1,
        peopleIsLoading: false,
        peopleData: action.data.longCritics,
        peopleTotal: action.data.total
      };

    case LOAD_ALL_PEOPLE_REVIEWS_ARCHIVE_FAIL:
      return {
        ...state,
        peopleIsLoading: false,
        ...{ statusCodePeopReview: action.error }
      };

    case LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE:
      return {
        ...state,
        typeReview: "iran",
        articleIsLoading: true
      };
    case LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE_SUCCESS:
      return {
        ...state,
        articlePage: state.articlePage + 1,
        articleIsLoading: false,
        articleData: action.data.articles,
        articleTotal: action.data.total
      };

    case LOAD_ALL_ARTICLE_REVIEWS_ARCHIVE_FAIL:
      return {
        ...state,
        articleIsLoading: false,
        ...{ statusCodeArticle: action.error }
      };

    case LOAD_RECENT_REVIEWS_ARCHIVE:
      return {
        recentReviewsArchive: [],
        recentReviewsArchiveIsLoading: true
      };

    case LOAD_RECENT_REVIEWS_ARCHIVE_SUCCESS:
      return {
        ...state,
        recentReviewsArchiveIsLoading: false,
        recentReviewsArchive: action.data.longCritics
      };

    case LOAD_RECENT_REVIEWS_ARCHIVE_FAIL:
      return {
        ...state,
        recentReviewsArchiveIsLoading: false,
        ...{
          error: action.err
        }
      };

    case LOAD_RECENT_ARTICLES_ARCHIVE:
      return {
        recentArticlesArchive: [],
        recentArticlesArchiveIsLoading: true
      };

    case LOAD_RECENT_ARTICLES_ARCHIVE_SUCCESS:
      return {
        ...state,
        recentArticlesArchiveIsLoading: false,
        recentArticlesArchive: action.data.articles
      };

    case LOAD_RECENT_ARTICLES_ARCHIVE_FAIL:
      return {
        ...state,
        recentArticlesArchiveIsLoading: false,
        ...{
          error: action.err
        }
      };

    default:
      return state;
  }
}
