import { ADD, REMOVE } from "posts/actions/alert";

export const initialState = {
  data: [],
  visible: false
};
export default function alert(state = initialState, action) {
  switch (action.type) {
    // Case SUCCESS_ALERT:
    //   return{
    //     ...state,
    //     data: {
    //       mssg: action.mssg,
    //     },
    //     visible: true
    // }
    case ADD:
      return {
        ...state,
        data: [...state.data, action.alert],
        visible: true
      };
    // Case REMOVE:
    //   return {
    //     ...state,
    //     data: state.data.filter((alert) => alert.id !== action.id)
    //   };
    // case SUCCESS_DISMISS:
    //   return{
    //     ...state,
    //     visible: false
    // }
    case REMOVE:
      return {
        ...state,
        data: state.data.filter(alert => alert.id !== action.id),
        visible: false
      };
    default:
      return state;
  }
}
