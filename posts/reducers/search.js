/* eslint-disable no-case-declarations */
import {
  LOAD_COMPLETE_SEARCH_SUCCESS,
  LOAD_COMPLETE_SEARCH_FAILURE,
  ADD_SELECTED_ITEM,
  SET_SEARCH_INPUT_VALUE,
  SET_MOVIE_SEARCH_INPUT_VALUE,
  LOAD_MOVIE_SEARCH_SUCCESS,
  LOAD_MOVIE_SEARCH_FAILURE,
  LOAD_NEWS_SEARCH_SUCCESS,
  LOAD_NEWS_SEARCH_FAILURE,
  SET_NEWS_SEARCH_INPUT_VALUE
} from "../actions/search";
export const exampleInitialState = {
  dataInfo: [],
  error: null,
  selectedItem: [],
  inputValue: "",
  actorMovieInputValue: "",
  movieInputValue: "",
  newsInputValue: ""
};

export default function search(state = exampleInitialState, action) {
  switch (action.type) {
    case LOAD_COMPLETE_SEARCH_SUCCESS:
      let search = [];
      search = action.data.movies;
      Array.prototype.push.apply(search, action.data.users);
      // Array.prototype.push.apply(search, action.data.cinemas);
      return {
        ...state,
        ...{ dataInfo: search }
      };
    case LOAD_COMPLETE_SEARCH_FAILURE:
      return {
        ...state,
        ...{ error: action.error }
      };
    case LOAD_MOVIE_SEARCH_SUCCESS:
      return {
        ...state,
        ...{ dataInfo: action.data }
      };
    case LOAD_MOVIE_SEARCH_FAILURE:
      return {
        ...state,
        ...{ error: action.error }
      };
    case LOAD_NEWS_SEARCH_SUCCESS:
      return {
        ...state,
        ...{ dataInfo: action.data }
      };
    case LOAD_NEWS_SEARCH_FAILURE:
      return {
        ...state,
        ...{ error: action.error }
      };

    case ADD_SELECTED_ITEM:
      const info = [...state.selectedItem];
      info.push(action.data);
      return {
        ...state,
        selectedItem: info
      };
    case SET_SEARCH_INPUT_VALUE:
      return {
        ...state,
        inputValue: action.data
      };
    case SET_MOVIE_SEARCH_INPUT_VALUE:
      return {
        ...state,
        movieInputValue: action.data
      };
    case SET_NEWS_SEARCH_INPUT_VALUE:
      return {
        ...state,
        newsInputValue: action.data
      };
    default:
      return state;
  }
}
