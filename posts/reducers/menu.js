import { actionTypes } from "posts/actions/menu";
import { HYDRATE } from "next-redux-wrapper";

export const initialState = {
  isOpen: false,
  isOpenSidebar: true,
  data: []
  // isOpenSubMenu: true,
};

export default function menu(state = initialState, action) {
  switch (action.type) {
    case HYDRATE: {
      return { ...state, ...action.payload.menu };
    }
    case actionTypes.TOGGLE_MENU:
      return {
        ...state,
        isOpen: action.data
      };
    case actionTypes.SET_SIDEBAR_OPEN:
      return {
        ...state,
        isOpenSidebar: action.data
      };
    case actionTypes.ADD_SUBMENU_ITEMS:
      return {
        ...state,
        data: action.data
      };
    case actionTypes.SET_SUBMENU_OPEN: {
      const copyState = state.data.map(item => ({ ...item }));
      for (let i = 0; i < copyState.length; i++) {
        if (copyState[i].id === action.id) {
          copyState[i].collapsed = action.collapsed;
        }
      }
      return {
        ...state,
        data: copyState
      };
    }
    default:
      return state;
  }
}
