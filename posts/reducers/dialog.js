import { CLOSE_LOGIN_MODAL, OPEN_LOGIN_MODAL } from "posts/actions/dialogLogin";

export const initialState = {
  openLoginModal: false
};

export default function list(state = initialState, action) {
  switch (action.type) {
    case OPEN_LOGIN_MODAL:
      return {
        ...state,
        loading: true,
        openLoginModal: true,
        loginDialogMssg: action.mssg
      };
    case CLOSE_LOGIN_MODAL:
      return {
        ...state,
        loading: true,
        openLoginModal: false
      };
    default:
      return state;
  }
}
