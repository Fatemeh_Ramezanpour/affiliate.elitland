import { combineReducers } from "redux";
import search from "./search";
import alert from "./alert";
import list from "./list";
import auth from "./auth";
import dialog from "./dialog";
import news from "./news";
import movie from "./movie";
import reviews from "./review";
import profile from "./profile";
import movieDetail from "./movie-detail";
import comment from "./comment";
import menu from "./menu";
import download from "./download";
import downloadDetails from "./download-details";
import video from "./videoArchive";
import payoff from "./payoff";

export default combineReducers({
  alert,
  search,
  list,
  auth,
  dialog,
  news,
  movie,
  reviews,
  profile,
  movieDetail,
  comment,
  menu,
  download,
  downloadDetails,
  video,
  payoff
});
