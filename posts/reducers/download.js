import { actionTypes } from "posts/actions/download";
import { HYDRATE } from "next-redux-wrapper";

export const initialState = {
  downloadList: [],
  collLoading: false,
  downloadListLoading: false,
  downloadColleaguePanel: {
    loading: false,
    data: null
  },
  companyMomentGeneral: {
    loading: false,
    data: null
  },
  category: { data: [], catLoading: false },
  reportLoading: false
};
let ldcpd;

export default function download(state = initialState, action) {
  switch (action.type) {
    case HYDRATE: {
      return { ...state, ...action.payload.download };
    }
    case actionTypes.LOAD_DOWNLOAD_LIST:
      return {
        downloadList: [],
        downloadListLoading: true
      };

    case actionTypes.LOAD_DOWNLOAD_LIST_SUCCESS:
      return {
        ...state,
        downloadPage: action.page,
        downloadList: action.data.movies,
        downloadTotal: action.data.total,
        downloadListLoading: false
      };

    case actionTypes.LOAD_DOWNLOAD_LIST_FAIL:
      return {
        ...state,
        downloadListLoading: false,
        ...{
          error: action.err
        }
      };

    case actionTypes.LOAD_DOWNLOAD_COLLEAGUE_LINKS:
      return {
        ...state,
        downloadColleaguePanel: {
          loading: true,
          links: null
        }
      };

    case actionTypes.LOAD_DOWNLOAD_COLLEAGUE_LINKS_SUCCESS:
      return {
        ...state,
        downloadColleaguePanel: {
          loading: false,
          links: action.data,
          page: action.page
        }
      };

    case actionTypes.LOAD_DOWNLOAD_COLLEAGUE_LINKS_FAIL:
      return {
        ...state,
        downloadColleaguePanel: {
          loading: false,
          links: null
        },
        ...{
          error: action.err
        }
      };

    case actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL:
      return {
        downloadColleaguePanel: {
          loading: true,
          data: null
        }
      };

    case actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_SUCCESS:
      return {
        ...state,
        downloadColleaguePanel: {
          loading: false,
          data: action.data,
          page: action.page
        }
      };
    case actionTypes.LOAD_MOVIE_COLLEAGUE_PANEL:
      return {
        downloadColleaguePanel: {
          loading: true,
          data: null
        }
      };

    case actionTypes.LOAD_MOVIE_COLLEAGUE_PANEL_SUCCESS:
      return {
        ...state,
        downloadColleaguePanel: {
          loading: false,
          data: action.data,
          page: action.page
        }
      };

    case actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS:
      ldcpd = {
        ...state.downloadColleaguePanelDetails
      };
      ldcpd[action.uid] = {
        loading: true,
        data: null
      };
      return {
        ...state,
        downloadColleaguePanelDetails: ldcpd
      };

    case actionTypes.LOAD_DOWNLOAD_COLLEAGUE_PANEL_DETAILS_SUCCESS:
      ldcpd = {
        ...state.downloadColleaguePanelDetails
      };
      ldcpd[action.uid] = {
        loading: false,
        data: action.data
      };
      return {
        ...state,
        downloadColleaguePanelDetails: ldcpd
      };

    case actionTypes.LOAD_DOWNLOAD_BUY_INFO:
      return {
        downloadBuyInfo: {
          loading: true,
          data: null
        }
      };

    case actionTypes.LOAD_DOWNLOAD_BUY_INFO_SUCCESS:
      return {
        ...state,
        downloadBuyInfo: {
          loading: false,
          ...action.data,
          page: action.page
        }
      };
    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_SUCCESS:
      return {
        ...state,
        campaign: {
          loading: false,
          data: action.data
        }
      };
    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_FAIL:
      return {
        ...state,
        campaign: {
          loading: false,
          statusCode: action.err
        }
      };

    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_PURCHASED_SUCCESS:
      return {
        ...state,
        campaign: {
          loading: false,
          purchased: action.data
        }
      };
    case actionTypes.LOAD_CAMPAIGN_DOWNLOAD_PURCHASED_FAIL:
      return {
        ...state,
        campaign: {
          loading: false,
          purchased: null,
          statusCode: action.err
        }
      };
    case actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL:
      return {
        downloadColleaguePanel: {
          loading: true,
          data: null
        }
      };
    case actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_SUCCESS:
      return {
        ...state,
        downloadColleaguePanel: {
          loading: false,
          data: action.data,
          page: action.page
        }
      };
    case actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS:
      ldcpd = {
        ...state.downloadColleaguePanelDetails
      };
      ldcpd[action.uid] = {
        loading: true,
        data: null
      };
      return {
        ...state,
        downloadColleaguePanelDetails: ldcpd
      };

    case actionTypes.LOAD_DOWNLOAD_COMPANY_PANEL_DETAILS_SUCCESS:
      ldcpd = {
        ...state.downloadColleaguePanelDetails
      };
      ldcpd[action.uid] = {
        loading: false,
        data: action.data
      };
      return {
        ...state,
        downloadColleaguePanelDetails: ldcpd
      };
    case actionTypes.LOAD_DOWNLOAD_COLLEAGUE_MOMENT_INFO:
      return {
        ...state,
        downloadMomentInfo: {
          loading: true
        }
      };
    case actionTypes.LOAD_DOWNLOAD_MOMENT_INFO:
      return {
        ...state,
        downloadMomentInfo: {
          loading: true
        }
      };
    case actionTypes.LOAD_DOWNLOAD_MOMENT_INFO_SUCCESS:
      return {
        ...state,
        downloadMomentInfo: {
          loading: false,
          ...action.data,
          page: action.page
        }
      };
    case actionTypes.LOAD_COLLEAGUE_INFO:
      return {
        ...state,
        collLoading: true
      };
    case actionTypes.LOAD_COLLEAGUE_INFO_SUCCESS:
      return {
        ...state,
        collLoading: false,
        loaded: true,
        colleague: action.data,
        error: null
      };
    case actionTypes.LOAD_COLLEAGUE_INFO_FAIL:
      return {
        ...state,
        collLoading: false,
        loaded: false,
        colleague: null,
        error: action.error
      };
    case actionTypes.LOAD_COMPANY_GENERAL_MOMENT_INFO:
      return {
        ...state,
        companyMomentGeneral: {
          loading: true
        }
      };
    case actionTypes.LOAD_COMPANY_GENERAL_MOMENT_INFO_SUCCESS:
      return {
        ...state,
        companyMomentGeneral: {
          loading: false,
          ...action.data,
          page: action.page
        }
      };
    case actionTypes.LOAD_COMPANY_GENERAL_MOMENT_INFO_FAIL:
      return {
        ...state,
        companyMomentGeneral: {
          loading: false,
          error: action.error
        }
      };
    case actionTypes.LOAD_RECOMM_DOWNLOAD:
      return {
        ...state,
        recommDownload: {
          loading: true
        }
      };
    case actionTypes.LOAD_RECOMM_DOWNLOAD_SUCCESS:
      return {
        ...state,
        recommDownload: {
          loading: false,
          data: action.data
        }
      };
    case actionTypes.LOAD_DOWNLOAD_CATEGORY:
      return {
        ...state,
        catLoading: true
      };

    case actionTypes.LOAD_DOWNLOAD_CATEGORY_SUCCESS: {
      let finalCategory = [];
      if (state.category) {
        finalCategory = state.category.data.concat(action.data.data);
      } else {
        finalCategory = action.data.data;
      }

      return {
        ...state,
        catLoading: false,
        category: {
          ...state.category,
          data: finalCategory,
          total: action.data.total
        }
      };
    }

    case actionTypes.LOAD_DOWNLOAD_CATEGORY_FAIL:
      return {
        ...state,
        catLoading: false,
        category: {
          error: action.error
        }
      };
    case actionTypes.LOAD_COLLEAGUE_REPORT:
      return {
        ...state,
        reportLoading: true
      };

    case actionTypes.LOAD_COLLEAGUE_REPORT_SUCCESS: {
      return {
        ...state,
        reportLoading: false,
        accounting: { ...action.data }
      };
    }

    case actionTypes.LOAD_COLLEAGUE_REPORT_FAIL:
      return {
        ...state,
        reportLoading: false,
        ...action.error
      };

    case actionTypes.LOAD_SERIES_COLLEAGUE_PANEL:
      return {
        downloadColleaguePanel: {
          loading: true,
          data: null
        }
      };

    case actionTypes.LOAD_SERIES_COLLEAGUE_PANEL_SUCCESS:
      return {
        ...state,
        downloadColleaguePanel: {
          loading: false,
          data: action.data,
          page: action.page
        }
      };

    default:
      return state;
  }
}
