import {
  LOAD_VIDEO,
  LOAD_VIDEO_SUCCESS,
  LOAD_VIDEO_FAIL,
  LOAD_VIDEO_DETAIL,
  LOAD_VIDEO_DETAIL_SUCCESS,
  LOAD_VIDEO_DETAIL_FAIL
} from "posts/actions/videoArchive";

export const initialState = {
  video: {},
  videoDetail: null,
  videoDetailIsLoading: false
};

export default function video(state = initialState, action) {
  switch (action.type) {
    case LOAD_VIDEO: {
      const data = {};
      data[action.videoType] = {};
      data[action.videoType].loading = true;
      return {
        video: data
      };
    }
    case LOAD_VIDEO_SUCCESS: {
      let basicInformation = {};
      basicInformation = { ...state.video[action.videoType] };
      basicInformation.loading = false;
      basicInformation.statusCode = "";
      basicInformation = action.data;

      return {
        video: {
          ...state.video,
          [action.videoType]: basicInformation
        }
      };
    }
    case LOAD_VIDEO_FAIL: {
      let basic = {};
      basic = { ...state.video[action.videoType] };
      basic.loading = false;
      basic.statusCode = action.error;

      // const data = {};
      // data[action.videoType] = {};
      // data[action.videoType].statusCode = action.error;
      return {
        video: {
          ...state.video,
          [action.videoType]: basic
        }
      };
    }
    // Load Video Detail
    case LOAD_VIDEO_DETAIL:
      return {
        videoDetail: null,
        videoDetailIsLoading: true
      };

    case LOAD_VIDEO_DETAIL_SUCCESS:
      return {
        ...state,
        videoDetailIsLoading: false,
        videoDetail: action.data
      };

    case LOAD_VIDEO_DETAIL_FAIL:
      return {
        ...state,
        videoDetailIsLoading: false,
        ...{
          error: action.err
        }
      };
    default:
      return state;
  }
}
