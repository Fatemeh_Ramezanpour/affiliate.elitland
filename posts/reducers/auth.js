import Cookies from "universal-cookie";
import { HYDRATE } from "next-redux-wrapper";

const auth_cookie = new Cookies();
import { actionTypes } from "posts/actions/auth";

export const initialState = {
  user: null,
  registerMobile: null,
  verify: true,
  newUser: false,
  setPass: false,
  forgotPass: false,
  setNewPass: false,
  changeOldPass: false
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case HYDRATE: {
      return { ...state, ...action.payload.auth };
    }
    case actionTypes.LOAD_USER_INFO:
      return {
        ...state
        // ...{ user: null }
      };
    case actionTypes.LOAD_USER_INFO_SUCCESS:
      return {
        ...state,
        ...{ user: action.data }
      };
    case actionTypes.SAVE_MOBILE_FOR_CONFIRM:
      return {
        ...state,
        loading: false,
        loaded: false,
        registerMobile: action.mobile
      };
    case actionTypes.REGISTER_GUEST_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        registerData: action.result,
        newUser: true,
        verify: false
      };
    case actionTypes.REGISTER_GUEST_USER_FAILURE:
      if (action.err && action.err.response && action.err.response.status === 409) {
        return {
          ...state,
          loading: false,
          loaded: false,
          registerData: null,
          registerError: action.err,
          newUser: false,
          verify: false
        };
      }

      return {
        ...state,
        loading: false,
        loaded: false,
        user: null,
        registerData: null,
        registerError: action.err
      };
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.data.profile,
        token: action.data.token,
        newUser: false,
        verify: true
      };
    case actionTypes.LOGIN_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        user: null,
        token: null,
        loginError: action.err,
        newUser: false,
        verify: false
      };
    case actionTypes.OTP_CREATION:
      return {
        ...state,
        loading: true
      };
    case actionTypes.OTP_CREATION_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        verifyData: action.data,
        forgotPass: true
      };
    case actionTypes.OTP_CREATION_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        verifyData: null,
        verifyError: action.err
      };
    case actionTypes.SAVE_VERIFY_CODE:
      return {
        ...state,
        loading: false,
        loaded: false,
        verifyCode: action.code
      };
    case actionTypes.VERIFY_MOBILE:
      return {
        ...state,
        loading: true
      };
    case actionTypes.VERIFY_MOBILE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        setNewPass: true,
        setPass: true,
        verifyInfo: action.data,
        user: action.data.profile,
        token: action.data.token
      };
    case actionTypes.VERIFY_MOBILE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        setPass: false,
        verifyInfo: null,
        verifyInfoError: action.err
      };
    case actionTypes.CHANGE_PASSWORD:
      return {
        ...state,
        loading: true
      };
    case actionTypes.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        changePass: action.data
      };
    case actionTypes.CHANGE_PASSWORD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        changePass: null,
        changeError: action.err
      };
    case actionTypes.SET_PASSWORD_SIGNUP:
      return {
        ...state,
        loading: true
      };
    case actionTypes.SET_PASSWORD_SIGNUP_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        setPass: action.data,
        user: action.data.profile
      };
    case actionTypes.SET_PASSWORD_SIGNUP_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        setPass: null,
        setPassError: action.err
      };
    case actionTypes.LOGOUT:
      return {
        ...state,
        loggingOut: true
      };
    case actionTypes.LOGOUT_SUCCESS:
      auth_cookie.remove("token_elitland", { path: "/", domain: cookieDomain });
      // Cookie.remove('token_elitland', { path: '/', domain: config.cookieDomain });
      return {
        ...state,
        loggingOut: false,
        user: null,
        token: null
      };
    case actionTypes.LOGOUT_FAIL:
      return {
        ...state,
        loggingOut: false,
        logoutError: action.error
      };
    case actionTypes.SET_TOKEN:
      return {
        ...state,
        token_fateme: action.data
      };
    case actionTypes.FORGOT_PASSWORD_In_OLD_SYSTEM:
      return {
        ...state,
        loading: true
      };
    case actionTypes.FORGOT_PASSWORD_In_OLD_SYSTEM_SUCCESS:
      return {
        ...state,
        loading: false,
        changeOldPass: action.data
        // user: action.data.profile
      };
    case actionTypes.FORGOT_PASSWORD_In_OLD_SYSTEM_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        changeOldPass: null,
        changeOldPassError: action.err
      };
    default:
      return state;
  }
}
