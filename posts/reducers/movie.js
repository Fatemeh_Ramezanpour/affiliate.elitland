import {
  LOAD_FOREIGN_MOVIE_ARCHIVE,
  LOAD_FOREIGN_MOVIE_ARCHIVE_FAIL,
  LOAD_FOREIGN_MOVIE_ARCHIVE_SUCCESS,
  LOAD_FOREIGN_MOVIE_TOP_VIEW,
  LOAD_FOREIGN_MOVIE_TOP_VIEW_FAIL,
  LOAD_FOREIGN_MOVIE_TOP_VIEW_SUCCESS,
  LOAD_PERSIAN_MOVIE_ARCHIVE,
  LOAD_PERSIAN_MOVIE_ARCHIVE_FAIL,
  LOAD_PERSIAN_MOVIE_ARCHIVE_SUCCESS,
  LOAD_PERSIAN_MOVIE_TOP_VIEW,
  LOAD_PERSIAN_MOVIE_TOP_VIEW_FAIL,
  LOAD_PERSIAN_MOVIE_TOP_VIEW_SUCCESS
} from "posts/actions/movie";

export const initialState = {
  movieTopView: {
    persian: [],
    foreign: []
  },
  movieArchive: {
    persianArchive: [],
    foreignArchive: []
  },
  newsisloading: false,
  foreNewsloading: false,
  perMovieloading: false,
  perMovieArchLoading: false,
  foreMovieArchLoading: false,
  popularNews: [],
  statusCodePerMovie: "",
  statusCodeForeMovie: ""
};

export default function movie(state = initialState, action) {
  switch (action.type) {
    case LOAD_PERSIAN_MOVIE_TOP_VIEW:
      return {
        movieTopView: {
          persian: [],
          foreign: []
        },
        perMovieloading: true
      };
    case LOAD_PERSIAN_MOVIE_TOP_VIEW_SUCCESS:
      return {
        ...state,
        movieTopView: {
          ...state.movieTopView,
          persian: action.data
        },
        perMovieloading: false
      };
    case LOAD_PERSIAN_MOVIE_TOP_VIEW_FAIL:
      return {
        ...state,
        perMovieloading: false,
        ...{ error: action.err }
      };
    case LOAD_FOREIGN_MOVIE_TOP_VIEW:
      return {
        movieTopView: {
          persian: [],
          foreign: []
        },
        perMovieloading: true
      };
    case LOAD_FOREIGN_MOVIE_TOP_VIEW_SUCCESS:
      return {
        ...state,
        movieTopView: {
          ...state.movieTopView,
          foreign: action.data
        },
        perMovieloading: false
      };
    case LOAD_FOREIGN_MOVIE_TOP_VIEW_FAIL:
      return {
        ...state,
        perMovieloading: false,
        ...{ error: action.err }
      };
    case LOAD_PERSIAN_MOVIE_ARCHIVE:
      return {
        movieTopView: {
          persian: [],
          foreign: []
        },
        movieArchive: {
          persianArchive: [],
          foreignArchive: []
        },
        perMovieArchLoading: true
      };
    case LOAD_PERSIAN_MOVIE_ARCHIVE_SUCCESS:
      return {
        ...state,
        // MovieTopView: {
        //   ...state.movieTopView,
        //   foreign: action.data
        // },
        movieArchive: {
          persianArchive: action.data,
          ...state.foreignArchive
        },
        perMovieArchLoading: false
      };
    case LOAD_PERSIAN_MOVIE_ARCHIVE_FAIL:
      return {
        ...state,
        perMovieArchLoading: false,
        ...{ statusCodePerMovie: action.error }
      };
    case LOAD_FOREIGN_MOVIE_ARCHIVE:
      return {
        movieTopView: {
          persian: [],
          foreign: []
        },
        movieArchive: {
          persianArchive: [],
          foreignArchive: []
        },
        foreMovieArchLoading: true
      };
    case LOAD_FOREIGN_MOVIE_ARCHIVE_SUCCESS:
      return {
        ...state,
        // MovieTopView: {
        //   ...state.movieTopView,
        //   foreign: action.data
        // },
        movieArchive: {
          foreignArchive: action.data,
          ...state.persianArchive
        },
        foreMovieArchLoading: false
      };
    case LOAD_FOREIGN_MOVIE_ARCHIVE_FAIL:
      return {
        ...state,
        foreMovieArchLoading: false,
        ...{ statusCodeForeMovie: action.error }
        // ...{ error: action.err },
      };
    default:
      return state;
  }
}
