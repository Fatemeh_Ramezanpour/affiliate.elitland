import {
  DELETE_WATCHLIST,
  DELETE_WATCHLIST_FAIL,
  DELETE_WATCHLIST_SUCCESS,
  LOAD_WATCHLIST,
  LOAD_WATCHLIST_SUCCESS,
  LOAD_WATCHLIST_TOP_SUCCESS,
  MORE_REORDER_WATCHLIST_SUCCESS,
  REORDER_WATCHLIST,
  REORDER_WATCHLIST_SUCCESS,
  SAVE_MOVIE_VOTE,
  SAVE_MOVIE_VOTE_SUCCESS
} from "posts/actions/watchlist";

export const initialState = {
  watchlist: [],
  favoriteMovies: null,
  deleteErr: null,
  movie: null,
  loadingWatchlist: false,
  select: null,
  page: 1,
  showMoreBtn: true
};

const list = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_WATCHLIST:
      return {
        ...state,
        loadingWatchlist: true
      };
    case LOAD_WATCHLIST_SUCCESS:
      return {
        ...state,
        watchlist: [...state.watchlist, ...action.data.watchlist],
        page: state.page + 1,
        showMoreBtn: action.data?.watchlist?.length !== 0,
        loadingWatchlist: false
      };
    case SAVE_MOVIE_VOTE:
      return {
        ...state,
        watchlist: state.watchlist.map(item =>
          item.uid === action.MovieUid ? { ...item, current_user_rate: action.vote } : item
        )
      };
    case SAVE_MOVIE_VOTE_SUCCESS:
      return state;
    case LOAD_WATCHLIST_TOP_SUCCESS:
      return {
        ...state,
        favoriteMovies: action.data
      };
    case DELETE_WATCHLIST:
      return state;
    case DELETE_WATCHLIST_SUCCESS:
      return {
        ...state,
        watchlist: state.watchlist.filter(item => item.uid !== action.data.movie_uid)
      };
    case DELETE_WATCHLIST_FAIL:
      return {
        ...state,
        deleteErr: action.err
      };
    case REORDER_WATCHLIST:
      return {
        ...state,
        page: 1,
        watchlist: []
      };
    case REORDER_WATCHLIST_SUCCESS:
      return {
        ...state,
        watchlist: [...state.watchlist, ...action.data.watchlist],
        page: 1,
        showMoreBtn: action.data?.watchlist?.length !== 0,
        loadingWatchlist: false,
        select: action.orderType
      };
    case MORE_REORDER_WATCHLIST_SUCCESS:
      return {
        ...state,
        watchlist: [...state.watchlist, ...action.data.watchlist],
        page: state.page + 1,
        showMoreBtn: action.data?.watchlist?.length !== 0,
        loadingWatchlist: false,
        select: action.orderType
      };
    default:
      return state;
  }
};

export default list;
