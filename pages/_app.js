/* eslint-disable react/react-in-jsx-scope */
import React from "react";
import App from "next/app";
import { END } from "redux-saga";
import { wrapper } from "posts/store";

import Head from "next/head";
import Cookies from "universal-cookie";
import cookies from "next-cookies";

import { loadUserinfo, setToken } from "posts/actions/auth";
import Footer from "components/Footer/Footer";
import api from "services/api";
import ReactGA from "react-ga";
import OriginalWrapper from "components/OriginalWrapper";
import "styles/global.css";

class WrappedApp extends App {
  static getInitialProps = async ({ Component, ctx }) => {
    // static async getInitialProps({ Component, ctx }) {

    if (!ctx.req) {
      // apiCall.token = ctx.store.getState().auth.token;
      if (!ctx?.store?.getState()?.auth.user) {
        ctx.store.dispatch(loadUserinfo());
      }
    } else {
      // const cookie = cookies(ctx).token_v1;
      const cookie = cookies(ctx).token_elitland;
      ctx.store.dispatch(setToken(cookie));
      // apiCall.token = cookie;
      api.token = cookie;
      ctx.store.dispatch(loadUserinfo());
    }

    //Redux Wrapper for Next.js
    // 1. Wait for all page actions to dispatch
    const pageProps = {
      ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {})
    };
    // 2. Stop the saga if on server
    if (ctx.req) {
      ctx.store.dispatch(END);
      await ctx.store.sagaTask.toPromise();
    }

    // 3. Return props
    return {
      pageProps
    };
  };

  constructor(props) {
    super(props);
    const cook = new Cookies();
    const cookie = cook.get("token_elitland");
    api.token = cookie;
    this.state = {
      hideMenu: false,
      login: false
    };
  }

  // TestEsLintpageContext = null;
  componentDidMount() {
    if (!window.GA_INITIALIZED) {
      ReactGA.initialize("UA-47287252-2", { debug: false });
      window.GA_INITIALIZED = true;
    }
    ReactGA.set({ page: window.location.pathname });
    ReactGA.pageview(window.location.pathname);

    const { router } = this.props;
    const login = /\/login.*/;
    const hamgonah = /\/campaign\/hamgonah\/HamgonahCampaign.*/;
    // const hamgonahAwards = /\/campaign\/hamgonah\/HamgonahAwards.*/;
    const hamgonahBuy = /\/campaign\/hamgonah\/HamgonahBuy.*/;
    const hamgonahSelect = /\/campaign\/hamgonah\/HamgonahSelect.*/;

    if (
      router.route.match(hamgonah) ||
      router.route.match(hamgonahBuy) ||
      router.route.match(hamgonahSelect)
    ) {
      this.setState({ hideMenu: true });
    }
    if (router.route.match(login)) {
      this.setState({ login: true });
    }
  }

  render() {
    const { Component, pageProps, router } = this.props;
    return (
      <>
        <Head>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
          />
        </Head>
        <OriginalWrapper params={router} {...pageProps}>
          <Component {...pageProps} params={router} />
        </OriginalWrapper>
        <Footer className={this.state.hideMenu ? "hideFooter" : "showFooter"} />
      </>
    );
  }
}

// export default withRouter(withRedux(createStore)(withReduxSaga({ async: true })(MyApp)));
export default wrapper.withRedux(WrappedApp);
