import React, { Component } from "react";
import { connect } from "react-redux";

import DownloadColleaguePanelComponent from "components/Download/DownloadColleaguePanel/DownloadColleaguePanel";
import { NextSeo } from "next-seo";
import { loadMovieColleaguePanel, loadDownloadColleaguePanelDetails } from "posts/actions/download";
import { movieColleagueSalePanelUrl } from "services/url";
import { loadDownloadDetailsListAction } from "posts/actions/download-details";

class MovieColleaguePanel extends Component {
  componentDidMount() {
    const { params } = this.props;
    const page = params.query.page || 1;
    this.props.loadMovieColleaguePanel(page);
  }

  render() {
    return (
      <React.Fragment>
        <>
          <NextSeo title="گزارش فروش فیلم‌ها" description="" canonical="#" />
          <DownloadColleaguePanelComponent
            {...this.props}
            url={movieColleagueSalePanelUrl()}
            title="آمار فروش فیلم‌ها"
          />
        </>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  colleagueSale: state.download.downloadColleaguePanel,
  panelDetails: state.download.downloadColleaguePanelDetails,
  downloadInfo: state.downloadDetails.downloadDetailsList,
  movieRank: state.downloadDetails.data
});

const mapDispatchToProps = dispatch => {
  return {
    loadDownloadColleaguePanelDetails: uid => {
      dispatch(loadDownloadColleaguePanelDetails(uid));
    },
    loadMovieColleaguePanel: page => {
      dispatch(loadMovieColleaguePanel(page));
    },
    loadDownloadDetailsListAction: uid => {
      dispatch(loadDownloadDetailsListAction(uid));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieColleaguePanel);
