import React from "react";
import { connect } from "react-redux";
import { NextSeo } from "next-seo";

import PayoffReportComponent from "components/Download/DownloadPayoff/PayoffReport";
import Loading from "components/Loading/Loading";

import { payoffReportAction } from "posts/actions/payoff";

import Container from "reactstrap/lib/Container";

class PayoffReport extends React.Component {
  static async getInitialProps(props) {
    const { store } = props.ctx;
    store.dispatch(payoffReportAction());
  }

  render() {
    const { payoffreport, loading } = this.props;
    return (
      <Container>
        <NextSeo title="گزارش پرداخت همکاران فروش" description="" canonical="#" />
        {!loading && <PayoffReportComponent reportInfo={payoffreport} />}
        {loading && <Loading />}
      </Container>
    );
  }
}
PayoffReport.getInitialProps = async ctx => {
  const { store } = ctx;
  store.dispatch(payoffReportAction());
};
const mapStateToProps = state => ({
  payoffreport: state.payoff.payoffReport,
  colleagueData: state.download.colleague,
  loading: state.payoff.reportLoading
});
export default connect(mapStateToProps)(PayoffReport);
