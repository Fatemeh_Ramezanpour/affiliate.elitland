import React, { Component } from "react";
import { connect } from "react-redux";

import DownloadColleaguePanelComponent from "components/Download/DownloadColleaguePanel/DownloadColleaguePanel";
import { NextSeo } from "next-seo";
import {
  loadDownloadColleaguePanel,
  loadDownloadColleaguePanelDetails
} from "posts/actions/download";
import {
  loadDownloadDetailsListAction,
  loadcolleagueRankAction,
  loadColleagueMovieRankAction
} from "posts/actions/download-details";
import { homeUrl, elitlandUrl } from "services/url";

class panelHome extends Component {
  componentDidMount() {
    const { params } = this.props;
    const page = params.query.page || 1;
    this.props.loadDownloadColleaguePanel(page);
    this.props.loadcolleagueRankAction();
  }

  render() {
    const { user } = this.props;
    return (
      <React.Fragment>
        <>
          <NextSeo title="پنل همکار فروش" description="" canonical="#" />
          {user && <DownloadColleaguePanelComponent {...this.props} url={homeUrl()} />}
          {!user && (
            <div
              style={{ margin: "2% auto", width: "50%" }}
              className="alert alert-warning"
              role="alert"
            >
              <p>ابتدا وارد حساب کاربری خود شوید.</p>
              <p>
                <a href={elitlandUrl()}>ورود به اکانت</a>
              </p>
            </div>
          )
          // <div style={{margin: "2% auto", width: "50%", color: "#fff", fontSize: "17px", textAlign: "center", backgroundColor: "#4726bf"}}>
          // </div>
          }
        </>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  colleagueSale: state.download.downloadColleaguePanel,
  panelDetails: state.download.downloadColleaguePanelDetails,
  downloadInfo: state.downloadDetails.downloadDetailsList,
  rank: state.downloadDetails.rank,
  rankLoading: state.downloadDetails.rankLoading,
  movieRank: state.downloadDetails.data
});

const mapDispatchToProps = dispatch => {
  return {
    loadDownloadColleaguePanelDetails: uid => {
      dispatch(loadDownloadColleaguePanelDetails(uid));
    },
    loadDownloadColleaguePanel: page => {
      dispatch(loadDownloadColleaguePanel(page));
    },
    loadDownloadDetailsListAction: uid => {
      dispatch(loadDownloadDetailsListAction(uid));
    },
    loadcolleagueRankAction: () => {
      dispatch(loadcolleagueRankAction());
    },
    loadColleagueMovieRankAction: uid => {
      dispatch(loadColleagueMovieRankAction(uid));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(panelHome);
