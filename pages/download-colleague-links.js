import React, { Component } from "react";
import { connect } from "react-redux";
import { NextSeo } from "next-seo";

import DownloadColleagueLinksDashboardComponent from "components/Download/DownloadColleagueLinksDashboard/DownloadColleagueLinksDashboard";
import { loadDownloadColleagueLinks } from "posts/actions/download";

class DownloadColleagueLinks extends Component {
  componentDidMount() {
    const { params } = this.props;
    const page = params.query.page || 1;
    this.props.loadDownloadColleagueLinks(page);
  }

  render() {
    return (
      <React.Fragment>
        <>
          <NextSeo title=" لینک همکاران فروش" description="" canonical="#" />
          <DownloadColleagueLinksDashboardComponent {...this.props} />
        </>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  colleagueLinks: state.download.downloadColleaguePanel
});

const mapDispatchToProps = dispatch => {
  return {
    loadDownloadColleagueLinks: page => {
      dispatch(loadDownloadColleagueLinks(page));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DownloadColleagueLinks);
