import React, { Component } from "react";
import { connect } from "react-redux";
import { NextSeo } from "next-seo";

import DownloadColleagueMomentRepoComponent from "components/Download/DownloadColleagueMomentRepo/DownloadColleagueMomentRepo";
import { loadDownloadColleagueMomentInfoAction } from "posts/actions/download";
import Loading from "components/Loading/Loading";

class DownloadColleagueMomentRepo extends Component {
  componentDidMount() {
    const { params } = this.props;

    const page = params.query.page || 1;
    const movieUid = params.query.uid;

    this.props.loadDownloadColleagueMomentInfoAction(page, movieUid);
  }
  render() {
    const { data, params } = this.props;
    return (
      <React.Fragment>
        <>
          <NextSeo title="پنل گزارش لحظه‌ای همکار فروش" description="" canonical="#" />
          {data && (
            <>
              {!data.loading && (
                <DownloadColleagueMomentRepoComponent
                  page={data ? data.page : 1}
                  totalRecord={data.total}
                  momentData={data.payments}
                  movie={data.movie}
                  movieUid={params.query.uid}
                />
              )}
              {data.loading && <Loading />}
            </>
          )}
        </>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => ({
  user: state.auth.user,
  data: state.download.downloadMomentInfo
});

const mapDispatchToProps = dispatch => {
  return {
    loadDownloadColleagueMomentInfoAction: (page, movieUid) => {
      dispatch(loadDownloadColleagueMomentInfoAction(page, movieUid));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DownloadColleagueMomentRepo);
