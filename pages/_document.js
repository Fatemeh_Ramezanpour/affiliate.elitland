import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";

class MyDocument extends Document {
  render() {
    // const { pageContext } = this.props;

    return (
      <Html lang="en" dir="rtl">
        <Head>
          <link rel="stylesheet" href="/public/styles/rtl.bootstrap.v4.2.1.min.css" />
          {/* <link rel="stylesheet" href="/public/styles/bootstrap.min.css" /> */}
          <link rel="shortcut icon" href="/public/favicon.ico" />
          <link
            rel="stylesheet"
            type="text/css"
            charSet="UTF-8"
            href="/public/styles/slick.min.css"
          />
          <link rel="stylesheet" type="text/css" href="/public/styles/slick-theme.min.css" />
          <link rel="stylesheet" href="/public/styles/video-react.css" />
          <link rel="stylesheet" href="/public/styles/main.css" />
          <link rel="stylesheet" href="/public/styles/swiper.min.css" />
          {/* <link rel="stylesheet" href="/public/styles/style.css" /> */}

          <script
            dangerouslySetInnerHTML={{
              __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','GTM-MBDTMBV');`
            }}
          />
        </Head>
        <body style={{ minHeight: "100%", background: "#e4e5e6" }}>
          <noscript
            dangerouslySetInnerHTML={{
              __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MBDTMBV"
              height="0" width="0" style="dispExpected server Hlay:none;visibility:hidden"></iframe>`
            }}
          />
          <Main />
          <NextScript />
          <script
            dangerouslySetInnerHTML={{
              __html: `!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="3f7190a5-15bd-476a-97ed-65e3683ef7e6";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();`
            }}
          />
          <script
            type="text/javascript"
            src="https://elitland.com/public/js/sharethis.js#property=5ecaa00fe653cf001211cc85&product=inline-share-buttons&cms=sop"
            async="async"
          ></script>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
