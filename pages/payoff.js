import React, { Component } from "react";
import { connect } from "react-redux";
import { NextSeo } from "next-seo";

import DownloadPayoffForm from "components/Download/DownloadPayoff/DownloadPayoffForm";
import Loading from "components/Loading/Loading";
import { panelPayoffAction, loadPayoffAccountingAction } from "posts/actions/payoff";

class Payoff extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(loadPayoffAccountingAction());
  }

  postSubmit = data => {
    const { dispatch } = this.props;
    const payoffData = {
      amount: data.amount,
      account_no: data.accountNo
    };
    dispatch(panelPayoffAction(payoffData));
    //  .then((data)=>{
    //     const { dialogMessage } = this.props;
    //     this.props.pushState("/payoff-report");
    //     // dialogMessage("درخواست شما ثبت شد، پس از بررسی درآمد شما واریز خواهد شد.");
    //   })
    //   .catch((errors)=>{
    //     const { dialogMessage } = this.props;
    //     dialogMessage(errors[0],"danger");
    //   });
  };

  render() {
    const { colleagueData } = this.props;
    return (
      <>
        <NextSeo title=" داشبورد همکار فروش" description="" canonical="" />
        {colleagueData && !colleagueData.loading && (
          <div className="grid grid-cols-4 gap-3 ">
            <div className="col-start-2 col-span-2">
              <DownloadPayoffForm
                submitData={this.postSubmit}
                accountMony={colleagueData?.data?.pure_income}
              />
              <p>* تسویه حساب‌ درخواست‌های کاربران، آخر هر هفته انجام خواهد شد</p>
            </div>
          </div>
        )}
        {colleagueData.loading && <Loading />}
      </>
    );
  }
}
const mapStateToProps = state => ({
  colleagueData: state.payoff.accounting
  // syncErrors: getFormSyncErrors("DownloadPayoffForm")(state),
  // fields: getFormMeta("DownloadPayoffForm")(state)
});

export default connect(mapStateToProps)(Payoff);
