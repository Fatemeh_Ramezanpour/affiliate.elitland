import React, { Component } from "react";
import { connect } from "react-redux";

import SeriesColleaguePanelComponent from "components/Download/SeriesColleaguePanel/SeriesColleaguePanel";
import { NextSeo } from "next-seo";
import {
  loadSeriesColleaguePanelAction,
  loadDownloadColleaguePanelDetails
} from "posts/actions/download";
import { seriesColleagueSalePanelUrl } from "services/url";
import {
  loadDownloadDetailsListAction,
  loadColleagueMovieRankAction
} from "posts/actions/download-details";
import DownloadColleaguePanel from "components/Download/DownloadColleaguePanel/DownloadColleaguePanel";

class SeriesColleaguePanel extends Component {
  // componentDidMount() {
  //   const { params } = this.props;
  //   const page = params.query.page || 1;
  //   this.props.loadSeriesColleaguePanelAction(page, params.query.uid);
  // }

  // static async getInitialProps(props) {
  //   const { store, query } = props.ctx;
  //   // console.log("props.ctx",props.ctx.pathname)
  //   // store.dispatch(loadDownloadList(1));
  //   const page = query.page || 1;
  //   store.dispatch(loadSeriesColleaguePanelAction(page, query.uid));
  // }

  render() {
    const { params, colleagueSale } = this.props;
    return (
      <React.Fragment>
        <>
          <NextSeo title="گزارش فروش سریال‌ها" description="" canonical="#" />
          {params && params.query && params.query.uid && (
            <DownloadColleaguePanel
              {...this.props}
              title={`سریال ${colleagueSale?.data?.series_name}`}
              url={seriesColleagueSalePanelUrl(params.query.uid)}
              paramsUid={params.query.uid}
              series
            />
          )}
          {!params.query.uid && (
            <SeriesColleaguePanelComponent
              {...this.props}
              url={seriesColleagueSalePanelUrl()}
              title="آمار فروش سریال‌ها"
            />
          )}
        </>
      </React.Fragment>
    );
  }
}
SeriesColleaguePanel.getInitialProps = async ctx => {
  const { store, query } = ctx;
  // console.log("props.ctx",props.ctx.pathname)
  // store.dispatch(loadDownloadList(1));
  const page = query.page || 1;
  store.dispatch(loadSeriesColleaguePanelAction(page, query.uid));
};
const mapStateToProps = state => ({
  user: state.auth.user,
  colleagueSale: state.download.downloadColleaguePanel,
  panelDetails: state.download.downloadColleaguePanelDetails,
  downloadInfo: state.downloadDetails.downloadDetailsList,
  movieRank: state.downloadDetails.data
});

const mapDispatchToProps = dispatch => {
  return {
    loadDownloadColleaguePanelDetails: uid => {
      dispatch(loadDownloadColleaguePanelDetails(uid));
    },
    loadSeriesColleaguePanelAction: (page, uid) => {
      dispatch(loadSeriesColleaguePanelAction(page, uid));
    },
    loadDownloadDetailsListAction: uid => {
      dispatch(loadDownloadDetailsListAction(uid));
    },
    loadColleagueMovieRankAction: uid => {
      dispatch(loadColleagueMovieRankAction(uid));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SeriesColleaguePanel);
