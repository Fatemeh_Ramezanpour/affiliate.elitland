import React, { Component } from "react";
import { connect } from "react-redux";

import ReportColleaguePanel from "components/Download/ReportColleaguePanel/ReportColleaguePanel";
import { NextSeo } from "next-seo";
import { loadColleagueReport } from "posts/actions/download";

class ReportColleague extends Component {
  componentDidMount() {
    this.props.loadColleagueReport();
  }

  render() {
    return (
      <React.Fragment>
        <NextSeo title="گزارش فروش" description="" canonical="#" />
        <ReportColleaguePanel {...this.props} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  colleagueSale: state.download.accounting,
  loading: state.download.reportLoading
});

const mapDispatchToProps = dispatch => {
  return {
    loadColleagueReport: () => {
      dispatch(loadColleagueReport());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReportColleague);
