import React, { Component } from "react";
import { connect } from "react-redux";

import DownloadColleaguePanelComponent from "components/Download/DownloadColleaguePanel/DownloadColleaguePanel";
import { NextSeo } from "next-seo";
import {
  loadDownloadColleaguePanel,
  loadDownloadColleaguePanelDetails
} from "posts/actions/download";
import { loadDownloadDetailsListAction } from "posts/actions/download-details";

class DownloadColleaguePanel extends Component {
  componentDidMount() {
    const { params } = this.props;
    const page = params.query.page || 1;
    this.props.loadDownloadColleaguePanel(page);
  }

  render() {
    return (
      <React.Fragment>
        <>
          <NextSeo title="پنل همکار فروش" description="" canonical="#" />
          <DownloadColleaguePanelComponent {...this.props} />
        </>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  colleagueSale: state.download.downloadColleaguePanel,
  panelDetails: state.download.downloadColleaguePanelDetails,
  downloadInfo: state.downloadDetails.downloadDetailsList
});

const mapDispatchToProps = dispatch => {
  return {
    loadDownloadColleaguePanelDetails: uid => {
      dispatch(loadDownloadColleaguePanelDetails(uid));
    },
    loadDownloadColleaguePanel: page => {
      dispatch(loadDownloadColleaguePanel(page));
    },
    loadDownloadDetailsListAction: uid => {
      dispatch(loadDownloadDetailsListAction(uid));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DownloadColleaguePanel);
